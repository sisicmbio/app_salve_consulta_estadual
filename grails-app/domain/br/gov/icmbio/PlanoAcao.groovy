package br.gov.icmbio

class PlanoAcao {
 	String sgPlanoAcao
	String txPlanoAcao

 	static constraints = {
 	}

 	static mapping = {
	 	version 	false
	  	sort        sgPlanoAcao: 'asc'
	   	table       name:'salve.plano_acao'
   		id          column:'sq_plano_acao'
	}
}

