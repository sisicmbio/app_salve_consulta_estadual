package br.gov.icmbio

class FichaSinonimia {

    String noSinonimia
    String noAutor
    Integer nuAno

    static belongsTo = [ficha:Ficha]

    static constraints = {
    }

    static mapping = {
        version 	false
        sort        noSinonimia : 'asc'
        table       name 	: 'salve.ficha_sinonimia'
        id          column 	: 'sq_ficha_sinonimia'
        ficha 		column 	: 'sq_ficha'
    }
}
