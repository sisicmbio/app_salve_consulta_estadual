package br.gov.icmbio

class FichaHabitoAlimentarEsp {
	Ficha ficha
    Taxon taxon
    DadosApoio categoriaIucn
    String  dsFichaHabitoAlimentarEsp

    static constraints = {
		ficha nullable:false
		taxon nullable:false
		categoriaIucn nullable:true
		dsFichaHabitoAlimentarEsp  nullable:true
    }
	static mapping = {
    	version 			false
	   	table       		name 	: 'salve.ficha_habito_alimentar_esp'
	   	id          		column 	: 'sq_ficha_habito_alimentar_esp'
	   	ficha 				column  : 'sq_ficha'
		taxon 				column 	: 'sq_taxon'
		categoriaIucn 		column	: 'sq_categoria_iucn'
    }

    public String getRefBibHtml()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_habito_alimentar_esp','sq_ficha_habito_alimentar_esp',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label tag pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label tag pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
    }
}
