package br.gov.icmbio

class FichaAnexo {

	Ficha ficha
	DadosApoio contexto
	String deLegenda
	String noArquivo
	String deTipoConteudo
	String deLocalArquivo
    String inPrincipal

    static constraints = {
        inPrincipal     maxSize:1,inList:['S','N']
    }

   static mapping = {
    	version 	false
    	sort        id 		: 'asc'
	   	table       name 	: 'salve.ficha_anexo'
	   	id          column 	: 'sq_ficha_anexo'
	   	ficha		column 	: 'sq_ficha'
	   	contexto    column  : 'sq_contexto'
    }

    def beforeInsert() {

        // só pode ter um principal por contexto
        if (this.inPrincipal == 'S')
        {
            FichaAnexo.findAllByFichaAndContextoAndInPrincipal( this.ficha,this.contexto,'S').each{
                it.inPrincipal='N';
            }
        }
    }
}
