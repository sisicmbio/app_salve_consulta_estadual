package br.gov.icmbio

class FichaNomeComum {

    String noComum
    String deRegiaoLingua
    VwFicha vwFicha
    //String nomesComuns // campo formula https://stackoverflow.com/questions/35930600/how-to-use-native-sql-function-with-hql-query


    static belongsTo = [ficha: Ficha]

    static constraints = {
        vwFicha nullable:true
    }

    static mapping = {
        version 	false
        sort        id      : 'asc'
        table       name 	: 'salve.ficha_nome_comum'
        id          column 	: 'sq_ficha_nome_comum'
        ficha 		column 	: 'sq_ficha'
        vwFicha 	column 	: 'sq_ficha',insertable: false,updateable: false

        // https://stackoverflow.com/questions/35930600/how-to-use-native-sql-function-with-hql-query
        //nomesComuns formula: "array_to_string( array_agg( no_comum ),', ' )"
    }
}
