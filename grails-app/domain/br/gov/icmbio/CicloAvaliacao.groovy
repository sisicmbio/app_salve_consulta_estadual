package br.gov.icmbio

class CicloAvaliacao {

		Integer 	nuAno
		String 		deCicloAvaliacao
		String 		inSituacao

		static constraints = {
		}

		static mapping = {
			version 	false
			sort        nuAno: 'asc'
			table       name:'salve.ciclo_avaliacao'
			id 			column:'sq_ciclo_avaliacao'
		}

    Integer getAnoFinal()
    {
    	if( this.nuAno > 0 )
    	{
    		return this.nuAno + 4
    	}
    	else
    	{
    		return this.nuAno
    	}
    }

}
