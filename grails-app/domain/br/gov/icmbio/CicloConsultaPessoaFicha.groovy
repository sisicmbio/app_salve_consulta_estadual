package br.gov.icmbio

class CicloConsultaPessoaFicha {

    static belongsTo = [cicloConsultaFicha:CicloConsultaFicha, cicloConsultaPessoa:CicloConsultaPessoa]
    static constraints = {}
    static mapping = {
        version 	false
        table               name  : 'salve.ciclo_consulta_pessoa_ficha'
        id                  column: 'sq_ciclo_consulta_pessoa_ficha'
        cicloConsultaPessoa column: 'sq_ciclo_consulta_pessoa'
        cicloConsultaFicha  column: 'sq_ciclo_consulta_ficha'

    }

}
