package br.gov.icmbio

class TipoPublicacao {

	TipoUso tipoUso;
	String deTipoPublicacao;
	String coTipoPublicacao;

    static constraints = {
    	coTipoPublicacao nullable:true,blank:true,maxSize:50
    }

    static mapping = {
		version 				false;
		sort 					deTipoPublicacao:'asc'
	   	table       			name 	: 'taxonomia.tipo_publicacao';
	   	id          			column 	: 'sq_tipo_publicacao',generator:'identity';
	   	tipoUso 				column 	: 'sq_tipo_uso';
	}
}
