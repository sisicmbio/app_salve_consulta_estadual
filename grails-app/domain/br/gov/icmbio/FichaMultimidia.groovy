package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject
import org.hibernate.spatial.GeometryType

class FichaMultimidia {

    Ficha ficha
    DadosApoio tipo
    DadosApoio situacao
    DadosApoio datum
    String deLegenda
    String txMultimidia
    String noAutor
    String deEmailAutor
    Date dtElaboracao
    String noArquivo
    String noArquivoDisco
    Date dtInclusao
    Boolean inPrincipal
    Geometry geometry

    static constraints = {
        txMultimidia nullable:true
        deEmailAutor nullable:true
        geometry nullable: true
        datum nullable : true
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.ficha_multimidia'
        id          			column 	: 'sq_ficha_multimidia',generator:'identity'
        ficha 					column  : 'sq_ficha'
        tipo                    column  : 'sq_tipo'
        situacao                column  : 'sq_situacao'
        datum                   column  : 'sq_datum'
        geometry                type: GeometryType, sqlType: "GEOMETRY"
        geometry                column: 'ge_multimidia'
    }
    def beforeValidate()
    {
        if( !dtInclusao )
        {
            dtInclusao = new Date()
        }
    }

    def afterUpdate()
    {
        //println 'afterUpdate() verificar principal'
    }

    def afterInsert()
    {
        //println 'afterInsert() verificar principal'
    }

    JSONObject asJson()
    {
        Map data = [:]
        data.put( 'sqFichaMultimidia',this.id)
        data.put( 'sqTipo'           ,this.tipo.id)
        data.put( 'sqSituacao'       ,this.situacao.id)
        data.put( 'deLegenda'        ,this.deLegenda)
        data.put( 'txMultimidia'     ,this.txMultimidia)
        data.put( 'noAutor'          ,this.noAutor)
        data.put( 'deEmailAutor'     ,this.deEmailAutor)
        data.put( 'dtElaboracao'     ,this.dtElaboracao.format('dd/MM/yyyy') )
        data.put( 'inPrincipal'      ,this.inPrincipal)
        data.put( 'sqDatum'          ,this?.datum?.id)
        return data as JSON
    }




    String getInPrincipalText() {
        if( inPrincipal )
        {
            return 'Sim'
        }
        return 'Não'
    }


}
