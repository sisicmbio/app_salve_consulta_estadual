package br.gov.icmbio

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes='username')
@ToString(includes='noUsuario', includeNames=true, includePackage=false)
//class WebUsuario implements Serializable {
class WebUsuario
{

	//private static final long serialVersionUID = 1

	//static transients = ['springSecurityService']

	transient springSecurityService
	WebInstituicao instituicao
	String username // vai ser o email
	String password
	boolean enabled = false // st_conta_ativada
	boolean accountLocked = false
	boolean accountExpired = false
	boolean passwordExpired = false
	String deHashAtivacao
	String noUsuario
	Date dtUltimoAcesso
	Date dtInclusao


    WebUsuario(String username, String password) {
		this()
		this.username = username
		this.password = password
	}

	Set<WebRole> getAuthorities() {
		WebUsuarioRole.findAllByWebUsuario(this)*.webRole
	}

	def beforeValidate() {
		if( !this.dtInclusao )
		{
			this.dtInclusao = new Date()
		}
        if( this.email )
        {
            this.username = this.username.toLowerCase();
        }
		this.username = this.username.toLowerCase().trim()
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
        password = password.toLowerCase().trim()
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}

	static constraints = {
		username blank: false, unique: true
		password blank: false
		deHashAtivacao  nullable:true, blank:true
		dtUltimoAcesso  nullable:true
	}

	static mapping = {
		version false
		table       name		: 'salve.web_usuario'
		id column				: 'sq_web_usuario', generator:'identity'
		password column			: 'de_senha'
		username column			: 'de_email'
		enabled  column			: 'st_conta_ativada'
		accountLocked column	: 'st_conta_bloqueada'
		accountExpired column	: 'st_conta_expirada'
		passwordExpired column	: 'st_senha_expirada'
		deHashAtivacao column 	: 'de_hash_ativacao'
		noUsuario column 		: 'no_usuario'
		dtUltimoAcesso column 	: 'dt_ultimo_acesso'
		dtInclusao column 		: 'dt_inclusao'
		instituicao column		: 'sq_web_instituicao'
	}

	String getFullName()
	{
		return noUsuario
	}

	String getEmail()
	{
		return username
	}

	void refreshHash()
	{
		this.deHashAtivacao = springSecurityService.encodePassword( this.id + this.email + new Date().format('ddMMyyyyhhmmssS') )
	}
}
