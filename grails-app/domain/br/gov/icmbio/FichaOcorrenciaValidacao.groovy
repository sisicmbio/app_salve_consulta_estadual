package br.gov.icmbio

class FichaOcorrenciaValidacao {

	FichaOcorrencia fichaOcorrencia
	FichaOcorrenciaPortalbio fichaOcorrenciaPortalbio
    CicloConsultaFicha consultaFicha // informar em qual consulta ou revisão a colaboração foi cadastrada
	Usuario usuario
	String inValido
	String txObservacao
	Date dtInclusao

    static constraints = {
    	fichaOcorrencia nullable:true
    	fichaOcorrenciaPortalbio nullable:true
    	txObservacao nullable:true
    	inValido nullable:true
        consultaFicha nullable: true
    }

    static mapping = {
      	version 					false
	   	table       				name  : 'salve.ficha_ocorrencia_validacao'
	   	id          				column: 'sq_ficha_ocorrencia_validacao', generator:'identity'
	   	fichaOcorrencia 			column: 'sq_ficha_ocorrencia'
	   	fichaOcorrenciaPortalbio    column: 'sq_ficha_ocorrencia_portalbio'
	   	usuario 				    column: 'sq_web_usuario'
        consultaFicha               column: 'sq_ciclo_consulta_ficha'
    }

    def beforeValidate()
    {
    	dtInclusao = new Date()
    }

    String getInValidoText()
    {
        if( inValido =='S')
        {
            return 'Sim'
        }
        else if( inValido =='N')
        {
            return 'Não'
        }
        return ''
    }
}
