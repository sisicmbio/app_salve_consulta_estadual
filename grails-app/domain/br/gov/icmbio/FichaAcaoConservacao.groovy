package br.gov.icmbio

class FichaAcaoConservacao {

	Ficha ficha
	DadosApoio acaoConservacao
	DadosApoio situacaoAcaoConservacao
	PlanoAcao planoAcao
	DadosApoio tipoOrdenamento
	String txFichaAcaoConservacao



    static constraints = {}


	static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_acao_conservacao'
	   	id          			column 	: 'sq_ficha_acao_conservacao'
	   	ficha 					column  : 'sq_ficha'
	   	acaoConservacao			column 	: 'sq_acao_conservacao'
		situacaoAcaoConservacao column 	: 'sq_situacao_acao_conservacao'
		planoAcao 				column 	: 'sq_plano_acao'
		tipoOrdenamento			column 	: 'sq_tipo_ordenamento'
	}

	public String getDescricaoCompleta()
  	{
    	return this.acaoConservacao.codigo+' - '+this.acaoConservacao.descricao;
  	}

    public String getRefBibHtml()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_acao_conservacao','sq_ficha_acao_conservacao',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label tag pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label tag pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
    }

    public String getRefBibText()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_acao_conservacao','sq_ficha_acao_conservacao',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao);
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)' );
            }
        }
        return nomes.join('\n');
    }
}
