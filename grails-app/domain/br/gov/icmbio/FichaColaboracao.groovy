package br.gov.icmbio

class FichaColaboracao {

    DadosApoio situacao
    // DadosApoio tipoConsulta // identificar a partir de que tipo de consulta a colaboração foi feita. Direta,ampla ou revisão
    Usuario usuario
    String noCampoFicha
    String dsColaboracao
    String dsRotulo
    String dsRefBib

    Date dtInclusao
    Date dtAlteracao

    static belongsTo = [consultaFicha:CicloConsultaFicha]

    static constraints = {
        dtAlteracao nullable: true
        dsColaboracao nullable:true
    }

    static mapping = {
        version false
        table       name		: 'salve.ficha_colaboracao'
        id          column	    : 'sq_ficha_colaboracao', generator:'identity'
        consultaFicha column    : 'sq_ciclo_consulta_ficha'
        situacao    column      : 'sq_situacao'
        usuario     column      : 'sq_web_usuario'
    }

    def beforeValidate() {
        if( ! this.dtInclusao )
        {
            this.dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()
    }

    def beforeInsert() {

    }

    def beforeUpdate() {

    }

    // identifica se a colaboração foi feita por um especialista convidado ou cidação comum
    boolean isDireta() {
        return consultaficha?.cicloConsulta?.tipoConsulta?.codigoSistema == 'CONSULTA_DIRETA'
    }

    boolean isRevisao() {
        return consultaficha?.cicloConsulta?.tipoConsulta?.codigoSistema == 'REVISAO_POS_OFICINA'
    }

    String getValorOriginal()
    {
        try
        {
            return consultaFicha.ficha[noCampoFicha]
        }
        catch( Exception e) {
            println e.getMessage()
        }
        return ''
    }
}
