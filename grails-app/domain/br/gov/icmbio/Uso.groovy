package br.gov.icmbio

class Uso {

    Uso pai
	DadosApoio criterioAmeacaIucn
    String descricao
    String codigo
    String ordem

	static hasMany = [itens: Uso]
    static constraints = {}
    static mapping = {
    	version				false
    	sort 				ordem:'asc'
    	table				name:'salve.uso'
    	id          		column: 'sq_uso'
	   	descricao			column: 'ds_uso'
        codigo              column: 'co_uso'
	   	pai         		column: 'sq_uso_pai'
        itens               column: 'sq_uso_pai'
        ordem               column: 'nu_ordem'
	   	criterioAmeacaIucn  column: 'sq_criterio_ameaca_iucn'
    }

    public String getDescricaoCompleta()
    {
        return this.codigo+' - '+this.descricao
    }

}