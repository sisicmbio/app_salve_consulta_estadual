package br.gov.icmbio

class Ficha {
    // transient utilityService
    static transient colaborarService
    static transients = ['dsJustificativaAvaliacaoFinal','idUltimaAvaliacaoNacional']

    // String trilha // coluna formula no banco de dados

    Integer idUltimaAvaliacaoNacional = 0
    Taxon taxon
    String dsNotasTaxonomicas
    String dsDiagnosticoMorfologico
    String nmCientifico
    DadosApoio situacao
    DadosApoio grupo
    String stEndemicaBrasil
    String dsDistribuicaoGeoNacional
    String dsDistribuicaoGeoGlobal
    String dsHistoriaNatural
    String dsHabitoAlimentar
    String stHabitoAlimentEspecialista
    String dsUsoHabitat
    String stRestritoHabitatPrimario
    String stEspecialistaMicroHabitat
    String dsEspecialistaMicroHabitat
    String dsInteracao
    DadosApoio unidIntervaloNascimento
    Double vlIntervaloNascimento
    DadosApoio unidTempoGestacao
    String vlTempoGestacao
    String vlTamanhoProle
    String dsReproducao

    Double vlMaturidadeSexualFemea
    Double vlMaturidadeSexualMacho
    Double vlPesoFemea
    Double vlPesoMacho
    Double vlComprimentoFemea
    Double vlComprimentoMacho
    Double vlComprimentoFemeaMax
    Double vlComprimentoMachoMax
    Double vlSenilidadeReprodutivaFemea
    Double vlSenilidadeReprodutivaMacho
    Double vlLongevidadeFemea
    Double vlLongevidadeMacho

    DadosApoio unidMaturidadeSexualMacho
    DadosApoio unidMaturidadeSexualFemea
    DadosApoio unidadePesoFemea
    DadosApoio unidadePesoMacho
    DadosApoio medidaComprimentoFemea
    DadosApoio medidaComprimentoMacho
    DadosApoio medidaComprimentoFemeaMax
    DadosApoio medidaComprimentoMachoMax

    DadosApoio unidadeSenilidRepFemea
    DadosApoio unidadeSenilidRepMacho
    DadosApoio unidadeLongevidadeFemea
    DadosApoio unidadeLongevidadeMacho

    String dsRazaoSexual
    String dsTaxaMortalidade
    String dsCaracteristicaGenetica
    DadosApoio tendenciaPopulacional
    String dsPopulacao
    String stMigratoria
    DadosApoio padraoDeslocamento
    String dsAmeaca
    String dsUso
    CicloAvaliacao cicloAvaliacao
    DadosApoio categoriaFinal
    String dsCriterioAvalIucnFinal
    String dsJustificativaFinal
    String dsJustificativaAvaliacaoFinal
    String dsAcaoConservacao
    String dsPresencaUc
    String dsPesquisaExistNecessaria
    String stPresencaListaVigente

    static hasMany = [nomesComuns : FichaNomeComum,sinonimias:FichaSinonimia];

    static constraints = {

    }

    static mapping = {
        //trilha formula      : "json_extract_path_text(taxonomia.criar_trilha( sq_taxon ),'especie')"
        //trilha formula      : "taxonomia.criar_trilha( sq_taxon )"

        version     				false
        cache : true
        table       				name  :'salve.ficha'
        id          				column:'sq_ficha'
        taxon                       column:'sq_taxon'
        situacao                    column:'sq_situacao_ficha'
        unidIntervaloNascimento     column:'sq_unid_intervalo_nascimento'
        unidTempoGestacao           column:'sq_unid_tempo_gestacao'
        unidMaturidadeSexualFemea   column:'sq_unid_maturidade_sexual_femea'
        unidMaturidadeSexualMacho   column:'sq_unid_maturidade_sexual_macho'
        unidadePesoFemea            column:'sq_unidade_peso_femea'
        unidadePesoMacho            column:'sq_unidade_peso_macho'
        medidaComprimentoFemea      column:'sq_medida_comprimento_femea'
        medidaComprimentoMacho      column:'sq_medida_comprimento_macho'
        medidaComprimentoFemeaMax   column:'sq_medida_comprimento_femea_max'
        medidaComprimentoMachoMax   column:'sq_medida_comprimento_macho_max'
        unidadeSenilidRepFemea      column:'sq_unidade_senilid_rep_femea'
        unidadeSenilidRepMacho      column:'sq_unidade_senilid_rep_macho'
        unidadeLongevidadeFemea     column:'sq_unidade_longevidade_femea'
        unidadeLongevidadeMacho     column:'sq_unidade_longevidade_macho'
        tendenciaPopulacional       column:'sq_tendencia_populacional'
        padraoDeslocamento          column:'sq_padrao_deslocamento'
        cicloAvaliacao              column:'sq_ciclo_avaliacao'
        categoriaFinal              column:'sq_categoria_final'
        grupo                       column:'sq_grupo'
    }


    String getNoCientifico()
    {
        if( this.taxon.nivelTaxonomico.coNivelTaxonomico == 'ESPECIE')
        {
            return this.taxon.pai.noTaxon+' '+this.taxon.noTaxon
        }
        else if( this.taxon.nivelTaxonomico.coNivelTaxonomico == 'SUBESPECIE')
        {
            return this?.taxon.pai?.pai?.noTaxon+' '+this?.taxon.pai?.noTaxon+' '+this?.taxon.noTaxon
        }
        return this.taxon.noTaxon
    }

    String getNoCientificoItalico()
    {
        List aPartes = nmCientifico.split( ' ')
        if( aPartes.size() > 1 )
        {
            //return '<i>' + aPartes[0]+' '+aPartes[1]+'</i> ' + aPartes.minus([aPartes[0],aPartes[1]]).join( ' ').trim()
            return '<i>' + aPartes[0]+' '+aPartes[1]+'</i> ' + aPartes.drop(2).join(' ').trim()
        }
        return '<i>' + nmCientifico + '</i>'
    }

    String getNoCientificoCompletoItalico()
    {
        return this?.taxon?.noCientificoCompletoItalico
    }

    List getBreadcrumb()
    {
        List h = []
        String abreItalico ='<i>'
        String fechaItalico='</i>'
        this.taxon.structure.each { key, value ->
            if( key ==~ /^no.*/ && key != 'noCientifico')
            {
                if( key=='noGenero' || key=='noEspecie' || key=='noSubespecie' )
                {
                    abreItalico = '<i>'
                    fechaItalico='</i>'
                }
                else
                {
                    abreItalico=''
                    fechaItalico=''
                }
                if( key=='noEspecie' || key=='noSubespecie')
                {
                    value = value.toString().toLowerCase()
                }
                h.push( abreItalico + value + fechaItalico )
            }
        }
        return h.reverse()
    }
    String trim(String value )
    {
        value = value.toString().trim().replaceAll(/<p>&nbsp;<\/>$/,'')
        if( value == 'null')
        {
            value = ''
        }
        return value
    }

    String getNomesComunsText()
    {
        /*List data = []
        String lingua = ''
        nomesComuns.sort{it.id}.each {
            lingua = (it.deRegiaoLingua ?: '')
            if (lingua && lingua.indexOf('(') == -1)
            {
                lingua = ' (' + lingua + ')'
            }
            data.push(it.noComum + lingua)
        }
        return data.join(', ')
        */
        Map data = [:]
        String lingua = '-'
        nomesComuns.sort{it.id}.each {
            lingua = (it.deRegiaoLingua ?: '-')
            if (lingua && lingua.indexOf('(') == -1)
            {
                lingua = ' (' + lingua + ')'
            }
            if( !data[lingua])
            {
                data[lingua] = []
            }
            data[lingua].push( it.noComum )
        }
        List resultado = []
        data.each {
            if( data.size() == 1 )
            {
                resultado.push( ( it.value.join(', ') + (it.key.trim() != '(-)' ? ' '+it.key : '') ) )
            }
            else
            {
                resultado.push( ( it.value.join(' '+it.key+', ') + (it.key.trim() != '(-)' ? ' '+it.key : '') ) )
            }
        }
        return resultado.join(', ')
    }

    String getSinonimiasText()
    {
        List data =[]
        String autor = ''
        sinonimias.each {
            autor = it.noAutor ?: ''
            if( autor.indexOf('(')==-1)
            {
                autor = '(' + autor + ')'
                autor= autor?.replaceAll(/\)\)/,')')?.replaceAll(/\(\(/,'(')
            }
            data.push( '<span><i>'+it.noSinonimia+'</i>  '+autor+'</span>')
        }
        return data.join(', ')+(data?'.':'')
    }


    String getSnd(String value)
    {
        if( value == 'S')
        {
            return 'Sim'
        }
        else if( value == 'N')
        {
            return 'Não'
        }
        else if( value == 'D')
        {
            return 'Desconhecido'
        }
            return ''
    }

    String printLabelValue(String label, String value)
    {
        value = trim( value )
        value = value?.replaceAll(/null/,'').trim()
        if ( value == 'null')
        {
            value = ''
        }
        return  """<div class="label-value" style="font-size:1rem;font-weight:normal;">
                        <span class="flabel">${label}</span>&nbsp;<span class="fvalue">${value}</span>
                  </div>
                """
    }

    String printSndTopic(String label, String value)
    {
        return printLabelValue( label, getSnd(value) )
    }


    String getEndemicaBrasilText()
    {
        return getSnd( this.stEndemicaBrasil )
    }

    Integer getIdMapaDistribuicao()
    {
        DadosApoio contexto = DadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','MAPA_DISTRIBUICAO')
        return FichaAnexo.findByFichaAndContextoAndInPrincipal(this,contexto,'S' )?.id ?: 0
    }

    String getLegendaMapaDistribuicao()
    {
        DadosApoio contexto = DadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO','MAPA_DISTRIBUICAO')
        FichaAnexo fa = FichaAnexo.findByFichaAndContextoAndInPrincipal(this, contexto,'S' )
        if( fa )
        {
            return fa.deLegenda
        }
        return ''
    }

    String getImagensHtml( String contexto )
    {
        String html=''
        List listImagens = colaborarService.getFichaImages( this, contexto)
        if( listImagens.size() > 0)
        {
            html='<div class="small-12 medium-12 columns text-center" style="background-color:#fff;"><br />'
            listImagens.eachWithIndex { it, i ->
                if( i > 0)
                {
                    html += '<hr>'
                }
                html += '<img src="/salve-consulta/ficha/getAnexo/'+it.id+'"/>'
                if( it.legenda )
                {
                    html +='<p>'+it.legenda+'</p>'
                }
            }
            html +='</div>'
        }
        return html
    }

    String getImagemPrincipalHtml()
    {
        DadosApoio tipo = DadosApoioService.getByCodigo("TB_TIPO_MULTIMIDIA",'IMAGEM')
        FichaMultimidia fm = FichaMultimidia.findByFichaAndTipoAndInPrincipal( this,tipo,true)
        if ( fm ) {
            String html = """<img src="/salve-consulta/ficha/getMultimidia/${fm.id}"/>
                <div class="specie-photo ${fm.noAutor?:'hidden'}}">
                     <p title="${fm.deEmailAutor?:''}">Autor:${fm.noAutor?:''}</p>
                     <p>${fm.deLegenda?:''}</p>
                 </div>"""
            return html
        }
        else
        {
            return '<span>Sem imagem</span>'
        }
    }


    String getGridOcorrencias()
    {
        String html = """
         <table class="table table-ficha-completa">
                    <thead>
                      <tr>
                         <th>Latitude</th>
                         <th>Longitude</th>
                         <th>Datum</th>
                         <th>Precisão da Coordenada</th>
                         <th>Referência de Aproximação</th>
                         <th>Localidade</th>
                         <th>Referência Bibliográfica</th>
                       </tr>
                     </thead>
                     <tbody>"""
            Map latLon
            FichaOcorrencia.findAllByFichaAndContexto(this,DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA','REGISTRO_OCORRENCIA'),[sort: "id"]).each
            {
                //latLon= it.coordenadasGms
                html += '<tr>'+
                //'<td>' + latLon?.lat?.gms+'</td>' +
                //'<td>' + latLon?.lon?.gms+'</td>' +
                '<td>' + it?.geometry?.y+'</td>' +
                '<td>' + it?.geometry?.x+'</td>' +
                '<td>' + (it?.datum?.descricao?:'')+'</td>' +
                '<td>' + (it?.precisaoCoordenada?.descricao?:'')+'</td>' +
                '<td>' + (it?.refAproximacao?.descricao?:'')+'</td>' +
                '<td>' + (it.noLocalidade?:'')+'</td>' +
                '<td>' + it.refBibHtml+'</td>' +
                '</tr>'
            }

            html += """
                       </tr>
                     </tbody>
                  </table>
                  """

        return html
    }

    String getEstadosText()
    {
        List list = FichaOcorrencia.findAllByFichaAndContexto(this, DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'ESTADO'));
        if( list.size > 0 )
        {
            return '<p>'+list?.collect{it?.estado?.noEstado+' ('+it?.estado?.sgEstado+')'}.join(', ')+'.</p>'
        }
        return ''
    }

    String getBiomasText()
    {

        List list = FichaOcorrencia.findAllByFichaAndContexto(this, DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'BIOMA'));
        if( list.size > 0 )
        {
            return '<p>'+list?.collect{it?.bioma?.descricao}.join(', ')+'.</p>'
        }
        return ''
    }

    String getBaciasText()
    {
        List list = FichaOcorrencia.findAllByFichaAndContexto(this, DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'BACIA_HIDROGRAFICA'));
        if( list.size > 0 )
        {
            return '<p>' + list?.collect{it?.baciaHidrografica?.descricao}.join(', ')+'.</p>'
        }
        return ''
    }

    String getAreasRelevantesText()
    {
        List list = FichaAreaRelevancia.findAllByFicha(this)
        if( list.size > 0 )
        {
            String html = '''<table class="table table-ficha-completa">
                <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>Local</th>
                        <th>Estado</th>
                        <th>Municipio</th>
                        <th>Ref. Bibliográfica</th>
                    </tr>
                </thead>
                <tbody>
            '''
            list.each {
                html += '<tr>'+
                        '<td>'+trim( it.tipoRelevancia.descricao)+'</td>'+
                        '<td>'+trim( it.txLocal)+'</td>'+
                        '<td>'+trim( it.estado.noEstado)+'</td>'+
                        '<td>'+trim( it.municipiosHtml)+'</td>'+
                        '<td>'+trim( it.refBibHtml)+'</td>'+
                        '</tr>'

            }
            html += '</tbody></table>'
            return html
        }
        return ''
    }

    String getEspecieMigratoriaText()
    {

        return getSnd( this.stMigratoria ) + ( this.stMigratoria=='S' && this?.padraoDeslocamento ?  ', padrão de deslocamento ' + this.padraoDeslocamento.descricao : '' )
    }

    String getHabitoAlimentarGrid()
    {
        List list = FichaHabitoAlimentar.findAllByFicha(this)
        String html = """
            <table class="table table-ficha-completa">
                <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>Referência Bibliográfica</th>
                    </tr>
                </thead>
                <tbody>
            """
            list.each {
                html += """
                    <tr>
                        <td>${it?.tipoHabitoAlimentar?.descricao}</td>
                        <td>${it?.refBibHtml}</td>
                    </tr>
                """
            }
            html += '</tbody></table>'
            return html
    }

    String getHabitoAlimentarEspGrid()
    {
        List list = FichaHabitoAlimentarEsp.findAllByFicha(this)
        String html = """
            <table class="table table-ficha-completa">
                <thead>
                    <tr>
                        <th>Taxon</th>
                        <th>Categoria</th>
                    </tr>
                </thead>
                <tbody>
            """
            list.each {
                html += """
                    <tr>
                        <td>${it?.taxon?.noCientificoCompletoItalico}</td>
                        <td>${it?.categoriaIucn?.descricaoCompleta}</td>
                    </tr>
                """
            }
            html += '</tbody></table>'
            return html
    }

    String getHabitatsGrid(){
        List list = FichaHabitat.findAllByFicha(this).sort{ it.descricao.toUpperCase() }
        String html = """<table class="table table-ficha-completa">
                <thead>
                    <tr>
                        <th>Habitat</th>
                        <th>Referênica Bibliográfica</th>
                    </tr>
                </thead>
                <tbody>
            """
            list.each {
                html += """
                    <tr>
                        <td>${ it?.descricao }</td>
                        <td>${ it.refBibHtml }</td>
                    </tr>
                """
            }
            html += '</tbody></table>'
            return html
    }

    String getIteracaoText(){
        List list = FichaInteracao.findAllByFicha(this)
        String html = """<table class="table table-ficha-completa">
                <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>Taxon</th>
                        <th>Categoria</th>
                        <th>Referênica Bibliográfica</th>
                    </tr>
                </thead>
                <tbody>
            """
            list.each {
                html += """
                    <tr>
                        <td>${ it?.tipoInteracao?.descricao }</td>
                        <td>${ it?.taxon?.nivelTaxonomico?.deNivelTaxonomico }</td>
                        <td>${ it?.categoriaIucn?.descricaoCompleta}</td>
                        <td>${ it.refBibHtml }</td>
                    </tr>
                """
            }
            html += '</tbody></table>'
            return html
    }

    String getIntervaloNascimentoText()
    {
        return (this?.vlIntervaloNascimento ? this.vlIntervaloNascimento.toString().replaceAll(/\.0$/, '') + ' ' + this?.unidIntervaloNascimento?.descricao : '')
    }

   String getTempoGestacaoText()
    {
        return this.vlTempoGestacao.toString().replaceAll(/\.0$/, '') + ' ' + this?.unidTempoGestacao?.descricao
    }

   String getTamanhoProleText()
    {
        return (this.vlTamanhoProle ? this.vlTamanhoProle.toString().replaceAll(/\.0$/, '') + ' individuo(s)' : '')
    }

    String getGridReproducaoText()
    {

        String html = """<table class="table table-ficha-completa">
                            <thead>
                                 <tr>
                                    <th>Campo</th>
                                    <th>Macho</th>
                                    <th>Fêmea</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Maturidade sexual</td>
                                    <td>${this?.vlMaturidadeSexualMacho ? this?.vlMaturidadeSexualMacho.toString() + ' ' + this?.unidMaturidadeSexualMacho?.descricao : ''}</td>
                                    <td>${this?.vlMaturidadeSexualFemea ? this?.vlMaturidadeSexualFemea.toString() + ' ' + this?.unidMaturidadeSexualFemea?.descricao : ''}</td>
                                </tr>
                                <tr>
                                    <td>Peso médio (adulto)</td>
                                    <td>${this?.vlPesoMacho ? this.vlPesoMacho.toString() + ' ' + this?.unidadePesoMacho?.descricao : ''}</td>
                                    <td>${this?.vlPesoFemea ? this.vlPesoFemea.toString() + ' ' + this?.unidadePesoFemea?.descricao : ''}</td>
                                </tr>
                                <tr>
                                    <td>Comprimento máximo</td>
                                    <td>${this?.vlComprimentoMachoMax ? this.vlComprimentoMachoMax.toString() + ' ' + this?.medidaComprimentoMachoMax?.descricao : ''}</td>
                                    <td>${this?.vlComprimentoFemeaMax ? this.vlComprimentoFemeaMax.toString() + ' ' + this?.medidaComprimentoFemeaMax?.descricao : ''}</td>
                                </tr>
                                <tr>
                                    <td>Comprimento na maturidade sexual</td>
                                    <td>${this?.vlComprimentoMacho ? this.vlComprimentoMacho.toString() + ' ' + this?.medidaComprimentoMacho?.descricao : ''}</td>
                                    <td>${this?.vlComprimentoFemea ? this.vlComprimentoFemea.toString() + ' ' + this?.medidaComprimentoFemea?.descricao : ''}</td>
                                </tr>
                                <tr>
                                    <td>Senilidade reprodutiva</td>
                                    <td>${this?.vlSenilidadeReprodutivaMacho ? this.vlSenilidadeReprodutivaMacho.toString() + ' ' + this?.unidadeSenilidRepMacho?.descricao : ''}</td>
                                    <td>${this?.vlSenilidadeReprodutivaFemea ? this.vlSenilidadeReprodutivaFemea.toString() + ' ' + this?.unidadeSenilidRepFemea?.descricao : ''}</td>
                                </tr>
                                <tr>
                                    <td>Longevidade</td>
                                    <td>${this?.vlLongevidadeMacho ? this.vlLongevidadeMacho.toString() + ' ' + this?.unidadeLongevidadeMacho?.descricao : ''}</td>
                                    <td>${this?.vlLongevidadeFemea ? this.vlLongevidadeFemea.toString() + ' ' + this?.unidadeLongevidadeFemea?.descricao : ''}</td>
                                </tr>
                         </tbody>
                     </table>
                        """
    }

    String getGridAmeacasText()
    {
        List list = FichaAmeaca.findAllByFicha(this)
        String html = """<table class="table table-ficha-completa">
                            <thead>
                                 <tr>
                                    <th>Ameaça</th>
                                    <th>Referência Bibliográfica</th>
                                </tr>
                            </thead>
                            <tbody>
                            """
        list.each {
            html +="""
                    <tr>
                        <td>${ it?.criterioAmeacaIucn?.descricao}</td>
                        <td>${ it?.refBibHtml }</td>
                    </tr>
                    """
                    if( it?.regioesText )
                    {
                        html+='<tr><td colspan="2" style="padding-left:35px;">'+it.regioesHtml+'</td></tr>'
                    }
                }
        return html + '</tbody></table>'
    }

    String getGridUsosText()
    {
        List list = FichaUso.findAllByFicha(this)
        String html = """<table class="table table-ficha-completa">
                            <thead>
                                 <tr>
                                    <th>Uso</th>
                                    <th>Referência Bibliográfica</th>
                                </tr>
                            </thead>
                            <tbody>
                            """
        list.each {
            html +="""
                    <tr>
                        <td>${ it?.uso?.descricao }</td>
                        <td>${ it?.refBibHtml }</td>
                    </tr>
                    """
                    if( it?.regioesText )
                    {
                        html+='<tr><td colspan="2" style="padding-left:35px;">'+it.regioesHtml+'</td></tr>'
                    }
                }
        return html + '</tbody></table>'
    }

    String getUltimaAvaliacaoNacionalText()
    {
        String ano
        String categoria
        String criterio
        String justificativa
        idUltimaAvaliacaoNacional = 0

        if( ! this.categoriaFinal )
        {
           // recuperar a ultima avaliação nacional
            DadosApoio tipo = DadosApoioService.getByCodigo('TB_TIPO_AVALIACAO', 'NACIONAL_BRASIL');
            /*List listAvaliacao = FichaHistoricoAvaliacao.createCriteria().list() {
                createAlias('ficha', 'f')
                eq('f.taxon', Taxon.get(this.taxon.id))
                eq('tipoAvaliacao', tipo)
                order('nuAnoAvaliacao', 'desc')
                order('id', 'desc')
                maxResults(1)
            }*/

            List listAvaliacao = TaxonHistoricoAvaliacao.createCriteria().list() {
                eq('taxon', Taxon.get(this.taxon.id))
                eq('tipoAvaliacao', tipo)
                order('nuAnoAvaliacao', 'desc')
                order('id', 'desc')
                maxResults(1)
            }


            if ( listAvaliacao.size() > 0 )
            {
                idUltimaAvaliacaoNacional = listAvaliacao[0].id
                ano = listAvaliacao[0].nuAnoAvaliacao.toString()
                criterio = listAvaliacao[0].deCriterioAvaliacaoIucn.toString()
                categoria = listAvaliacao[0].categoriaIucn?.descricaoCompleta
                justificativa = listAvaliacao[0].txJustificativaAvaliacao
            }
        }
        else
        {
            ano             = this.cicloAvaliacao.nuAno.toString()
            categoria       = this.categoriaFinal?.descricao
            criterio        = this.dsCriterioAvalIucnFinal
            justificativa   = this.dsJustificativaFinal

        }
        dsJustificativaAvaliacaoFinal = justificativa
        return printLabelValue('Ano:', ano )+
               printLabelValue('Categoria:',categoria )+
               printLabelValue('Critério:', criterio )+
               printLabelValue('Justificativa',dsJustificativaAvaliacaoFinal)
    }

    String getHistoricoAvaliacoesText()
    {
        boolean existe=false
        String html = """<table class="table table-ficha-completa">
                            <thead>
                                 <tr>
                                    <th>Tipo</th>
                                    <th>Ano</th>
                                    <th>Abrangência</th>
                                    <th>Categoria</th>
                                    <th>Critério</th>
                                    <th>Referência Bibliográfica</th>
                                </tr>
                            </thead>
                            <tbody>"""
        TaxonHistoricoAvaliacao.findAllByTaxon( this.taxon ).sort { it.nuAnoAvaliacao.toString() + it.tipoAvaliacao.ordem }.each{
            if( it.id != idUltimaAvaliacaoNacional )
            {
                existe=true;
                html += '<tr>'+
                            '<td>'+trim( it.tipoAvaliacao?.descricao)+'</td>'+
                            '<td>'+trim( it.nuAnoAvaliacao?.toString())+'</td>'+
                            '<td>'+trim( ( it.tipoAbrangencia?.codigoSistema == 'OUTRA' ? it.noRegiaoOutra : it?.abrangencia?.descricao)) +'</td>'+
                            '<td>'+trim( it.categoriaIucn?.descricaoCompleta)+'</td>'+
                            '<td>'+trim( it.deCriterioAvaliacaoIucn )+'</td>'+
                            '<td>'+trim( it.refBibHtml )+'</td>'+
                        '</tr>'

            }
        }
        html +='</tbody></table>'
        return ( existe ? html : '') // <b>Não Avaliada Anteriormente</b><br />')
    }

    String getPresencaConvencoesText()
    {
        boolean existe=false
        String html = """<table class="table table-ficha-completa">
                            <thead>
                                 <tr>
                                    <th>Convenção</th>
                                    <th>Ano</th>
                                </tr>
                            </thead>
                            <tbody>"""

        FichaListaConvencao.findAllByFicha(this).each{
            existe=true
            html += '<tr>'+
                        '<td>'+trim( it.listaConvencao?.descricao)+'</td>'+
                        '<td>'+trim( it?.nuAno.toString())+'</td>'+
                    '</tr>'

        }
            html +='</tbody></table>'
        return ( existe ? html : '' )
    }

    String getAcoesConservacaoText()
    {
        String html = """<table class="table table-ficha-completa">
                            <thead>
                                 <tr>
                                    <th>Ação</th>
                                    <th>Situação</th>
                                    <th>Referência Bibliográfica</th>
                                </tr>
                            </thead>
                            <tbody>"""

        FichaAcaoConservacao.findAllByFicha(this).each{
            html += '<tr>'+
                        '<td>'+trim( it?.acaoConservacao?.descricaoCompleta)+'</td>'+
                        '<td>'+trim( it?.situacaoAcaoConservacao?.descricao)+'</td>'+
                        '<td>'+trim( it?.refBibHtml)+'</td>'+
                    '</tr>'

        }

        html +='</tbody></table>'
        return html
    }

   String getPresencaUcText() {
       String html = """<table class="table table-ficha-completa">
                            <thead>
                                 <tr>
                                    <th>UC</th>
                                    <th>Referência Bibliográfica</th>
                                </tr>
                            </thead>
                            <tbody>"""


       FichaOcorrencia.createCriteria().list {
           eq('ficha', this)
           eq('contexto', DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA'))

           or {
               isNotNull('sqUcFederal')
               isNotNull('sqUcEstadual')
               isNotNull('sqRppn')
           }

       }.unique { [it.sqUcFederal, it.sqUcEstadual, it.sqRppn] }.each {
           html += '<tr>' +
                   '<td>' + trim(it?.ucHtml) + '</td>' +
                   '<td>' + trim(it?.refBibHtml) + '</td>' +
                   '</tr>'
       }


       /*   FichaOcorrencia.createCriteria().list{
            eq( 'ficha', this )
            eq( 'contexto', DadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA') )
            and {
                or {
                    isNotNull('sqUcFederal')
                    isNotNull('sqUcEstadual')
                    isNotNull('sqRppn')
                }
            }
        }.each {
            html += '<tr>'+
                        '<td>'+trim( it?.ucHtml )+'</td>'+
                        '<td>'+trim( it?.refBibHtml )+'</td>'+
                    '</tr>'

        }
        */

       html += '</tbody></table>'
       return html
   }

    String getPesquisasText()
    {
        String html = """<table class="table table-ficha-completa">
                            <thead>
                                 <tr>
                                    <th>Tema</th>
                                    <th>Situação</th>
                                    <th>Referência Bibliográfica</th>
                                </tr>
                            </thead>
                            <tbody>"""

        FichaPesquisa.findAllByFicha( this ).each {
            html += '<tr>'+
                        '<td>'+trim( it?.tema?.descricao )+'</td>'+
                        '<td>'+trim( it?.situacao.descricao )+'</td>'+
                        '<td>'+trim( it?.refBibHtml )+'</td>'+
                    '</tr>'

        }

        html +='</tbody></table>'
        return html
    }

    String getRefBibsText()
    {

        String html=''
        // Referencias Bibliograficas
        FichaRefBib.createCriteria().list() {
                eq('ficha', this)
                isNotNull('publicacao') // não incluir a comunicação pessoal
            }.unique { it.publicacao ? it.publicacao.noAutor : it.noAutor }.sort { it.publicacao ? it.publicacao.noAutor : it.noAutor }.each
            {

                html += '<div class="div-ref-bib">' + it.referenciaHtml + '</div>'
            }
            return html
    }
}
