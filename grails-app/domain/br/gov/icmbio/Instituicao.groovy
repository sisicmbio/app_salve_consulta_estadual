package br.gov.icmbio

class Instituicao {

    Pessoa pessoa
    Instituicao pai
    DadosApoio tipo // TODO - VALIDAR SE É NECESSÁRIO TER O TIPO AINDA
    String sgInstituicao
    String nuCnpj
    String noContato
    String nuTelefone
    Boolean stInterna
    Date dtAlteracao

    static constraints = {
        nuCnpj unique: true,nullable: true, blank: false
        noContato nullable: true, blank:false
        nuTelefone nullable: true, blank:false
        tipo nullable: true
        pai nullable: true
        dtAlteracao nullable: true
        stInterna nullable: true

    }

    static mapping = {
        version false
        sort 		pai: 'asc'
        table       name: 'salve.instituicao'
        id          column:'sq_pessoa', generator: 'foreign', params: [property: 'pessoa']
        pessoa      insertable: false, updateable: false, column: 'sq_pessoa'
        pai         column : 'sq_pessoa_pai'
        tipo        column: 'sq_tipo'
    }

    def beforeValidate() {
        dtAlteracao = new Date()
        if( this.nuCnpj ) {
            this.nuCnpj = this.nuCnpj.replaceAll(/[^0-9]/,'')
            this.nuCnpj = this.nuCnpj.length() != 14 ? null : this.nuCnpj
        } else {
            this.nuCnpj=null
        }

        // definir como false se estiver null
        if( ! stInterna ) {
            stInterna = false
        }
    }

    String getCnpjFormatado(){
        return Util.formatCnpj(nuCnpj)
    }

    /**
     * retornar a quantidade de instituições subordinadas
     * @return
     */
    Integer getNumFilhos(){

        return Instituicao.createCriteria().count{
            eq('pai',this)
        }
    }

    Map asMap(){
        return [
                sqPessoa            : id  ?: '',
                noPessoa            : pessoa ? pessoa.noPessoa: '',
                sgInstituicao       : sgInstituicao ?: '',
                stInterna           : stInterna ? true : false,
                noContato           : noContato ?:'',
                nuTelefone          : nuTelefone ?:'',
                deEmail             : pessoa?.deEmail ?:'',
                nuCnpj              : cnpjFormatado,
                sqPai               : pai ? pai.id : null,
                noPai               : pai ? pai.pessoa.noPessoa :'',
                sgPai               : pai ? pai.sgInstituicao : '',
                sqTipo              : tipo ? tipo.id : null,
                dsTipo              : tipo ? tipo.descricao : ''
            ]
    }

    String getSgUnidade(){
        if( !sgUnidade ){
            return ''
        }
        return sgInstituicao ?: ''
    }

    String getSiglaNome(){
        if( !sgUnidade ){
            return ''
        }
        return  sgUnidade + ( (pessoa.noPessoa == sgUnidade) ? '':' - ' + pessoa.noPessoa )
    }

    String getNoInstituicao()
    {
        if( pessoa ) {
            return sgUnidade ?: pessoa.noPessoa
        }
        return ''
    }
}
