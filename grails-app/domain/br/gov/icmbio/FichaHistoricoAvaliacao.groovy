package br.gov.icmbio

class FichaHistoricoAvaliacao {

	Ficha ficha
	DadosApoio tipoAvaliacao
    DadosApoio tipoAbrangencia // Estado, bioma
	Abrangencia abrangencia     // Goiás, Cerrado
 	DadosApoio categoriaIucn
	Integer nuAnoAvaliacao
	String deCriterioAvaliacaoIucn
	String txJustificativaAvaliacao
	String txRegiaoAbrangencia
	String noRegiaoOutra

    static constraints = {}

    static mapping = {
		version 				false
        sort                    nuAnoAvaliacao:'asc'
	   	table       			name 	: 'salve.ficha_historico_avaliacao'
	   	id          			column 	: 'sq_ficha_historico_avaliacao'
	   	ficha 					column  : 'sq_ficha'
	   	tipoAvaliacao			column 	: 'sq_tipo_avaliacao'
        categoriaIucn           column  : 'sq_categoria_iucn'
        tipoAbrangencia         column  : 'sq_tipo_abrangencia' // Estado, bioma
        abrangencia             column  : 'sq_abrangencia'      // Goias, Cerrado,
	}

    public String getRefBibHtml()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_historico_avaliacao','sq_ficha_historico_avaliacao',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label tag pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label tag pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
    }

    public String getRefBibText()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_historico_avaliacao','sq_ficha_historico_avaliacao',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao);
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)');
            }
        }
        return nomes.join('\n');
    }
}