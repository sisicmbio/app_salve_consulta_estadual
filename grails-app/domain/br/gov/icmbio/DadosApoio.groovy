package br.gov.icmbio

class DadosApoio {

    DadosApoio pai
    String codigo
    String descricao
    String ordem
    String codigoSistema

    static hasMany = [itens : DadosApoio ]

    static constraints = {
    }

    static mapping = {
        version false
        cache true
        sort            ordem   : 'asc'
        table           name    : 'salve.dados_apoio'
        id              column  : 'sq_dados_apoio'
        pai             column  : 'sq_dados_apoio_pai'
        itens           column  : 'sq_dados_apoio_pai'
        descricao       column  : 'ds_dados_apoio'
        codigo          column  : 'cd_dados_apoio'
        ordem           column  : 'de_dados_apoio_ordem'
        codigoSistema   column  : 'cd_sistema'
    }

    String getDescricaoCompleta()
    {
      if( this.codigo == 'AMEACADA')
      {
        return this.descricao;
      }
        if( this.pai && this.pai.codigoSistema=='TB_CATEGORIA_IUCN')
        {
            return this.descricao+' ('+this.codigo+')';
        }
      return this.codigo+' - '+this.descricao;
    }
}
