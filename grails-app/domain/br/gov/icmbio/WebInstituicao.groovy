package br.gov.icmbio

class WebInstituicao {

    Unidade instituicao
    String noInstituicao
    String sgInstituicao // sigla
    Date dtInclusao

    static constraints = {
        instituicao nullable:true
        noInstituicao nullable:true,blank:true // nome 
        sgInstituicao nullable:true,blank:true // sigla
        
    }

    static mapping = {
        version false
        table name:'salve.web_instituicao'
        id column:'sq_web_instituicao', generator:'identity'
        instituicao column:'sq_unidade_org'
    }

    def beforeValidate() {
        if( !this.dtInclusao )
        {
            this.dtInclusao = new Date()
        }
    }

    String getNoInstituicao()
    {
        return this?.instituicao ? this?.instituicao?.sgUnidade : this?.noInstituicao
    }
}
