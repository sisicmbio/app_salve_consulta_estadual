package br.gov.icmbio

class FichaHabitoAlimentar {

	Ficha  ficha;
	DadosApoio tipoHabitoAlimentar
    String dsFichaHabitoAlimentar

    static constraints = {}

    static mapping = {
    	version 			false
	   	table       		name 	: 'salve.ficha_habito_alimentar'
	   	id          		column 	: 'sq_ficha_habito_alimentar'
	   	ficha 				column  : 'sq_ficha'
	   	tipoHabitoAlimentar column 	: 'sq_tipo_habito_alimentar'
    }

    public String getRefBibHtml()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_habito_alimentar','sq_ficha_habito_alimentar',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label tag pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label tag pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
    }

    public String getRefBibText()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_habito_alimentar','sq_ficha_habito_alimentar',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao);
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)');
            }
        }
        return nomes.join("\n");
    }

}
