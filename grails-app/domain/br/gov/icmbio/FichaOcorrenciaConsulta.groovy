package br.gov.icmbio

import grails.converters.JSON

class FichaOcorrenciaConsulta {

	FichaOcorrencia fichaOcorrencia
    // DadosApoio tipoConsulta // identificar a partir de que tipo de consulta a colaboração foi feita. Direta,ampla ou revisão
    Usuario usuario
    String txObservacao

    static belongsTo = [cicloConsultaFicha:CicloConsultaFicha]
    static hasMany = [fichaConsultaMultimidia:FichaConsultaMultimidia]

    static constraints = {
        txObservacao nullable:true,blank:true
        //tipoConsulta nullable:true
    }

    static mapping = {
        version             false
        table               name:'salve.ficha_ocorrencia_consulta'
        id                  column:'sq_ficha_ocorrencia_consulta',generator:'identity'
        usuario             column:'sq_web_usuario'
        cicloConsultaFicha  column:'sq_ciclo_consulta_ficha'
        fichaOcorrencia     column:'sq_ficha_ocorrencia'
        //tipoConsulta        column:'sq_tipo_consulta'
    }

    public asJson()
    {
        Map data = [:]
        data.put( 'sqFichaOcorrenciaConsulta'   ,this.id)
        data.put( 'vlLat'                       ,this?.fichaOcorrencia.geometry.y)
        data.put( 'vlLon'                       ,this?.fichaOcorrencia?.geometry.x)
        data.put( 'sqDatum'                     ,this?.fichaOcorrencia?.datum?.id ?:'')

        data.put( 'nuDiaRegistro'               ,this?.fichaOcorrencia?.nuDiaRegistro ?:'')
        data.put( 'nuMesRegistro'               ,this?.fichaOcorrencia?.nuMesRegistro ?:'')
        data.put( 'nuAnoRegistro'               ,this?.fichaOcorrencia?.nuAnoRegistro ?:'')
        data.put( 'hrRegistro'                  ,this?.fichaOcorrencia?.hrRegistro ?:'')
        data.put( 'inDataDesconhecida'          ,this?.fichaOcorrencia?.inDataDesconhecida ?:'')

        data.put( 'sqPrecisaoCoordenada'        ,this?.fichaOcorrencia?.precisaoCoordenada?.id ?:'')
        data.put( 'sqRefAproximacao'            ,this?.fichaOcorrencia?.refAproximacao?.id ?:'')
        data.put( 'txObservacao'                ,this?.txObservacao ?:'')
        data.put( 'noLocalidade'                ,this?.fichaOcorrencia?.noLocalidade ?:'')

        // ler dados da referencia bibliografica / comunicacao pessoa
        FichaRefBib refBibOcorrencia = FichaRefBib.findByFichaAndSqRegistroAndNoTabelaAndNoColuna(fichaOcorrencia.ficha, fichaOcorrencia.id, 'ficha_ocorrencia', 'sq_ficha_ocorrencia')
        data.put( 'txRefBib'                    ,( refBibOcorrencia?.publicacao ? refBibOcorrencia.publicacao.referenciaHtml : '' ) )
        data.put( 'sqPublicacao'                ,  refBibOcorrencia?.publicacao?.id ?:'')
        data.put( 'deComunicacaoPessoal'        ,( refBibOcorrencia?.publicacao ? '' : (refBibOcorrencia?.noAutor ?:'') ) )
        data.put( 'sqPrazoCarencia'             ,this.fichaOcorrencia?.prazoCarencia?.id ?:'' )

        // ler as fotos relacionadas ao registro
        data.put('fotos',[])
        if( fichaConsultaMultimidia )
        {
            fichaConsultaMultimidia.sort{it.fichaMultimidia.dtElaboracao}.each {
                data.fotos.push([idFoto         : it.id
                                 , dtFoto     : it.fichaMultimidia.dtElaboracao.format('dd/MM/yyyy')
                                 , hrFoto     : it.fichaMultimidia.dtElaboracao.format('hh:mm')
                                 , noAutorFoto: it.fichaMultimidia.noAutor ?: ''
                                 , deFoto     : it.fichaMultimidia.txMultimidia ?: ''
                                 , noArquivo  : it.fichaMultimidia.noArquivo ?: ''
                                 , inAprovada : ( it.fichaMultimidia.situacao.codigoSistema=='APROVADA' ? 'S' : 'N')
                                 , file       : ''
                                 , url        : 'getMultimidia/'+it.fichaMultimidia.id.toString()+'/thumb'
                                ])

            }
        }
        return data as JSON
    }
}
