package br.gov.icmbio

class FichaAmeacaRegiao {

	FichaAmeaca fichaAmeaca;
	FichaOcorrencia fichaOcorrencia

    static constraints = {
    }

    static mapping = {

		version 	  	false
     	table       	name 	:'salve.ficha_ameaca_regiao'
     	id          	column	:'sq_ficha_ameaca_regiao'
     	fichaAmeaca 	column  :'sq_ficha_ameaca'
     	fichaOcorrencia column  :'sq_ficha_ocorrencia'
	}
	public String getNoRegiao()
	{
		return this?.fichaOcorrencia?.noRegiao;
	}

	public String getTxLocal()
	{
		return this?.fichaOcorrencia?.txLocal;
	}

	public String getSqTipoAbrangencia()
	{
		return this?.fichaOcorrencia?.tipoAbrangencia?.id;
	}
	public String getDeTipoAbrangencia()
	{
		return this?.fichaOcorrencia?.tipoAbrangencia?.descricao;
	}
}
