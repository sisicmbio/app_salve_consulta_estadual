package br.gov.icmbio

class PublicacaoArquivo {
  Publicacao publicacao
  String noPublicacaoArquivo
  String deLocalArquivo
  String deTipoConteudo
 
    static constraints = {

    }
    static mapping = {
    	version 	false
     	table       name  : 'taxonomia.publicacao_arquivo'
     	id          column: 'sq_publicacao_arquivo'
     	publicacao  column: 'sq_publicacao'
    }
}
