package br.gov.icmbio

class Taxon {

    transient tree = [:]

    NivelTaxonomico nivelTaxonomico
    Taxon pai
    String noTaxon
    String noAutor
    Integer nuAno

    static constraints = {
    }

    static mapping = {
        version 	false
        cache       true
        table           name:'taxonomia.taxon'
        id         	    column:'sq_taxon'
        nivelTaxonomico column:'sq_nivel_taxonomico'
        pai 			column:'sq_taxon_pai'
        pai             fetch: 'join'
        pai             lazy: true

    }
    String getNoCientifico()
    {
        if( this.nivelTaxonomico.coNivelTaxonomico == 'ESPECIE')
        {
            return this?.pai?.noTaxon+' '+this?.noTaxon
        }
        else if( this.nivelTaxonomico.coNivelTaxonomico == 'SUBESPECIE')
        {
            return this?.pai?.pai?.noTaxon+' '+this?.pai?.noTaxon+' '+this?.noTaxon
        }
        return this.noTaxon;
    }

    String getNoCientificoCompleto()
    {
        if( this.noCientifico && this.noAutor )
        {
          return this.noCientifico +' '+ this.noAutor
        }
        return this.noCientifico
    }

    String getNoCientificoItalico()
    {
        if( this.nivelTaxonomico.coNivelTaxonomico == 'ESPECIE')
        {
            return '<i>' + this?.pai?.noTaxon+' '+this?.noTaxon+'</i>'
        }
        else if( this.nivelTaxonomico.coNivelTaxonomico == 'SUBESPECIE')
        {
            return '<i>' + this?.pai?.pai?.noTaxon+' '+this?.pai?.noTaxon+' '+this?.noTaxon+'</i>'
        }
        return '<i>' + this.noTaxon+'</i>'
    }


    String getNoCientificoCompletoItalico()
    {
        if( this.noCientifico && this.noAutor )
        {
          return '<i>'+this.noCientifico +'</i> '+ this.noAutor
        }
        return '<i>' + this.noCientifico + '</i>'
    }

    protected Map recursiveTaxon( Taxon t )
    {
        Map mapa = [:]
        if( this.id )
        {
            mapa.put('id' + t.nivelTaxonomico.coNivelTaxonomico.toLowerCase().capitalize(), t.id)
            mapa.put('no' + t.nivelTaxonomico.coNivelTaxonomico.toLowerCase().capitalize(), t.noTaxon.toLowerCase().capitalize() )
            if (t.pai)
            {
                mapa << recursiveTaxon(t.pai)
            }
        }
        return mapa
    }

    Map getStructure()
    {
        if( !tree )
        {
            Map structure = [:]
            if (this.id)
            {
                structure = recursiveTaxon(this)
                structure['coNivel'] = this.nivelTaxonomico.coNivelTaxonomico
                if (this.nivelTaxonomico.coNivelTaxonomico == 'ESPECIE')
                {
                    structure['noCientifico'] = structure['noGenero'] + ' ' + structure['noEspecie'].toLowerCase()
                } else if (this.nivelTaxonomico.coNivelTaxonomico == 'SUBESPECIE')
                {
                    structure['noCientifico'] = structure['noGenero'] + ' ' + structure['noEspecie'].toLowerCase() + ' ' + structure['noSubespecie'].toLowerCase()
                } else
                {
                    structure['noCientifico'] = structure['no' + this.nivelTaxonomico.coNivelTaxonomico.toLowerCase().capitalize()]
                }
                structure['coNivelTaxonomico'] = this.nivelTaxonomico.coNivelTaxonomico
            }
            tree = structure
        }
        return tree
    }
}
