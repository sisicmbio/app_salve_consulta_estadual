package br.gov.icmbio

class UsuarioPerfilInstituicao {

    UsuarioPerfil usuarioPerfil
    Instituicao instituicao
    Date dtInclusao

    static constraints = {

    }

    static mapping = {
        version false
        table       name: 'salve.usuario_perfil_instituicao'
        id          column: 'sq_usuario_perfil_instituicao' ,generator:'identity'
        usuarioPerfil column: 'sq_usuario_perfil'
        instituicao column: 'sq_instituicao'
    }


    def beforeValidate() {
        if( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
    }

}
