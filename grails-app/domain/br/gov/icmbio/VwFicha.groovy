package br.gov.icmbio

class VwFicha {

    Ficha ficha
    Integer sqCicloAvaliacao

    // nome cientifico
    Integer sqTaxon
    String  nmCientifico
    String coNivelTaxonomico
    Integer nuGrauTaxonomico

    // unidade organizacional
    Integer sqUnidadeOrg
    String  sgUnidadeOrg

    // usuário alteracao
    Integer sqUsuarioAlteracao
    String  noUsuarioAlteracao
    Date    dtAlteracao

    // situacao
    Integer sqSituacaoFicha
    String  dsSituacaoFicha
    String  cdSituacaoFichaSistema

    Integer sqGrupoSalve
    Integer sqGrupo
    Integer sqSubgrupo
    Integer sqCategoriaIucn
    Integer sqCategoriaFinal
    String stPossivelmenteExtintaFinal

    Integer vlPercentualPreenchimento
    String stPossivelmenteExtinta

    // quantidade de pendências
    Integer nuPendencia

    // publicacao
    Integer sqUsuarioPublicacao
    Date dtPublicacao

    // manter LC
    Boolean stManterLc

    // transferida
    Boolean stTransferida


    static constraints = {
    }

    static mapping = {
        //cache : true
        version false
        sort   nmCientifico:  'asc'
        table  name: 'salve.vw_ficha'
        id     column :'sq_ficha', insertable: false, updateable: false
        ficha  column :'sq_ficha', insertable: false, updateable: false
        ficha  lazy: true
        publicadoPor lazy:true
        //ficha  fetch: 'join'
    }

    CicloAvaliacao getCicloAvaliacao()
    {
        return sqCicloAvaliacao ? CicloAvaliacao.get( sqCicloAvaliacao) : null
    }

    boolean canModify( Sicae user )
    {
        if( !id )
        {
            return true
        }

        return true

    }


    Taxon getTaxon()
    {
        return sqTaxon ? Taxon.get(sqTaxon): null
    }

    DadosApoio getSituacaoFicha()
    {
        return sqSituacaoFicha ? DadosApoio.get(sqSituacaoFicha): null
    }

    String getAlteradoPorHtml()
    {
        if( ! sqUsuarioAlteracao || !dtAlteracao )
        {
            return ''
        }
        List nomes = noUsuarioAlteracao.split(' ')
        return nomes[0]+ ( nomes[1] ? ' '+nomes[1]:'')+(nomes[2] ?' '+nomes[2].substring(0,1)+'.':'')+'<br/>'+dtAlteracao?.format('dd/MM/yyyy HH:mm:ss')
    }

    Integer getQtdPendencia()
    {
        return nuPendencia ?: 0
    }

    Integer getPercentualPreenchimento()
    {
        return vlPercentualPreenchimento ?: 0
    }

    String getNoCientificoItalico()
    {
        return sqTaxon ? taxon.noCientificoItalico : ''
    }

    String getNoCientificoCompletoItalico()
    {
        return sqTaxon ? taxon.noCientificoCompletoItalico : ''
    }

    Unidade getUnidade() {
        return sqUnidadeOrg ? Unidade.get(sqUnidadeOrg) : null
    }

    String getColaboradoresHtml() {
       return ''
    }

    Map getUltimaAvaliacaoNacional()
    {
        //return ficha.ultimaAvaliacaoNacional
        return [:]
    }

    DadosApoio getCategoriaIucn()
    {
        return sqCategoriaIucn ? DadosApoio.get( sqCategoriaIucn ) : null
    }

    DadosApoio getCategoriaFinal()
    {
        return sqCategoriaFinal ? DadosApoio.get( sqCategoriaFinal ) : null
    }

    DadosApoio getGrupoSalve()
    {
        return sqGrupoSalve ? DadosApoio.get( sqGrupoSalve ): null
    }

    DadosApoio getGrupo()
    {
        return sqGrupo ? DadosApoio.get(sqGrupo): null
    }

    DadosApoio getSubgrupo()
    {
        return sqSubgrupo ? DadosApoio.get(sqSubgrupo): null
    }

    String getCodigoCategoriaUltimaAvaliacaoNacionalText()
    {
        Map dadosUltimaAvaliacaoNacional = getUltimaAvaliacaoNacional()
        return dadosUltimaAvaliacaoNacional.coCategoriaIucn
    }

    String getNomesComuns()
    {
        return ''
    }

    PessoaFisica getPublicadoPor()
    {

        if( ! sqUsuarioPublicacao )
        {
            return null
        }
        return PessoaFisica.get( sqUsuarioPublicacao )
    }

    String getPrimeiroNomeComum()
    {
        String nomesComuns = getNomesComuns()
        if( nomesComuns )
        {
            return nomesComuns.split(', ')[0]
        }
        return ''
    }
}
