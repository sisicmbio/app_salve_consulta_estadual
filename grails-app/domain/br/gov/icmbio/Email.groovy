package br.gov.icmbio

class Email {

	Pessoa pessoa
	String txEmail

    static constraints = {
    }

    static mapping = {
    	version 	false
    	//sort        tipo: 'asc'
     	table       name:'corporativo.vw_email'
     	id          column:'sq_email'
     	tipo 		column:'sq_tipo_email'
     	pessoa      column:'sq_pessoa'
    }
}
