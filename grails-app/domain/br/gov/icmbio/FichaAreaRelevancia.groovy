package br.gov.icmbio

class FichaAreaRelevancia {

	Ficha ficha
	DadosApoio tipoRelevancia
	Estado estado
    Integer sqUcFederal
    Integer sqUcEstadual
    Integer sqRppn
    String txRelevancia
	String txLocal

    static constraints = {
    }

    static mapping = {
    	version 	false
    	sort        id : 'asc'
	   	table       name 	: 'salve.ficha_area_relevancia'
	   	id          column 	: 'sq_ficha_area_relevancia'
	   	ficha		column 	: 'sq_ficha'
	   	tipoRelevancia column: 'sq_tipo_relevancia'
	   	estado      column: 'sq_estado'
    }

    def beforeValidate() {
    }

    public String getRefBibHtml()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_area_relevancia','sq_ficha_area_relevancia',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label tag pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label tag pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
    }

    public String getRefBibText()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_area_relevancia','sq_ficha_area_relevancia',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao )
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)')
            }
        }
        return nomes.join("\n");
    }

    public String getMunicipiosHtml()
    {
        List nomes = [];
        List municipios = FichaAreaRelevanciaMunicipio.findAllByFichaAreaRelevancia(this);
        municipios.each{
            nomes.push( it.municipio.noMunicipio/*+'-'+it.municipio.estado.sgEstado*/);
        }
        return nomes.join(', ');
    }

    /**
     * Retorna o nome da uc ou do local quando não tiver uc
     * @return [description]
     */
    public getLocalHtml()
    {
        String uc = this.getUcHtml();
        if( ! uc  )
        {
            return this.txLocal ?: '';
        }
        return uc?:'';
    }

    /**
     * retorna string com o nome ou sigla da uc
     * @return [description]
     */
    String getUcHtml()
    {
        Uc uc = this.uc
        if (uc)
        {
            return uc.noUc ? uc.noUc : uc.sgUnidade;
        }
        return '';
    }

    /**
     * retorno objeto uc
     * @return [description]
     */
    Uc getUc()
    {
        Integer id = 0;
        String esfera = '';
        if (this.sqUcFederal)
        {
            id = this.sqUcFederal;
            esfera='F'
        }
        else if (this.sqUcEstadual)
        {
            id = this.sqUcEstadual;
            esfera='E'
        }
        else if (this.sqRppn)
        {
            id=this.sqRppn;
            esfera='R'

        }
        if( id > 0 )
        {
            return Uc.findBySqUcAndInEsfera(id,esfera);
        }
        return null;
    }
}
