package br.gov.icmbio

class FichaRefBibTag {

	FichaRefBib fichaRefBib
	DadosApoio tag

    static constraints = {

    }

	static mapping = {
		version 				false;
	   	table       			name 	: 'salve.ficha_ref_bib_tag';
	   	id          			column 	: 'sq_ficha_ref_bib_tag',generator:'identity';
	   	fichaRefBib 	   		column  : 'sq_ficha_ref_bib';
	   	tag                     column  : 'sq_tag'
	}
}
