package br.gov.icmbio

class FichaPesquisa {

    Ficha       ficha
    DadosApoio  tema
    DadosApoio  situacao
	String txFichaPesquisa

    static constraints = {}

    static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_pesquisa'
	   	id          			column 	: 'sq_ficha_pesquisa'
	   	ficha 					column  : 'sq_ficha'
	   	tema 					column 	: 'sq_tema_pesquisa'
	   	situacao				column 	: 'sq_situacao_pesquisa'
	}

    public String getRefBibHtml()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_pesquisa','sq_ficha_pesquisa',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label tag pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label tag pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
    }

    public String getRefBibText()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_pesquisa','sq_ficha_pesquisa',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao);
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)');
            }
        }
        return nomes.join('\n');
    }
}