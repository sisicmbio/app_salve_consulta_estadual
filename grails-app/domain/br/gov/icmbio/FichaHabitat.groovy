package br.gov.icmbio

class FichaHabitat {
	Ficha ficha
	DadosApoio habitat
    static constraints = {
    }

    static mapping = {
    	version 			false
        sort                habitat : 'asc'
	   	table       		name 	: 'salve.ficha_habitat'
	   	id          		column 	: 'sq_ficha_habitat'
	   	ficha 				column  : 'sq_ficha'
	   	habitat 			column 	: 'sq_habitat'
    }
    public String getDescricao()
    {
        return (this.habitat.pai && this.habitat.pai.pai ? this.habitat.pai.descricao +' / ':'') + this.habitat.descricao
    }

    public String getRefBibHtml()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_habitat','sq_ficha_habitat',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label tag pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label tag pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
    }

    public String getRefBibText()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_habitat','sq_ficha_habitat',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao )
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)' )
            }
        }
        return nomes.join('\n')
    }














}