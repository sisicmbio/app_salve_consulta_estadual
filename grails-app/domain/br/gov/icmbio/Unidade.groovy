package br.gov.icmbio
/**
 * Manter compatibilidade com SALVE ICMBIo. As referencias a esta classem devem ser substituidas pela classe Instituicao
 */
class Unidade {

    Pessoa pessoa
    Unidade pai
    DadosApoio tipo
    String sgInstituicao
    String nuCnpj
    String noContato
    String nuTelefone
    Date dtAlteracao

    static constraints = {
        nuCnpj unique: true,nullable: true, blank: false
        noContato nullable: true, blank:false
        nuTelefone nullable: true, blank:false
        tipo nullable: true
        pai nullable: true
        dtAlteracao nullable: true

    }

    static mapping = {
        version false
        sort 		pai: 'asc'
        table       name: 'salve.instituicao'
        id          column:'sq_pessoa', generator: 'foreign', params: [property: 'pessoa']
        pessoa      insertable: false, updateable: false, column: 'sq_pessoa'
        pai         column : 'sq_pessoa_pai'
        tipo        column: 'sq_tipo'
    }

    def beforeValidate() {
        dtAlteracao = new Date()
        if( this.nuCnpj ) {
            this.nuCnpj = this.nuCnpj.replaceAll(/[^0-9]/,'')
            this.nuCnpj = this.nuCnpj.length() != 14 ? null : this.nuCnpj
        } else {
            this.nuCnpj=null
        }
    }

    String getCnpjFormatado(){
        return Util.formatCnpj(nuCnpj)
    }

    /**
     * retornar a quantidade de instituições subordinadas
     * @return
     */
    Integer getNumFilhos(){

        return Unidade.createCriteria().count{
            eq('pai',this)
        }
    }

    Map asMap(){
        return [
                sqPessoa            : id  ?: '',
                noPessoa            : pessoa ? pessoa.noPessoa: '',
                sgInstituicao       : sgInstituicao ?: '',
                noContato           : noContato ?:'',
                nuTelefone          : nuTelefone ?:'',
                deEmail             : pessoa?.deEmail ?:'',
                nuCnpj              : cnpjFormatado,
                sqPai               : pai ? pai.id : null,
                noPai               : pai ? pai.pessoa.noPessoa :'',
                sgPai               : pai ? pai.sgInstituicao : '',
                sqTipo              : tipo ? tipo.id : null,
                dsTipo              : tipo ? tipo.descricao : ''
            ]
    }

    String getSgUnidade(){
        return sgInstituicao ?: ''
    }

    String getSiglaNome(){
        return  sgUnidade + ( (pessoa.noPessoa == sgUnidade) ? '':' - ' + pessoa.noPessoa )
    }
}
