package br.gov.icmbio

class FichaRefBib {

	Ficha ficha;
	Publicacao publicacao;
	String noTabela;
	String noColuna;
	String deRotulo;
	Integer sqRegistro;
	Integer nuAno;
	String noAutor;

    static constraints = {
        noAutor nullable: true
        nuAno nullable: true
        publicacao nullable: true
    }

	static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_ref_bib'
	   	id          			column 	: 'sq_ficha_ref_bib', generator:'identity'
	   	ficha 					column  : 'sq_ficha'
	   	publicacao 				column  : 'sq_publicacao'
	}

	public String getTagsIds()
	{
		List tags = FichaRefBibTag.findAllByFichaRefBib( this );
		List result=[];
		tags.each {
			result.push( it.tag.id )
		}
		return result
	}

	public String getTagsAsHtml()
	{
		List tags = FichaRefBibTag.findAllByFichaRefBib( this );
		List result=[];
		tags.each {
			//result.push( '<span class="badge">'+it.tag.descricao+'</span>' )
			result.push( '<span class="label label-default tag">'+it.tag.descricao+'</span>' )
		}
		return result.join( ' ')
	}

    public String getReferenciaHtml()
    {
        if(this.publicacao )
        {
            return this.publicacao.referenciaHtml;
        }
        else
        {
            return this.noAutor+' Com.Pess. '+this.nuAno+'.';
        }
    }
}
