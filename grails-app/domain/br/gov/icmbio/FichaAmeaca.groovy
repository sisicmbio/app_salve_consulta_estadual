package br.gov.icmbio

class FichaAmeaca {

	Ficha ficha
	DadosApoio criterioAmeacaIucn
	String txFichaAmeaca

    static constraints = {
       }

    static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_ameaca'
	   	id          			column 	: 'sq_ficha_ameaca'
	   	ficha 					column  : 'sq_ficha'
	   	criterioAmeacaIucn		column 	: 'sq_criterio_ameaca_iucn'
	}

    public String getRefBibHtml()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_ameaca','sq_ficha_ameaca',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label tag pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label tag pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
    }

    public String getRefBibText()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_ameaca','sq_ficha_ameaca',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao);
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)' )
            }
        }
        return nomes.join('\n');
    }

    public String getRegioesHtml()
    {
        List regioes = [];
         DadosApoio contexto = DadosApoio.findByCodigo('TB_CONTEXTO_OCORRENCIA');
        contexto = DadosApoio.findByCodigoSistemaAndPai('AMEACA_REGIAO',contexto);
        List dados = FichaAmeacaRegiao.createCriteria().list {
            createAlias('fichaOcorrencia', 'b')
            eq("fichaAmeaca", this)
            eq('b.contexto',contexto)
        }
        dados.each {
            regioes.push( '<li>'+it.deTipoAbrangencia+(it?.noRegiao ? ' - '+it.noRegiao:'')+'</li>');
        }

        return (regioes.size() > 1 ? '<br>':'') + '<ol>'+regioes.join('')+'</ol>';
    }

    public String getRegioesText()
    {
        List regioes = [];
         DadosApoio contexto = DadosApoio.findByCodigo('TB_CONTEXTO_OCORRENCIA');
        contexto = DadosApoio.findByCodigoSistemaAndPai('AMEACA_REGIAO',contexto);
        List dados = FichaAmeacaRegiao.createCriteria().list {
            createAlias('fichaOcorrencia', 'b')
            eq("fichaAmeaca", this)
            eq('b.contexto',contexto)
        }
        dados.eachWithIndex { regiao, index ->
            int i = index+97
            regioes.push( Character.toString( (char)i ) + '. '+regiao.deTipoAbrangencia+ (regiao?.noRegiao ? ' - '+regiao.noRegiao : '') )
        }
        return regioes.join('\n')
    }

    public String getGeoHtml()
    {
        List coords = [];
        DadosApoio contexto = DadosApoio.findByCodigo('TB_CONTEXTO_OCORRENCIA');
        contexto = DadosApoio.findByCodigoSistemaAndPai('AMEACA_GEO',contexto);
        List dados = FichaAmeacaRegiao.createCriteria().list {
            createAlias('fichaOcorrencia', 'b')
            eq("fichaAmeaca", this)
            eq('b.contexto',contexto)
        }
        dados.each {
            coords.push( it.fichaOcorrencia.coordenadaGmsHtml);
        }
        return '<br/>'+coords.join('<hr class="hr-separador">');
    }
}
