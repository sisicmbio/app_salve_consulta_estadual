package br.gov.icmbio

class CicloConsulta {

    CicloAvaliacao cicloAvaliacao
    DadosApoio tipoConsulta
    Date dtInicio
    Date dtFim
    //String txEmail

    static hasMany = [pessoas:CicloConsultaPessoa,
                      fichas :CicloConsultaFicha]

    static constraints = {
    }

    static mapping = {
        version 	false
        table           name:'salve.ciclo_consulta'
        id              column:'sq_ciclo_consulta'
        cicloAvaliacao  column:'sq_ciclo_avaliacao'
        tipoConsulta    column : 'sq_tipo_consulta'

    }
}
