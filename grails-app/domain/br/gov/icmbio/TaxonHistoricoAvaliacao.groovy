package br.gov.icmbio

import grails.converters.JSON

class TaxonHistoricoAvaliacao {

    Taxon taxon
    DadosApoio tipoAvaliacao
    Abrangencia abrangencia     // Goiás, Cerrado
    DadosApoio categoriaIucn
    Integer nuAnoAvaliacao
    String deCriterioAvaliacaoIucn
    String txJustificativaAvaliacao
    String stPossivelmenteExtinta
    String noRegiaoOutra
    String deCategoriaOutra

    static constraints = {
        taxon                       nullable:false
        tipoAvaliacao               nullable:false
        nuAnoAvaliacao              nullable:false

        abrangencia				    nullable:true
        categoriaIucn               nullable:true
        deCriterioAvaliacaoIucn		nullable:true
        txJustificativaAvaliacao	nullable:true
        stPossivelmenteExtinta      nullable:true, defaultValue:false
        noRegiaoOutra				nullable:true
        deCategoriaOutra            nullable:true
    }

    static mapping = {
        version 				false
        //sort                    nuAnoAvaliacao:'taxon'
        table       			name 	: 'salve.taxon_historico_avaliacao'
        id          			column 	: 'sq_taxon_historico_avaliacao',generator:'identity'
        taxon 					column  : 'sq_taxon'
        tipoAvaliacao			column 	: 'sq_tipo_avaliacao'
        categoriaIucn           column  : 'sq_categoria_iucn'
        abrangencia             column  : 'sq_abrangencia'      // Goias, Cerrado,
    }

    JSON asJson()
    {
        Map data = [:]
        data.put( 'sqTaxonHistoricoAvaliacao'	,this.id)
        data.put( 'sqTipoAvaliacao'				,this.tipoAvaliacao.id)
        data.put( 'nuAnoAvaliacao'				,this.nuAnoAvaliacao)

        data.put( 'sqAbrangencia' 		        ,this?.abrangencia?.id )
        data.put( 'deTipoAbrangencia'			    ,this?.abrangencia?.tipo?.descricao)
        data.put( 'deAbrangencia' 		        ,this?.abrangencia?.descricao)

        data.put( 'sqCategoriaIucn'				,this?.categoriaIucn?.id)
        data.put( 'deCategoriaOutra'				,this?.deCategoriaOutra)

        data.put( 'dsCriterioAvalIucn'	    	,this.deCriterioAvaliacaoIucn)
        data.put( 'stPossivelmenteExtinta'	    ,this.stPossivelmenteExtinta)
        data.put( 'txJustificativaAvaliacao'	    ,this.txJustificativaAvaliacao)
        data.put( 'noRegiaoOutra'				    ,this.noRegiaoOutra)
        return data as JSON
    }

    String getRefBibHtml( Long sqFicha = null )
    {
        List nomes = []
        FichaRefBib.createCriteria().list {
            eq('noTabela', 'taxon_historico_avaliacao')
            eq('noColuna', 'sq_taxon_historico_avaliacao')
            if ( sqFicha ) {
                eq('ficha.id', sqFicha)
                eq('sqRegistro', this.id.toInteger())
                //eq('sqRegistro', sqRegistro)
            } else {
                eq('sqRegistro', this.id.toInteger())
            }
            isNotNull('publicacao') // para não incluir as comunicações pessoais
        }.unique{ [it.publicacao] }.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label label-warning tag cursor-pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label label-warning tag cursor-pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>')
    }

    String getRefBibText( Long sqFicha = null )
    {
        List nomes = []
        FichaRefBib.createCriteria().list{
            eq('noTabela','taxon_historico_avaliacao' )
            eq('noColuna','sq_taxon_historico_avaliacao')
            if ( sqFicha ) {
                eq('ficha.id', sqFicha)
            } else {
                eq('sqRegistro', this.id.toInteger())
            }
            isNotNull('publicacao') // para não incluir as comunicações pessoais
        }.unique{ [it.publicacao] }.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao )
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)' )
            }
        }
        return nomes.join('\n')

        //List nomes = [];
        /*List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_historico_avaliacao','sq_ficha_historico_avaliacao',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao);
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)');
            }
        }
        return nomes.join('\n');
        */
    }

    DadosApoio getTipoAbrangencia()
    {
        return this?.abrangencia?.tipo
    }
}
