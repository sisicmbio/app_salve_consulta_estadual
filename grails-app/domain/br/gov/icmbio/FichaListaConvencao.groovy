package br.gov.icmbio

class FichaListaConvencao {


	Ficha ficha
	DadosApoio listaConvencao
	Integer nuAno

    static constraints = {}

    static mapping = {
		version 				false
	   	table       			name 	: 'salve.ficha_lista_convencao'
	   	id          			column 	: 'sq_ficha_lista_convencao'
	   	ficha 					column  : 'sq_ficha'
	   	listaConvencao			column 	: 'sq_lista_convencao'
	}
}