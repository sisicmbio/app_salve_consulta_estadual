package br.gov.icmbio

class CicloConsultaPessoa {

    PessoaFisica pessoa
    DadosApoio situacao
    Date dtEnvio
    String deEmail
    String deObservacao

    static belongsTo = [cicloConsulta:CicloConsulta]
    static hasMany = [fichas:CicloConsultaPessoaFicha]

    static constraints = {
        deObservacao nullable: true
    }

    static mapping = {
        version 	false
        table           name:'salve.ciclo_consulta_pessoa'
        id              column:'sq_ciclo_consulta_pessoa'
        cicloConsulta   column: 'sq_ciclo_consulta'
        pessoa          column : 'sq_pessoa'
        situacao        column : 'sq_situacao'
    }
}
