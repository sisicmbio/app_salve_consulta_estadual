package br.gov.icmbio

class CicloConsultaFicha {
    Ficha ficha
    VwFicha vwFicha

    static belongsTo = [cicloConsulta:CicloConsulta]
    static hasMany = [colaboracoes:FichaColaboracao,colaboracoesOcorrencia:FichaOcorrenciaConsulta]
    static constraints = {
        vwFicha    nullable:true
    }
    static mapping = {
        version 	false
        table           name  :'salve.ciclo_consulta_ficha'
        id              column:'sq_ciclo_consulta_ficha', generator:'identity'
        ficha           column:'sq_ficha'
        vwFicha         column:'sq_ficha',insertable: false, updateable: false
        cicloConsulta   column:'sq_ciclo_consulta'
    }
}
