package br.gov.icmbio

class UsuarioPerfil {

    Usuario usuario
    Perfil perfil
    Date dtInclusao

    //static hasMany = [usuarioPerfilInstituicao:UsuarioPerfilInstituicao]

    static constraints = {

    }

    static mapping = {
        version false
        table       name: 'salve.usuario_perfil'
        id          column: 'sq_usuario_perfil' ,generator:'identity'
        usuario     column: 'sq_pessoa'
        perfil      column: 'sq_perfil'
    }


    def beforeValidate() {
        if( ! this.dtInclusao ) {
            this.dtInclusao = new Date()
        }
    }

    List instituicoes() {
        return UsuarioPerfilInstituicao.findAllByUsuarioPerfil(this)
    }

}
