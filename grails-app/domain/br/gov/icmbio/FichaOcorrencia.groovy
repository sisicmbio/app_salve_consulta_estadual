/**
 * Domain da classe FichaOcorrencia.
 * 1. documentação relacionada com o tipo geometry:
 * 		- http://www.hibernatespatial.org/
 * 		- http://www.hibernatespatial.org/repository/org/hibernate/hibernate-spatial/4.3/
 *
 * 2 - Quando a precisão da coordenada for APROXIMADA, o campo referencia da aproximação é obrigatório
 *
 *
 *
 *
 *
 */
package br.gov.icmbio
import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType

class FichaOcorrencia {
    transient geoValidate = true
    // desativar a validação do Estado e municipio na coordenada durante a importação de planilhas
    transient ucControle //  controle de gravação das UCs em federal,estadual ou rppn

    Ficha ficha
    DadosApoio tipoRegistro
    DadosApoio refAproximacao
    DadosApoio datum
    DadosApoio precisaoCoordenada
    DadosApoio formatoCoordOriginal
    DadosApoio continente
    DadosApoio metodoAproximacao
    DadosApoio habitat
    DadosApoio qualificadorValTaxon // identificacao incerta
    DadosApoio prazoCarencia
    DadosApoio contexto
    DadosApoio baciaHidrografica
    DadosApoio bioma
    DadosApoio tipoAbrangencia
    DadosApoio situacao // TB_SITUACAO_COLABORACAO ACEITA/NAO_ACEITA/RESOLVER_OFICINA ETC..
    DadosApoio situacaoAvaliacao // TB_SITUACAO_REGISTRO_OCORRENCIA: REGISTRO_UTILIZADO_AVALIACAO / REGISTRO_NAO_UTILIZADO_AVALIACAO / REGISTRO_NAO_CONFIRMADO / REGISTRO_CONFIRMADO_ADICIONADO_APOS_AVALIACAO

    DadosApoio divisaoAdministrativa
    DadosApoio ecoregiao
    PessoaFisica pessoaRevisor
    PessoaFisica pessoaValidador
    PessoaFisica pessoaCompilador

    Pais pais
    Estado estado
    Municipio municipio
    String txMetodoAproximacao
    Taxon taxonCitado
    Integer sqUcFederal
    Integer sqUcEstadual
    Integer sqRppn
    String txLocal
    String noLocalidade
    Date dtRegistro // talvez não va precisar mais deste campo
    String hrRegistro
    Double vlElevacaoMin
    Double vlElevacaoMax
    Double vlProfundidadeMin
    Double vlProfundidadeMax
    String deIdentificador
    Date dtIdentificacao
    String deCentroResponsavel
    String inPresencaAtual
    String txTombamento
    String txInstituicao
    Date dtCompilacao
    Date dtRevisao
    Date dtValidacao
    Integer nuDiaRegistro
    Integer nuMesRegistro
    Integer nuAnoRegistro
    Integer nuDiaRegistroFim
    Integer nuMesRegistroFim
    Integer nuAnoRegistroFim

    String inDataDesconhecida
    String inUtilizadoAvaliacao
    String txNaoUtilizadoAvaliacao
    DadosApoio motivoNaoUtilizadoAvaliacao
    Date dtInvalidado
    Date dtInclusao
    Date dtAlteracao
    PessoaFisica pessoaInclusao
    PessoaFisica pessoaAlteracao
    String idOrigem
    String txObservacao
    String txNaoAceita

    Integer nuAltitude
    String inSensivel

    Geometry geometry

    static constraints = {
        ficha nullAble: false
        estado nullable: true
        bioma nullable: true
        pessoaInclusao nullable: true
        pessoaAlteracao nullable: true

        baciaHidrografica nullable: true
        divisaoAdministrativa nullable: true
        ecoregiao nullable: true

        geometry nullable: true
        prazoCarencia nullable: true
        datum nullable: true
        formatoCoordOriginal nullable: true

        // regra de negocio para a coluna precisao da coordenada
        precisaoCoordenada nullable: true
        precisaoCoordenada(validator: { value, object ->
            if (object?.precisaoCoordenada?.codigo == 'APROXIMADA') {
                if (!object?.refAproximacao?.id) {
                    //return ['fichaOcorrencia.refAproximacao','Para precisão APROXIMADA, é Necessário informar a referência da aproximação!'];
                    return ['ficha.ocorrencia.referencia.aproximacao.aproximada.message'];
                }
            }
            return true;
        })


        refAproximacao nullable: true
        metodoAproximacao nullable: true
        txMetodoAproximacao nullable: true

        taxonCitado nullable: true
        inPresencaAtual nullable: true, maxSize: 1, inList: ['S', 'N', 'D']
        tipoRegistro nullable: true

        dtRegistro nullable: true
        inDataDesconhecida nullable: true, inList: ['S', 'N']
        nuDiaRegistro nullable: true;
        nuMesRegistro nullable: true;
        nuAnoRegistro nullable: true;
        nuDiaRegistroFim nullable: true;
        nuMesRegistroFim nullable: true;
        nuAnoRegistroFim nullable: true;

        hrRegistro nullable: true

        continente nullable: true
        pais nullable: true
        municipio nullable: true

        qualificadorValTaxon nullable: true
        habitat nullable: true
        txLocal nullable: true
        noLocalidade nullable: true
        deIdentificador nullable: true
        dtIdentificacao nullable: true
        txTombamento nullable: true
        txInstituicao nullable: true

        pessoaCompilador nullable: true
        pessoaRevisor nullable: true
        pessoaValidador nullable: true
        dtCompilacao nullable: true
        dtValidacao nullable: true
        dtRevisao nullable: true

        deCentroResponsavel nullable: true
        vlElevacaoMin nullable: true
        vlElevacaoMax nullable: true
        vlProfundidadeMin nullable: true
        vlProfundidadeMax nullable: true
        sqUcFederal nullable: true
        sqUcEstadual nullable: true
        sqRppn nullable: true
        tipoAbrangencia nullable: true

        inUtilizadoAvaliacao nullable: true, inList: ['S', 'N', 'E']
        txNaoUtilizadoAvaliacao nullable: true
        motivoNaoUtilizadoAvaliacao nullable:true
        dtInvalidado nullable: true
        situacao nullable: true
        situacaoAvaliacao: nullable: false
        idOrigem nullable: true, blank:true
        txObservacao nullable:true, blank:true
        txNaoAceita nullable:true, blank:true
        nuAltitude nullable:true
        inSensivel nullable:true, inList: ['S', 'N']
        //stAdicionadoAposAvaliacao nullable:true
    }

    static mapping = {
        version false
        table name: 'salve.ficha_ocorrencia'
        id column: 'sq_ficha_ocorrencia', generator: 'identity'
        ficha column: 'sq_ficha'
        contexto column: 'sq_contexto'
        estado column: 'sq_estado'

        bioma column: 'sq_bioma'
        baciaHidrografica column: 'sq_bacia_hidrografica'

        habitat column: 'sq_habitat'
        continente column: 'sq_continente'
        pais column: 'sq_pais'
        municipio column: 'sq_municipio'

        geometry type: GeometryType, sqlType: "GEOMETRY"
        geometry column: 'ge_ocorrencia'
        prazoCarencia column: 'sq_prazo_carencia'
        datum column: 'sq_datum'
        formatoCoordOriginal column: 'sq_formato_coord_original'
        precisaoCoordenada column: 'sq_precisao_coordenada'
        refAproximacao column: 'sq_ref_aproximacao'
        metodoAproximacao column: 'sq_metodo_aproximacao'

        taxonCitado column: 'sq_taxon_citado'
        tipoRegistro column: 'sq_tipo_registro'
        qualificadorValTaxon column: 'sq_qualificador_val_taxon'

        pessoaCompilador column: 'sq_pessoa_compilador'
        pessoaRevisor column: 'sq_pessoa_revisor'
        pessoaValidador column: 'sq_pessoa_validador'
        tipoAbrangencia column: 'sq_tipo_abrangencia'

        divisaoAdministrativa column: 'sq_divisao_administrativa'
        ecoregiao column: 'sq_ecoregiao'
        pessoaInclusao column: 'sq_pessoa_inclusao'
        pessoaAlteracao column: 'sq_pessoa_alteracao'
        situacao column: 'sq_situacao'
        situacaoAvaliacao column: 'sq_situacao_avaliacao'
        motivoNaoUtilizadoAvaliacao column: 'sq_motivo_nao_utilizado_avaliacao'
    }

    def beforeValidate()
    {
        if( !this.dtInclusao )
        {
            this.dtInclusao = new Date()
        }
        this.dtAlteracao = new Date()

        if( ! this.situacaoAvaliacao ){
            this.situacaoAvaliacao = DadosApoio.findByCodigoSistemaAndPai('REGISTRO_NAO_CONFIRMADO', DadosApoio.findByCodigoSistema('TB_SITUACAO_REGISTRO_OCORRENCIA'));
        }
    }

    String getRefBibHtml()
    {
        String coluna = 'sq_ficha_ocorrencia';

        if (this.contexto.codigoSistema == 'ESTADO')
        {
            coluna = 'sq_estado'
        } else if (this.contexto.codigoSistema == 'BIOMA')
        {
            coluna = 'sq_bioma'
        } else if (this.contexto.codigoSistema == 'BACIA_HIDROGRAFICA')
        {
            coluna = 'sq_bacia_hidrografica'
        }
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha, 'ficha_ocorrencia', coluna, this.id);
        refs.each {
            if (it.publicacao)
            {
                nomes.push(it.publicacao.citacao + '&nbsp;<span class="label tag pointer pull-right" title="' + it.publicacao.deTitulo.replaceAll(/"/, "&quot;") + '">...</span>');
            } else
            {
                nomes.push( it.noAutor + ( it.nuAno ?', ' + it.nuAno : '' )+ '&nbsp;<span class="label tag pointer pull-right" title="Comunicação Pessoal">...</span>');
            }
        }
        return nomes.join('<br>');
    }

    String getRefBibText()
    {
        String coluna = 'sq_ficha_ocorrencia';

        if (this.contexto.codigoSistema == 'ESTADO')
        {
            coluna = 'sq_estado'
        } else if (this.contexto.codigoSistema == 'BIOMA')
        {
            coluna = 'sq_bioma'
        } else if (this.contexto.codigoSistema == 'BACIA_HIDROGRAFICA')
        {
            coluna = 'sq_bacia_hidrografica'
        }
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha, 'ficha_ocorrencia', coluna, this.id);
        refs.each {
            if (it.publicacao)
            {
                nomes.push(it.publicacao.citacao);
            } else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)')
            }
        }
        return nomes.join('\n');
    }

    String getNoRegiao()
    {
        // o nome da região, vai depender de qual campo está preenchido, uf, bacia, pais, município etc...
        if (this.continente)
        {
            return this.continente.descricao;
        }
        if (this.pais)
        {
            return this.pais.noPais;
        }

        if (this.estado)
        {
            return this.estado.noEstado + '-' + this.estado.sgEstado;
        }

        if (this.municipio)
        {
            return this.municipio.noMunicipio + '-' + this.municipio.estado.sgEstado;
        }

        if (this.baciaHidrografica)
        {
            String pai = ''
            if (this.baciaHidrografica.pai)
            {
                pai = this.baciaHidrografica.pai.descricao + ' / ';
            }
            return this.baciaHidrografica.codigo + ' - ' + pai + this.baciaHidrografica.descricao;
        }

        if (this.bioma)
        {
            return this.bioma.descricao;
        }

        if (this.uc)
        {
            return this.uc.sgUc
        }

        if (this.divisaoAdministrativa)
        {
            return this.divisaoAdministrativa.descricao
        }

        if (this.ecoregiao)
        {
            return this.ecoregiao.descricao
        }

        return this.noLocalidade;
    }

    Integer getIdRegiao()
    {
        // o nome da região, vai depender de qual campo está preenchido, uf, bacia, pais, município etc...
        if (this.continente)
        {
            return this.continente.id;
        }
        if (this.pais)
        {
            return this.pais.id;
        }

        if (this.estado)
        {
            return this.estado.id;
        }

        if (this.municipio)
        {
            return this.municipio.id;
        }

        if (this.baciaHidrografica)
        {
            return this.baciaHidrografica.id;
        }

        if (this.bioma)
        {
            return this.bioma.id;
        }

        if (this.uc)
        {
            return this.uc.id;
        }

        if (this.divisaoAdministrativa)
        {
            return this.divisaoAdministrativa.id;
        }

        if (this.ecoregiao)
        {
            return this.ecoregiao.id;
        }
        return 0;
    }

    Map  getCoordenadasGms()
    {

        if (!this.geometry)
        {
            return [:]
        }

        Double valor
        String nslw;
        Map result = ['lat'  : ['graus': 0, 'minutos': 0, 'segundos': 0, 'gms': '']
                      , 'lon': ['graus': 0, 'minutos': 0, 'segundos': 0, 'gms': '']
            ];

        // latitude
        valor = this.geometry.y
        if (valor < 0)
        {
            nslw = 'S'
            valor *= -1;
        } else
        {
            nslw = 'N'
        }
        result.lat.graus = valor.round();
        result.lat.minutos = (Double) ((valor - result.lat.graus) * 60).round();
        result.lat.segundos = (Double) ((((valor - result.lat.graus) * 60) - result.lat.minutos) * 60).round(2);
        result.lat.gms = result.lat.graus.toString() + "° " + result.lat.minutos.toString() + "' " + result.lat.segundos.toString() + "\" " + nslw;
        // longitude
        valor = this.geometry.x
        if (valor < 0)
        {
            nslw = 'W'
            valor *= -1;
        } else
        {
            nslw = 'L'
        }

        result.lon.graus = valor.round();
        result.lon.minutos = (Double) ((valor - result.lon.graus) * 60).round();
        result.lon.segundos = (Double) ((((valor - result.lon.graus) * 60) - result.lon.minutos) * 60).round(2);
        result.lon.gms = result.lon.graus.toString() + "° " + result.lon.minutos.toString() + "' " + result.lon.segundos.toString() + "\" " + nslw;
        return result
    }


    /**
     * retorna string com o nome ou sigla da uc
     * @return [description]
     */
    String getUcHtml()
    {
        Uc uc = this.uc
        if (uc)
        {
            return uc.noUc ? uc.noUc : uc.sgUnidade;
        }
        return '';
    }

    /**
     * retorno objeto uc
     * @return [description]
     */
    Uc getUc()
    {
        Integer id = 0;
        String esfera = '';
        if (this.sqUcFederal)
        {
            id = this.sqUcFederal;
            esfera='F'
        }
        else if (this.sqUcEstadual)
        {
            id = this.sqUcEstadual;
            esfera='E'
        }
        else if (this.sqRppn)
        {
            id=this.sqRppn;
            esfera='R'

        }
        if( id > 0 )
        {
            return Uc.findBySqUcAndInEsfera(id,esfera);
        }
        return null;
    }
    String getInPresencaAtualText()
    {
        if( this.inPresencaAtual =='S')
        {
            return 'Sim'
        }
        else if( this.inPresencaAtual =='N')
        {
            return 'Não'
        }
        else if( this.inPresencaAtual =='D')
        {
            return 'Desconhecido'
        }
        return ''
    }

    String getInUtilizadoAvaliacaoText()
    {
        if (this.inUtilizadoAvaliacao == 'S')
        {
            return 'Sim'
        } else if (this.inUtilizadoAvaliacao == 'N')
        {
            return 'Não'
        } else if (this.inUtilizadoAvaliacao == 'D')
        {
            return 'Desconhecido'
        }
        return ''
    }

    String getInDataDesconhecidaText()
    {
        if( this.inDataDesconhecida =='S')
        {
            return 'Sim'
        }
        else if( this.inDataDesconhecida =='N')
        {
            return 'Não'
        }
        else if( this.inDataDesconhecida =='D')
        {
            return 'Desconhecido'
        }
        return ''
    }

    Double getLatDecimal()
    {
        return ( this?.geometry ? this.geometry.y: null )
    }

    Double getLonDecimal()
    {
        return ( this?.geometry ? this.geometry.x: null )
    }

    String getNoUcFederal()
    {
        if( this.sqUcFederal )
        {
            Uc uc = Uc.findBySqUcAndInEsfera(this.sqUcFederal,'F');
            if( uc )
            {
                return uc.sgUnidade ?: uc.noUc
            }
        }
        return ''
    }
    String getNoUcEstadual()
    {
        if( this.sqUcEstadual )
        {
            Uc uc = Uc.findBySqUcAndInEsfera(this.sqUcFederal,'E');
            if( uc )
            {
                return uc.sgUnidade ?: uc.noUc
            }
        }
        return ''
    }
    String getNoRppn()
    {
        if( this.sqRppn )
        {
            Uc uc = Uc.findBySqUcAndInEsfera(this.sqUcFederal,'R');
            if( uc )
            {
                return uc.sgUnidade ?: uc.noUc
            }
        }
        return ''
    }

    DadosApoio getAmbiente()
    {
        if( this.habitat )
        {
            return DadosApoio.get( this.habitat.rootId )
        }
        return null;
    }

    String getDataHora() {
        String resultado=''
        if( inDataDesconhecida == 'S')
        {
            resultado =  'desconhecida'
        }
        else if( nuAnoRegistro && nuMesRegistro && nuDiaRegistro )
        {
            resultado = nuDiaRegistro+'/'+nuMesRegistro.toString().padLeft(2,'0')+'/'+nuAnoRegistro
        }
        else if( nuAnoRegistro && nuMesRegistro )
        {
            resultado = nuMesRegistro.toString().padLeft(2,'0')+'/'+nuAnoRegistro
        }
        else if( nuAnoRegistro )
        {
            resultado = nuAnoRegistro
        }
        if( hrRegistro )
        {
            resultado += ' ' + hrRegistro
        }
        return resultado
    }

    Date getCarencia()
    {
        Integer prazo = 0i
        if( this.dtInclusao )
        {
            if( this.prazoCarencia )
            {
                try {
                    String medida = this.prazoCarencia.codigoSistema.replaceAll(/[0-9_]/, '')
                    if (medida ==~ /DIAS?|MES(ES)?|ANOS?/)
                        prazo = this.prazoCarencia.codigoSistema.replaceAll(/[^0-9]/, '').toInteger()
                    if (prazo > 0) {

                        if (medida ==~ /DIAS?/) {
                            return this.dtInclusao + prazo
                        } else if (medida ==~ /ANOS?/) {
                            return this.dtInclusao + (365 * prazo)
                        } else if (medida ==~ /MES(ES)?/) {
                            return this.dtInclusao + (30 * prazo)
                        } else {
                            return null
                        }
                    }
                }
                catch( Exception e )
                {
                    println 'Prazo carencia invalido ' +this?.prazoCarencia?.codigoSistema
                    return null
                }
            }
        }
        return null
    }

    String getDataFimCarencia() {
        Date dtFimCarencia = this.carencia
        if( dtFimCarencia )
        {
            return this.prazoCarencia.descricao + ' ('+ dtFimCarencia.format('dd/MM/yyyy')+')'
        }
        return 'Sem carência'
    }

    Boolean getEmCarencia() {
        if( ! this.carencia )
        {
            return false
        }
        if ( this.carencia > new Date() ) {
            return true
        }
        return false
    }

}
