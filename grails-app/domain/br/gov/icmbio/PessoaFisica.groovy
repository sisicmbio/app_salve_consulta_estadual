package br.gov.icmbio

class PessoaFisica {

    Pessoa pessoa
    Instituicao instituicao
    String nuCpf
    Date dtAlteracao

    static constraints = {
        nuCpf nullable:true,unique: true
        instituicao nullable: true
    }

    static mapping = {
        version false
        table       name: 'salve.pessoa_fisica'
        id          column:'sq_pessoa', generator: 'foreign', params: [property: 'pessoa']
        pessoa      insertable: false, updateable: false, column: 'sq_pessoa'
        instituicao column:'sq_instituicao'
    }

    def beforeValidate() {
        this.dtAlteracao = new Date()

        // CPF deve conter 11 digitos
        if( this.nuCpf ){
            this.nuCpf = this.nuCpf.replaceAll(/[^0-9]/,'')
            if( this.nuCpf.length() != 11){
                this.nuCpf=null
            }
        }
    }

    String getNoPessoa(){
        return ( pessoa ) ? pessoa.noPessoa : ''
    }

    String getEmail(){
       return ( pessoa && pessoa?.deEmail) ? pessoa.deEmail : ''
    }

    String getCpfFormatado() {
        if( nuCpf ) {
            return Util.formatCpf( nuCpf )
        }
        return ''
    }

    String getPrimeiroNome(){
        return pessoa ? pessoa.primeiroNome : ''
    }
}
