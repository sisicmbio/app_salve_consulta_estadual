package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType

class FichaOcorrenciaPortalbio {

    Ficha ficha
    String deUuid
    String noCientifico
    String noLocal
    String noInstituicao
    String noAutor
    Date dtOcorrencia
    Date dtCarencia
    String txOcorrencia
    String deAmeaca
    DadosApoio situacao
    DadosApoio situacaoAvaliacao

    Date dtRevisao
    Date dtValidacao
    PessoaFisica pessoaRevisor
    PessoaFisica pessoaValidador
    String inUtilizadoAvaliacao
    String txNaoUtilizadoAvaliacao
    DadosApoio motivoNaoUtilizadoAvaliacao
    String txObservacao

    Integer idOrigem
    Date dtAlteracao
    String noBaseDados

    Integer sqPais
    Integer sqEstado
    Integer sqMunicipio
    Integer sqBioma
    Integer sqBaciaHidrografica
    Integer sqUcFederal
    Integer sqUcEstadual
    Integer sqRppn
    String inDataDesconhecida
    String txNaoAceita
    String inPresencaAtual

    Geometry geometry

    static constraints = {
        //deUuid          unique:true
        noLocal         nullable:true
        noInstituicao   nullable:true
        noAutor         nullable:true
        dtOcorrencia    nullable:true
        dtCarencia      nullable:true
        deAmeaca        nullable:true
        situacao        nullable: true
        inUtilizadoAvaliacao nullable: true
        situacaoAvaliacao column: 'sq_situacao_avaliacao'
        motivoNaoUtilizadoAvaliacao nullable:true
    }

    static mapping = {
        version     false
        sort        id: 'asc'
        table       name:'salve.ficha_ocorrencia_portalbio'
        id          column:'sq_ficha_ocorrencia_portalbio',generator:'identity'
        ficha       column: 'sq_ficha'
        geometry type: GeometryType, sqlType: "GEOMETRY"
        geometry column: 'ge_ocorrencia'
        situacao column:'sq_situacao'
        situacaoAvaliacao column: 'sq_situacao_avaliacao'
        motivoNaoUtilizadoAvaliacao column: 'sq_motivo_nao_utilizado_avaliacao'

        pessoaRevisor column: 'sq_pessoa_revisor'
        pessoaValidador column: 'sq_pessoa_validador'

    }
}
