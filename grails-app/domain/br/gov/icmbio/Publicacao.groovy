package br.gov.icmbio

class Publicacao {

	TipoPublicacao tipoPublicacao
	String deTitulo;
	String noAutor;
	Integer nuAnoPublicacao;
	String noRevistaCientifica;
	String deVolume;
	String deIssue;
	String dePaginas;
	String deRefBibliografica;
	Boolean inListaOficialVigente;
	String deTituloLivro;
	String deEditores;
	String noEditora;
	String noCidade;
	String nuEdicaoLivro;
	String deUrl;
	Date dtAcessoUrl;
	String noUniversidade;
	String deDoi;
	String deIssn;

    static hasMany = [anexos : PublicacaoArquivo ];

	static constraints = {
		noRevistaCientifica nullable:true,blank:true,maxSize:100;
		deVolume nullable:true,blank:true,maxSize:100;
		deIssue nullable:true,blank:true,maxSize:100;
		dePaginas nullable:true,blank:true,maxSize:100;
		deRefBibliografica nullable:true,blank:true,maxSize:100;
		deTituloLivro nullable:true,blank:true;
		deEditores nullable:true,blank:true;
		noEditora nullable:true,blank:true,maxSize:100;
		noCidade nullable:true,blank:true,maxSize:100;
		nuEdicaoLivro nullable:true,blank:true,maxSize:100;
		deUrl nullable:true,blank:true,maxSize:255;
		dtAcessoUrl nullable:true
		noUniversidade nullable:true,blank:true;
		deDoi nullable:true,blank:true,maxSize:255;
		deIssn  nullable:true,blank:true,maxSize:255;
	}

	static mapping = {
		version 				false;
		sort 					noAutor: 'asc'
	   	table       			name 	: 'taxonomia.publicacao';
	   	id          			column 	: 'sq_publicacao',generator:'identity';
	   	tipoPublicacao 			column 	: 'sq_tipo_publicacao';
	}

	public String getTituloAutorAno()
    {
       return this.deTitulo+' / '+this.noAutor+' / '+this.nuAnoPublicacao;
    }

    public String getAutores()
    {
        return getNomes( this.noAutor)
    }

    public String getEditores(String editores )
    {
        return getNomes( editores)
    }

    public String getNomes(String nomes)
    {
        if( ! nomes )
        {
            return '';
        }

        // trocar todos os '; & \n' para ';'
        List listNomes = nomes.replaceAll(/\s+;\s+|\s+&\s+|\n/,'; ').split(';');
        // ler somente o sobrenome dos nomes;
        listNomes.eachWithIndex{ nome,i->
            if( nome.indexOf(',') )
            {
                listNomes[i]=nome.split(',')[0];
            }
        }

        switch( listNomes.size())
        {
            case 1:
                return listNomes[0].trim();
                break;
            case 2:
                return listNomes[0].trim()+' & '+listNomes[1].trim();
                break;
            default:
                return listNomes[0].trim()+' et al.';
        }
    	return nomes;
    }

    public String getCitacao()
    {
        String autores = this.autores;
        autores += (this.nuAnoPublicacao ? ' '+this.nuAnoPublicacao : '');
        return autores;
    }

    public String getReferenciaHtml()
    {
        List autores = []
        if( this.noAutor ) {
            autores = this.noAutor.toString().replaceAll(/\s+;\s+|\s+&\s+|\n/, '; ')?.split(';');
        }
        String autorAno;
        if( autores.size() > 1 )
        {
            autorAno = autores.init().join('; ')+' & '+autores.last();
        }
        else
        {
            autorAno = autores.join('; ');
        }
        autorAno += (this.nuAnoPublicacao ? ' '+this.nuAnoPublicacao+'.':'');

 		String negritoOpen	= '<b>';
 		String negritoClose	= '</b>';
        String url = '<a target="_blank" href="'+this.deUrl+'">'+this.deUrl+'</a>.';
        if( this?.tipoPublicacao )
        {
	       switch(this?.tipoPublicacao?.coTipoPublicacao)
    	   {
    		case 'RESUMO_CONGRESSO':
                return autorAno+
                (this.deTitulo ? ' '+this.deTitulo+'.':'')+
                (this.noRevistaCientifica ? ' <b>'+this.noRevistaCientifica+'</b>,':'')+
                (this.deVolume ? ' '+this.deVolume:'')+
                (this.deIssue ? ' ('+this.deIssue+')' : '')+(this.deVolume ? ':':'')+
                (this.dePaginas ? ' p.'+this.dePaginas + '.' : '' )+
                (this.deEditores ? ' <i>In</i>:'+this.getEditores(this.deEditores) +'.':'')+
                (this.deTituloLivro ? '<i>In</i>: <b>'+this.deTituloLivro+'</b>.':'')+
                (this.noUniversidade ? this.noUniversidade+'.':'')+
                (this.noCidade ? ' '+this.noCidade+'.':'')+
                (this.deUrl ? ' Disponível em: '+url:'')+
                (this.dtAcessoUrl ? ' Acessado em: '+this.dtAcessoUrl.format('dd/MM/YYYY')+'.':'')+' (Resumo)';
            break;

            case 'CAPITULO_LIVRO':
            case 'ARTIGO_CIENTIFICO':
    		case 'SITE':
    		case 'ATO_OFICIAL':
    			return autorAno+
    			(this.deTitulo ? ' '+this.deTitulo+'.':'')+
    			(this.noRevistaCientifica ? ' <b>'+this.noRevistaCientifica+'</b>,':'')+
    			(this.deVolume ? ' '+this.deVolume:'')+
    			(this.deIssue ? ' ('+this.deIssue+')' : '')+(this.deVolume ? ':':'')+
    			(this.dePaginas ? ' p.'+this.dePaginas + '.' : '' )+
    			(this.deEditores ? ' <i>In</i>:'+this.getEditores(this.deEditores) +'.':'')+
    			(this.deTituloLivro ? ' <b>'+this.deTituloLivro+'</b>.':'')+
    			(this.noEditora ? ' ' + this.noEditora : '' ) +
                (this.noUniversidade ? this.noUniversidade+'.':'')+
    			(this.noCidade ? ' '+this.noCidade+'.':'')+
    			(this.deUrl ? ' Disponível em: '+url:'')+
    			(this.dtAcessoUrl ? ' Acessado em: '+this.dtAcessoUrl.format('dd/MM/YYYY')+'.':'');
    		break;

    		//------------------------------------------------------------
    		case 'LIVRO':
                return autorAno +
                (this.deTitulo ? ' <b>'+this.deTitulo+'</b>.':'')+
                (this.noRevistaCientifica ? ' <b>'+this.noRevistaCientifica+'</b>,':'')+
                (this.deVolume ? ' '+this.deVolume:'')+
                (this.deIssue ? ' ('+this.deIssue+')':'')+(this.deVolume ? ':':'')+
                (this.dePaginas ? ' p.'+this.dePaginas+'.':'')+
                (this.deEditores ? ' <i>In</i>: '+this.getEditores(this.deEditores)+' (eds.).':'')+
                (this.deTituloLivro ? ' '+this.deTituloLivro : '')+
                (this.noEditora ? ' ' + this.noEditora : '' ) +
                (this.noCidade ? ' '+this.noCidade+'.':'')+
                (this.deUrl ? ' Disponível em: '+url :'')+
                (this.dtAcessoUrl ? ' Acessado em: '+this.dtAcessoUrl.format('dd/MM/YYYY')+'.':'');
            break;
    		case 'MIDIA_DIGITAL':
    		case 'PLANO_MANEJO':
    		case 'RELATORIO_TECNICO':
    			if( this.tipoPublicacao.coTipoPublicacao != 'LIVRO' && this.tipoPublicacao.coTipoPublicacao !='PLANO_MANEJO')
    			{
    				negritoOpen='';
    				negritoClose='';
    			}
    			//return autores +
    			//(this.nuAnoPublicacao ? this.nuAnoPublicacao.toString()+'.':'')+
                return autorAno+
    			(this.deTitulo ? negritoOpen+' '+this.deTitulo+negritoClose+'.':'')+
    			(this.nuEdicaoLivro ? ' '+this.nuEdicaoLivro:'')+
    			(this.deEditores ? ' '+this.getEditores(this.deEditores)+'.':'')+
    			(this.noCidade ? ' '+this.noCidade+'.':'')+
    			(this.deVolume ? ' '+this.deVolume+'.':'')+
    			(this.deIssue ? ' ('+this.deIssue+')':'')+(this.deVolume ? ':':'')+
    			(this.dePaginas ? ' p.'+this.dePaginas+'.':'')+
    			(this.deUrl ? ' Disponível em:'+url:'')+
    			(this.dtAcessoUrl ? 'Acessado em: '+this.dtAcessoUrl.format('dd/MM/YYYY')+'.':'');
    		break;
    		//------------------------------------------------------------
    		case 'DISSERTACAO_MESTRADO':
    			//return autores +
    			//(this.nuAnoPublicacao ? this.nuAnoPublicacao.toString()+'.':'')+
                return autorAno+
    			(this.deTitulo ? ' '+negritoOpen+this.deTitulo+negritoClose+'. Dissertação de Mestrado.':'')+
    			(this.noUniversidade ? ' '+this.noUniversidade+'.':'')+
    			(this.noCidade ? ' '+this.noCidade+'.':'')+
    			(this.dePaginas ? ' p.'+this.dePaginas+'.':'');

    		break;
    		//------------------------------------------------------------
    		case 'TESE_DOUTORADO':
    			//return autores +
    			//(this.nuAnoPublicacao ? this.nuAnoPublicacao.toString()+'.':'')+
    			return autorAno+
                (this.deTitulo ? ' '+negritoOpen+this.deTitulo+negritoClose+'. Tese de Doutorado.':'')+
    			(this.noUniversidade ? ' '+this.noUniversidade+'.':'')+
    			(this.noCidade ? ' '+this.noCidade+'.':'')+
    			(this.dePaginas ? ' p.'+this.dePaginas+'.':'');
    		break;
    		//------------------------------------------------------------
    		default:
    			return autorAno+(this.deTitulo ? negritoOpen+' '+this.deTitulo+negritoClose:'');
	   	   }
        }
        else
        {
            return ''
        }
    }
}
