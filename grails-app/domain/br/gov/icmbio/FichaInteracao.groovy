package br.gov.icmbio

class FichaInteracao {

    Ficha       ficha
    DadosApoio  tipoInteracao
    DadosApoio  classificacao
    Taxon       taxon
    DadosApoio  categoriaIucn
	String 		dsFichaInteracao

    static constraints = {}

    static mapping ={
    	version 			false
	   	table       		name 	: 'salve.ficha_interacao'
	   	id          		column 	: 'sq_ficha_interacao'
	   	ficha 				column  : 'sq_ficha'
	   	tipoInteracao 		column 	: 'sq_tipo_interacao'
	   	classificacao 		column	: 'sq_classificacao'
	   	taxon 				column  : 'sq_taxon'
	   	categoriaIucn 		column  : 'sq_categoria_iucn'
    }

    public String getRefBibHtml()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_interacao','sq_ficha_interacao',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao+'&nbsp;<span class="label tag pointer" title="'+it.publicacao.deTitulo.replaceAll(/"/,"&quot;")+'">...</span>' );
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)&nbsp;<span class="label tag pointer" title="Comunicação Pessoal">...</span>' );
            }
        }
        return nomes.join('<br>');
    }

    public String getRefBibText()
    {
        List nomes = [];
        List refs = FichaRefBib.findAllByFichaAndNoTabelaAndNoColunaAndSqRegistro(this.ficha,'ficha_interacao','sq_ficha_interacao',this.id);
        refs.each{
            if( it.publicacao )
            {
                nomes.push( it.publicacao.citacao)
            }
            else
            {
                nomes.push( it.noAutor + ', ' + it.nuAno + ' (Com.Pess.)')
            }
        }
        return nomes.join('\n')
    }
}
