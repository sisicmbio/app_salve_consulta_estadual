package br.gov.icmbio

class FichaConsultaAnexo {

    Usuario webUsuario
    DadosApoio situacao
    String noArquivo
    String deLocalArquivo
    String deTipoConteudo
    Date dtInclusao

    static belongsTo = [consultaFicha:CicloConsultaFicha]
    static constraints = {
    }

    static mapping = {
        version 				false
        table       			name 	: 'salve.ficha_consulta_anexo'
        id          			column 	: 'sq_ficha_consulta_anexo',generator:'identity'
        webUsuario              column  : 'sq_web_usuario'
        consultaFicha           column  : 'sq_ciclo_consulta_ficha'
        situacao                column  : 'sq_situacao'
    }

    def beforeValidate() {
        if( ! this.dtInclusao )
        {
            this.dtInclusao = new Date()
        }
    }
}
