package br.gov.icmbio

class Rppn {

    Esfera esfera
    String coCnuc
    String sgRppn

    static constraints = {
    }

    static mapping = {
        version 	false
        table       name:'corporativo.vw_rppn'
        id          column:'sq_pessoa'
        esfera      column: 'sq_esfera'
    }
}
