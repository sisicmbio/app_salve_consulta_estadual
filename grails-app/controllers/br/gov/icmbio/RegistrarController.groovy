package br.gov.icmbio

import grails.converters.JSON;


class RegistrarController extends BaseController {

    def registrarService

    private Map res = [:]

    def index() {
        render(view:'index');
    }

    def save()
    {
        render registrarService.save( params ) as JSON
    }
}
