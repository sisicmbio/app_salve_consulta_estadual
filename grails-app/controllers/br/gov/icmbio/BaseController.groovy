package br.gov.icmbio
import grails.converters.JSON
import org.apache.catalina.core.ApplicationHttpRequest
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import org.springframework.web.context.request.ServletRequestAttributes
import org.springframework.web.context.request.RequestContextHolder
// import org.codehaus.groovy.grails.web.json.JSONObject

class BaseController {

    // variavel de retorno de dados para chamadas ajax
    public Map res = [:]

    /**
    *   Definir método que irá iterceptar todas as ações submetidas
    *   def beforeInterceptor = [action:this.&auth,except:['/controller/action','/controller/action'] ]
    */
    def beforeInterceptor = [action:this.&auth, except: '']

    private Boolean auth()
    {
        // limpar variável res a cada requisição
        res = [status:0,msg:'', errors:[], data:[:]]

        this.trimParams()
        return true
    }

    /*
    Método para remover os espaços do início e fim dos parâmetros string recebidos
    */
    private Map trimParams() {
        try {
            if (params && params.size() > 2) {
                if (params?.action == 'ping') {
                    return
                }
                log.info('\n')
                if (session?.user && session?.user?.noUsuario) {
                    log.info('Usuario: ' + session?.user?.noUsuario + ' (' + session?.user?.username + ')')
                }
                log.info('Parametros ' + new Date().format('dd/MM/YYYY HH:mm:ss'))
                log.info('----------------------------------------')
                params?.each {
                    try {
                        if (it?.getClass() == String) {
                            it = it?.toString()?.trim()
                            it = it.replaceAll(/<p>&nbsp;<\/p>$/, '')
                            log.info(it)
                        } else if (it?.value?.getClass() == String) {
                            it.value = it.value.toString().trim()
                            it.value = it.value.replaceAll(/<p>&nbsp;<\/p>$/, '')
                            if (it.key =~ /(?i)^(senha|password)[s2]?$/) {
                                log.info(it.key + ' = **********')
                            } else {
                                log.info(it.key + ' = ' + it.value)
                            }
                        }
                    } catch (Exception e) {
                    }
                }
                log.info('\n')
            }
        } catch (Exception e) {
        }
    }

    def index() {
        render 'Bem-vindo'
    }

    def renderRes()
    {
        response.setHeader('Content-Type','application/json')
        // criar parâmetros padrão de retorno ajax
        if( !res.errors )
        {
            res.errors = []
        }
        if( !res.msg )
        {
            res.msg = ''
        }
        if( !res.status )
        {
            res.status = 0
        }
        render res as JSON
    }

    /**
    * Retorna o IP da requisição
    */
    private String getIp(ApplicationHttpRequest request = null ) {
        String ipAddress = ''
        if ( request ) {
            ipAddress = request.getHeader("Client-IP")
            if (!ipAddress) {
                ipAddress = request.getHeader("X-Forwarded-For")
            } else if (!ipAddress) {
                ipAddress = request.remoteAddr
            } else if (!ipAddress) {
                ipAddress = request.getRemoteAddr();
            }
        }
        if( ! ipAddress ) {
            ipAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
        }
        return (ipAddress == '0:0:0:0:0:0:0:1') ? 'localhost' : ipAddress
    }
}
