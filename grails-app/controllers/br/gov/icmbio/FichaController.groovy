package br.gov.icmbio

import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.Geometry
import com.vividsolutions.jts.geom.GeometryFactory
import grails.converters.JSON
import groovy.sql.Sql
import org.springframework.web.multipart.commons.CommonsMultipartFile

class FichaController extends AjaxController {
    def dadosApoioService
    def dataSource
    def fichaMultimidiaService
    def geoService
    def colaborarService
    def fichaConsultaFotoService

    def index() {
        render 'Ficha Controller Index Ok'
    }

    /**
     * ler a lista de grupos avaliados para filtro das fichas
     */
    def gruposAvaliados() {
        Map resGrupo = [:]
        resGrupo.data = [:]
        resGrupo.data = [ itens: [ [sqGrupo:0, dsGrupo : '-- todos --' ] ] ]
        DadosApoio tbGrupo = DadosApoio.findByCodigo('TB_GRUPO')
        DadosApoio.createCriteria().list {
            eq('pai',tbGrupo);
            order('ordem');
            order('descricao')
        }.each {
            if( ! resGrupo?.data?.itens ) {
                resGrupo.data.itens = [];
            }
           resGrupo.data.itens.push( [sqGrupo:it.id, dsGrupo:it.descricao] );
        }
        render resGrupo as JSON
        //super.renderRes()
    }

    /**
     * ler a lista de subgrupos de um grupo avaliado para filtro das fichas
     */
    def subgruposAvaliados() {
        Map resSubgrupo = [:]
        resSubgrupo.data=[:]
        resSubgrupo.data = ['itens': [ [sqSubgrupo:0,dsSubgrupo:'-- todos --'] ] ]
        if( params.sqGrupo ) {
            DadosApoio grupo  = DadosApoio.get( params.sqGrupo.toLong() )
            if( grupo ) {
                DadosApoio.createCriteria().list {
                    eq('pai', grupo);
                    order('ordem');
                    order('descricao')
                }.each {
                    if( ! resSubgrupo?.data?.itens ) {
                        resSubgrupo.data.itens = []
                    }
                    resSubgrupo.data.itens.push([sqSubgrupo: it.id, dsSubgrupo: it.descricao]);
                }
            }
        }
        render resSubgrupo as JSON
        //super.renderRes()
    }

    /**
     * ler as fichas atribuidas ao especilista utilizando o email como chave
     * @return
     */
    def consultaDireta() {
        Map resDireta = [:]
        resDireta.data = [:]
        resDireta.data = [itens:[]]

        if ( session?.sicae?.user ){

            def sql = new Sql( dataSource )
            Pessoa pessoa= Pessoa.get(session.sicae.user.sqPessoa)
            if( pessoa )
            {
                try
                {
                    String hoje = new Date().format('yyyy-MM-dd')
                    String qry = """select distinct b.sq_ciclo_consulta_ficha, e.sq_ciclo_consulta
                , e.dt_fim
                , c.nm_cientifico
                , f.cd_sistema
                , c.sq_grupo
                , c.sq_subgrupo
                , j.json_trilha||'' as trilha
                , array_to_string( array_agg( i.no_comum ),', ' ) as no_comum
                from salve.ciclo_consulta_pessoa_ficha a
                INNER JOIN salve.ciclo_consulta_ficha b on b.sq_ciclo_consulta_ficha = a.sq_ciclo_consulta_ficha
                INNER JOIN salve.ficha c on c.sq_ficha = b.sq_ficha
                INNER JOIN salve.dados_apoio d on d.sq_dados_apoio = c.sq_situacao_ficha
                INNER JOIN salve.ciclo_consulta e on e.sq_ciclo_consulta = b.sq_ciclo_consulta
                INNER JOIN salve.dados_apoio f on f.sq_dados_apoio = e.sq_tipo_consulta
                INNER JOIN salve.ciclo_consulta_pessoa g on g.sq_ciclo_consulta_pessoa = a.sq_ciclo_consulta_pessoa
                INNER JOIN salve.dados_apoio h on h.sq_dados_apoio = g.sq_situacao
                LEFT JOIN salve.ficha_nome_comum i on i.sq_ficha = c.sq_ficha
                INNER JOIN taxonomia.taxon j on j.sq_taxon = c.sq_taxon
                --left  outer join salve.dados_apoio k on k.sq_dados_apoio = c.sq_grupo 
                --left  outer join salve.dados_apoio l on l.sq_dados_apoio = c.sq_subgrupo 
                where f.cd_sistema in ( 'CONSULTA_DIRETA','REVISAO_POS_OFICINA') 
                and (f.cd_sistema = 'REVISAO_POS_OFICINA' or d.cd_sistema in ('CONSULTA','AVALIADA_EM_REVISAO','VALIDADA_EM_REVISAO','EM_VALIDACAO') )
                and e.dt_inicio <= '$hoje' and e.dt_fim >= '$hoje'
                and g.sq_pessoa = $pessoa.id
                group by b.sq_ciclo_consulta_ficha, e.sq_ciclo_consulta
                , e.dt_fim
                , c.nm_cientifico
                , f.cd_sistema
                , c.sq_grupo
                , c.sq_subgrupo
                , j.json_trilha||''
                order by c.nm_cientifico
                """

                    List itens = []
                    sql.eachRow(qry) { it ->
                        itens.push([sqConsultaFicha   : it?.sq_ciclo_consulta_ficha   ? it.sq_ciclo_consulta_ficha.toString() : ''
                                       , sqCicloConsulta : it?.sq_ciclo_consulta         ?:''
                                       , noCientifico    : it?.nm_cientifico             ? Util.ncItalico( it.nm_cientifico ) : ''
                                       , arvoreTaxonomica: it?.trilha                    ? it.trilha.toString() : ''
                                       , noComum         : it?.no_comum                  ?: ''
                                       , dtFim           : it?.dt_fim                    ? it.dt_fim.format('dd/MM/yyyy') : ''
                                       , codigoSistema   : it?.cd_sistema                ?: ''
                                       , sqGrupo          : it.sq_grupo  ?: ''
                                       , sqSubgrupo       : it.sq_subgrupo ?: ''
                        ])
                    }
                    resDireta.data.itens=itens
                } catch( Exception e ) {
                    println ' '
                    println 'Erro Salve-consulta - fichaController/consultaDireta() ' + new Date().format('dd/MM/yyyy hh:mm:ss')
                    println e.getMessage()
                }
            }

        }
        render resDireta as JSON
    }

    /**
     * ler as fichas em consulta ampla
     * @return
     */
    def consultaAmpla() {
        sleep(1000)
        def sql = new Sql( dataSource )
        this.res.data.data=[]
        /*
        List fichas = []
        DadosApoio consultaAmpla = dadosApoioService.getByCodigo('TB_TIPO_CONSULTA_FICHA','CONSULTA_AMPLA')
        CicloConsultaFicha.createCriteria().list {
            createAlias('cicloConsulta','c')
            eq('c.tipoConsulta',consultaAmpla)
            le("c.dtInicio", new Date() )
            ge("c.dtFim", new Date())
        }.each {
            fichas.push([sqConsultaFicha: it.id
                , noCientifico: it.vwFicha.noCientificoCompletoItalico
                , arvoreTaxonomica : it.vwFicha.taxon.structure
                , noComum : it.vwFicha.nomesComuns
                , dtFim:it?.cicloConsulta?.dtFim?.format('dd/MM/yyyy')
                , codigoSistema: it.cicloConsulta.tipoConsulta.codigoSistema
            ])
        }
        */
        /*
        res.data = CicloConsulta.executeQuery("""
        select distinct new map(a.id as sqConsultaFicha
        , c.nmCientifico as noCientifico
        , d.nomesComuns as noComum
        , b.dtFim as dtFim
        , e.codigoSistema as codigoSistema
        , c.trilha||'' as arvoreTaxonomica
        )
        from CicloConsultaFicha a
        inner join a.ficha c
        left join c.nomesComuns d
        inner join a.cicloConsulta b
        inner join b.tipoConsulta e
        where b.dtInicio <= :hoje and b.dtFim >= :hoje
        and e.codigoSistema=  'CONSULTA_AMPLA'
        group by a.id, c.nmCientifico, b.dtFim, e.codigoSistema, c.trilha||''
        order by c.nmCientifico
        """,[ hoje: new Date()] ).collect {
            it.dtFim = it.dtFim.format('dd/MM/YYYY')
            it.noCientifico = Util.ncItalico( it.noCientifico )
            return it
        }
        */

         try {
            String hoje = new Date().format('yyyy-MM-dd')
            String qry = """select b.sq_ciclo_consulta_ficha, a.sq_ciclo_consulta, a.sg_consulta, a.dt_fim, c.nm_cientifico
            --, h.sq_dados_apoio as sq_grupo, i.sq_dados_apoio as sq_subgrupo
            , c.sq_grupo, c.sq_subgrupo
            , array_to_string( array_agg( d.no_comum ),', ' ) as no_comum
            , g.json_trilha||'' as trilha, e.cd_sistema
            from salve.ciclo_consulta a
            inner join salve.ciclo_consulta_ficha b on b.sq_ciclo_consulta = a.sq_ciclo_consulta
            inner join salve.ficha c on c.sq_ficha = b.sq_ficha
            left  join salve.ficha_nome_comum d on d.sq_ficha = b.sq_ficha
            inner join salve.dados_apoio e on e.sq_dados_apoio = a.sq_tipo_consulta
            inner join salve.dados_apoio f on f.sq_dados_apoio = c.sq_situacao_ficha
            inner join taxonomia.taxon g on g.sq_taxon = c.sq_taxon
            -- left  outer join salve.dados_apoio h on h.sq_dados_apoio = c.sq_grupo 
            -- left  outer join salve.dados_apoio i on i.sq_dados_apoio = c.sq_subgrupo 
            where a.dt_inicio <= '$hoje' and a.dt_fim >= '$hoje'
            and e.cd_sistema = 'CONSULTA_AMPLA'
            AND f.cd_sistema = 'CONSULTA'
            GROUP BY a.sq_ciclo_consulta, a.sg_consulta, a.dt_fim, b.sq_ciclo_consulta_ficha , c.nm_cientifico, e.cd_sistema, c.sq_grupo, c.sq_subgrupo, g.json_trilha||''
            -- GROUP BY a.dt_fim, b.sq_ciclo_consulta_ficha , c.nm_cientifico, e.cd_sistema, i.sq_dados_apoio, h.sq_dados_apoio, g.json_trilha||''
            order by c.nm_cientifico"""

//println qry
            // armazenar os nomes/siglas das consulta
            this.res.consultas=[];
            sql.eachRow( qry ) { it ->
                this.res.data.data.push( [ 'sqConsultaFicha'  : it?.sq_ciclo_consulta_ficha ? it.sq_ciclo_consulta_ficha.toString() : ''
                            , 'noCientifico'    : it?.nm_cientifico  ? Util.ncItalico(it.nm_cientifico) : ''
                            , 'arvoreTaxonomica': it?.trilha        ? it.trilha.toString() : ''
                            , 'noComum'         : it?.no_comum      ?: ''
                            , 'dtFim'           : it?.dt_fim        ? it.dt_fim.format('dd/MM/yyyy') : ''
                            , 'codigoSistema'   : it?.cd_sistema    ?:''
                            , 'sqCicloConsulta' : it?.sq_ciclo_consulta ?:''
                            , 'sqGrupo'         : it?.sq_grupo      ?:''
                            , 'sqSubgrupo'      : it?.sq_subgrupo   ?:''  ]
                )
                if( it.sg_consulta ) {
                    Map consulta = [sqCicloConsulta: it.sq_ciclo_consulta, sgConsulta: it.sg_consulta, cdSistema:'CONSULTA_AMPLA','dsSistema':'Consulta ampla']
                    if (!this.res.consultas.contains(consulta)) {
                        this.res.consultas.push(consulta)
                    };
                }
            }

            if ( session?.sicae?.user ) {
                // ler as revisões abertas para adicionar na seção filtros das consultas/revisoes
                qry = """select distinct a.sq_ciclo_consulta, a.sg_consulta
            from salve.ciclo_consulta a
            inner join salve.ciclo_consulta_ficha b on b.sq_ciclo_consulta = a.sq_ciclo_consulta
            inner join salve.dados_apoio e on e.sq_dados_apoio = a.sq_tipo_consulta
            where a.dt_inicio <= '$hoje' and a.dt_fim >= '$hoje'
            and e.cd_sistema = 'REVISAO_POS_OFICINA'"""

                sql.eachRow(qry) { it ->
                    if (it.sg_consulta) {
                        Map consulta = [sqCicloConsulta: it.sq_ciclo_consulta, sgConsulta: it.sg_consulta, cdSistema: 'REVISAO_POS_OFICINA', 'dsSistema': 'Revisão']
                        if (!this.res.consultas.contains(consulta)) {
                            this.res.consultas.push(consulta)
                        };
                    }
                }
            }


        } catch( Exception e){
            println ' '
            println 'Erro Salve-consulta - fichaController/consultaAmpla() ' + new Date().format('dd/MM/yyyy hh:mm:ss')
            println e.getMessage()
        }
        this.renderRes()
    }

    /**
     * Selecionou uma ficha para fazer a colaboração e exibe a página inicial
     * @return
     */
    def colaborar() {

       if ( ! session?.sicae?.user || !params.sqConsultaFicha )
        {

            session.message = 'Sessão Expirada. Efetue login novamente!'
            redirect('uri':'/')
        }
        else
        {
            CicloConsultaFicha consultaFicha = CicloConsultaFicha.get( params.sqConsultaFicha )

            if( ! consultaFicha )
            {
                redirect('uri':'/')
                return;
            }
            if( !consultaFicha?.vwFicha )
            {
                redirect('uri':'/')
                return;
            }
            Map colaboracoes = [:]
            String hoje = new Date().format('yyyy-MM-dd')
            //Map colaboracoesOcorrencias = []
            // ler as colaborações da pessoa na ficha independente do tipo de consulta desde que não esteam finalizadas
            List lista = FichaColaboracao.executeQuery("""
            select new map( a.noCampoFicha as noCampoFicha, a.dsColaboracao as dsColaboracao, a.dsRefBib as dsRefBib)
            from FichaColaboracao a
            where a.usuario.id = :sqUsuario
            and a.consultaFicha.ficha.id = :sqFicha
            and a.consultaFicha.cicloConsulta.dtFim >= '${hoje}'
            """,[sqUsuario:session.sicae.user.sqPessoa,sqFicha:consultaFicha.vwFicha.id.toLong() ]).each
            {
                  colaboracoes[it.noCampoFicha] = [colaboracao:it.dsColaboracao,refBib:it.dsRefBib]
            }

            // ler dados apoio para preencher os campos selects
            List listCarencias          = dadosApoioService.getTable('TB_PRAZO_CARENCIA')
            List listDatuns             = dadosApoioService.getTable('TB_DATUM')
            List listFormatosOriginais  = dadosApoioService.getTable('TB_FORMATO_COORDENADA_ORIGINAL')
            List listPrecisoesCoord     = dadosApoioService.getTable('TB_PRECISAO_COORDENADA')
            List listRefsAproximacao    = dadosApoioService.getTable('TB_REFERENCIA_APROXIMACAO')

            // ler dados da planilha anexada com ocorrencias
            FichaConsultaAnexo fichaConsultaAnexo = FichaConsultaAnexo.createCriteria().get {
                eq('webUsuario.id', session.sicae.user.sqPessoa.toLong())
                eq('consultaFicha', consultaFicha)
                maxResults(1)
            }

            // ultima avaliação nacional




            //List listMetodosAproximacao = dadosApoioService.getTable('TB_METODO_APROXIMACAO')
            render(view: 'colaborarFicha'
                , model: [consultaFicha:consultaFicha
                ,colaboracoes:colaboracoes
                ,listDatuns:listDatuns
                ,listCarencias:listCarencias
                ,listFormatosOriginais:listFormatosOriginais
                ,listPrecisoesCoord :listPrecisoesCoord
                ,listRefsAproximacao:listRefsAproximacao
                ,fichaConsultaAnexo : fichaConsultaAnexo
                ,tipoConsultaSistema:params.codigoSistema

               //,tipoConsulta:tipoConsulta
            ])
        }
    } // codigo_sistema

    /**
     * Salvar as colaborções
     * @return
     */
    def salvarColaboracao(){
        boolean excluir=false
        res.type='error'
        res.status=1
        if ( ! session?.sicae?.user )
        {
            res.msg = 'Sessão Expirada. Efetue login novamente!'
        }
        else if( !params.sqConsultaFicha )
        {
            res.msg='Ficha inválida!'
        }
        else if( !params.fieldName )
        {
            res.msg='Campo para colaboração não detectado!'
        }
        else
        {

/** /
 println params
 println "-"*100
 println ' '
 res.msg='Ver parametros terminal ' +session.sicae.user.noPessoa
 return renderRes()
 /**/

            CicloConsultaFicha consultaFicha = CicloConsultaFicha.get(params.sqConsultaFicha.toInteger())
            if (!consultaFicha)
            {
                res.msg = 'Ficha não está mais em consulta!'
                return renderRes()
            }
            Ficha ficha = consultaFicha.ficha

            //println 'Situacao:' + ficha.situacao.codigoSistema
            //println '/'*100
            if( consultaFicha.cicloConsulta.tipoConsulta.codigoSistema != 'REVISAO_POS_OFICINA') {
                if (!(ficha.situacao.codigoSistema =~ /^(CONSULTA|AVALIADA_EM_REVISAO|VALIDADA_EM_REVISAO|EM_VALIDACAO)$/)) {
                    res.msg = 'Ficha não está em Consulta Ampla/Direta ou Revisão'
                    return renderRes()
                }
            } else {
                // a ficha pode ser colocada para revisão nestas situações, antão tem que
                // verificar se REVISAO_POS_OFICINA está aberta
                // if ((ficha.situacao.codigoSistema =~ /^(VALIDADA_EM_REVISAO|EM_VALIDACAO)$/)) {
                    int qtd = CicloConsultaFicha.createCriteria().count {
                        eq('ficha', ficha)
                        cicloConsulta {
                            ge('dtFim', Util.hoje());
                        }
                    }
                    if (qtd == 0) {
                        res.msg = 'A revisão da ficha foi encerrada.'
                        return renderRes()
                    }
                //}
            }


            DadosApoio situacaoNaoAvaliada = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'NAO_AVALIADA')
            if (!situacaoNaoAvaliada)
            {
                res.msg = 'Situação NAO_AVALIADA não cadastradas na tabela de Apoio TB_SITUACAO_COLABORACAO'
                renderRes()
                return
            }

            // verificar se informou Ref. Bibliográfica ou Comunicação pessoal
            if (br.gov.icmbio.Util.stripTags(params.refBibValue) == '' &&
                    br.gov.icmbio.Util.stripTags(params.fieldValue) != '')
            {
                res.msg = 'Necessário informar uma referência bibligráfica ou comunicação pessoal.'
                renderRes()
                return
            }

            // verificar se informou o texto da colaboração
            if (br.gov.icmbio.Util.stripTags(params.refBibValue) != '' &&
                    br.gov.icmbio.Util.stripTags(params.fieldValue) == '')
            {
                res.msg='Necessário informar a colaboração.'
                renderRes()
                return
            }
            if( br.gov.icmbio.Util.stripTags(params.fieldValue) == '' )
            {
                excluir=true
            }
            FichaColaboracao fichaColaboracao
            String hoje = new Date().format('yyyy-MM-dd')
            List lista = FichaColaboracao.executeQuery("""
            select a.id from FichaColaboracao a
             where a.usuario.id = :sqUsuario
               and a.consultaFicha.ficha.id = :sqFicha
               and a.consultaFicha.cicloConsulta.dtFim >= '${hoje}'
               and a.noCampoFicha = :noCampoFicha
            """, [sqUsuario: session.sicae.user.sqPessoa, sqFicha: consultaFicha.vwFicha.id.toLong(), noCampoFicha: params.fieldName])

            if (lista)
            {
                fichaColaboracao = FichaColaboracao.get(lista[0].toLong())
                // se o topo da consulta for direta ou revisao, sobrepoe a que estiver
                if (consultaFicha.cicloConsulta.tipoConsulta.codigoSistema != 'CONSULTA_AMPLA')
                {
                    fichaColaboracao.consultaFicha = consultaFicha
                }
            } else
            {
                // verificar se a colaboração já existe
                //fichaColaboracao = FichaColaboracao.findByConsultaFichaAndUsuarioAndNoCampoFicha(consultaFicha,usuario,params.fieldName)
                // gravar
                fichaColaboracao = new FichaColaboracao()
                fichaColaboracao.consultaFicha = consultaFicha
                fichaColaboracao.usuario = Usuario.get(session.sicae.user.sqPessoa)
                fichaColaboracao.noCampoFicha = params.fieldName

            }
            fichaColaboracao.dsColaboracao = params.fieldValue
            fichaColaboracao.dsRefBib = params.refBibValue
            fichaColaboracao.dsRotulo = params?.fieldLabel?.trim()


            // gravar o tipo da consulta que a colaboração foi feita. ( Direta, Ampla ou Revisao )
            // não é mais necessário, a tabela ja esta ligada na cicloConsultaFicha->cicloConsulta
            // que tem o tipo
            /*if (params.sqTipoConsulta){
                fichaColaboracao.tipoConsulta = DadosApoio.get(params.sqTipoConsulta.toInteger())
            }*/

            if (!excluir)
            {

                // sempre ao salvar alterar a situção para não avaliada
                fichaColaboracao.situacao = situacaoNaoAvaliada
                fichaColaboracao.validate()
                if (fichaColaboracao.hasErrors())
                {
                    println fichaColaboracao.getErrors()
                    res.msg = 'Ocorreu um erro de integridade.'
                } else
                {
                    if (!fichaColaboracao.save(flush: true))
                    {

                        println 'Parametros recebidos - SALVE-CONSULTA ' + new Date()
                        println params
                        println ' '
                        println 'Usuario: ' + session?.sicae?.user?.noPessoa
                        println 'Erro encontrado '
                        println fichaColaboracao.getErrors()
                        println "-" * 100
                        println ' '
                        res.msg = 'Ocorreu um problema ao salvar sua informação. Tente novamente em alguns segundos.'
                    } else
                    {
                        res.status = 0
                        res.msg = (params.fieldValue ? 'Colaboração' : 'Referência Bibliográfica') + ' gravada com SUCESSO!'
                        res.type = 'success'
                    }
                }
            } else
            {
                fichaColaboracao.delete()
                res.status = 0
                res.msg = 'Colaboração EXCLUÍDA com SUCESSO!'
                res.type = 'success'
            }
        }
        renderRes()
    }

    def salvarOcorrencia() {
        Usuario usuario
        res.status = 1
        res.msg = ''
        res.type = 'error'
        res.errors = []
        res.data = ['fotos':[]]

        if ( ! session?.sicae?.user )
        {
            res.msg = 'Sessão Expirada. Efetue login novamente!'
        }
        else if( !params.sqConsultaFicha )
        {
            res.msg='Ficha inválida!'
        }
        else
        {
            usuario = Usuario.get( session.sicae.user.sqPessoa )
            CicloConsultaFicha consultaFicha = CicloConsultaFicha.get(params.sqConsultaFicha)
            Ficha ficha = consultaFicha.ficha
            if( consultaFicha.cicloConsulta.tipoConsulta.codigoSistema != 'REVISAO_POS_OFICINA') {
                if (ficha.situacao.codigoSistema != 'CONSULTA' && ficha.situacao.codigoSistema != 'AVALIADA_EM_REVISAO') {
                    res.msg = 'Ficha não está em Consulta Ampla/Direta ou Avaliada em Revisão'
                    return renderRes()
                }
            }
            if( !params.sqConsultaFicha)
            {
                res.errors.push('Ficha deve ser informada!')
            }

            if( !params.sqPublicacao && !params.deComunicacaoPessoal && !params.txObservacao)
            {
                res.errors.push('Ref. bibliográfica/Comunicação Pessoal deve ser informada!')
            }

            if( !params.vlLat || !params.vlLon )
            {
                res.errors.push('Coordenada inválida!')
            }

            if( !params.sqDatum)
            {
                res.errors.push('Datum deve ser informado!')
            }

            if( !params.sqPrecisaoCoordenada)
            {
                res.errors.push('Precisão da coordenada deve ser informada!')
            }

            if( !params.sqPrazoCarencia)
            {
                res.errors.push('Prazo de carência deve ser informado!')
            }

            if( !params.nuAnoRegistro )
            {
                res.errors.push('Ano do registro/ocorrência é obrigatório.')
            }

            if( params.nuAnoRegistro )
            {
                params.inDataDesconhecida='N'
            }
            else
            {
                params.inDataDesconhecida='S'
                params.nuDiaRegistro = null
                params.nuMesRegistro = null
                params.nuAnoRegistro = null
                params.hrRegistro = null

            }
            // validar dia / mes / ano / hora
            if( params.inDataDesconhecida == 'N' )
            {
                if( params.nuDiaRegistro )
                {
                    params.nuDiaRegistro = params.nuDiaRegistro.toString().padLeft(2,'0')
                }
                if( params.nuMesRegistro )
                {
                    params.nuMesRegistro = params.nuMesRegistro.toString().padLeft(2,'0')
                }
                if( params.nuAnoRegistro )
                {
                    params.nuAnoRegistro = params.nuAnoRegistro.toString().padLeft(4,'0')
                }

                if( params.nuAnoRegistro.toInteger() < 1500 )
                {
                    res.errors.push('Ano inválido.')
                }
                else
                {
                    //def dateParser = new java.text.SimpleDateFormat("dd/MM/yyyy")
                    //dateParser.lenient = false

                    if( params.nuMesRegistro && ( params.nuMesRegistro.toInteger() < 1 || params.nuMesRegistro.toInteger() > 12 ) )
                    {
                        res.errors.push('Mês inválido.')
                    }
                    else if( params.nuDiaRegistro )
                    {
                        if( ! Util.isValidDate( params.nuDiaRegistro+'/'+params.nuMesRegistro+'/'+params.nuAnoRegistro ) )
                        {
                            res.errors.push(params.nuDiaRegistro+'/'+params.nuMesRegistro+'/'+params.nuAnoRegistro +' - data inválida.')
                        }
                    }

                    if( params.hrRegistro )
                    {
                        /*try {
                            dateParser = new java.text.SimpleDateFormat("HH:mm")
                            dateParser.lenient = false
                            dateParser.parse( params.hrRegistro.toString().trim() )
                        } catch( Exception e )
                        {
                            println e.getMessage();
                            res.errors.push(params.hrRegistro + ' - hora inválida.')
                        }
                        */
                        if( ! Util.isValidTime( params.hrRegistro ) )
                        {
                            res.errors.push(params.hrRegistro + ' - hora inválida.')
                        }
                    }
                }
            }

            DadosApoio precisao = DadosApoio.get( params.sqPrecisaoCoordenada)
            if( !params.sqRefAproximacao && precisao?.codigoSistema == 'APROXIMADA')
            {
                res.errors.push('Referência da aproximação não informada!')
            }
            GeometryFactory gf = new GeometryFactory()
            // nova tabela de ocorrencia
            FichaOcorrenciaConsulta foc

            if( res.errors.size() == 0 ) {
                if (params.sqFichaOcorrenciaConsulta)
                {
                    foc = FichaOcorrenciaConsulta.get(params.sqFichaOcorrenciaConsulta.toInteger())
                } else {
                    foc = new FichaOcorrenciaConsulta()
                    foc.fichaOcorrencia = new FichaOcorrencia()
                    foc.fichaOcorrencia.ficha = ficha
                    foc.cicloConsultaFicha = consultaFicha
                    foc.usuario = usuario
                }
                foc.txObservacao = params.txObservacao ? params.txObservacao.trim() : null
                // gravar o tipo da consulta que a colaboração foi feita. ( Direta, Ampla ou Revisao )
                /* --tipo_consulta--
                if( params.sqTipoConsulta )
                {
                    foc.tipoConsulta = DadosApoio.get(params.sqTipoConsulta.toInteger() )
                }
                */

                // gravar a ocorrencia
                //FichaOcorrencia ficha= new FichaOcorrencia()
                foc.fichaOcorrencia.hrRegistro = params.hrRegistro
                foc.fichaOcorrencia.nuDiaRegistro = params.nuDiaRegistro ? params.nuDiaRegistro.toInteger() : null
                foc.fichaOcorrencia.nuMesRegistro = params.nuMesRegistro ? params.nuMesRegistro.toInteger() : null
                foc.fichaOcorrencia.nuAnoRegistro = params.nuAnoRegistro ? params.nuAnoRegistro.toInteger() : null
                foc.fichaOcorrencia.inDataDesconhecida = params.inDataDesconhecida
                foc.fichaOcorrencia.datum = DadosApoio.get(params.sqDatum)
                foc.fichaOcorrencia.precisaoCoordenada = precisao
                foc.fichaOcorrencia.refAproximacao = DadosApoio.get(params.sqRefAproximacao)
                foc.fichaOcorrencia.noLocalidade = params.noLocalidade
                foc.fichaOcorrencia.geometry = gf.createPoint(new Coordinate(Double.parseDouble(params.vlLon), Double.parseDouble(params.vlLat)));
                foc.fichaOcorrencia.situacao = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'NAO_AVALIADA')
                foc.fichaOcorrencia.contexto = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'CONSULTA')
                foc.fichaOcorrencia.prazoCarencia = DadosApoio.get( params.sqPrazoCarencia )
                // localizar o municipio e o estado pelas coordenadas
                foc.fichaOcorrencia.estado = geoService.getUfByCoord( foc.fichaOcorrencia.geometry.y, foc.fichaOcorrencia.geometry.x );
                if( foc.fichaOcorrencia.estado ) {
                    foc.fichaOcorrencia.municipio = geoService.getMunicipioByCoord( foc.fichaOcorrencia.geometry.y, foc.fichaOcorrencia.geometry.x, foc.fichaOcorrencia.estado );
                } else {
                    foc.fichaOcorrencia.municipio=null
                }
                if ( ! foc.fichaOcorrencia.save(flush: true)) {
                    println 'Erro gravando ficha ocorrencia'
                    res.errors.push('Erro gravando ficha ocorrencia')
                    println foc.fichaOcorrencia.getErrors()
                    renderRes()
                    return
                }
                if (foc.save(flush: true)) {

                    // criar o registro na tabela de registros - AINDA NÃO IMPLEMENTADO
                    // registroService.saveOcorrenciaSalve( foc.fichaOcorrencia )

                    // retorno para atualizar o gride
                    res.data.id = foc.fichaOcorrencia.id
                    res.data.idEdit = foc.id
                    res.data.dataHora = foc.fichaOcorrencia.dataHora
                    res.data.carencia = foc.fichaOcorrencia.dataFimCarencia

                    // gravar a ref. bibliográfica / comunicação pessoal
                    FichaRefBib refBibOcorrencia = FichaRefBib.findByFichaAndSqRegistroAndNoTabelaAndNoColuna(ficha, foc.fichaOcorrencia.id, 'ficha_ocorrencia', 'sq_ficha_ocorrencia')
                    if( !params.sqPublicacao && !params.deComunicacaoPessoal )
                    {
                        if( refBibOcorrencia )
                        {
                            refBibOcorrencia.delete( flush:true)
                        }
                    }
                    else
                    {

                        if (!refBibOcorrencia)
                        {
                            refBibOcorrencia = new FichaRefBib()
                        }
                        refBibOcorrencia.publicacao = null
                        refBibOcorrencia.noAutor = null
                        refBibOcorrencia.nuAno = null
                        res.data.refBibHtml = ''
                        if (params.sqPublicacao)
                        {
                            refBibOcorrencia.publicacao = Publicacao.get(params.sqPublicacao)
                            res.data.refBibHtml = refBibOcorrencia.publicacao.referenciaHtml
                        } else if (params.deComunicacaoPessoal)
                        {
                            refBibOcorrencia.noAutor = params.deComunicacaoPessoal
                            res.data.refBibHtml = params.deComunicacaoPessoal
                        }
                        refBibOcorrencia.ficha = ficha
                        refBibOcorrencia.sqRegistro = foc.fichaOcorrencia.id
                        refBibOcorrencia.noTabela = 'ficha_ocorrencia'
                        refBibOcorrencia.noColuna = 'sq_ficha_ocorrencia'
                        refBibOcorrencia.deRotulo = 'ocorrência'
                        if (!refBibOcorrencia.save(flush: true))
                        {
                            println refBibOcorrencia.getErrors()
                        }
                    }

                    // salvar as fotos enviadas
                    // http://grails-dev.blogspot.com/2013/07/multipart-file-upload-functionality.html
                    DadosApoio tipoMultimidia = dadosApoioService.getByCodigo('TB_TIPO_MULTIMIDIA','IMAGEM')
                    Date fotoDataHora

                    params.fotoData.eachWithIndex { it, i ->
                        String index = i.toString()
                        try {
                            if ( Util.isValidDate(params.fotoData[index]) && Util.isValidTime(params.fotoHora[index])) {
                                fotoDataHora = Util.strToDate(params.fotoData[index] + ' ' + params.fotoHora[index])
                                FichaConsultaMultimidia fcm
                                if (params.fotoId[index]) {
                                    fcm = FichaConsultaMultimidia.get(params.fotoId[index].toLong())
                                }
                                // se não existe a imagem ainda ou se ela existe e ainda não foi aprovada, pode ser alterada
                                if( !fcm || fcm.fichaOcorrenciaConsulta.fichaOcorrencia.situacao.codigoSistema != 'APROVADA') {
                                    FichaMultimidia fm = fichaMultimidiaService.save(
                                            !fcm ? null : fcm.fichaMultimidia.id
                                            , consultaFicha.ficha.id.toLong()
                                            // se for uma alteração, pode não ter a foto
                                            , ( ( params.foto && params.foto[index] ) ? ((CommonsMultipartFile) params.foto[index]) : null)
                                            , tipoMultimidia.id.toLong()
                                            , null
                                            , 'Colaboração externa'
                                            , params.fotoDescricao[index].toString()
                                            , params.fotoAutor[index].toString()
                                            , null
                                            , fotoDataHora
                                            , !fcm ? false : fcm.fichaMultimidia.inPrincipal
                                            , !foc.fichaOcorrencia.datum ? null : foc.fichaOcorrencia.datum.id
                                            , (Geometry) foc.fichaOcorrencia.geometry
                                    )
                                    if (fm.id && !fcm) {
                                        fcm = new FichaConsultaMultimidia()
                                        fcm.fichaMultimidia = fm
                                        fcm.fichaOcorrenciaConsulta = foc
                                        fcm.save(flush: true)
                                        // dados retorno com os ids das novas fotos
                                        res.data.fotos.push(['index': index, id: fcm.id, 'arquivo': ( ( params.foto && params.foto[index] ) ? params.foto[index].originalFilename :'' ) ])
                                    }
                                }
                            }

                        } catch (Exception e) {
                            println e.getMessage()
                            res.errors.push( e.getMessage() )
                        }
                    }
                } else
                {
                    println foc.getErrors()
                }
            }
        }
        renderRes()
    }

    def excluirMinhaFoto() {
        Map res = [status:1,type:'error',msg:'']
        try {

            // verificar se o id da foto foi informado
            if (!params.sqFichaConsultaMultimidia) {
                throw new Exception ( 'Id da Foto não informado!' )
            }

            // verificar se o id informado é valido
            FichaConsultaMultimidia foto = FichaConsultaMultimidia.get(params.sqFichaConsultaMultimidia.toLong())
            if (!foto) {
                throw new Exception ( 'Id da foto inexistnte!' )
            }
            Usuario usuario = Usuario.get( session.sicae.user.sqPessoa )

            if (!usuario) {
                throw new Exception ( 'Usuário inexistnte!' )
            }

            // verificar se a foto é do usuário logado
            if( foto.fichaOcorrenciaConsulta.usuario != usuario )
            {
                throw new Exception ( 'Foto foi cadastrada por outra pessoa!' )
            }
            foto.fichaMultimidia.delete(flush:true)
            res.status = 0
            res.type   = 'success'
            res.msg    = 'Foto excluída com SUCESSO!'
        } catch( Exception e) {
            res.msg = e.getMessage()
        }
        render res as JSON
    }

    def editarOcorrencia() {
        if( !params.sqFichaOcorrenciaConsulta )
        {
            render [:] as JSON
            return
        }
        render FichaOcorrenciaConsulta.get( params.sqFichaOcorrenciaConsulta.toInteger() ).asJson()
    }

    def excluirOcorrencia()
    {
        res.status=0
        res.type='error'
        if( !params.sqFichaOcorrenciaConsulta )
        {
            res.msg="Id não informado!"
            res.status=1
            renderRes()
            return;
        }
        FichaOcorrenciaConsulta foc = FichaOcorrenciaConsulta.get(params.sqFichaOcorrenciaConsulta.toInteger() )
        res.status=0
        res.type='success'
        res.msg=''
        if( foc )
        {
            // excluir ocorrencia e a ref. bib
            FichaOcorrencia fo = foc.fichaOcorrencia
            FichaRefBib refBibOcorrencia = FichaRefBib.findByFichaAndSqRegistroAndNoTabelaAndNoColuna(fo.ficha, fo.id, 'ficha_ocorrencia', 'sq_ficha_ocorrencia')

            // excluir a colaboracao
            foc.delete( flush:true )

            // excluir a referencia bibliografica
            if (refBibOcorrencia)
            {
                refBibOcorrencia.delete(flush: true)
            }

            // excluir a ocorrencia
            if( fo )
            {
                fo.delete(flush:true)
            }
        }
        else
        {
            res.msg = 'Erro exclusão'
            res.status = 1
        }
        renderRes()
    }


    def getAnexo() {


        if (!params.id) {
            response.sendError(404);
            return;
        }
        FichaAnexo fa = FichaAnexo.get(params.id);
        if (!fa) {
            response.sendError(404);
            return;
        }
        String hashFileName = fa.deLocalArquivo
        Integer posicao = hashFileName.lastIndexOf('/')
        if( posicao > 0 )
        {
            hashFileName = hashFileName.substring(posicao+1)
            fa.deLocalArquivo = hashFileName
            fa.save(flush:true)
        }
        File file
        if( params.thumb )
        {
            file = new File(grailsApplication.config.anexos.dir+'thumbnail.'+hashFileName);
            if ( file.exists() )
            {
                hashFileName = grailsApplication.config.anexos.dir+'thumbnail.'+hashFileName
            }
            else
            {
                file=null
            }
        }
        if( ! file )
        {
            file = new File( grailsApplication.config.anexos.dir+hashFileName )
        }
        if ( ! file.exists() )
        {
            println 'Arquivo ' + grailsApplication.config.anexos.dir+hashFileName + ' nao encontrado'
            response.sendError(400);
            return;
        }
        String fileType = fa.deTipoConteudo ? fa.deTipoConteudo : 'image';
        String fileName = fa.noArquivo.replaceAll(' |,|\\(.+\\)', '_').replaceAll('_+','-').replaceAll('-\\.','.')
        if( fileType )
        {
            render(file: file, fileName: fileName, contentType: fileType);
        }
        else
        {
            render(file: file, fileName: fileName)
        }
    }


    def getMultimidia() {
        if (!params.id) {
            response.sendError(404)
            return
        }
        FichaMultimidia reg = FichaMultimidia.get(params.id)

        if (!reg) {
            response.sendError(404)
            return
        }
        String mmDir = grailsApplication.config.multimidia.dir

        String hashFileName = reg.noArquivoDisco
        Integer posicao = hashFileName.lastIndexOf('/')
        if (posicao > 0) {
            hashFileName = hashFileName.substring(posicao + 1)
            reg.noArquivoDisco = hashFileName
            reg.save(flush: true)
        }
        File file
        if (params.thumb) {
            file = new File(mmDir + 'thumbnail.' + hashFileName);
            if (file.exists()) {
                hashFileName = mmDir + 'thumbnail.' + hashFileName
            } else {
                file = null
            }
        }


        if (!file) {
            file = new File(mmDir + hashFileName)
        }

        if (!file.exists()) {
            response.sendError(400)
            return
        }
        String fileName = reg.noArquivo.replaceAll(' |,|\\(.+\\)', '_').replaceAll('_+', '-').replaceAll('-\\.', '.')
        String fileExtension = Util.getFileExtension(fileName)
        String fileType = 'application/octet-stream';
        if (reg.tipo.codigoSistema == 'IMAGEM') {
            fileType = 'image/' + fileExtension
        } else if (reg.tipo.codigoSistema == 'SOM') {
            fileType = 'audio/mp3'
        }
        if (fileType) {
            render(file: file, fileName: fileName, contentType: fileType)
        } else {
            render(file: file, fileName: fileName)
        }
    }

    def salvarPlanilhaOcorrencias(){
        res.status = 1
        res.msg = ''
        res.type = 'error'
        res.errors = []
        res.data = null
        if( params.arquivo ) {
            try {
                FichaConsultaAnexo fca = colaborarService.saveFichaConsultaAnexo(
                        ( params.sqFichaConsultaAnexo ? params.sqFichaConsultaAnexo.toLong() : null )
                        , params.sqCicloConsulta.toLong()
                        ,(CommonsMultipartFile) params.arquivo
                        ,session.sicae.user.sqPessoa)
                res.data    = [sqFichaConsultaAnexo:fca.id]
                res.status  = 0
                res.msg     = 'Arquivo gravado com SUCESSO!'
                res.type    = 'success'
            } catch( Exception e ) {
              res.errors.push( e.getMessage() )
            }
        }
        return renderRes()
    }

    def excluirPlanilhaOcorrencia() {
        res.status = 1
        res.msg = ''
        res.type = 'error'
        res.errors = []
        res.data = null
        try {
            if ( params.sqFichaConsultaAnexo ) {
                FichaConsultaAnexo fca = FichaConsultaAnexo.get( params.sqFichaConsultaAnexo.toLong() )
                if( ! fca ) {
                    throw  new Exception('Arquivo não encontrado')
                }
                if( fca.webUsuario.id.toLong() != session.sicae.user.sqPessoa.toLong()) {
                    throw  new Exception('Arquivo não pertence ao usuário logado')
                }
                fca.delete(flush:true)
                res.data = [sqFichaConsultaAnexo: fca.id]
                res.status = 0
                res.msg = 'Arquivo excluído com SUCESSO!'
                res.type = 'success'
            }
        } catch (Exception e) {
            res.errors.push(e.getMessage())
        }
        return renderRes()
    }

    def downloadPlanilhaModeloOcorrencias() {
        try {
            File f;
            String dirArquivos = grailsApplication.config.arquivos.dir
            String contentDisposition = ''
            String contentType = 'application/zip'
            f = new File( dirArquivos + 'planilha_modelo_importacao_registros_salve_consulta.zip' )
            if( f ) {
                contentDisposition = "filename=planilha_modelo_importacao_registros_salve_consulta.zip"
            }
            if ( f && f.exists() ) {
                response.setContentType(contentType)
                response.setHeader("Content-disposition", contentDisposition)
                response.setHeader("Content-Length", f.size().toString() )
                response.outputStream << f.bytes
            }
            else {
                render '<html><body><br><center><h3>Erro! Arquivo planilha_modelo_importacao_registros_salve_consulta.zip não encontrado!</h3></center></body></html>'

            }
        } catch (Exception e) {
            render e.getMessage()
        }
    }

     /**
     * Método responsavel por disponibilizar ao usuário para download um arquivo desejado
     * @return [description]
     */
    def download()
    {
        File f;
        String dirArquivos = grailsApplication.config.arquivos.dir;
        String dirAnexos   = grailsApplication.config.anexos.dir;
        String contentType = "application/octet-stream"
        String contentDisposition = ""
        contentType = 'application/pdf'
        f = new File(dirArquivos+params.id+'.pdf');
        if( f )
        {
            contentDisposition = "filename=${f.name}"
        }
        if ( f && f.exists() ) {
            response.setContentType(contentType)
            response.setHeader("Content-disposition", contentDisposition)
            response.setHeader("Content-Length", f.size().toString() )
            response.outputStream << f.bytes
            return
        }
        else {
            render '<html><body><br><center><h3>Opssss!!! Arquivo <i>'+dirArquivos+params.id+'.pdf</i> não encontrado!</h3></center></body></html>'
        }
    }

    // getGrid
    def getMinhasOcorrencias() {
        Map data = [ status:0, msg:'', data:['ocorrencias':[] ] ]
        CicloConsultaFicha cicloConsultaFicha
        FichaOcorrenciaValidacao fichaOcorrenciaValidacao
        Ficha ficha
        List listFotos = []

        if( ! params.sqConsultaFicha ){
            data.status=1
            data.msg = 'Ficha inválida!'
        }
        else {
            cicloConsultaFicha = CicloConsultaFicha.get(params.sqConsultaFicha.toInteger())
            Usuario usuario
            if (!params.sqConsultaFicha) {
                data.status = 1
                data.msg = 'Ciclo consulta inválido!'
                render res as JSON
                return
            }
            ficha = cicloConsultaFicha?.ficha

            usuario = Usuario.get( session.sicae.user.sqPessoa )
            if (!usuario) {
                throw new Exception ( 'Usuário inexistnte!' )
            }

            // ler ocorrencias de colaboração dos usuários no módulo consulta
            // para os registros adicionados na consulta que não forem do usuário logado,
            // so exibl-los se não tiverem aceitos e utilizados na avaliação

            //FichaOcorrenciaConsulta.findAllByCicloConsultaFicha(cicloConsultaFicha, [sort: "id", order: "desc"]).each {
            FichaOcorrenciaConsulta.createCriteria().list{
                createAlias('cicloConsultaFicha', 'ccf')
                createAlias('ccf.ficha', 'fi')
                eq('fi.id',ficha.id)
            }.each {
                Boolean listarRegistro = true
                if (it.usuario != usuario) {
                    if( it.fichaOcorrencia?.situacao?.codigoSistema == 'ACEITA' ){
                        listarRegistro=false
                    }
                    if( it.fichaOcorrencia?.inUtilizadoAvaliacao?.toString() != 'S' ){
                        listarRegistro=false
                    }
                    if (it?.fichaOcorrencia?.carencia) {
                        if (it.fichaOcorrencia.carencia > new Date()) {
                           listarRegistro = false
                        }
                    }
                }
                if ( listarRegistro ) {
                    listFotos=[]
                    it.fichaConsultaMultimidia.each{
                        listFotos.push([noAutorFoto:it.fichaMultimidia.noAutor
                                        ,url:'getMultimidia/'+it.fichaMultimidia.id.toString()+'/thumb'
                                        ,dtFoto:it.fichaMultimidia.dtElaboracao.format('dd/MM/yyyy HH:mm')
                                        ,noArquivo:it.fichaMultimidia.noArquivo
                        ])
                    }
                    // verificar se o usuário validou/não validou o ponto
                    fichaOcorrenciaValidacao = FichaOcorrenciaValidacao.findByUsuarioAndFichaOcorrencia(usuario, it.fichaOcorrencia)
                    data.data.ocorrencias.push([id              : it.fichaOcorrencia.id
                                               , idEdit        : it.id
                                               , idConsulta    : params.sqConsultaFicha.toInteger()
                                               , lat           : it.fichaOcorrencia.geometry.y
                                               , lon           : it.fichaOcorrencia.geometry.x
                                               , datum         : it?.fichaOcorrencia?.datum?.descricao
                                               , estado        : it.fichaOcorrencia?.estado?.sgEstado
                                               , municipio     : it.fichaOcorrencia?.municipio?.noMunicipio
                                               , localidade    : it.fichaOcorrencia?.noLocalidade
                                               , precisao      : it.fichaOcorrencia?.precisaoCoordenada?.descricao
                                               , refAproximacao: it?.fichaOcorrencia.refAproximacao?.descricao
                                               , carencia      : it?.fichaOcorrencia?.dataFimCarencia
                                               , responsavel   : ( it.usuario ? it.usuario.noPessoa : it.fichaOcorrencia?.pessoaInclusao?.noPessoa )
                                               , dataHora      : it.fichaOcorrencia.dataHora
                                               , refBib        : it.fichaOcorrencia.refBibHtml
                                               , baseDados     : (it.usuario == usuario ? 'minha-colaboracao' : 'Salve consulta')
                                               , fonte         : 'salve-consulta'
                                               , valido        : (fichaOcorrenciaValidacao ? fichaOcorrenciaValidacao.inValido : '')   // S/N se o uusuário logado aceitou / não aceitou o registro
                                               , obs           : (fichaOcorrenciaValidacao ? fichaOcorrenciaValidacao.txObservacao : '')
                                               , utilizadoAvaliacao : ( it.fichaOcorrencia.inUtilizadoAvaliacao == 'S' )
                                               , fotos         : listFotos
                    ])
                }

            }

            /** /
            println ' '
            println 'OCORRENCIAS MINHAS'
            println data.data.ocorrencias
            /**/

            // ler ocorrências do portalbio
            listFotos=[]
            FichaOcorrenciaPortalbio.findAllByFicha(ficha).each {

                Boolean emCarencia = false
                if (it?.dtCarencia) {
                    if (it.dtCarencia >= new Date()) {
                        emCarencia = true
                    }
                }
                if (!emCarencia) {

                    // verificar se o usuário validou/não validou o ponto
                    fichaOcorrenciaValidacao = FichaOcorrenciaValidacao.findByUsuarioAndFichaOcorrenciaPortalbio(usuario, it)
                    data.data.ocorrencias.push([id              : it.id
                                               , idConsulta    : params.sqConsultaFicha.toInteger()
                                               , lat           : it.geometry.y
                                               , lon           : it.geometry.x
                                               , datum         : ''
                                               , estado        : ''
                                               , municipio     : ''
                                               , localidade    : it?.noLocal
                                               , precisao      : ''
                                               , refAproximacao: ''
                                               , carencia      : (it?.dtCarencia ? it.dtCarencia.format('dd/MM/yyyy') : '')
                                               , responsavel   : it?.noAutor
                                               , dataHora      : it?.dtOcorrencia?.format('dd/MM/yyyy')
                                               , refBib        : ''
                                               , baseDados     : it?.noInstituicao.replaceAll(/ICMBio\//, '')
                                               , fonte         : 'PortalBio'
                                               , valido        : (fichaOcorrenciaValidacao ? fichaOcorrenciaValidacao.inValido : '')   // S/N se o uusuário logado aceitou / não aceitou o registro
                                               , obs           : (fichaOcorrenciaValidacao ? fichaOcorrenciaValidacao.txObservacao : '')
                                               , utilizadoAvaliacao : ( it.inUtilizadoAvaliacao == 'S' )
                                               , fotos         : listFotos
                    ]
                    )
                } // fim ocorrencias portalbio
            }
            // ler ocorrencias do salve
            DadosApoio contextoOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')
            listFotos=[]
            FichaOcorrencia.findAllByFichaAndContexto(ficha, contextoOcorrencia).each {
                if (it.geometry) {
                    if ( ! it.emCarencia ) {
                        // verificar se o usuário validou/não validou o ponto
                        fichaOcorrenciaValidacao = FichaOcorrenciaValidacao.findByUsuarioAndFichaOcorrencia(usuario, it)
                        data.data.ocorrencias.push([id              : it.id
                                                   , idConsulta    : params.sqConsultaFicha.toInteger()
                                                   , lat           : it.geometry.y
                                                   , lon           : it.geometry.x
                                                   , datum         : it?.datum?.descricao
                                                   , estado        : it?.estado?.sgEstado
                                                   , municipio     : it?.municipio?.noMunicipio
                                                   , localidade    : it?.noLocalidade
                                                   , precisao      : it?.precisaoCoordenada?.descricao
                                                   , refAproximacao: it?.refAproximacao?.descricao
                                                   , carencia      : it.dataFimCarencia
                                                   , responsavel   : ''//it?.pessoaInclusao?.noPessoa
                                                   , dataHora      : it.dataHora
                                                   , refBib        : it.refBibHtml
                                                   , baseDados     : 'Salve'
                                                   , fonte         : 'Salve'
                                                   , valido        : (fichaOcorrenciaValidacao ? fichaOcorrenciaValidacao.inValido : '')   // S/N se o uusuário logado aceitou / não aceitou o registro
                                                   , obs           : (fichaOcorrenciaValidacao ? fichaOcorrenciaValidacao.txObservacao : '')
                                                   , utilizadoAvaliacao : ( it.inUtilizadoAvaliacao == 'S' || ! it.inUtilizadoAvaliacao )
                                                   , fotos         : listFotos
                        ])
                    }
                }
            }

            // ler os pontos das subespecies
            if (ficha.taxon.nivelTaxonomico.coNivelTaxonomico == 'ESPECIE') {
                FichaOcorrencia.createCriteria().list {
                    createAlias('ficha', 'f')
                    createAlias('ficha.taxon', 'taxon')
                    eq('taxon.pai', ficha.taxon)
                    eq('contexto', contextoOcorrencia)
                }.each {
                    if (it.geometry) {
                        if (!it.emCarencia) {
                            // verificar se o usuário validou/não validou o ponto
                            fichaOcorrenciaValidacao = FichaOcorrenciaValidacao.findByUsuarioAndFichaOcorrencia(usuario, it)
                            listFotos = []
                            data.data.ocorrencias.push([id              : it.id
                                                       , idConsulta    : params.sqConsultaFicha.toInteger()
                                                       , lat           : it.geometry.y
                                                       , lon           : it.geometry.x
                                                       , datum         : it?.datum?.descricao
                                                       , estado        : it?.estado?.sgEstado
                                                       , municipio     : it?.municipio?.noMunicipio
                                                       , localidade    : it?.noLocalidade
                                                       , precisao      : it?.precisaoCoordenada?.descricao
                                                       , refAproximacao: it?.refAproximacao?.descricao
                                                       , carencia      : it.dataFimCarencia
                                                       , responsavel   : ''//it?.pessoaInclusao?.noPessoa
                                                       , dataHora      : it.dataHora
                                                       , refBib        : raw(it.refBibHtml)
                                                       , baseDados     : 'Salve'
                                                       , fonte         : 'Salve'
                                                       , valido        : (fichaOcorrenciaValidacao ? fichaOcorrenciaValidacao.inValido : '')   // S/N se o uusuário logado aceitou / não aceitou o registro
                                                       , obs           : (fichaOcorrenciaValidacao ? fichaOcorrenciaValidacao.txObservacao : '')
                                                       , utilizadoAvaliacao : ( it.inUtilizadoAvaliacao == 'S' || ! it.inUtilizadoAvaliacao )
                                                       , fotos         : listFotos
                            ])
                        }
                    }
                }
            } // fim ocorrências salve
        }
        params.pageNumber = params.pageNumber ? params.pageNumber.toInteger() : 1
        // fazer a paginação dos registros
        Map pagination = [ pageSize:100
                ,pageNumber: params.pageNumber
                ,totalRecords :data.data.ocorrencias.size()
                ,totalPages : 1
                ,rowNumber : 1
        ]
        pagination.totalPages = Math.max(1, Math.ceil( pagination.totalRecords / pagination.pageSize) ).toInteger()
        pagination.pageNumber = Math.min( pagination.totalPages, pagination.pageNumber)

        // calcular numero da linha
        pagination.rowNum = ((pagination.pageNumber - 1) * pagination.pageSize) + 1

        // fatiar registros para pegar a pagina correta
        data.data.ocorrencias = data.data.ocorrencias.collate(pagination.pageSize)[pagination.pageNumber - 1]

        /*
        println' '
        println'PAGINAÇAO'
        println 'Página..............: ' + pagination.pageNumber.toString()
        println 'Total de Ocorrencias: ' + pagination.totalRecords.toString()
        println 'Registros por página: ' + pagination.pageSize.toString()
        println 'Total páginas.......: ' + pagination.totalPages.toString()
        println 'Linha numero........: ' + pagination.rowNum.toString()
         */
        data.pagination = pagination
        //println ' '
        //println pagination
        render data as JSON
    }

    def getPontos()
    {

        // o retorno será JSON
        response.setHeader("Content-Type", "application/json");

        // cabeçalho padrão do GeoJson da resposta
        CicloConsultaFicha consultaFicha
        Map geoJson = ["type": "FeatureCollection", 'crs': ['type': 'name', 'properties': ['name': 'EPSG:4326']], "features": []];
        String url  = grailsApplication.config.url.biocache;
        String noCientifico;
        Ficha ficha
        String color
        List fixedColors = ['#ff0000','#00ff00','#0000ff','#808080','#C0C0C0','#2F4F4F','#00008B','#483D8B','#4B0082','#9370DB','#4682B4','#483D8B','#B0C4DE','#800080','#FF00FF','#FF1493','#FF69B4','#DB7093','#DC143C','#FF0000','#800000','#B22222','#FFB6C1','#FA8072','#FF6347','#FF7F50','#D2691E','#CD853F','#BC8F8F','#DEB887','#FF4500','#FF8C00','#006400','#556B2F','#808000','#00FF00','#9ACD32','#ADFF2F','#7FFF00','#00FF7F','#20B2AA','#00FFFF','#00CED1','#1E90FF','#00BFFF','#008B8B','#7FFFD4','#98FB98','#DAA520','#FFFF00','#FFD700','#F0E68C']
        Map cores =[:]
        def feature

        if( !grailsApplication?.config?.url?.biocache )
        {
            render geoJson as JSON;
            println 'Constante url.biocache não definido no arquivo de confirações'
            return;
        }

        if( !grailsApplication?.config?.url?.solr )
        {

            render geoJson as JSON;
            println 'Constante url.solr não definido no arquivo de confirações'
            return;
        }

        if(!params.sqConsultaFicha)
        {
            render geoJson as JSON;
        }
        else
        {
            consultaFicha = CicloConsultaFicha.get(params.sqConsultaFicha.toInteger() )
            ficha = consultaFicha?.ficha
            if( !ficha )
            {
                render '{}';
                return;
            }

            // ler os pontos cadastrados no ciclo
            noCientifico = ficha.taxon.noCientifico
            if( ! noCientifico )
            {

                render '{}'
                return;
            }

            FichaOcorrenciaPortalbio.findAllByFicha(ficha).each {
                if( !cores[it.noInstituicao] )
                {

                    if( cores.size() >= fixedColors.size() )
                    {
                        cores[it.noInstituicao]=br.gov.icmbio.Util.getRandomColor()
                    }
                    else
                    {
                        cores[it.noInstituicao] = fixedColors[ cores.size() ]
                    }

                }

                feature = ["type"      : "Feature",
                           "id"        : it.id,
                           "properties": [
                                   "text"    : '<b>Portalbio/' + it.noInstituicao + '</b>' +
                                           '<br/>' + it.noLocal +
                                           '<br/>' + it.noCientifico +
                                           '<br/>Coletor:' + it.noAutor +
                                           (it.dtOcorrencia ? it.dtOcorrencia.format("dd/MMM/yyyy") : '') +
                                           (it.dtCarencia ? it.dtCarencia.format("dd/MMM/yyyy") : '') ,
                                           //'<br><a title="Abrir página do Portal da Biodiversidade" target="_blank" href="https://portaldabiodiversidade.icmbio.gov.br/portal/occurrences/' + it.deUuid + '">Abrir página do PortalBio</a>',
                                   "bd"      : 'portalbio',
                                   "uuid"    : it.deUuid,
                                   "color"   : cores[it.noInstituicao], //color,
                                   "legend"  : it.noInstituicao,//(color=="#ff000"?'Espécie Ameaçada':'Espécie Não Ameaçada'),
                                   "ameaca_s": it.deAmeaca,
                                   // TODO - adicionar mais atributtos aqui
                           ],
                           "geometry"  : [
                                   "type"       : "Point",
                                   "coordinates": [it.geometry.x, it.geometry.y]
                           ]
                ];
                geoJson.features.push(feature)
            }

            // ler os pontos adicionados no salve
            DadosApoio contextoOcorrencia = dadosApoioService.getByCodigo('TB_CONTEXTO_OCORRENCIA', 'REGISTRO_OCORRENCIA')
            FichaOcorrencia.findAllByFichaAndContexto(ficha, contextoOcorrencia ).each {
                if (it.geometry)
                {
                   //color = "#ffff00"
                    /*if( it?.inPresencaAtual == 'N')
                    {
                        color='#b38e6e' // marron
                    }
                    */

                    if( !cores['salve'] )
                    {
                        if( cores.size() >= fixedColors.size() )
                        {
                            cores['salve'] = Util.getRandomColor()
                        }
                        else
                        {
                            cores['salve'] = fixedColors[ cores.size() ]
                        }
                    }

                    feature = ["type"      : "Feature",
                               "id"        : it.id,
                               "properties": [
                                       "text": '<b>SALVE<br>Espécie</b><br>'+
                                       it.ficha.noCientifico+'<br>'+
                                       '<span class="nowrap">Presença Atual na Coordenada: <b>'+(it?.inPresencaAtualText)+'</b></span>',
                                       "bd"  : 'salve',
                                       "color": cores['salve'], //color,
                                       "legend":'Salve', //it.ficha.nmCientifico,
                                       // TODO - adicionar mais atributtos aqui
                               ],
                               "geometry"  : [
                                       "type"       : "Point",
                                       "coordinates": [it?.geometry?.x, it?.geometry?.y]
                               ]
                    ];
                    geoJson.features.push(feature)
                }
            }
            // ler os pontos das subespecies
            if( ficha.taxon.nivelTaxonomico.coNivelTaxonomico=='ESPECIE')
            {
                FichaOcorrencia.createCriteria().list {
                    createAlias('ficha','f')
                    createAlias('ficha.taxon','taxon')
                    eq('taxon.pai', ficha.taxon)
                    eq('contexto',contextoOcorrencia )
                }.each {
                    if (it.geometry)
                    {
                        if( !cores[it.ficha.nmCientifico] )
                        {

                          if( cores.size() > fixedColors.size() )
                            {
                                cores[it.ficha.nmCientifico]=br.gov.icmbio.Util.getRandomColor()
                            }
                            else
                            {
                                cores[it.ficha.nmCientifico] = fixedColors[ cores.size() ]
                            }
                        }

                        feature = ["type"      : "Feature",
                                   "id"        : it.id,
                                   "properties": [
                                           "text": '<b>SALVE<br>Subespecie</b><br>'+
                                                   it.ficha.nmCientifico+'<br>'+
                                                   '<span class="nowrap">Presença Atual na Coordenada: <b>'+(it?.inPresencaAtualText)+'</b></span>',
                                           "bd"  : 'salve',
                                           "color": cores['salve'], //color,
                                           "legend":'Salve' ,
                                           // TODO - adicionar mais atributtos aqui
                                   ],
                                   "geometry"  : [
                                           "type"       : "Point",
                                           "coordinates": [it?.geometry?.x, it?.geometry?.y]
                                   ]
                        ];
                        geoJson.features.push(feature);
                    }
                }
            }

            // ler os pontos adicionados no salve
            FichaOcorrenciaConsulta.findAllByCicloConsultaFicha( consultaFicha ).each {
                if (it.fichaOcorrencia.geometry)
                {
                    if( !cores['salve-consulta'] )
                    {

                        if( cores.size() > fixedColors.size() )
                        {
                            cores['salve-consulta']=br.gov.icmbio.Util.getRandomColor()
                        }
                        else
                        {
                            cores['salve-consulta'] = fixedColors[ cores.size() ]
                        }
                    }
                    feature = ["type"      : "Feature",
                               "id"        : it.id,
                               "properties": [
                                       "bd"  : 'salve-consulta',
                                       //"id"  : it.id,
                                       "text": '<b>Salve-Consulta</b><br>'+
                                               it.fichaOcorrencia.noLocalidade+
                                               '<br>Situação: ' + it.cicloConsultaFicha.ficha.situacao.descricao+
                                               '<br>Data do Registro: '+it.fichaOcorrencia.dataHora+
                                               '<br>Cadastrado por: '+it.usuario.noUsuario+
                                               '<br>Cadastrado em: '+it.fichaOcorrencia?.dtInclusao.format('dd/MM/yyyy hh:mm:ss')+
                                               '<br>Última alteração: '+it.fichaOcorrencia?.dtAlteracao.format('dd/MM/yyyy hh:mm:ss')+
                                               '<br>Observações: '+it.txObservacao,
                                       "color": cores['salve-consulta'], //color,
                                       "legend":'Salve Consulta' ,

                               ],
                               "geometry"  : [
                                       "type"       : "Point",
                                       "coordinates": [it.fichaOcorrencia?.geometry?.x, it.fichaOcorrencia?.geometry?.y]
                               ]
                    ];
                    geoJson.features.push(feature);
                }
            }
            render geoJson as JSON;
        }
    }

    def validarRegistro(){
        Usuario usuario
        Map res = [status:1,msg:'',type:'error']
        if( !params.fonte )
        {
            res.msg='Fonte não informada.'
            render res as JSON
            return
        }
        if( !session.sicae.user ) {
            res.msg='Sessão expirada.'
            render res as JSON
            return
        }
        usuario = Usuario.get( session.sicae.user.sqPessoa )
        if (!usuario) {
            res.msg='Usuário inexistente.'
            render res as JSON
            return
        }
        String fonte = params.fonte.toLowerCase()
        FichaOcorrenciaPortalbio fichaOcorrenciaPortalbio = null
        FichaOcorrencia fichaOcorrencia = null
        FichaOcorrenciaConsulta fichaOcorrenciaConsulta = null
        if( fonte == 'portalbio')
        {
            fichaOcorrenciaPortalbio =  FichaOcorrenciaPortalbio.get( params.id.toInteger() )
        }
        else if( fonte == 'salve' ||  fonte == 'salve-consulta')
        {
            fichaOcorrencia = FichaOcorrencia.get( params.id.toInteger() )
        }
        if( !fichaOcorrencia && ! fichaOcorrenciaPortalbio && !fichaOcorrenciaConsulta )
        {
            res.msg='Id inválido!'
            render res as JSON
            return
        }

        FichaOcorrenciaValidacao validacao = FichaOcorrenciaValidacao.createCriteria().get{
            if( fichaOcorrenciaPortalbio )
            {
                eq('fichaOcorrenciaPortalbio', fichaOcorrenciaPortalbio)
            }
            else if( fichaOcorrencia )
            {
                eq('fichaOcorrencia', fichaOcorrencia )
            }
            else if( fichaOcorrenciaConsulta )
            {
                //TODO - a tabela fichaOcorrenciaValidacao não tem a consulta. Verificar o porque foi feito assim
                // se for detectado que o relacionamento deve existir tem que criar o coluna sq_ciclo_consulta_ficha
                // e popular baseado na data da colaboracao com o período que a consulta da ficha esteve aberto
                //eq('fichaOcorrenciaConsulta', fichaOcorrenciaConsulta )
            }
            eq('usuario', usuario)
        }
        if( !validacao ) {
            // não gerar registro quando o usuário não responder sim ou não
            if( !params.valido ){
                res.msg=''
                res.status=0
                res.type='success'
                render res as JSON
                return
            }
            validacao = new FichaOcorrenciaValidacao()
            validacao.fichaOcorrencia            = fichaOcorrencia
            validacao.fichaOcorrenciaPortalbio   = fichaOcorrenciaPortalbio
            validacao.usuario                    = usuario
        }
        validacao.inValido = params.valido?:null
        if( validacao.inValido == 'S' ) {
            params.obs=null
        }
        validacao.txObservacao = params.obs ? params.obs.trim() : null

        res.msg = ''
        res.status = 0
        res.type = 'success'
        // quando o usuário não concordar nem discordar excluir a validação
        if( !validacao.inValido ){
            validacao.delete( flush:true )
        } else {
            // adicionar qual foi a consulta/revisao que o usuario realizou a colaboracao
            if( params.idConsulta ) {
                validacao.consultaFicha = CicloConsultaFicha.get( params.idConsulta.toLong() )
            }

            if ( ! validacao.save( flush: true)) {
                println validacao.getErrors()
                res.msg = 'Erro ao gravar validação.'
            }
        }
        render res as JSON
    }

    /**
    * Salva as fotos de colaboração 
    * (OBS: Não são as fotos das ocorrências)
    */
    def salvarFotoColaboracao() {
        Map res = [ status:0, msg:'', type:'error', errors:[], data:['fotos':[] ] ]  

        if ( ! session.sicae.user ) {
            res.msg = 'Sessão Expirada. Efetue login novamente!'
            render res as JSON
        }
  
        try {       
            params.ip_user      = getIp()
            params.user_agent   = request.getHeader("user-agent")
            res = fichaConsultaFotoService.salvar(params, session)

        } catch( Exception e) {
            res.errors.push( e.getMessage() )             
        }

        render res as JSON
    }

    /**
    * Retorna as colaborações de fotos do usuário logado
    */
    def minhasFotosColaboracao() {  
        Map data = [ status:0, msg:'', data:['fotosColaboracao':[] ] ]        

        try {                    
            data = fichaConsultaFotoService.minhasFotosColaboracao(params, session)            
        } catch( Exception e) {
            res.msg = e.getMessage()
        }
        render data as JSON
    }

    /**
    * Editar Foto Colaboração do usuário
    */
    def editarFotoColaboracao() {
        if( !params.sqFichaConsultaFoto ){
            render [:] as JSON
            return
        }
        render FichaConsultaFoto.get( params.sqFichaConsultaFoto.toInteger() ).asJson()
    }

    /**
    * Deleta a foto - FichaConsultaFoto e a FichaConsultaMultimidia
    */
    def excluirFotoColaboracao() {
        Map res = [status:1,type:'error',msg:'']
        try {        
            fichaConsultaFotoService.delete(params.sqFichaConsultaFoto.toLong(), session)
            res.status = 0
            res.type   = 'success'
            res.msg    = 'Foto excluída com SUCESSO!'
        } catch( Exception e) {
            res.msg = e.getMessage()
        }
        render res as JSON
    }
    

}
