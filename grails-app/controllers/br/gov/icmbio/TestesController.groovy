package br.gov.icmbio

class TestesController {

    def emailService

    private List caracteresEspeciais = ["Ç|&Ccedil;", "ç|&ccedil;", "Á|&Aacute;", "Â|&Acirc;", "Ã|&Atilde;", "É|&Eacute;", "Ê|&Ecirc;", "Í|&Iacute;", "Ô|&Ocirc;", "Õ|&Otilde;", "Ó|&Oacute;", "Ú|&Uacute;", "á|&aacute;", "â|&acirc;", "ã|&atilde;", "é|&eacute;", "ê|&ecirc;", "í|&iacute;", "ô|&ocirc;", "õ|&otilde;", "ó|&oacute;", "ú|&uacute;"]

    def index() {
        render '<pre><br>--------------------------------------------<br>'

        render emailService.sendEmailAtivacaoConta(Usuario.get(195l) )

        render '<br>--------------------------------------------'
        render '<br>fim</pre>'
    }


    /**
     * método para converter acentos no formato html ex: ç = &ccedil
     * @param texto [description]
     * @return [description]
     */
    private String str2html( String texto) {
        this.caracteresEspeciais.each {
            List a = it.split(/\|/)
            texto = texto.replace(a[0], a[1])
        }
        return texto
    }

    private String html2str( String texto) {
        this.caracteresEspeciais.each {
            List a = it.split(/\|/)
            texto = texto.replace(a[1], a[0])
        }
        return texto
    }

}
