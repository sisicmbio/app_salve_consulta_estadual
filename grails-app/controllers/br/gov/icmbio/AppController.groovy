package br.gov.icmbio

import grails.converters.JSON
import grails.util.Environment

class AppController {

    def emailService

    def index() {
        String ambiente =  ( Environment.current.toString() == 'PRODUCTION' ? 'PROD': 'DEV' )

        if ( session.sicae.user ){
            if( ambiente != 'DEV' ) {
                session.sicae.user.dtUltimoAcesso = new Date()
            }
            redirect( 'uri':'/')
        }
        else
        {
            chain(action:'login')
        }
    }

    def logout(){
        session.sicae = [user:[:] ]
        redirect( 'uri':'/')
    }

    def login() {
        try {
            if (params.fldEmail && params.fldSenha) {
                String hashSenha = Util.md5(params.fldSenha.toString().toLowerCase())
                Pessoa pessoa = Pessoa.findByDeEmail(params.fldEmail.toString().toLowerCase())
                if (!pessoa) {
                    throw new Exception('Usuario/Senha não cadastrado')
                }
                PessoaFisica pf = PessoaFisica.get( pessoa.id )
                if (!pf) {
                    throw new Exception('Usuario/Senha não cadastrado')
                }
                Usuario usuario = Usuario.findByPessoaFisicaAndDeSenha(pf,hashSenha)
                if (!usuario) {
                    throw new Exception('Usuario/Senha inválidos')
                }
                if( !usuario.stAtivo ){
                    throw new Exception('Conta ainda não foi ativada. Ao se registrar foi enviado um e-mail com o link para fazer a ativação')
                }
                usuario.dtUltimoAcesso = new Date();
                usuario.deHash=null
                usuario.save(flush:true)
                session.sicae = [user:usuario.asMap()]
                redirect( 'uri':'/')
                return
            }
        } catch( Exception e ){
            flash.error = e.getMessage()
        }
        render( view:'login')
     }

     def ativar() {
         Usuario user = Usuario.findByDeHash(params.id.toString())
         if (user) {
             user.stAtivo = true
             user.deHash = null
             user.save(flash: true)
             render(view: 'bemvindo', model: [user: user])
         } else {
             render(view: 'erroValidacao')
         }
     }

     def forgotPassword() {
         // TODO - IMPLEMENTAR SALVE ESTADUAL
         Map res = [msg:'',status:0]
         if( !params.email )
         {
            res.msg="Informe o e-mail!"
         }
         else
         {
            params.email = params.email.toString().toLowerCase()
            Pessoa pessoa = Pessoa.findByDeEmail(params.email)
            if( ! pessoa ) {
                res.msg = 'Email não cadastrado!'
            }
            else
            {
                Usuario usr = Usuario.get( pessoa.id )
                if( !usr ){
                    res.msg = 'Email não cadastrado!'
                } else {
                    if (!emailService.sendEmailForgotPassword(usr)) {
                        res.msg = 'Falha no envio do e-mail. Tente novamente mais tarde.'
                    }
                }
            }
         }
         render res as JSON
     }


     def resetPassword() {

         if (request.getMethod().toUpperCase().equals('GET'))
         {
             if (params.id)
             {
                 render(view: 'resetPassword', model: ['hash': params.id])
             } else
             {
                 redirect(uri: '/');
             }
         }
         else //if (request.getMethod().toUpperCase().equals('POST'))
         {
                Map res=[status:1,msg:'Dados inválidos',errors:[]]
                if( params.fldSenha && params.fldSenha2 && params.fldHash) {
                    Usuario user = Usuario.findByDeHash(params.fldHash)
                    if( user ) {
                        user.deSenha = Util.md5(params.fldSenha)
                        user.deHash = null
                        user.save(flush:true)
                        res.status=0
                        res.msg='Senha Alterada com Sucesso!'
                    }
                }
                render res as JSON
         }
     }

     def sobre()
     {
        render( view:'sobre')
     }

     def links()
     {
        render( view:'links')
     }

     def setPassword() {
         // TODO - IMPLEMENTAR SALVE ESTADUAL
         /*
        if ( request.getMethod().toUpperCase().equals('GET') )
        {
            render ''
            return
        }
        String ambiente =  ( Environment.current.toString() == 'PRODUCTION' ? 'PROD': 'DEV' )

        if( !params.hash || params.hash !='3f801277c23f0730884de3ecd8f3de4bfaa253a263ffd439c268eea7f98ffb22'+ambiente)
        {
            render 'Parametro hash inválido! '+ params.hash
            return;
        }
        if( ! params.sqWebUsuario || !params.deSenha )
        {
            render 'Usuário e senha deve ser informado!'
            return;
        }
        WebUsuario user = WebUsuario.get( params.sqWebUsuario.toInteger() )
        if( !user )
        {
            render 'Id ' + params.sqWebUsuario+' não encontrado!'
            return
        }
        user.password = params.deSenha
        user.save()
        render 'Senha alterada com SUCESSO!'
        */
     }
}
