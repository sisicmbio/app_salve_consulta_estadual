package br.gov.icmbio

import grails.converters.JSON

class AjaxController extends BaseController {

    def publicacaoService
    def sqlService
    /**
    *   Definir método que irá iterceptar todas as ações submetidas
    *   def beforeInterceptor = [action:this.&auth,except:['/controller/action','/controller/action'] ]
    */
    def beforeInterceptor = [action:this.&auth, except: ['/ping']]

    private Boolean auth()
    {
        // limpar variável de retorno
        res = [ status:0, msg:'', data:[:], errors:[] ]

        if( ! super.auth() )
        {
            return false
        }
/* POR ENQUANTO ACEITAR REQUISIÇÃO GET
        // somente requisições POST
        if( ! request.getMethod().toUpperCase().equals('POST') )
        {
            res.status=1
            res.msg='Requisições '+request.getMethod().toUpperCase()+' não permitida!'
            renderRes()
            return false
        }
*/
        return true
    }

    def index() {
        render 'Ajax controller chamado'
    }

    def ping()
    {
        render ''
    }

    def checkEmail()
    {
        if( params.email )
        {
            Integer qtd = Pessoa.countByDeEmail( params.email.trim() )
            if( qtd > 0)
            {
                res.status=1
                res.msg='Email "'+params.email.trim()+'" já está cadastrado!'
            }
        }
        renderRes()
    }

    def getInstitutions()
    {
        List options = []
        if( params.q ) {
            params.q = '%'+params.q+'%'
            sqlService.execSql("""select i.sq_pessoa, i.sg_instituicao, p.no_pessoa from salve.instituicao i
                inner join salve.pessoa p on p.sq_pessoa = i.sq_pessoa
                where i.sg_instituicao ilike :q or p.no_pessoa ilike :q
            """, [q:params.q]).sort{it.no_pessoa}.each {
                options.push([value:it.sq_pessoa,label:it.no_pessoa,sigla:it.sg_instituicao])
            }
        }
        render options as JSON
    }

    def getRefBib()
    {
        Map data = ['items': []];
        if( ! params.q )
        {
            render 'sem parâmetro para pesquisar'
            return
        }
        List lista = publicacaoService.search(params.q, params.contexto, params.inListaOficialVigente=='true');
        if (lista) {
            lista.each {
                data.items.push(['id'       : it.id,
                                 'titulo'   : it.deTitulo,
                                 'autor'    : it.noAutor,
                                 'ano'      : it.nuAnoPublicacao,
                                 'citacao'  : it.referenciaHtml,
                                 'descricao': it.tituloAutorAno
                ]);
            }
        }
        render data as JSON;
    }
}
