package br.gov.icmbio

class FaleConoscoController extends BaseController {

    def emailService

    def index() {
    	render ( view:'index')
     }

   def save()
   {
       if( !params.fldMensagem )
       {
           res = [status:1,msg:'',errors:['Mensagem não pode ser em branco!']];
       }
       else
       {
           if( emailService.sendFaleConosco(params.fldMensagem,params.fldEmail) )
           {
               res = [status: 0, msg: 'Email foi enviado com SUCESSO!']
           }
           else
           {
               res = [status: 1, errors:['Erro no envio do e-mail. Tente mais tarde.'] ]
           }
        }
   		renderRes()
   }
}

