class BootStrap {
    def grailsApplication

    def init = { servletContext ->

        if( !  grailsApplication?.config?.url?.salve?.consulta ) {
             grailsApplication.config.url.salve.consulta = 'http://localhost:8090/salve-consulta/'
        }

        println ' '
        println ' '
        println '*'*100
        println 'Salve Estadual - Consulta Incializado'
        println '-'*100
        println 'Url: ' + grailsApplication.config.url.salve.consulta
        println '-'*100
        println 'Configuracoes para envio de email'
        println '-'*100

        // verificar configurações para envio de email
        if( !grailsApplication?.config?.email?.host )
        {
            println 'Dados para envio de email não configurado'
        }
        else
        {
            println 'email.host....: '+ grailsApplication?.config?.email?.host
            println 'email.username: '+ grailsApplication?.config?.email?.username
            println 'email.password: '+ grailsApplication?.config?.email?.password
            println 'email.from....: '+ grailsApplication?.config?.email?.from
            println 'email.port....: '+ grailsApplication?.config?.email?.port
        }
        println '-'*100
        println ' '
        println ' '
        println ' '
    }
    def destroy = {
    }
}
