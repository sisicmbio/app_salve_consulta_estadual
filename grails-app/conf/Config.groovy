import org.apache.log4j.*
// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

default_config = "/data/salve-estadual/config.properties"

if( ! new File( default_config ).exists() )
{
    println 'Ops!!! Arquivo de configuracoes nao encontrado: '+default_config;
    println '';
    println '# ------------------------------------------------------------';
    println '# Exemplo para conexao com postgre';
    println 'dataSource.url=jdbc:postgresql://10.197.93.14/db_dev_cotec';
    println 'dataSource.username=usuario';
    println 'dataSource.password=senha';
    println 'dataSource.configClass=br.gov.icmbio.ForeingKeyRename';
    println 'dataSource.dbCreate=';
    println '#------------------------------------------------------------'
    System.exit(1);
}

grails.gorm.failOnError = false
grails.gorm.autoFlush = false
grails.project.groupId = 'br.gov.icmbio'

// nenhuma configura externa
grails.config.locations = []
grails.config.locations.add( "file:" + default_config );

grails.databinding.dateFormats = [
        'dd-MM-yyyy', 'dd-MM-yyyy HH:mm:ss.S', "dd-MM-yyyy'T'hh:mm:ss'Z'"
]


// The ACCEPT header will not be used for content negotiation for user agents containing the following strings (defaults to the 4 major rendering engines)
grails.mime.disable.accept.header.userAgents = ['Gecko', 'WebKit', 'Presto', 'Trident']
grails.mime.types = [ // the first one is the default format
    all:           '*/*', // 'all' maps to '*' or the first available format in withFormat
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    hal:           ['application/hal+json','application/hal+xml'],
    xml:           ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// Legacy setting for codec used to encode data with ${}
grails.views.default.codec = "html"

// The default scope for controllers. May be prototype, session or singleton.
// If unspecified, controllers are prototype scoped.
grails.controllers.defaultScope = 'singleton'

// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside ${}
                scriptlet = 'html' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        // filteringCodecForContentType.'text/html' = 'html'
    }
}


grails.converters.encoding = "UTF-8"
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

// configure passing transaction's read-only attribute to Hibernate session, queries and criterias
// set "singleSession = false" OSIV mode in hibernate configuration after enabling
grails.hibernate.pass.readonly = false
// configure passing read-only to OSIV session by default, requires "singleSession = false" OSIV mode
grails.hibernate.osiv.readonly = false

environments {
    development {
        grails.logging.jul.usebridge = true
    }
    production {
        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.com"
    }
}

// log4j configuration
/*
log4j.main = {
    error  'org.codehaus.groovy.grails.web.servlet',        // controllers
           'org.codehaus.groovy.grails.web.pages',          // GSP
           'org.codehaus.groovy.grails.web.sitemesh',       // layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping',        // URL mapping
           'org.codehaus.groovy.grails.commons',            // core / classloading
           'org.codehaus.groovy.grails.plugins',            // plugins
           'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'
}
*/

log4j.main = {
    // https://logging.apache.org/log4j/1.2/apidocs/org/apache/log4j/PatternLayout.html
    def pattern = new PatternLayout("[%p] [%d{ISO8601}] [%c{1}] %m%n")
    appenders {
        appender new DailyRollingFileAppender(
                name:"file",
                file:"/data/salve-estadual/logs/salve_consulta.log",
                layout: pattern,
                datePattern: "'.'yyyy-MM-dd")
        console name:"stdout",
                layout: pattern
    }
    environments {
        development {
            root {
                info "file","stdout"
            }
        }
        production  {
            root {
                info "file"
            }
        }
    }

    all  'grails.app'

    debug  'org.codehaus.groovy.grails.web.servlet'

    info 'org.codehaus.groovy.grails.web.pages',          // GSP
         'org.codehaus.groovy.grails.web.sitemesh'       // layouts
    //'org.codehaus.groovy.grails.web.servlet'        // controllers

    warn    'org.springframework',
            'org.hibernate',
            'groovyx.net.http'

    error   'org.codehaus.groovy.grails.web.servlet',        // controllers
            'org.codehaus.groovy.grails.web.pages',          // GSP
            'org.codehaus.groovy.grails.web.sitemesh',       // layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping',        // URL mapping
            'org.codehaus.groovy.grails.commons',            // core / classloading
            'org.codehaus.groovy.grails.plugins',            // plugins
            'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate'
}

// configuracao do gzip - https://github.com/double16/grails-ziplet/blob/master/README.md
grails.ziplet.enabled = true

assets {
    minifyOptions = [
        languageMode: 'ES6',
        targetLanguage: 'ES5',
        optimizationLevel: 'SIMPLE'
    ]
}

assets {
    minifyJs: false
}


assets {
    configOptions = [
        babel: [
            enabled: false,
            processJsFiles: false  // add if you want to transpile '*.js'
        ]
    ]
}
