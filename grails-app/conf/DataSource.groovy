dataSource {
  pooled = true
  driverClassName = "org.postgresql.Driver"
  //dialect="org.hibernate.dialect.PostgreSQLDialect"
  dialect="org.hibernate.spatial.dialect.postgis.PostgisDialect"
  formatSql=false
  logSql=false
  loggingSql=false
  useSqlComments=false
}


hibernate {
  cache.use_second_level_cache = true
  cache.use_query_cache = false
  //cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory' // Hibernate 3
  cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
}

// environment specific settings
environments
{
    development {
        //dataSource {
            //dbCreate = "" // one of 'create', 'create-drop', 'update', 'validate', ''
            // dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''

            // casa
            /**  /
                  url = "jdbc:postgresql://localhost/db_dev_cotec"
                  username = "usr_developer"
                  password = "icmbio2016"
      			/**/

      			// icmbio
            /** /
      			url = "jdbc:postgresql://10.197.93.14/db_dev_cotec"
      			username = "luis.barbosa"
      			password = "icmbio2016"
      			/**/

      			// alteração para criação de nomes amigaveis para as foreing keys
      			// configClass = 'br.gov.icmbio.ForeingKeyRename'
        //  }
    }
    test {
        /*dataSource {
            dbCreate = "update"
            url = "jdbc:postgresql://localhost:5432/postgres"
        }
        */
    }
    production {
        /*dataSource {
            dbCreate = ""
  			// icmbio
  			url = "jdbc:postgresql://10.197.93.14/db_dev_cotec"
  			username = "luis.barbosa"
  			password = "icmbio2016"
        properties {
              maxActive = -1
              minEvictableIdleTimeMillis=1800000
              timeBetweenEvictionRunsMillis=1800000
              numTestsPerEvictionRun=3
              testOnBorrow=true
              testWhileIdle=true
              testOnReturn=false
              validationQuery="SELECT 1"
              jdbcInterceptors="ConnectionState"
          }
          // alteração para criação de nomes amigaveis para as foreing keys
          configClass = 'br.gov.icmbio.ForeingKeyRename'
          */
   }
}
