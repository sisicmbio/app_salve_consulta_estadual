class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "/login"(controller:'app', action:'login')
        "/logout"(controller:'app', action:'logout')

        "/ativar/$id"(controller:'app', action:"ativar")
        "/forgotPassword"(controller:'app', action:"forgotPassword")
        "/resetPassword/$id?"(controller:'app', action:"resetPassword")
        "/ficha/getAnexo/$id/$thumb?"( controller:"ficha", action:"getAnexo")
        "/ficha/getMultimidia/$id/$thumb?"( controller:"ficha", action:"getMultimidia")
        "/getMultimidia/$id/$thumb?"( controller:"ficha", action:"getMultimidia")
        "/sobre"( controller:"app", action:"sobre")
        "/links"( controller:"app", action:"links")

        "500"(view:'/error')
	}
}
