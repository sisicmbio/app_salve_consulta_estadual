<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>SALVE-Estadual consulta</title>
	</head>
	<body>

    %{--INICIAR NA SESSION A VARIAVEL sicae.user para EVITAR ERRO AO VERIFICAR SE O USUARIO ESTA LOGADO --}%
    <div style="display: none">
        <g:if test="${! session?.sicae }">
            ${session.sicae=[user:[:]]}
        </g:if>
    </div>

    <!-- view: /views/index.gsp -->
    <div class="wrap" id="div-wrap">
		<div class="row">
            %{-- <div class="small-12 medium-9 large-9 columns"> --}%
			<div class="small-12 medium-12 large-12 columns">
                <g:if test="${session.message}">
                    <div class="callout alert text-center">
                        <h3>${session.message}</h3>
                            ${session.message=null}
                    </div>
                </g:if>

            <g:if test="${ ! session?.sicae?.user }">
                <div class="row">
                    <div class="small-12 column">
                        <div class="callout alert" style="font-size:1.2rem;">
                            <p style="text-align:center;"><b>Para enviar suas informações, é necessário registrar-se e efetuar o login.</b></p>
                        </div>
                    </div>
                </div>
            </g:if>


        %{-- INICIO ACCORDION --}%
                <ul class="accordion" data-accordion data-multi-expand="true" data-reset-on-close="true" data-allow-all-closed="true">
                  <li class="accordion-item is-active" data-accordion-item>
                    <!-- Accordion tab title -->
                    <a href="#" class="accordion-title"><h5>O que é o SALVE</h5></a>
                    <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                    <div class="accordion-content" data-tab-content>
                        <p style="text-align:justify;">O Sistema de Avaliação do Risco de Extinção da Biodiversidade – SALVE foi elaborado para facilitar o processo de avaliação da fauna brasileira, funcionando como uma base de dados das espécies avaliadas e como uma ferramenta para o controle e o acompanhamento das diferentes etapas do processo. O módulo de consultas do Sistema permite que qualquer pessoa envie informações sobre as espécies que estão em processo de avaliação. As contribuições serão armazenadas e posteriormente analisadas pela equipe do ICMBio em conjunto com o Coordenador de Táxon do grupo e demais especialistas da comunidade científica.</p>
                    </div>
                  </li>

                  <li class="accordion-item" data-accordion-item>
                    <!-- Accordion tab title -->
                    <a href="#" class="accordion-title"><h5>O Processo de Avaliação da Fauna Brasileira</h5></a>
                    <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                    <div class="accordion-content" data-tab-content>
                        <p style="text-align:justify;">A Portaria MMA n° 43/2014, que institui o Programa Nacional de Conservação das Espécies Ameaçadas de Extinção – Pró-Espécies, estabelece que o Instituto Chico Mendes é responsável pelo processo de Avaliação do Estado de Conservação da Fauna Brasileira, cujos resultados subsidiam o Ministério do Meio Ambiente na publicação das Listas Nacionais Oficiais de Espécies da Fauna Ameaçadas de Extinção. O Instituto Chico Mendes conduz o processo desde 2009, quando foi iniciado o primeiro ciclo de avaliação, tendo avaliado 12.254 táxons da fauna brasileira, incluindo todos os vertebrados conhecidos até 2014. O processo é normatizado pela Instrução Normativa n° 34/2013 do Instituto, que define as suas diretrizes gerais, a metodologia utilizada, os atores envolvidos e suas funções, as etapas e os prazos. O Sistema SALVE foi elaborado observando-se essas diretrizes.</p>
                        <p style="text-align:justify;">Basicamente, o processo inicia-se com a compilação de informações relevantes para a avaliação e elaboração de um mapa de distribuição, incluindo uma etapa de consulta aos especialistas e à sociedade como um todo. Em seguida, as espécies são classificadas em alguma categoria de risco, de acordo com a metodologia de categorias e critérios da IUCN, por especialistas em uma oficina de trabalho. Por fim, os resultados ainda passam por uma validação, na qual é verificada a consistência da aplicação do método. Mais informações sobre o processo podem ser obtidas no “Roteiro Metodológico para avaliação do Estado de Conservação das Espécies da Fauna Brasileira”.</p>
                        <img class="img-responsive" src="images/etapas_processo_avaliacao.png" alt="Imagem Etapas"></img>
                    </div>
                  </li>


                  <li class="accordion-item" data-accordion-item>
                    <!-- Accordion tab title -->
                    <a href="#" class="accordion-title"><h5>O Método de Avaliação</h5></a>
                    <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                    <div class="accordion-content" data-tab-content>
                        <p style="text-align:justify;">
                            Para avaliar o risco de extinção das espécies é utilizado o método desenvolvido pela União Internacional para a Conservação da Natureza (IUCN), amplamente utilizado em avaliações do estado de conservação de espécies em nível global e já adotada por diversos países. O método classifica as espécies em categorias, utilizando critérios objetivos e quantitativos, que consideram informações sobre a distribuição da espécie, tamanho e tendência populacionais, ecologia, como uso do habitat, ameaças que incidem sobre a espécie e medidas de conservação já existentes. O método foi inicialmente desenvolvido para ser aplicado em nível global, mas, para espécies não endêmicas do país, pode ser utilizado em nível nacional observando-se certos ajustes. Maiores informações e detalhes sobre o método podem ser obtidos nos materiais originais da IUCN:
                            <ul>
                            <li><a href="http://s3.amazonaws.com/iucnredlist-newcms/staging/public/attachments/3108/redlist_cats_crit_en.pdf" target="_blank">IUCN Red List Categories and Criteria: Version 3.1 (2001)</a>;</li>
                            <li><a href="http://cmsdocs.s3.amazonaws.com/RedListGuidelines.pdf" target="_blank">Guidelines for Using the IUCN Red List Categories and Criteria. Version 13 (2017)</a>;</li>
                            <li><a href="http://cmsdocs.s3.amazonaws.com/keydocuments/Reg_Guidelines_en_web%2Bcover%2Bbackcover.pdf" target="_blank">Guidelines for Application of IUCN Red List Criteria at Regional and National Levels: Version 4.0. (2012)</a>.</li>
                            </ul>
                        </p>
                        <p class="text-align:text-center">
                            <img class="img-responsives" src="images/categorias_iucn.png" style="width:100%;height:100%;border:none;" alt="Categorias IUCN"></img>
                        </p>
                    </div>
                  </li>

                  <li class="accordion-item" data-accordion-item>
                    <!-- Accordion tab title -->
                    <a href="#" class="accordion-title"><h5>Listas Nacionais de Espécies Ameaçadas de Extinção</h5></a>
                    <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                    <div class="accordion-content" data-tab-content>
                        <p style="text-align:justify;">As listas nacionais de espécies ameaçadas são um dos principais instrumentos para a conservação de espécies, com efeitos de restrição de uso e priorização de ações de conservação e recuperação das populações.</p>
                        <p>Em 2014 o Ministério do Meio Ambiente publicou as atuais Listas Nacionais Oficiais de Espécies da Fauna Ameaçadas de Extinção. A Portaria <a href="http://www.icmbio.gov.br/portal/images/stories/biodiversidade/fauna-brasileira/avaliacao-do-risco/PORTARIA_N%C2%BA_444_DE_17_DE_DEZEMBRO_DE_2014.pdf" target="_blank">MMA n°444</a> lista as espécies de mamíferos, aves, répteis, anfíbios e invertebrados terrestres, e a Portaria <a href="http://www.icmbio.gov.br/cepsul/images/stories/legislacao/Portaria/2014/p_mma_445_2014_lista_peixes_amea%C3%A7ados_extin%C3%A7%C3%A3o.pdf" target="_blank">MMA n°445</a>, os peixes e invertebrados aquáticos. As listas foram elaboradas a partir do processo de avaliação conduzido pelo ICMBio entre 2009 e 2014, no qual foram avaliados todos os vertebrados que ocorrem em território nacional e um significativo número de invertebrados, totalizando 12.254 táxons, no maior esforço global para avaliar o risco de extinção de espécies em nível nacional. Com essas listas, o Brasil passou a ter um diagnóstico fidedigno da situação de conservação de nossas espécies. A lista das espécies ameaçadas, com algumas informações adicionais, também pode ser vista no Sumário Executivo do <a href="http://www.icmbio.gov.br/portal/images/stories/comunicacao/publicacoes/publicacoes-diversas/dcom_sumario_executivo_livro_vermelho_ed_2016.pdf" target="_blank">Livro Vermelho da Fauna Brasileira Ameaçada de Extinção</a>, publicado em 2016.</p>
                    </div>
                  </li>
                </ul>
                %{-- FIM ACCORDION --}%
            </div>

            %{-- CONSULTAS AMPLAS ABERTAS--}%
            <fieldset class="small-12 column" v-if="listaConsultas.length > 0 " id="fsConsultas">
                <legend title="Clique nos nomes abaixo para ativar/desativar o filtro correspondente">Consultas / Revisões abertas</legend>
                <div class="row">
                    <div class="small-12 columns nomes-consultas">
                        <a v-for="consulta in listaConsultas"
                             @click.prevent="filtrarEspeciesConsulta(consulta)"
                             :href="consulta.sgConsulta"
                             :key="consulta.sqCicloConsulta"
                             :title="'Clique para selecionar as fichas (' + consulta.dsSistema + ')'"
                             :class="{selected : filtroConsulta.sqCicloConsulta == consulta.sqCicloConsulta}"
                              class="text-center no-wrap">{{consulta.sgConsulta}}</a>
                    </div>
                </div>
            </fieldset>

%{--
			<div class="small-12 medium-9 large-3 columns hidden">
				<h4>Estatísticas Gerais</h4>
				<div class="row">
					<div class="small-2 columns">Total</div>
					<div class="small-10 columns"><b>1000 Fichas</b></div>
				</div>
				<div class="row">
					<div class="small-2 columns">(VU)</div>
					<div class="small-10 columns"><b>20</b></div>
				</div>
				<div class="row">
					<div class="small-2 columns">(EN)</div>
					<div class="small-10 columns"><b>120</b></div>
				</div>
			</div> --}%
		</div>


        %{-- Filtros para as fichas --}%
        <div class="row" v-if="pesquisaVisible" id="rowFiltro">
            <div class="small-12 medium-12 columns">
                <fieldset>
                    %{--<legend class="sub-legend">Pesquisar</legend>
                    <div class="input-group">
                        <input v-model="filtroNoCientifico" id="inputPesquisa" class="input-group-field input-filter blue" type="search" placeholder="Pesquisar por...">
                    </div>--}%

                    <fieldset style="margin:0px;">
                        <legend class="sub-legend pointer"
                                @click="pesquisaAvancadaVisible = !pesquisaAvancadaVisible" title="Exibir/Esconder pesquisa avançada.">Busca avançada&nbsp;<i class="i-click" :class="pesquisaAvancadaVisible?'fi-arrow-up':'fi-arrow-down'"></i></legend>
                        <div id="div_busca_avancada" v-if="pesquisaAvancadaVisible">
                            <div class="row">
                                <div class="small-12 medium-3 columns">
                                    <label>Nível Taxonômico</label>
                                    <select v-model="filtroNoNivel">
                                        <option value="">-- todos --</option>
                                        <option value="noReino">Reino</option>
                                        <option value="noFilo">Filo</option>
                                        <option value="noClasse">Classe</option>
                                        <option value="noOrdem">Ordem</option>
                                        <option value="noFamilia">Família</option>
                                        <option value="noGenero">Gênero</option>
                                        <option value="noEspecie">Espécie</option>
                                    </select>
                                </div>
                                <div class="small-12 medium-6 columns">
                                    <div class="input-group">
                                        <label>Taxon</label>
                                        <input :disabled="filtroNoNivel==''"
                                               v-model="filtroDeNivel"
                                               @keyup.enter="aplicarFiltro"
                                               @keyup="cancelarFiltro"
                                               type="text" class="input-group-field input-filter blue"
                                               style="max-width:400px;display:inline;color:blue;"
                                               placeholder="nome...">
                                         &nbsp;<input :disabled="filtroNoNivel==''" type="button"
                                               @click="aplicarFiltro"
                                            title="Clique aqui para aplicar o filtro ou pressione a telcla <enter> no campo taxon."
                                               class="button" style="margin:0px;" value="Aplicar"/>
                                    </div>
                                </div>
                                <div class="small-12 medium-3 columns">
                                </div>
                            </div>

                            %{-- GRUPO E SUBGRUPO--}%
                            <div class="row">
                                <div class="small-12 medium-3 columns">
                                    <label>Grupo Avaliado</label>
                                    <select v-model="filtroSqGrupo" @change="updateSubgrupos" style="color:blue;">
                                        <option v-for="grupo in listaGruposAvaliados" :value="grupo.sqGrupo" :key="grupo.sqGrupo">{{grupo.dsGrupo}}</option>
                                    </select>
                                </div>
                                <div class="small-12 medium-6 columns">
                                    <div class="input-group">
                                        <label>Subgrupo</label>
                                        <select v-model="filtroSqSubgrupo" :disabled="listaSubgruposAvaliados.length < 2"
                                                style="color:blue;max-width:400px;display:inline;"
                                                @change="aplicarFiltro">
                                            <option v-for="subgrupo in listaSubgruposAvaliados" :value="subgrupo.sqSubgrupo" :key="subgrupo.sqSubgrupo">{{subgrupo.dsSubgrupo}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="small-12 medium-3 columns">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </fieldset>
            </div>
        </div>

        <!-- Minhas Fichas  - Consulta direta -->
        <div class="row" v-if="listaFichasConsultaDireta.length > 0">
            %{-- <div class="small-12 medium-9 large-9 columns"> --}%
            <div class="small-12 medium-12 large-12 columns">

                <fieldset style="margin-top:20px;">
                    <legend class="sub-legend pointer" style="color:darkgreen"
                            @click="fichasConsultaDiretaRevisaoVisible = ! fichasConsultaDiretaRevisaoVisible" title="Exibir/Esconder minhas fichas."><span style="color:#148cff;">Minhas fichas</span>&nbsp;<i class="i-click" :class="fichasConsultaDiretaRevisaoVisible ? 'fi-arrow-up':'fi-arrow-down'"></i></legend>
                </fieldset>

                <div v-show="fichasConsultaDiretaRevisaoVisible">

                    %{-- TABELA DAS FICHAS EM REVISÃO --}%
                    <fieldset class="sub-fieldset" v-show="filtrarListaRevisao.length > 0 ">
                        <legend class="sub-legend2 pointer" style="color:darkgreen"
                                @click="fichasConsultaRevisaoVisible = !fichasConsultaRevisaoVisible" title="Exibir/Esconder fichas.">
                                <i class="fi-asterisk medium"></i> Em revisão&nbsp;<i class="i-click"
                                :class="fichasConsultaRevisaoVisible ? 'fi-arrow-up':'fi-arrow-down'"></i></legend>
                            <div v-show="fichasConsultaRevisaoVisible">
                                <div>
                                    <label>Pesquisar minhas fichas em revisão: </label>
                                    <input type="text" v-model="filtroPesquisaRevisao" class="blue" style="display:inline" value="" placeholder="por..."/>
                                </div>
                                <table id="tableFichasRevisao"  class="striped table-scroll">
                                    <thead style="background-color:#f3f2c5">
                                        <tr>
                                        <th class="text-center" style="width:400px;min-width:400px;max-width:400px;">Nome Científico</th>
                                        <th class="text-center" style="width:auto">Nome Comum</th>
                                        <th class="text-center" style="width:100px;min-width:100px;max-width:100px;">Prazo</th>
                                        <g:if test="${session?.sicae?.user}">
                                            <th class="text-center" style="width:100px;min-width:100px;max-width:100px;">Ação</th>
                                        </g:if>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="hover" data-tipo="CONSULTA_DIRETA"
                                                v-for="(item,key) in filtrarListaRevisao">
                                            <template v-if="item.codigoSistema == 'REVISAO_POS_OFICINA'">
                                                <td v-html="(key+1)+'&nbsp;-&nbsp;' + item.noCientifico"></td>
                                                <td v-text="item.noComum"></td>
                                                <td class="text-center">{{item.dtFim}}</td>
                                                <g:if test="${session?.sicae?.user}">
                                                    <td class="text-center">
                                                        <a @click="colaborarFicha(item)" class="button primary btn-table">Colaborar</a>
                                                    </td>
                                                </g:if>
                                            </template>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                    </fieldset>

                    %{-- TABELA DAS CONSULTAS DIRETAS--}%
                    <fieldset class="sub-fieldset" v-show="filtrarListaDireta.length > 0">
                        <legend class="sub-legend2 pointer" style="color:darkgreen"
                           @click="fichasConsultaDiretaVisible = !fichasConsultaDiretaVisible" title="Exibir/Esconder fichas.">
                            <i class="fi-asterisk medium"></i> Em consulta direta&nbsp;<i class="i-click"
                           :class="fichasConsultaDiretaVisible ? 'fi-arrow-up':'fi-arrow-down'"></i></legend>
                         <div v-show="fichasConsultaDiretaVisible">
                                <div><label>Pesquisar minhas fichas em consulta direta: </label>
                                    <input type="text" v-model="filtroPesquisaDireta" class="blue" style="display:inline" value="" placeholder="por...">
                                </div>
                                 <table id="tableMinhasFichas"  class="striped table-scroll">
                                    <thead style="background-color:#b0c4de;">
                                    <tr>
                                        <th class="text-center" style="width:400px;min-width:400px;max-width:400px;">Nome Científico</th>
                                        <th class="text-center" style="width:*">Nome Comum</th>
                                        <th class="text-center" style="width:100px;min-width:100px;max-width:100px;">Prazo</th>
                                        <g:if test="${session?.sicae?.user}">
                                            <th class="text-center" style="width:100px;min-width:100px;max-width:100px;">Ação</th>
                                        </g:if>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="hover" data-tipo="CONSULTA_DIRETA" v-for="(item,key) in filtrarListaDireta">
                                        <td v-html="(key+1)+'&nbsp;-&nbsp;' + item.noCientifico"></td>
                                        <td v-text="item.noComum"></td>
                                        <td class="text-center">{{item.dtFim}}</td>
                                        <g:if test="${session?.sicae?.user}">
                                            <td class="text-center">
                                                <a @click="colaborarFicha(item)" class="button primary btn-table">Colaborar</a>
                                            </td>
                                        </g:if>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                     </fieldset>
               </div>
            </div>
            %{-- <div class="small-12 medium-9 large-3 columns">
            </div> --}%
        </div>

        %{--ESPECIES EM CONSULTA AMPLA--}%

        <div class="row" v-if="listaFichasConsultaAmpla.length > 0">
            <div class="small-12 medium-12 columns">
                <fieldset style="margin-top:20px;">
                    <legend class="sub-legend pointer"
                            @click="toggleFichasConsultaAmpla()"
                            title="clique para abrir/fechar a listagem!">Espécie(s) em consulta ampla&nbsp;<i class="i-click" :class="fichasConsultaAmplaVisible ? 'fi-arrow-up':'fi-arrow-down'"></i>
                    </legend>
                    <div v-show="fichasConsultaAmplaVisible">
                       <div><label>Pesquisar ficha em consulta ampla: </label>
                           <input type="text" v-model="filtroPesquisaAmpla" class="blue" style="display:inline" value="" placeholder="por..."></div>
                        <div>
                            <table class="striped" id="tableFichasConsultaAmpla">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width:400px;min-width:400px;max-width:400px;">Nome Científico</th>
                                    <th class="text-center" style="width:*">Nome Comum</th>
                                    <th class="text-center" style="width:100px;min-width:100px;max-width:100px;">Prazo</th>
                                    <g:if test="${session?.sicae?.user}">
                                        <th class="text-center" style="width:100px;min-width:100px;max-width:100px;">Ação</th>
                                    </g:if>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(item,key) in filtrarLista" class="hover" >
                                    <td v-html="(key+1)+'&nbsp;-&nbsp;' + item.noCientifico"></td>
                                    <td v-text="item.noComum"></td>
                                    <td class="text-center  ">{{item.dtFim}}</td>
                                    <g:if test="${session?.sicae?.user}">
                                        <td class="text-center">
                                            <a @click="colaborarFicha(item)" class="button primary btn-table">Colaborar</a>
                                        </td>
                                    </g:if>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </fieldset>
            </div>
            %{-- <div class="small-12 medium-3 columns">
            </div> --}%
        </div>
        <div v-else class="row">
            <div class="small-12 medium-12 columns" id="div_loading_fichas">
                <h3><span>Carregando fichas...&nbsp;<i class="fa fa-spinner fa-spin"></i><span></h3>
            </div>
        </div>

    </div>
    <content tag="javascript">
        <script type="text/javascript" src="${assetPath(src: 'inicial.js')}"></script>
    </content>
    <form method="post" action="ficha/colaborar" name="frmPost" id="frmPost" style="visibility:hidden">
        <input type="hidden" name="sqConsultaFicha" id="sqConsultaFicha">
        <input type="hidden" name="codigoSistema" id="codigoSistema">
    </form>
    <footer>
        <div style="min-height:400px;">&nbsp;</div>
    </footer>
	</body>
</html>
