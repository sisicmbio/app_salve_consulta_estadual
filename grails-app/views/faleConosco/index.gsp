<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main"/>
        <title>SALVE-Fale Conosco</title>
    </head>
    <body>
        <!-- view: /views/faleConosco/index.gsp -->
        <div class="row align-middle" id="div-page-faleconosco" style="display:none;">
            <div class="large-centered large-8 small-12 columns">
                <fieldset>
                    <legend>Fale Conosco</legend>
                    <br/>
                     <form id="frmFaleConosco" name="frmFaleConosco" action="faleConosco/save">
                        <div class="row">
                            <div class="small-12 columns">
                                     %{-- mensagem erro --}%
                                    <div class="callout success" id="divMsg" :class="{ hidden: !errorMsg }">
                                        <button @click="errorMsg=''" aria-label="Fechar mensagem" class="close-button" type="button">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <h5>{{errorMsg}}</h5>
                                    </div>
                               <label for="fldEmail">
                                    Seu Email
                                </label>
                                <input id="fldEmail" name="fldEmail" placeholder="Email para resposta (opcional)" type="email" v-model="fields.email"/>
                                <label for="fldMensagem">
                                    Mensagem
                                </label>
                                <textarea id="fldMensagem" rows="10" cols="100" name="fldMensagem" placeholder="Texto da mensagem" required="true" v-model="fields.mensagem"></textarea>
                            </div>
                        </div>
                        <!-- botoes -->
                        <div class="row">
                            <div class="small-12 columns">
                                <button @click="enviar" class="primary button">Enviar</button>
                            </div>
                        </div>

                     </form>

                </fieldset>
            </div>
        </div>
        <content tag="javascript">
        <script type="text/javascript" src="${assetPath(src: 'faleConosco.js')}"></script>
        </content>
    </body>
</html>