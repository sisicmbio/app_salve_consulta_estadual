<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>SALVE-Colaboração</title>
    <!--[if IE]>
		<g:javascript src="openlayers/zip2shp/lib/jszip-utils-ie.js" />
	<![endif] -->
</head>

<body>
    <!-- view: /views/ficha/colaborarFicha.gsp -->

    <div class="point-popup" id="pointPopup1" style="display:none;">
        <h2>DivPopup1</h2>
    </div>
    <div class="point-popup" id="pointPopup2" style="display:none;">
        <h2>DivPopup2</h2>
    </div>
    <g:if test="${consultaFicha.ficha}">
        <g:set var="ultimaAvaliacao" value="${consultaFicha.ficha.ultimaAvaliacaoNacionalText}"></g:set>

        <input type="hidden" id="sqConsultaFicha" value="${consultaFicha.id}">
        <input type="hidden" id="sqFicha" value="${consultaFicha.ficha.id}">
        %{----tipo_consulta----}%
        %{--<input type="hidden" id="sqTipoConsulta" value="${tipoConsulta.id}">--}%

        <div v-if="visible" class="row align-middle" id="div-page-colaboracao">
            <div class="large-centered small-12 columns">
                <fieldset id="fieldset-colaborar" style="height:5000px;">

                    <legend class="nome-taxon">${raw(consultaFicha.ficha.taxon.noCientificoCompletoItalico)}</legend>

                    <g:if test="${tipoConsultaSistema=='REVISAO_POS_OFICINA'}">
                        <br>
%{--                        <g:if test="${( ultimaAvaliacao =~ /Ano:<\/span>&nbsp;<span class="fvalue">[0-9]{2,4}/)}">--}%
                            <h3 class="header">ÚLTIMA AVALIAÇÃO NACIONAL</h3>
                            ${raw(ultimaAvaliacao)}
                        <br>
%{--                        </g:if>--}%
                    </g:if>

                    %{--ARVORE ESPECIE SELECIONADA--}%

                   %{--  <nav id="breadcrumb" aria-label="Espécie selecionada" role="navigation">
                        <ul class="breadcrumbs">
                            <g:each var="item" in="${consultaFicha?.ficha?.breadcrumb}">
                                <li>${raw(item)}</li>
                            </g:each>
                        </ul>
                    </nav> --}%

                    <h3 class="header">CLASSIFICAÇÃO TAXONÔMICA</h3>
                    <table class="table table-no-stripped table-ficha-completa taxonomia-nivel" style="xmax-width:400px">
                        <tr>
                            <td class="nivel" style="width:100px;">Filo:</td>
                            <td class="nome" style="Width:auto"><b>${consultaFicha.ficha?.taxon?.structure?.noFilo}</b></td>
                            <td rowspan="6" style="width:300px;height:auto;vertical-align: middle;text-align: right">
                                    %{--exibir as imagens principal da espécie --}%
                                    ${raw( consultaFicha?.ficha?.imagemPrincipalHtml ) }
                            </td>
                        </tr>
                        <tr>
                            <td class="nivel">Classe:</td>
                            <td class="nome"><b>${consultaFicha.ficha?.taxon?.structure?.noClasse}</b></td>
                        </tr>
                        <tr>
                            <td class="nivel">Ordem:</td>
                            <td class="nome"><b>${consultaFicha.ficha?.taxon?.structure?.noOrdem}</b></td>
                        </tr>
                        <tr>
                            <td class="nivel">Família:</td>
                            <td class="nome"><b>${consultaFicha.ficha?.taxon?.structure?.noFamilia}</b></td>
                        </tr>
                        <tr>
                            <td class="nivel">Gênero:</td>
                            <td class="nome"><b><i>${consultaFicha.ficha?.taxon?.structure?.noGenero}</i></b></td>
                        </tr>
                        <g:if test="${consultaFicha.ficha?.taxon?.structure?.noSubespecie}">
                            <tr>
                                <td class="nivel">Espécie:</td>
                                <td class="nome"><b><i>${consultaFicha.ficha?.taxon?.structure?.noGenero+' '+consultaFicha.ficha?.taxon?.structure?.noEspecie}</i></b></td>
                            </tr>
                            <tr>
                                <td class="nivel">Subespécie:</td>
                                <td class="nome"><b><i>${consultaFicha.ficha?.taxon?.noCientifico}</i></b></td>
                            </tr>
                        </g:if>
                        <g:else>
                            <tr>
                                <td class="nivel">Espécie:</td>
                                <td class="nome"><b><i>${consultaFicha.ficha?.taxon?.noCientifico}</i></b></td>
                            </tr>
                        </g:else>
                    </table>

                    <br>
                    %{-- FOTOS --}%
                    <div class="text-left">
                        <div id="foto-colaboracao-container" class="topic pointer" >Fotos
                            <i class="tooltipster fi-info" title="<p class='text-justify'>Clique no botão [Adicionar foto] para colaborar com fotos da espécie.</p>"></i>
                        </div>
  
                            
                        <div class="subtopic">
                            <div class="row" style="padding-left:15px">                                    
                                <a @click.prevent="vmFotoColaboracao.show()" class="button medium" href="#" title="Clique aqui para adicionar uma nova foto">
                                    <i @click="vmFotoColaboracao.show()" class="fi-plus pointer" title="Adicionar Foto"></i> 
                                    Adicionar foto
                                </a>                                                                                                                    
                            </div>
                            <legend id="legend_meus_registros" style="padding-left:10px" @click="vmFotoColaboracao.toggleFotoColaboracao()">
                                Minha(s) Foto(s)&nbsp;<i class="fi-list-thumbnails"></i>
                            </legend>
                            
                            <div :class="fotosColaboracaoVidible ? 'sem-classe' : 'hidden'" id="divGridFotosColaboracao" style="height:auto;overflow:hidden;width:900px;">                                                                                                           
                                <div style="width:100%;height: auto;max-height: 500px;overflow: auto;border:1px solid silver;">
                                    <table class="table striped meus-registros hover gride" id="grideRegistrosFotosColaboracao" style="width:100%;">
                                        <!-- view: /views/ficha/colaborarFicha.gsp -->
                                        <!-- controller:${params?.controller} -->
                                        <!-- action: ${params?.action} -->
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="width:40px;">#</th>
                                                <th class="text-center" style="width:80px;">Foto</th>
                                                <th class="text-center" style="width:105px;">Situação</th>                                                    
                                                <th class="text-center" style="width:150px;">Data</th>
                                                <th class="text-center" style="width:70px;">Termo aceito</th>
                                                <th class="text-center" style="">Descrição</th>
                                                <th class="text-center" style="width:50px;">Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                                                    
                                        
                                            <template v-for="(registro,i) in resFotosColaboracao">
                                                <tr :data-foto-colaboracao="registro.id" :data-bd="registro.id">
                                                    <td class="text-center" style="width:40px;">{{ i + 1}}</td>                                                        
                                                    <td><img :src="registro.url" :id="'img_grid_colab_'+registro.id" style="width:100px;max-width:100px;height:auto;" @click="vmFotoColaboracao.viewFotoColaboracao(registro.url)" class="pointer" title="Clique para visualizar a foto no tamanho real"></td>                                                   
                                                    <td style="width:150px;" class="text-center">{{registro.inAprovada}}</td>
                                                    <td style="width:80px;" class="text-center">{{registro.dtFoto}} {{registro.hrFoto}}</td>
                                                    <td style="width:150px;" class="text-center">{{registro.termoAceito}}</td>                                                        
                                                    <td style="max-height: 50px; overflow: auto; ">{{registro.descricao}}</td>
                                                    <td style="width:50px;white-space: nowrap;" class="text-center">
                                                        <template v-if="registro.id">
                                                            <i @click="vmFotoColaboracao.show(registro.id)" title="Alterar" class="fi-page-edit"></i>&nbsp;
                                                            <i @click="vmFotoColaboracao.deleteFotoColaboracao(registro.idEdit)" title="Excluir" class="fi-page-delete"></i>
                                                        </template>
                                                    </td>
                                                </tr>                                                    
                                            </template>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                %{-- FIM TABLE WRAPPER --}%
                            </div>
                        </div>
                        
                    </div>
                    


                    %{-- NOMES COMUNS --}%
                    <ficha-colaboracao data-topic="Nomes Comuns" data-field-name="nomesComuns" data-field-label="Nomes Comuns" data-original-value="${consultaFicha.ficha?.nomesComunsText}" data-value="${colaboracoes?.nomesComuns?.colaboracao}" data-ref-bib="${colaboracoes?.nomesComuns?.refBib}"
                        data-visible="false">
                    </ficha-colaboracao>


                    %{-- SINONIMIAS --}%
                    <ficha-colaboracao data-topic="Sinonímias" data-field-name="sinonimias" data-field-label="Sinonímias" data-original-value="${consultaFicha.ficha?.sinonimiasText}" data-value="${colaboracoes?.sinonimias?.colaboracao}" data-ref-bib="${colaboracoes?.sinonimias?.refBib}"
                        data-visible="false">
                    </ficha-colaboracao>


                    %{-- NOTAS TAXONOMICAS --}%
                    <ficha-colaboracao data-topic="Notas Taxonômicas" data-help="Descreva questões taxonômicas relevantes para a avaliação. Por exemplo:<br><ul><li>Existem limitações taxonômicas relevantes à validação do táxon?</li><li>Representa um complexo de espécies?</li><li>Existem revisões taxonômicas que requerem uma reavaliação futura?</li></ul>"
                        data-field-name="dsNotasTaxonomicas" data-field-label="Notas Taxonômicas" data-original-value="${consultaFicha?.ficha?.dsNotasTaxonomicas}" data-value="${colaboracoes?.dsNotasTaxonomicas?.colaboracao}" data-ref-bib="${colaboracoes?.dsNotasTaxonomicas?.refBib}"
                        data-visible="false">
                    </ficha-colaboracao>

                    %{-- NOTAS MORFOLOGICAS --}%
                    <ficha-colaboracao data-topic="Notas Morfológicas" data-help="Descreva características morfológicas e diagnóstico, quando relevantes." data-field-name="dsDiagnosticoMorfologico" data-field-label="Notas Morfológicas" data-original-value="${consultaFicha.ficha?.dsDiagnosticoMorfologico}"
                        data-value="${colaboracoes?.dsDiagnosticoMorfologico?.colaboracao}" data-ref-bib="${colaboracoes?.dsDiagnosticoMorfologico?.refBib}" data-visible="false">
                    </ficha-colaboracao>

                    <br />
                    <h3 class="header">DISTRIBUIÇÃO GEOGRÁFICA</h3>
                    <div class="row">
                        <div class="small-12 medium-12 columns">
                            <div class="label-value" style="font-size:1.2em">
                                <span class="flabel">Endêmica do Brasil?</span>&nbsp;<span class="fvalue">${consultaFicha?.ficha?.endemicaBrasilText}</span>
                            </div>

                            <ficha-colaboracao data-topic="Distribuição Global" data-help="Descreva, de modo sucinto, a distribuição global do táxon, Sugere-se indicar continentes, países, bacias ou oceanos em que a espécie ocorre. Caso seja espécie endêmica do Brasil, descreva a distribuição da espécie de forma mais detalhada, procurando responder às seguintes questões:</p><ul><li class='text-justify'>Quais os limites de distribuição atual do táxon? Descreva utilizando estados, municípios, sistemas fluviais, lagos, ou outras referências geográficas relevantes. Para táxons marinhos, use estuários, litoral dos estados, zonas de pesca, características oceanográficas, ilhas, etc.</li><li class='text-justify'>O táxon é conhecido apenas da localidade-tipo ou de poucas localidades? Em caso positivo, a região é bem amostrada para o grupo em questão?</li><li class='text-justify'>Qual o grau de conhecimento sobre a distribuição do táxon? O táxon é críptico e de difícil detecção? Existem áreas onde o táxon supostamente ocorra ou que necessitem de mais amostragens?</li><li class='text-justify'>Os limites da distribuição original são conhecidos? Há indicações de que a área de ocorrência atual do táxon esteja reduzida em relação à sua área original? Há extinções locais comprovadas?</li></ul>"
                                data-field-name="dsDistribuicaoGeoGlobal" data-field-label="Distribuição Global" data-original-value="${ consultaFicha.ficha?.dsDistribuicaoGeoGlobal }" data-value="${colaboracoes?.dsDistribuicaoGeoGlobal?.colaboracao}" data-ref-bib="${colaboracoes?.dsDistribuicaoGeoGlobal?.refBib}"
                                data-visible="false">
                            </ficha-colaboracao>

                            <g:if test="${consultaFicha?.ficha?.stEndemicaBrasil != 'S'}">
                                <ficha-colaboracao data-topic="Distribuição Nacional" data-help="Descreva de forma mais detalhada a distribuição da espécie no Brasil. Procure responder às seguintes questões:<ul><li class='text-justify'>Quais os limites de distribuição atual do táxon? Descreva utilizando estados, municípios, sistemas fluviais, lagos, ou outras referências geográficas relevantes. Para táxons marinhos, use estuários, litoral dos estados, zonas de pesca, características oceanográficas, ilhas, etc.</li><li class='text-justify'>O táxon é conhecido apenas da localidade-tipo ou de poucas localidades? Em caso positivo, a região é bem amostrada para o grupo em questão?</li><li class='text-justify'>Qual o grau de conhecimento sobre a distribuição do táxon? O táxon é críptico e de difícil detecção? Existem áreas onde o táxon supostamente ocorra ou que necessitem de mais amostragens?</li><li class='text-justify'>Os limites da distribuição original são conhecidos? Há indicações de que a área de ocorrência atual do táxon esteja reduzida em relação à sua área original? Há extinções locais comprovadas? </li><li class='text-justify'>Possui distribuição marginal no Brasil?</li></ul>"
                                    data-field-name="dsDistribuicaoGeoNacional" data-field-label="Distribuição Nacional" data-original-value="${consultaFicha.ficha?.dsDistribuicaoGeoNacional}" data-value="${colaboracoes?.dsDistribuicaoGeoNacional?.colaboracao}"
                                    data-ref-bib="${colaboracoes?.dsDistribuicaoGeoNacional?.refBib}" data-visible="false">
                                </ficha-colaboracao>
                            </g:if>
                        </div>

                        %{--imagem do mapa de distriução principal--}%
                        <g:if test="${consultaFicha?.ficha?.idMapaDistribuicao}">
                            <div class="row text-center">
                            <img class="img-mapa" src="/salve-consulta/ficha/getAnexo/${consultaFicha?.ficha?.idMapaDistribuicao}" />
                            <g:if test="${consultaFicha?.ficha?.legendaMapaDistribuicao}">
                                <p>${consultaFicha?.ficha?.legendaMapaDistribuicao}</p>
                            </g:if>
                            </div>
                        </g:if>
                    </div>

                    <br />
                    <div class="text-left">
                        <div id="map-ocorrencia-container" class="topic pointer" @click="toggleOcorrencias()">Mapa de Registros&nbsp;<i title="Abrir/Fechar edição" class="i-click" :class="ocorrenciasVisible ? 'fi-arrow-up':'fi-map'"></i> &nbsp;
                            <i class="tooltipster fi-info" title="<p class='text-justify'>Caso queira adicionar registros à tabela, informe as <b>coordenadas geográficas</b> e o <b>datum</b>. Caso o datum não seja conhecido, assinale “Não informado”. Indique também a <b>precisão da coordenada</b>, de acordo com as seguintes definições:</p><ul><li class='text-justify'>Exata: coordenada de ponto obtida por meio de métodos georreferenciadores (GPS) no local exato do registro.</li><li class='text-justify'>Inferida: é o local exato do registro, mas cuja coordenada foi obtida a partir de plotagem em programas georreferenciadores ou documentos cartográficos com escala compatível com o foco do trabalho, com base em informação geográfica evidente (ponte, encruzilhada, encontro de rio, etc). </li><li class='text-justify'>Referência da área amostrada: coordenada de ponto obtida a partir de limites ou grides gerados por métodos georreferenciados exatos (GPS) do local onde foi feito o registro. Por exemplo, a coordenada exata do local não é conhecida, mas há uma coordenada exata da trilha onde a pesquisa foi realizada.</li><li class='text-justify'>Aproximada: coordenada de ponto obtida a partir da atribuição de valor usando como base alguma informação sobre o local do registro. Indique a referência utilizada  em <b>“Referência da aproximação”</b> (localidade, unidade de conservação, rio, município, etc.).</li><li class='text-justify'>Arredondada: coordenada obtida a partir de valor georreferenciado com precisão original inferior a graus, minutos e segundos (GG ou GG MM).</li><li class='text-justify'>Desconhecida: precisão do ponto não informada. Quando a coordenada é proveniente de uma publicação, normalmente a precisão não é conhecida.</li></ul><p>Informe também o nome da <b>localidade</b> e sempre indique a <b>referência bibliográfica</b> para o registro. Dados não publicados podem ser inseridos como comunicação pessoal. <span class='red'>Atenção: ao inserir um registro de ocorrência não publicado, como comunicação pessoal, o colaborador aceita ceder a informação para o ICMBio, que compromete-se a atribuir os devidos créditos caso a informação seja publicada.</span></p>"></i>
                        </div>
                        <div v-show="ocorrenciasVisible">
                            <!-- mapa -->
                            <div id="map-ocorrencias" class="map"></div>

                            <div class="subtopic">
                                <legend title="Clique para abrir/fechar a tabela de registros" id="legend_meus_registros" @click="toggleMinhasOcorrencias">
                                    <img v-if="minhasOcorrencias.length > 0" src="${ assetPath( src: 'green-check.png') }" style="width:16px;height:16px">&nbsp;Tabela de registros&nbsp;<i class="fi-list-thumbnails"></i>
                                </legend>
                                <div :class="minhasOcorrenciasVisible ? 'sem-classe' : 'hidden'" id="divGridOcorrencia" style="height:auto;overflow:hidden;width:900px;">
                                    <div class="row">
                                    %{--<i @click="modalOcorrencia.show()" class="fi-plus pointer" title="Adicionar Registro"></i>--}%
                                      &nbsp;&nbsp;<a @click.prevent="modalOcorrencia.show()" class="button medium" href="#" title="Clique aqui para informar registros da espécie utilizando o mapa.">Adicionar registro</a>
                                        <a @Click.prevent="$refs.fileInputPlanilha.click()" class="button medium" href="#" title="Também é possível baixar a planilha modelo e preenchê-la com vários registros, em seguida clique neste botão para enviá-la para análise. É prciso que ela esteja compactada no formato zip antes do envio.">Enviar planilha com registros</a>
                                        <a @click.prevent="downloadPlanilhaRegistro" class="button medium" href="#" title="Clique aqui para baixar a planilha modelo caso queira colaborar com vários registros da espécie.">Baixar a planilha modelo</a>
                                    <input type="file" ref="fileInputPlanilha" id="inputPlanilha" data-current-id="${fichaConsultaAnexo?.id}" data-current-file="${fichaConsultaAnexo?.noArquivo}"
                                           @change="onFileSelectedPlanilha" style="display:none;" accept="application/zip" >


                                    </div>
                                    <div class="row" v-if="noFichaConsultaAnexo">
                                        <div class="small-12 columns">
                                            <label>Planilha de registros enviada&nbsp;<button class="pointer"><i @click.prevent="deletePlanilhaRegistroEnviada" title="Remover" class="fi-trash"></i></button>
                                                <input type="text" disabled :value="noFichaConsultaAnexo">
                                            </label>
                                        </div>
                                    </div>

                                    %{-- PAGINAÇÃO --}%
                                    <div v-if="ocorrenciasPagination.totalPages > 1" style="height:50px;border:1px solid gray;padding: 5px">
                                        <nav style="padding-left:10px;">
                                            <ul class="pagination">
                                                <li class="pagination-previous"><a :class="{disabled:ocorrenciasPagination.pageNumber < 2}" href="#" @click.prevent="ocorrenciasPagination.pageNumber = 1;ocorrenciasPaginationChange()">Início</a></li>
                                                <li class="pagination-previous"><a :class="{disabled:ocorrenciasPagination.pageNumber < 2}" href="#" @click.prevent="ocorrenciasPagination.pageNumber--;ocorrenciasPaginationChange()">Anterior</a></li>
                                                <li>
                                                    <select v-model="ocorrenciasPagination.pageNumber" @change.prevent="ocorrenciasPaginationChange">
                                                        <option v-for="pageNum in ocorrenciasPagination.totalPages"
                                                                :value="pageNum"
                                                                :selected="pageNum == ocorrenciasPagination.pageNumber">{{ pageNum }}
                                                        </option>
                                                    </select>
                                                </li>
                                                <li class="pagination-next"><a :class="{disabled:ocorrenciasPagination.pageNumber == ocorrenciasPagination.totalPages}" @click.prevent="ocorrenciasPagination.pageNumber++; ocorrenciasPaginationChange()">Próxima</a></li>
                                                <li class="pagination-next"><a :class="{disabled:ocorrenciasPagination.pageNumber == ocorrenciasPagination.totalPages}" @click.prevent="ocorrenciasPagination.pageNumber = ocorrenciasPagination.totalPages; ocorrenciasPaginationChange()">Último</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div style="width:100%;height: auto;max-height: 800px;overflow: auto;border:1px solid silver;">
                                        <table class="table striped meus-registros hover gride" id="grideRegistros" style="width:100%">
                                            <!-- view: /views/ficha/colaborarFicha.gsp -->
                                            <!-- controller:${params?.controller} -->
                                            <!-- action: ${params?.action} -->
                                            <thead>
                                                <tr>
                                                    <th class="text-center" style="width:40px;">#</th>
                                                    <th class="text-center" style="width:80px;">Utilizado na<br>avaliação?</th>
                                                    <th class="text-center" style="width:105px;">Registro válido?<br><i class="fi-info" title="Responda caso NÃO concorde com a informação da coluna anterior."></i></th>
                                                    <th class="text-center" style="width:140px">Localização /<br>Ref. bib</th>

    %{--                                                <th class="text-center" style="width:80px;">Estado /<br>Município</th>--}%
    %{--                                                <th class="text-center" style="width:80px;">Município</th>--}%
                                                    <th class="text-center" style="width:150px;">Localidade</th>
                                                    <th class="text-center" style="width:80px;">Precisão<br>coordenada</th>
                                                    <th class="text-center" style="width:150px;">Autor / Responsável</th>
                                                    <th class="text-center" style="width:70px;">Data /<br>Base dados</th>
    %{--                                                <th class="text-center">Referência<br>bibliográfica</th>--}%
    %{--                                                <th class="text-center">Base de dados</th>--}%
                                                    %{--<th>Fonte</th>--}%
                                                    <th class="text-center" style="width:50px;">Ação</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <template v-for="(registro,i) in minhasOcorrencias">
                                                    <tr :class="{blue: registro.baseDados=='minha-colaboracao'}" :data-id="registro.id" :data-bd="registro.fonte.toLowerCase()">
                                                        <td class="text-center" style="width:40px;">{{ ocorrenciasPagination.rowNum + i }}</td>
                                                        <td style="width:80px;" class="text-center">{{ registro.utilizadoAvaliacao ? 'Sim' : 'Não' }}</td>

                                                        %{--REGISTRO VALIDO S/N--}%
                                                        <td class="text-center" style="width:105px;white-space:nowrap;vertical-align: middle" valign="middle">
                                                            <template v-if="registro.baseDados != 'minha-colaboracao'">
                                                                <select :class="{bgRed:registro.valido=='N',bgGreen:registro.valido=='S'}" id="sqFichaColaboracaoAceite" :value="registro.valido" @change="validarRegistro(registro,$event)" style="font-size:13px;width:100px;">
                                                                    <option value="">?</option>
                                                                    <option value="S">Concordo</option>
                                                                    <option value="N">Discordo</option>
                                                                </select>
                                                                <template v-if="registro.valido=='N'">
                                                                    <p><i title="Editar comentário." class="pointer fi-pencil blue" @click="abrirModalObservacao(registro)"></i></p>
                                                                </template>
                                                            </template>                                                            &nbsp;
                                                        </td>

                                                        %{--LOCALIZACAO--}%
                                                        <td :id="'tdGridRegistro-'+registro.fonte.toLowerCase()+registro.id" style="width:140px;white-space:nowrap;">Lat: {{registro.lat}}<br>Lon: {{registro.lon}}
                                                            &nbsp;
                                                            <i @click="identificarPonto( {fonte:registro.fonte,id:registro.id,center: true} )" title="Identificar este ponto no mapa!" class="fi-marker"></i>
                                                            <template v-if="registro.datum">
                                                               <br>Datum: {{registro.datum}}
                                                            </template>
                                                            <template v-if="registro.refAproximacao != ''">
                                                                <br>Ref. aprox: {{registro.refAproximacao}}
                                                            </template>
                                                            <br>{{registro.carencia ? 'Carência:' + registro.carencia : 'Sem carência.'}}
                                                            <template v-if="registro.refBib">
                                                                <p style="white-space: normal" v-html="'Ref. bib: '+registro.refBib"></p>
                                                            </template>
                                                        </td>

    %{--                                                    <td style="width:80px;">{{ registro.estado }}</td>--}%
    %{--                                                    <td style="width:80px;">{{ registro.municipio }}</td>--}%
    %{--                                                    <td style="width:150px;">{{ registro.localidade}}<br>{{registro.municipio}}/{{registro.estado }}--}%
                                                        <td style="width:150px;">
                                                            <span v-html="(registro.localidade ? registro.localidade : '') + (registro.municipio ? '<br>'+registro.municipio+'/'+registro.estado:'')"></span>
                                                            <template v-if="registro.fotos.length > 0">
                                                                <br>
                                                                <template v-for="(foto,i) in registro.fotos">
                                                                    <button @click="page.viewFoto(foto)" type="button"
                                                                            :title="'Foto: '+foto.noArquivo +'\nAutor: '+foto.noAutorFoto +'\nData: '+ foto.dtFoto + '\nClique para visualizar.'"
                                                                            class="button secondary small button-badge pointer" style="margin-right:3px;">
                                                                        <i class="fa fa-camera"></i>
                                                                    </button>
                                                                </template>
                                                            </template>
                                                        </td>
                                                        <td style="width:80px;">{{registro.precisao}}</td>
                                                        <td style="width:150px;">{{registro.responsavel}}</td>
                                                        <td style="width:70px;" class="text-center">{{registro.dataHora}}
                                                            <template v-if="registro.baseDados == 'minha-colaboracao'">
                                                                <br>{{registro.fonte}}</br>
                                                            </template>
                                                            <template v-else>
                                                                <br>{{registro.baseDados}}
                                                                <span v-if="registro.baseDados!=registro.fonte"> / {{registro.fonte}}</span>
                                                            </template>
                                                        </td>
    %{--                                                    <td style="min-width:150px;" v-html="registro.refBib"></td>--}%
    %{--                                                    <td style="min-width:150px;">{{registro.baseDados}}<span v-if="registro.baseDados!=registro.fonte"><br>Fonte:{{registro.fonte}}</span></td>--}%
                                                        %{--<td>{{registro.fonte}}</td>--}%
                                                        <td style="width:50px;white-space: nowrap;" class="text-center">
                                                            <template v-if="registro.baseDados=='minha-colaboracao'">
                                                                <i @click="editMinhaOcorrencia(registro.idEdit)" title="Alterar" class="fi-page-edit"></i>&nbsp;
                                                                <i @click="deleteMinhaOcorrencia(registro.idEdit)" title="Excluir" class="fi-page-delete"></i>
                                                            </template>
                                                        </td>
                                                    </tr>
                                                    %{--<template v-if="registro.fotos.length > 0 ">
                                                        <tr >
                                                            <td colspan="12">
                                                            <template v-for="(foto,i) in registro.fotos">
                                                                <div class="card" style="width:75px;border:1px solid silver;padding:5px;margin:5px;display:inline-block">
                                                                    <img :src="foto.url" class="pointer" :title="foto.noAutorFoto +' em '+ foto.dtFoto + '.\nClique para visualizar no tamanho real.'" @click="page.viewFoto(foto)" class="pointer">
                                                                </div>
                                                            </template>
                                                            </td>
                                                        </tr>
                                                    </template>--}%
                                                </template>
                                            </tbody>
                                        </table>
                                    </div>
                                    %{-- FIM TABLE WRAPPER --}%
                                </div>
                            </div>
                        </div>
                    </div>


                    %{-- ESTADOS --}%
                    <ficha-colaboracao data-topic="Estados" data-field-name="estados" data-field-label="Estados" data-original-value="${consultaFicha.ficha?.estadosText}" data-value="${colaboracoes?.estados?.colaboracao}" data-ref-bib="${colaboracoes?.estados?.refBib}" data-visible="false">
                    </ficha-colaboracao>

                    %{-- BIOMAS --}%
                    <ficha-colaboracao data-topic="Biomas" data-field-name="biomas" data-field-label="Biomas" data-original-value="${consultaFicha.ficha?.biomasText}" data-value="${colaboracoes?.biomas?.colaboracao}" data-ref-bib="${colaboracoes?.biomas?.refBib}" data-visible="false">
                    </ficha-colaboracao>

                    <g:if test="${ consultaFicha?.ficha?.grupo?.codigoSistema == 'PEIXES_AGUA_DOCE' || consultaFicha?.ficha?.grupo?.codigoSistema == 'INVERTEBRADOS_AGUA_DOCE'}">
                        %{-- BACIAS --}%
                        <ficha-colaboracao data-topic="Bacias Hidrográficas" data-field-name="bacias" data-field-label="Bacias Hidrográficas" data-original-value="${consultaFicha.ficha?.baciasText}" data-value="${colaboracoes?.biomas?.colaboracao}" data-ref-bib="${colaboracoes?.biomas?.refBib}"
                            data-visible="false">
                        </ficha-colaboracao>
                    </g:if>

                    %{-- AREAS RELEVANTES --}%
                    <g:if test="${consultaFicha.ficha?.areasRelevantesText !=''}">
                        <ficha-colaboracao data-topic="Áreas Relevantes" data-field-name="areasRelevantes" data-field-label="Áreas Relevantes" data-original-value="${consultaFicha.ficha?.areasRelevantesText}" data-value="${colaboracoes?.areasRelevantes?.colaboracao}" data-ref-bib="${colaboracoes?.areasRelevantes?.refBib}"
                            data-visible="false">
                        </ficha-colaboracao>
                    </g:if>

                    %{-- HISTORIA NATURAL --}%

                    <br />
                    <h3 class="header">HISTÓRIA NATURAL&nbsp;<i class="tooltipster fi-info" title="<p class='text-justify'>Descrição geral da história natural da espécie. Procure responder às seguintes perguntas:</p><ul><li>A espécie é migratória? Qual o padrão de deslocamento (nacional ou internacional)?</li> <li class='text-justify'>A espécie possui hábito alimentar especialista? Em caso afirmativo, indique qual o táxon (a espécie, o gênero ou a família) que é consumido? </li><li class='text-justify'>Qual a dieta? (carnívoro, onívoro, frugívoro, insetívoro, etc.). </li><li class='text-justify'>Descrever os tipos de habitat onde o táxon ocorre. Para táxons marinhos incluir profundidade relativa (pelágico, bentônico, etc) tipo de substrato (areia, lama, rocha, etc) e qualquer associação específica (recife dependente, mangue, etc). </li><li class='text-justify'>O táxon é restrito a habitats primários? Apresenta tolerância a modificações/perturbações no ambiente? </li><li class='text-justify'>O táxon muda de habitat ao longo da vida? Existe variação sazonal? </li><li class='text-justify'>O táxon possui relação estreita com outra espécie (ou níveis taxonômicos superiores, como gênero ou família)?</li><li class='text-justify'>Reprodução: qual a longevidade, maturidade sexual e senilidade reprodutiva? Qual a idade média de reprodução? Se desconhecido, é possível inferir a partir de táxons congenéricos ou relacionados? </li><li class='text-justify'>Existem fatores ecológicos ou biológicos que possam afetar a resiliência do táxon às ameaças (tais como baixa taxa reprodutiva ou características reprodutivas exclusivas)? </li></ul>"></i></h3>
                    ${ raw( consultaFicha.ficha.printLabelValue('Espécie é migratória?', consultaFicha?.ficha?.especieMigratoriaText) ) }
                    <br />
                    <ficha-colaboracao data-topic="" data-field-name="dsHistoriaNatural" data-field-label="História Natural" data-original-value="${consultaFicha.ficha?.dsHistoriaNatural}" data-value="${colaboracoes?.dsHistoriaNatural?.colaboracao}" data-ref-bib="${colaboracoes?.dsHistoriaNatural?.refBib}"
                        data-visible="false">
                    </ficha-colaboracao>

                    <br />

                    <g:if test="${consultaFicha?.ficha?.dsHabitoAlimentar}">
                        <div class="text-left">
                            <b>Habito Alimentar</b><br /> ${ raw( consultaFicha.ficha?.dsHabitoAlimentar) }
                        </div>
                        ${ raw( consultaFicha?.ficha?.habitoAlimentarGrid) } %{-- HABITO ALIMENTAR
                        <ficha-colaboracao data-topic="Hábito Alimentar" data-field-name="dsHabitoAlimentar" data-field-label="Hábito Alimentar" data-original-value="${consultaFicha.ficha?.dsHabitoAlimentar}" data-value="${colaboracoes?.dsHabitoAlimentar}" data-ref-bib="${colaboracoes?.dsHabitoAlimentar?.refBib}"
                            data-visible="false">
                        </ficha-colaboracao>
                        --}% %{-- HABITO ALIMENTAR ESPECIALISTA --}%
                        <br /> ${ raw( consultaFicha.ficha.printSndTopic('Habito Alimentar Especialista?', consultaFicha?.ficha?.stHabitoAlimentEspecialista) ) } ${ raw( consultaFicha.ficha.getHabitoAlimentarEspGrid()) }

                    </g:if>

                    %{-- HABITATS --}%
                    <g:if test="${consultaFicha.ficha.dsUsoHabitat}">

                        <div class="text-left">
                            <b>Habitat</b><br />
                        </div>
                        ${raw(consultaFicha?.ficha?.habitatsGrid)} ${ raw( consultaFicha.ficha.printSndTopic('Restrito a habitat primário?', consultaFicha?.ficha?.stRestritoHabitatPrimario ) ) } ${ raw( consultaFicha.ficha.printSndTopic('Especialista em micro habitat?', consultaFicha?.ficha?.stEspecialistaMicroHabitat
                        ) ) } %{--
                        <ficha-colaboracao data-topic="" data-field-name="dsEspecialistaMicroHabitat" data-field-label="Especialista Micro Habitat" data-original-value="${consultaFicha.ficha?.dsEspecialistaMicroHabitat}" data-value="${colaboracoes?.dsEspecialistaMicroHabitat?.colaboracao}"
                            data-ref-bib="${colaboracoes?.dsEspecialistaMicroHabitat?.refBib}" data-visible="false">
                        </ficha-colaboracao>
                        --}%

                        <ficha-colaboracao data-topic="Observações Sobre o Habitat" data-field-name="dsUsoHabitat" data-field-label="Habitat" data-original-value="${consultaFicha.ficha?.dsUsoHabitat}" data-value="${colaboracoes?.dsUsoHabitat?.colaboracao}" data-ref-bib="${colaboracoes?.dsUsoHabitat?.refBib}"
                            data-visible="false">
                        </ficha-colaboracao>
                    </g:if>

                    %{-- ITERACOES COM OUTRAS ESPECIES --}%
                    <g:if test="${consultaFicha.ficha.dsInteracao}">
                        <br>
                        <div class="topic">Interações com outras espécies</div>
                        ${raw(consultaFicha.ficha.iteracaoText)}
                        <ficha-colaboracao data-topic="" data-field-name="dsInteracao" data-field-label="Interação" data-original-value="${consultaFicha.ficha?.dsInteracao}" data-value="${colaboracoes?.dsInteracao?.colaboracao}" data-ref-bib="${colaboracoes?.dsInteracao?.refBib}"
                            data-visible="false">
                        </ficha-colaboracao>
                    </g:if>

                    <br />
                    <div class="topic">Reprodução</div>
                    ${ raw( consultaFicha.ficha.printLabelValue('Intervalo de nascimentos:', consultaFicha.ficha.intervaloNascimentoText ) ) } ${ raw( consultaFicha.ficha.printLabelValue('Tempo de gestação:', consultaFicha.ficha.tempoGestacaoText ) ) } ${ raw( consultaFicha.ficha.printLabelValue('Tamanho da prole:', consultaFicha.ficha.tamanhoProleText ) ) }
                    <br /> ${ raw( consultaFicha.ficha.gridReproducaoText ) }
                    <ficha-colaboracao data-topic="" data-field-name="dsReproducao" data-field-label="Reprodução" data-original-value="${consultaFicha.ficha?.dsReproducao}" data-value="${colaboracoes?.dsReproducao?.colaboracao}" data-ref-bib="${colaboracoes?.dsReproducao?.refBib}"
                        data-visible="false">
                    </ficha-colaboracao>

                    %{-- POPULACAO --}%
                    <br />
                    <h3 class="header">POPULAÇÃO</h3>
                    ${ raw( consultaFicha.ficha.printLabelValue('Tendência populacional:', consultaFicha.ficha?.tendenciaPopulacional?.descricao ?: '' ) ) } %{-- não exibir por enquanto os campos abaixo ${ raw( consultaFicha.ficha.printLabelValue('Razão sexual:', consultaFicha.ficha?.dsRazaoSexual
                    ) ) } ${ raw( consultaFicha.ficha.printLabelValue('Taxa de mortalidade natural:', consultaFicha.ficha.dsTaxaMortalidade ) ) } ${ raw( consultaFicha.ficha.printLabelValue('Taxa de crescimento populacional:', consultaFicha.ficha.dsCaracteristicaGenetica
                    ) ) } --}%


                    <ficha-colaboracao data-topic="Observações Sobre a População" data-help="<p class='text-justify'>O termo “População”, aqui, está sendo usado no sentido dado pela metodologia de avaliação da IUCN, relacionando-se ao número de indivíduos da espécie que existem na natureza. Procure responder às seguintes questões:</p><li class='text-justify'>A espécie é rara, abundante ou frequente em inventários? </li><li class='text-justify'>Quais dados estão disponíveis sobre a abundância do táxon ao longo da sua distribuição? </li><li class='text-justify'>Qual o tamanho populacional conhecido/estimado? </li><li class='text-justify'>Existem estimativas quantitativas relacionadas ao crescimento ou declínio populacional (ex. redução superior a X% ao longo de X gerações)? </li><li class='text-justify'>Quais as informações conhecidas sobre as densidades populacionais? (local, regional ou global). </li><li class='text-justify'>A população encontra-se severamente fragmentada? </li><li class='text-justify'>Para táxons que são recurso pesqueiro: se possível, estimar porcentagem de declínio da população a partir de dados disponíveis em estatísticas de pesca, tais como desembarque e/ou CPUE, ou outros indicadores, tais como diminuição observada no tamanho corporal ou na porcentagem de captura, comércio ou informações de mercado, estatísticas de captura acidental, etc. </li><li class='text-justify'>Existe aporte de indivíduos de fora do Brasil? A população do Brasil é fonte ou sumidouro? Espera-se que essa contribuição aumente/diminua no futuro? </li></ul>"
                        data-field-name="dsPopulacao" data-field-label="População" data-original-value="${consultaFicha.ficha?.dsPopulacao}" data-value="${colaboracoes?.dsPopulacao?.colaboracao}" data-ref-bib="${colaboracoes?.dsPopulacao?.refBib}" data-visible="false">
                    </ficha-colaboracao>

                    <div class="small-12 medium-12 columns text-center">
                        %{-- <img src="${assetPath(src:'mapa1.jpg')}" style="width:100%;height:100%;"> --}%
                        <br>
                        %{--exibir as imagens sobre População--}%
                        ${raw( consultaFicha?.ficha?.getImagensHtml('GRAFICO_POPULACAO') ) }
                    </div>

                    <ficha-colaboracao data-topic="Características Genéticas" data-help="Descreva informações citogenéticas e sobre a variabilidade genética do táxon. Dados sobre isolamento genético ou depressão endogâmica, se existentes, podem ser relevantes para a avaliação."
                        data-field-name="dsCaracteristicaGenetica" data-field-label="Características Genéticas" data-original-value="${consultaFicha.ficha?.dsCaracteristicaGenetica}" data-value="${colaboracoes?.dsCaracteristicaGenetica?.colaboracao}" data-ref-bib="${colaboracoes?.dsCaracteristicaGenetica?.refBib}"
                        data-visible="false">
                    </ficha-colaboracao>

                    %{-- AMEACAS --}%
                    <br />
                    <h3 class="header">AMEAÇAS&nbsp;<i class="tooltipster fi-info"
                        title="<p class='text-justify'>Indique as principais ameaças que afetam a espécie, a partir da lista disponível no pdf. Na descrição das ameaças, evite termos genéricos como “perda e fragmentação de habitat”. A perda e a fragmentação são consequências de alguma pressão. Procure detalhar e explicar as ameaças listadas, respondendo também às seguintes questões:</p><ul><li class='text-justify'>Há expectativas de ameaças futuras ao táxon?</li><li class='text-justify'>Existem ameaças documentadas apenas em partes da distribuição? Descreva a região em que a ameaça afeta a espécie.</li><li class='text-justify'>Qual a proporção da população da espécie que é afetada? É possível quantificar o efeito das ameaças à população?</li><li class='text-justify'>Para táxons que são recurso pesqueiro, e têm a sobrepesca como ameaça, descrever a história da pesca, indicando se é capturado na pesca comercial ou artesanal e se é alvo ou by-catch. Descrever se há colapso da pesca em certas áreas. Se possível, descrever o valor de mercado do táxon; o tipo de apetrecho de pesca ou tecnologia usada; o número de barcos ou permissões; desembarques ou capturas.</li></ul>"></i>
                        &nbsp;<a href="download/arvoreAmeacas?type=pdf" target="_blank"><img src="${assetPath(src: 'pdf-icon2.png')}" style="width:20px;height:20px;" title="Baixar PDF Ameaças"></img>
                        </a>
                    </h3>

                    ${ raw( consultaFicha.ficha.gridAmeacasText) }
                    <ficha-colaboracao data-topic="" data-field-name="dsAmeaca" data-field-label="Ameaça" data-original-value="${consultaFicha.ficha?.dsAmeaca}" data-value="${colaboracoes?.dsAmeaca?.colaboracao}" data-ref-bib="${colaboracoes?.dsAmeaca?.refBib}" data-visible="false"></ficha-colaboracao>

                    %{--exibir as imagens sobre Ameaça --}%
                    ${raw( consultaFicha?.ficha?.getImagensHtml('MAPA_AMEACA') ) }

                    %{-- USOS --}%
                    <br />
                    <h3 class="header">USOS&nbsp;<i class="tooltipster fi-info" title="Indique os usos da espécie, a partir da lista disponível no pdf. Cabe lembrar que nem toda forma de uso representa ameaça, e caso o uso em questão represente uma ameaça, ele deve ser listado também no campo de ameaças. Descreva os usos selecionados para a espécie, detalhando, se for o caso, as regiões em que ocorrem cada uso. Especificamente para os casos de pesca, é possível listar as diferentes modalidades de pesca industrial e artesanal que ocorrem no Brasil."></i>&nbsp;
                        <a href="download/arvoreUsos?type=pdf" target="_blank"><img src="${assetPath(src: 'pdf-icon2.png')}" style="width:20px;height:20px;" title="Baixar PDF Usos"></img>
                        </a>
                    </h3>
                    ${ raw( consultaFicha.ficha.gridUsosText ) }
                    <ficha-colaboracao data-topic="" data-field-name="dsUso" data-field-label="Uso" data-original-value="${consultaFicha.ficha?.dsUso}" data-value="${colaboracoes?.dsUso?.colaboracao}" data-ref-bib="${colaboracoes?.dsUso?.refBib}" data-visible="false">
                    </ficha-colaboracao>



                    %{-- CONSERVACAO --}%
                    <br />
                    <h3 class="header">CONSERVAÇÃO</h3>

                    <div class="topic">Ações de Conservação&nbsp;
                        <i class="tooltipster fi-info" title="Indique os usos da espécie, a partir da lista disponível no pdf. Cabe lembrar que nem toda forma de uso representa ameaça, e caso o uso em questão represente uma ameaça, ele deve ser listado também no campo de ameaças. Descreva os usos selecionados para a espécie, detalhando, se for o caso, as regiões em que ocorrem cada uso. Especificamente para os casos de pesca, é possível listar as diferentes modalidades de pesca industrial e artesanal que ocorrem no Brasil."></i>&nbsp;
                        <a href="download/arvoreAcoesConservacao?type=pdf" target="_blank"><img src="${assetPath(src: 'pdf-icon2.png')}" style="width:20px;height:20px;" title="Baixar PDF Ações de Conservação"></img>
                        </a>
                    </div>
                    ${raw( consultaFicha.ficha.acoesConservacaoText )}

                    <ficha-colaboracao data-topic="" data-help=""
                        data-field-name="dsAcaoConservacao" data-field-label="Ações de Conservação" data-original-value="${consultaFicha.ficha?.dsAcaoConservacao}" data-value="${colaboracoes?.dsAcaoConservacao?.colaboracao}" data-ref-bib="${colaboracoes?.dsAcaoConservacao?.refBib}"
                        data-visible="false">
                    </ficha-colaboracao>

                    <br />

                    <g:if test="${tipoConsultaSistema != 'REVISAO_POS_OFICINA'}">
%{--                        <g:if test="${( ultimaAvaliacao =~ /Ano:<\/span>&nbsp;<span class="fvalue">[0-9]{2,4}/)}">--}%
                            <div class="topic">Última Avaliação Nacional</div>
                            ${raw(ultimaAvaliacao)}
%{--                        </g:if>--}%
                    </g:if>

                    <div class="topic">Histórico das Avaliações</div>

                    ${raw( consultaFicha.ficha.historicoAvaliacoesText )}

                    <g:if test="${ consultaFicha?.ficha?.stPresencaListaVigente=='S'}">
                        ${ raw( consultaFicha.ficha.printSndTopic('Presença em Lista Nacional Oficial de Espécies Ameaçadas de Extinção?', consultaFicha?.ficha?.stPresencaListaVigente) ) }
                    </g:if>


                    <g:if test="${consultaFicha.ficha.presencaConvencoesText!=''}">
                        <br />
                        <div class="topic">Presença em Convenções</div>
                        ${raw( consultaFicha.ficha.presencaConvencoesText )}
                    </g:if>
                    <br />

                    <br />
                    <div class="topic">Presença em UC&nbsp;<i class="tooltipster fi-info" title="<p class='text-justify'>Indique as Unidades de Conservação (UC) em que a espécie ocorre. Procure distinguir as UC com registro confirmado das UC com provável ocorrência.</p>"></i></div>
                    ${raw( consultaFicha.ficha.presencaUcText )}

                    <ficha-colaboracao data-topic="" data-field-name="dsPresencaUc" data-field-label="Presença em UC" data-original-value="${consultaFicha.ficha?.dsPresencaUc}" data-value="${colaboracoes?.dsPresencaUc?.colaboracao}" data-ref-bib="${colaboracoes?.dsPresencaUc?.refBib}"
                        data-visible="false">
                    </ficha-colaboracao>


                    <br />
                    <div class="topic">Pesquisas&nbsp;<i class="tooltipster fi-info" title="<p class='text-justify'>Informe os temas de pesquisas relevantes para a conservação da espécie apontando também sua situação ( se existente ou necessária).</p>"></i></div>
                    ${raw( consultaFicha.ficha.pesquisasText )}
                    <ficha-colaboracao data-topic="" data-field-name="dsPesquisaExistNecessaria" data-field-label="Pesquisas" data-original-value="${consultaFicha.ficha?.dsPesquisaExistNecessaria}" data-value="${colaboracoes?.dsPesquisaExistNecessaria?.colaboracao}" data-ref-bib="${colaboracoes?.dsPesquisaExistNecessaria?.refBib}"
                        data-visible="false">
                    </ficha-colaboracao>

                    <br />
                    <div class="topic">Referências Bibliográficas</div>
                    ${raw( consultaFicha.ficha.refBibsText )}


                    <h4 class='text-center'>--- FIM --- </h4>
                </fieldset>
            </div>

            <!-- modal observãção validação -->
            <div class="reveal small fast" data-reveal id="divFrmObsValidacaoOcorrencia"
                 data-options="closeOnEsc:true;closeOnClick:false;" data-animation-in="hinge-in-from-top"
                 data-animation-out="hinge-out-from-top">
                <div class="row align-middle" id="div-page-obs-validacao-ocorrencia" style="height:250px;">
                    <div class="large-centered small-12 columns">
                        <label class="label-required">Comentário</label>
                        <textarea name="txObsValicaoOcorrencia" id="txObsValicaoOcorrencia" style="resize: none;min-height: 100px;max-height:130px;height:130px;width:100%;margin-bottom:1px;"></textarea>
                        <small class="small-required" style="display:block;">* - preenchimento obritatório</small>
                        <button class="primary button" @click.prevent="gravarValidacaoOcorrencia">Gravar</button>
                    </div>
                </div>
                <button class="close-button" data-close aria-label="Close modal" type="button">
                     <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        <!-- fim div-page-colaboracao -->


    </g:if>
    <g:else>
        <div class="callout text-center alert">Nenhuma ficha selecionada!</div>
    </g:else>

    %{-- inicio templates modais --}% %{-- http://foundation.zurb.com/sites/docs/motion-ui.html --}%
    <div class="reveal large fast" data-multiple-opened="false" data-reveal id="frmRefBib" data-options="closeOnEsc:true;closeOnClick:false;" data-animation-in="hinge-in-from-top" data-animation-out="hinge-out-from-top">
        <div class="row align-middle" id="div-page-ref-bib">
            <div class="large-centered small-12 columns">
                <fieldset>
                    <legend>Selecionar Referência Bibilográfica</legend>
                    <br>
                    <div class="row">
                        <div class="small-12 columns">
                            <label for="fldRefBib">Referência Bibliográfica</label>
                            </select>
                            <select style="width:100%;max-width:800px;" name="fldRefBib" id="fldRefBib"></select>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <button class="close-button" data-close aria-label="Close modal" type="button">
         <span aria-hidden="true">&times;</span>
      </button>
    </div>


 %{--FORMULÁRIO DE CADASTRO DE OCORRÊNCIAS--}%
    <div class="reveal large fast" data-reveal id="divFrmOcorrencia" data-multiple-opened="false" data-options="closeOnEsc:false;closeOnClick:false;" data-animation-in="hinge-in-from-top" data-animation-out="hinge-out-from-top">
        <div class="row align-middle" id="div-page-ocorrencia" style="height:auto;">
            <div class="large-centered small-12 columns">
                <fieldset>
                    <legend>Informar Registro de Ocorrência</legend>
                    <br>
                    <form name="frmOcorrencia" id="frmOcorrencia" data-abide novalidate>
                        <div class="row">

                            <div class="small-12 medium-6 columns">
                                %{--<div id="mapTools" style="position:absolute;top:87px;left:63px;z-index:1;">--}%
                                    %{--<span class="pointer" @click.prevent="modalOcorrencia.centerMarker()" style="font-size:2rem;padding-left:10px;color:rgba(0,60,136,.7);" title="Centralizar o balão de marcação no mapa!"><i class="fi-marker"></i></span>--}%
                                %{--</div>--}%
                                <div>Pressione <b style="color:blue;">CTRL+CLICK</b> no mapa para selecionar Lat e Lon.</div>
                                <div id="map-form-ocorrencias" class="map" style="position:relative;top:0;left:0;height:701px;background-color:#ffffff;border:1px solid #cacaca;"></div>
                            </div>

                            <div class="small-12 medium-6 columns">


                                <div class="row" v-if="deComunicacaoPessoal==''">
                                    <div class="small-12 columns">
                                        <label>Referência Bibliográfica
                                &nbsp;<i @click.prevent="modalRefBib.show(null,'divFrmOcorrencia',selectRefBibChange)" class="fi-magnifying-glass pointer" title="Selecionar Referência Bibliográfica"></i>
                                &nbsp;<i class="fi-info pointer" title="Caso não encontre a referência desejada na base de dados, informe-a no campo “Observações”. Registros inseridos sem referência bibliográfica ou comunicação pessoal não serão considerados."></i>
                                <!-- <span class="small-required">obrigatório</span> -->
                                <div @dblclick.prevent="modalRefBib.show(null,'divFrmOcorrencia',selectRefBibChange)" v-html="txRefBib" class="div-input pointer"></div>
                                </label>
                                    </div>
                                </div> %{-- fim row --}%

                                <div class="row" v-if="txRefBib==''">
                                    <div class="small-12 columns">
                                    <label>Comunicação Pessoal &nbsp;<button @click.prevent="modalOcorrencia.inserirComPessoal(this)"  class="pointer"><i title="Inserir comunicação pessoal" class="fi-torso"></i></button>
                                        <input type="text" v-model="deComunicacaoPessoal" placeholder="Comunicação pessoal">
                                    </label>
                                 </div>
                                </div> %{-- fim row --}%



                                <div class="row">
                                    <div class="small-12 medium-4 columns">
                                        <label>Latitude<span class="small-required">obrigatório</span>
                                           <input name="vlLatLat" id="vlLat" v-model="vlLat" @blur="setMarkerPosition(true,true)" type="text" placeholder="Latitude" required pattern="number">
                                            <span class="form-error">Campo obrigatório</span>
                                        </label>
                                    </div>
                                    <div class="small-12 medium-4 columns">
                                        <label>Longitude<span class="small-required">obrigatório</span>
                                            <input name="vlLon" id="vlLon" v-model="vlLon" @blur="setMarkerPosition(true,true)" type="text" placeholder="Longitude" required pattern="number">
                                            <span class="form-error">Campo obrigatório</span>
                                        </label>
                                    </div>
                                    <div class="small-12 medium-4 columns">
                                        <label>Datum<span class="small-required">obrigatório</span>
                                        <select name="sqDatum" id="sqDatum" v-model="sqDatum" required placeholder="Datum">
                                          <option value="">-- selecione --</option>
                                          <g:each var="item" in="${listDatuns}">
                                            <option data-codigo="${item.codigoSistema}"  value="${item.id}">${item.descricao}</option>
                                          </g:each>
                                        </select>
                                        </label>
                                    </div>

                                </div> %{-- fim row --}%
                                <div class="row">
                                    <div class="small-12 columns">
                                        <span><h6><small style="color:orange;">Dica: para selecionar a Lat/Lon, clique no mapa com a tecla control pressionada.</small></h6></span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="small-12 medium-6 columns">
                                        <label>Precisão da Coordenada<span class="small-required">obrigatório</span>
                                    <select name="sqPrecisaoCoordenada" id="sqPrecisaoCoordenada" v-model="sqPrecisaoCoordenada" placeholder="Precisão da coordenada" required>
                                      <option value="">-- selecione --</option>
                                      <g:each var="item" in="${listPrecisoesCoord}">
                                        <option data-codigo="${item.codigoSistema}"  value="${item.id}">${item.descricao}</option>
                                      </g:each>
                                    </select>
                                    </label>
                                    </div>
                                    <div class="small-12 medium-6 columns" v-show="showReferenciaCoordenada">
                                        <label>Referência<span class="small-required">obrigatório</span>
                                    <select name="sqRefAproximacao" id="sqRefAproximacao" v-model="sqRefAproximacao" placeholder="Referência da aproximação" required>
                                      <option value="">-- selecione --</option>
                                      <g:each var="item" in="${listRefsAproximacao}">
                                        <option data-codigo="${item.codigoSistema}"  value="${item.id}">${item.descricao}</option>
                                      </g:each>
                                    </select>
                                    </label>
                                </div>
                                </div> %{-- fim row --}%

                                <div class="row">
                                    <div class="small-12 medium-2 columns">
                                        <label>Dia
                                            <i class="fi-info" title="Informar a data ou período em que o registro foi realizado. Essa data pode ser diferente da data de publicação. É obrigatório informar pelo menos o ano da data do registro."></i>
                                            <input name="nuDiaRegistro" id="nuDiaRegistro" maxlength="2" v-model="nuDiaRegistro" type="text" pattern="number">
                                        </label>
                                    </div>
                                    <div class="small-12 medium-2 columns">
                                        <label>Mês
                                           %{-- <input name="nuMesRegistro" id="nuMesRegistro" maxlength="2" v-model="nuMesRegistro" type="text" pattern="number"> --}%
                                           <select id="nuMesRegistro" name="nuMesRegistro" v-model="nuMesRegistro" >
                                               <option value=""></option>
                                               <option value="1">Jan</option>
                                               <option value="2">Fev</option>
                                               <option value="3">Mar</option>
                                               <option value="4">Abr</option>
                                               <option value="5">Mai</option>
                                               <option value="6">Jun</option>
                                               <option value="7">Jul</option>
                                               <option value="8">Ago</option>
                                               <option value="9">Set</option>
                                               <option value="10">Out</option>
                                               <option value="11">Nov</option>
                                               <option value="12">Dez</option>
                                           </select>
                                        </label>
                                    </div>
                                    <div class="small-12 medium-2 columns">
                                        <label>Ano<span class="small-required pointer"><i class="fa fa-asterisk" title="Campo obrigatório"></i></span>
                                            <input name="nuAnoRegistro" id="nuAnoRegistro" maxlength="4" v-model="nuAnoRegistro" type="text" pattern="number">
                                            <span class="form-error"></span>
                                        </label>
                                    </div>
                                    <div class="small-12 medium-2 columns">
                                        <label>Hora
                                           <input name="hrRegistro" id="hrRegistro" maxlength="5"v-model="hrRegistro" type="text">
                                        </label>
                                    </div>
                                    <div class="small-12 medium-4 columns">
                                        %{--<label>Data e Hora Desconhecidos
                                           <input name="inDataDesconhecida" id="inDataDesconhecida" type="checkbox" value="S"  v-model="inDataDesconhecida">
                                        </label>--}%
                                    </div>
                                </div> %{-- fim row --}%

                                <div class="row">
                                    <div class="small-12 columns">
                                        <label>Localidade
                                  <textarea name="noLocalidade" id="noLocalidade" v-model="noLocalidade" style="height:95px;width:100%"></textarea>
                                </label>
                                    </div>
                                </div> %{-- fim row --}%

                                <div class="row">
                                    <div class="small-12 columns">
                                        <label>Observações
                                            <textarea name="txObservacao" id="txObservacao" v-model="txObservacao" style="height:65px;width:100%"></textarea>
                                        </label>
                                    </div>
                                </div> %{-- fim row --}%


                                %{--CAMPO CARÊNCIA--}%
                                <div class="row">
                                    <div class="small-12 columns">
                                        <label>Prazo de Carência<span class="small-required">obrigatório</span>
                                            <select id="sqPrazoCarencia" name="sqApoioPrazoCarencia" v-model="sqPrazoCarencia" required>
                                                <option value=""></option>
                                                <g:each var="item" in="${listCarencias}">
                                                    <option data-codigo="${item.codigoSistema}"  value="${item.id}">${item.descricao}</option>
                                                </g:each>
                                            </select>
                                        </label>
                                    </div>
                                </div> %{-- fim row --}%
                            </div>
                        </div>


                        %{-- FOTOS --}%


                        <fieldset>
                            <legend>Foto&nbsp;<i class="fi-info" title="Preencha as informações da foto e clique no botão &ldquo;Incluir foto&rdquo; para adicioná-la ao gride, após adicionar todas as fotos, clique no botão &ldquo;Gravar&rdquo; para enviar para o banco de dados."></i>&nbsp;<span class="small">(opcional)</span></legend>
                                <div class="row">
                                    <div class="small-12 large-8 columns">
                                        %{--CAMPOS DO FORMULARIO--}%
                                        <div class="row">
                                            <div class="small-12 large-3 columns">
                                                <label>Data<span class="small-required">obrigatório</span>
                                                    <input type="text" id="dtFoto" maxlength="10" v-model="dtFoto" />
                                                </label>
                                            </div>
                                            <div class="small-12 large-2 columns">
                                                <label>Hora
                                                    <input type="text" id="hrFoto" v-model="hrFoto" maxlength="5"/>
                                                </label>
                                            </div>

                                            <div class="small-12 large-7 columns">
                                                <label>Autor<span class="small-required">obrigatório
                                                    <input type="text" id="noAutorFoto" v-model="noAutorFoto" value="${session?.user?.noUsuario}"/>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="small-12 columns">
                                                <label>Descrição<span class="small-required">obrigatório</span>
                                                    <textarea v-model="deFoto" style="max-height:130px;height:70px;width:100%;margin-bottom: 0px" placeholder="" maxlength="500"></textarea>
                                                    <small>Max: 500 caracteres</small>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="small-12 columns">
                                                <button type="button" class="button primary" @click.prevent="addFoto" v-text="rowEditing == -1 ? 'Incluir foto' : 'Alterar foto'"></button>
                                                <button type="button" class="button alert" @click.prevent="resetFoto">Limpar</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="small-12 large-4 columns">
                                        <div class="row">
                                            <div class="small-12 columns">
                                                <input type="file" ref="fileInput" id="inputFoto" @change="onFileSelected" style="display:none;" accept="image/*" >
                                                %{--<img src="/salve-consulta/assets/no-image.png" style="width: 100%;height: auto;"/>--}%
                                                <img id="imgFoto" src="//via.placeholder.com/350x220" style="width: 100%;height: auto;"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-12 columns">
                                                <button type="button" class="button secondary expanded"
                                                    title="Clique para abrir a janela de seleção de foto."
                                                        @Click="$refs.fileInput.click()"><i class="fi-folder-add"></i>&nbsp;Escolher foto</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                %{-- gride de fotos --}%
                                <div class="row">
                                    <div class="small-12 columns" id="divGridFotos">
                                        <table class="table minhas-fotos gride">
                                            <thead>
                                            <tr>
                                                <th class="text-center" style="width:30px;">#</th>
                                                <th class="text-center">Foto</th>
                                                <th class="text-center">Situação</th>
                                                <th class="text-center">Arquivo</th>
                                                <th class="text-center">Data</th>
                                                <th class="text-center">Hora</th>
                                                <th class="text-center">Autor</th>
                                                <th class="text-center">Descrição
                                                </th>
                                                <th class="text-center">Ação</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr v-for="(foto,i) in minhasFotos">
                                                <td class="text-center">{{i+1}}</td>
                                                <td><img :src="foto.url" style="width:100px;max-width:100px;height:auto;" @click="page.viewFoto(foto)" class="pointer" title="Clique para visualizar a foto no tamanho real"></td>
                                                <td :class="[{'green':foto.inAprovada=='S','red':foto.inAprovada!='S','blue':!foto.idFoto},'text-center']" v-html="foto.inAprovada == 'S' ? 'Aprovada ' : (foto.idFoto && foto.inAprovada=='N'? 'Pendente<br>aprovação':'<b>Pendente<br>gravação</b>')"></td>
                                                <td class="text-center">{{ !foto.file ? foto.noArquivo : foto.file.name }}</td>
                                                <td class="text-center">{{foto.dtFoto}}</tdclass>
                                                <td class="text-center">{{foto.hrFoto}}</td>
                                                <td class="text-center">{{foto.noAutorFoto}}</td>
                                                <td class="text-justify" style="width: 400px;"><div style="max-height: 300px;overflow: auto;">{{foto.deFoto}}</div></td>
                                                <td class="text-center" style="white-space: nowrap">
                                                    <template v-if="foto.inAprovada != 'S' ">
                                                        <i @click="editMinhaFoto(foto,i)" title="Alterar" class="fi-page-edit"></i>&nbsp;
                                                        <i @click="deleteMinhaFoto(foto,i)" title="Excluir" class="fi-page-delete"></i>
                                                    </template>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                %{--<fim gride--}%
                        </fieldset>


                        %{--FIM FOTO--}%


                        %{-- botoes --}%
                        <div class="row">
                            <div class="small-12 columns">
                                <button class="primary button" @click.prevent="saveOcorrencia">Gravar</button>
                            </div>
                        </div> %{-- fim row --}%

                    </form>
                </fieldset>
            </div>
        </div>
         <button class="close-button" data-close aria-label="Close modal" type="button">
         <span aria-hidden="true">&times;</span>
      </button>
    </div>

    %{--FORMULÁRIO DE CADASTRO DE FOTOS COLABORACAO--}%
    <div class="reveal large fast" data-reveal id="divFrmFotoColaboracao" data-multiple-opened="false" data-options="closeOnEsc:false;closeOnClick:false;" data-animation-in="hinge-in-from-top" data-animation-out="hinge-out-from-top">
        <div class="row align-middle" id="div-page-ocorrencia" style="height:auto;">
            <div class="large-centered small-12 columns">
                <fieldset>
                    
                    <form name="frmFoto" id="frmFoto" data-abide novalidate>                    
                        %{-- FOTOS --}%
                        <fieldset>
                            <legend>
                                Foto&nbsp;<i class="fi-info" title="Preencha as informações da foto e clique no botão &ldquo;Incluir foto&rdquo; para adicioná-la a suas colaborações."></i>
                            </legend>
                                <div class="row">
                                    <div class="small-12 large-8 columns">
                                        %{--CAMPOS DO FORMULARIO--}%
                                        <div class="row">                                            
                                            <div class="small-12 large-6 columns">
                                                <label>Autor<span class="small-required">obrigatório
                                                    <input type="text" id="noAutorFoto" v-model="noAutorFoto" value="${session?.user?.noUsuario}"/>
                                                </label>
                                            </div>
                                            <div class="small-12 large-6 columns">
                                                <label>E-mail do autor<span class="small-required">obrigatório</span>
                                                    <input type="text" id="deEmailAutor" v-model="deEmailAutor" value="${session?.user?.username}" />
                                                </label>
                                            </div>
                                        </div>                                        
                                        <div class="row">
                                            <div class="small-12 columns">
                                                <label>Descrição&nbsp;<i class="fi-info" title="Utilize este campo para informar por exemplo: data, localização, caracteristicas, sexo, estado do animal e outras informações que achar pertinete"></i>
                                                    <span class="small-required">obrigatório</span>
                                                    <textarea v-model="deFoto" style="max-height:130px;height:70px;width:100%;margin-bottom: 0px" placeholder="" maxlength="500"></textarea>
                                                    <small>Max: 500 caracteres</small>
                                                </label>
                                            </div>
                                        </div> 
                                        <div class="row">                                            
                                            <div class="small-12 large-1 columns" style="text-align:left">
                                                <input type="radio" v-model="inTipoTermoResp" :checked="inTipoTermoResp == 'USO_IRRESTRITO'" name="in_tipo_termo_resp"  value="USO_IRRESTRITO">                                                                                                     
                                            </div>
                                            <div class="small-12 large-11 columns">                                                
                                                <label style="text-align:justify; ">                                                
                                                    <i>Declaro que cedo, a título gratuito, os direitos de uso não comercial das imagens anexadas ao Instituto Chico Mendes de Conservação da Biodiversidade - ICMBio, <b>para uso amplo e irrestrito</b>, desde que sejam acompanhadas dos créditos do autor.</i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">                                            
                                            <div class="small-12 large-1 columns" style="text-align:left">
                                                <input type="radio" v-model="inTipoTermoResp" :checked="inTipoTermoResp == 'USO_RESTRITO_AVALIACAO'" name="in_tipo_termo_resp"  value="USO_RESTRITO_AVALIACAO">                                                                                                     
                                            </div>
                                            <div class="small-12 large-11 columns">                                                
                                                <label style="text-align:justify; ">                                                
                                                    <i>Declaro que cedo, a título gratuito, os direitos de uso não comercial das imagens anexadas ao Instituto Chico Mendes de Conservação da Biodiversidade - ICMBio, <b>apenas para uso relacionado ao processo de avaliação do risco de extinção da fauna</b>, desde que sejam acompanhadas dos créditos do autor.</i>
                                                </label>
                                            </div>
                                        </div> 
                                        
                                        <div class="row">
                                            <div class="small-12 columns">
                                                <button type="button" class="button primary" @click.prevent="addFotoColaboracao" >Salvar</button>                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="small-12 large-4 columns">
                                        <div class="row">
                                            <div class="small-12 columns">
                                                <input type="file" ref="fileInputFotoColab" id="inputFotoColab" @change="onFileSelectedFotoColaboracao" style="display:none;" accept="image/*" >                                                
                                                <div id="conteinerImgFotoColaboracao" style="text-align:center;"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-12 columns">
                                                <button type="button" class="button secondary expanded"
                                                    id="btnSelectFotoColaborador" 
                                                    title="Clique para abrir a janela de seleção de foto."
                                                        @Click="$refs.fileInputFotoColab.click()"><i class="fi-folder-add"></i>&nbsp;Escolher foto</button>
                                            </div>
                                        </div>                                        
                                    </div>                                    
                                </div>                                
                        </fieldset>
                        %{--FIM FOTO--}%
                    </form>
                </fieldset>
            </div>
        </div>
         <button class="close-button" data-close aria-label="Close modal" type="button">
         <span aria-hidden="true">&times;</span>
      </button>
    </div>

    %{--TELA DE VISUALIZAÇÃO DE FOTOS NO TAMANHO REAL--}%
    <div class="reveal large fast" data-multiple-opened="true" data-reveal id="divViewImage"
         data-options="closeOnEsc:true;closeOnClick:true;"
         data-animation-in="hinge-in-from-top"
         data-animation-out="hinge-out-from-top">
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
        <br>
        <img id="imgViewFoto" src=//via.placeholder.com/800x600" style="width: 100%;height: auto;"/>
    </div>




    %{-- fim templates modais --}%

    <content tag="javascript">
        <g:javascript src="tinymce/tinymce.min.js" />
        <g:javascript src="openlayers/ol465.js" />
        <g:javascript src="openlayers/ol3-contextmenu.js" />
        <g:javascript src="openlayers/geocoder/ol-geocoder.js" />
        <g:javascript src="openlayers/shp.min.js" />
        <g:javascript src="turf/turf.min.js" />
        <g:javascript src="openlayers/ol3map.js" />
        <g:javascript src="floathead/jquery.floatThead.min.js" />

        %{--<g:javascript src="openlayers/zip2shp/proj4.js" />
        <g:javascript src="openlayers/zip2shp/lib/jszip.js" />
        <g:javascript src="openlayers/zip2shp/lib/jszip-utils.js" />
        <g:javascript src="openlayers/zip2shp/preprocess.js" />
        <g:javascript src="openlayers/zip2shp/preview.js" />--}%
        <asset:javascript src="colaborarFicha.js" />
    </content>

    <template id="_ficha_colaboracao">
    <div class="text-justify">
        <br v-if="dataTopic!=''"/>
        <div v-if="dataTopic!=''"  :id="'topic_'+dataFieldName" class="topic">{{dataTopic}}&nbsp;<i v-if="dataHelp" class="tooltipster fi-info" :title="dataHelp"></i></div>
        <span class="original-text" v-html="originalValueTrim"></span>
        <div class="subtopic">
            <legend :title="dataTopic"  :id="'legend_'+dataFieldName" :data-label="dataFieldLabel" @click="dataVisible =! eval(dataVisible)">
                <img v-if="page.fields[dataFieldName].originalValue != page.fields[dataFieldName].value && page.fields[dataFieldName].value != ''" src="${ assetPath( src: 'green-check.png') }" style="width:16px;height:16px">&nbsp;
                Minha Colaboração  <span v-if="dataTopic==''">&nbsp;<i v-if="dataHelp" class="tooltipster fi-info" :title="dataHelp"></i></span>
                &nbsp;<i title="Abrir/Fechar edição" class="i-click" :class="eval(dataVisible) ? 'fi-arrow-up':'fi-page-edit'"></i>
            </legend>
            <div :class="eval(dataVisible) ? '' : 'hidden'" class="content">
                <textarea :id="dataFieldName" v-html="valueTrim" class="wysiwyg"></textarea>
                <p class="red" style="font-size:16px;">“Atenção: sempre indique a referência bibliográfica completa que está sendo utilizada para a informação adicionada”</p>
                %{-- <span>Referência Bibliográfica&nbsp;<i @click="modalRefBib.show(dataFieldName)" title="Pesquisar banco de dados do SALVE." class="fi-magnifying-glass"></i><br></span> --}%
                <span><b>Referência Bibliográfica</b></span>
                <textarea :id="dataFieldName+'_ref'" v-html="dataRefBib" class="wysiwyg-ref-bib" style="width:100%;height:100px"></textarea>
            </div>
        </div>
    </div>
</template>
</body>

</html>



%{-- Registro de Ocorrência:
<p class='text-justify'>Caso queira adicionar registros à tabela, informe as <b>coordenadas geográficas</b> e o <b>datum</b>. Caso o datum não seja conhecido, assinale “Não informado”. Indique também a <b>precisão da coordenada</b>, de acordo com as seguintes definições:</p>Exata:
coordenada de ponto obtida por meio de métodos georreferenciadores (GPS) no local exato do registro.
<ul>
    <li class='text-justify'>Inferida: é o local exato do registro, mas cuja coordenada foi obtida a partir de plotagem em programas georreferenciadores ou documentos cartográficos com escala compatível com o foco do trabalho, com base em informação geográfica evidente (ponte,
        encruzilhada, encontro de rio, etc). </li>
    <li class='text-justify'>Referência da área amostrada: coordenada de ponto obtida a partir de limites ou grides gerados por métodos georreferenciados exatos (GPS) do local onde foi feito o registro. Por exemplo, a coordenada exata do local não é conhecida, mas há uma coordenada
        exata da trilha onde a pesquisa foi realizada.</li>
    <li class='text-justify'>Aproximada: coordenada de ponto obtida a partir da atribuição de valor usando como base alguma informação sobre o local do registro. Indique a referência utilizada em <b>“Referência da aproximação”</b> (localidade, unidade de conservação, rio, município,
        etc.).
    </li>
    <li class='text-justify'>Arredondada: coordenada obtida a partir de valor georreferenciado com precisão original inferior a graus, minutos e segundos (GG ou GG MM).</li>
    <li class='text-justify'>Desconhecida: precisão do ponto não informada. Quando a coordenada é proveniente de uma publicação, normalmente a precisão não é conhecida.</li>
</ul>
<p>Informe também o nome da <b>localidade</b> e sempre indique a <b>referência bibliográfica</b> para o registro. Dados não publicados podem ser inseridos como comunicação pessoal. <span class='red'>Atenção: ao inserir um registro de ocorrência não publicado, como comunicação pessoal, o colaborador aceita ceder a informação para o ICMBio, que compromete-se a atribuir os devidos créditos caso a informação seja publicada.</span></p>
--}%
