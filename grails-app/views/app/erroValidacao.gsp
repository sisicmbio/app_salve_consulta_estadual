<!DOCTYPE html>
<html>
    <head>
        <meta content="main" name="layout"/>
        <title>
            SALVE- ERRO
        </title>
    </head>
    <body>
            <!-- view: /views/app/erroValidacao.gsp -->
        <div class="row align-middle">
            <div class="large-centered large-6 small-12 columns">
                <div class="callout alert">
                     <div class="row">
                         <div class="small-12 columns">

                         <h4>
                            Ops!!!
                        </h4>

                         <h6>
                            <p>Ocorreu um ERRO na ativação!</p>
                            <p>Código inválido!</p>
                         </h6>

                         <br>
                         <p><a class="secondary button expanded" href="${request.contextPath}/.">Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<content tag="javascript">
    <script src="${assetPath(src: 'login.js')}" type="text/javascript">
    </script>
</content>
