<!DOCTYPE html>
<html>
    <head>
        <meta content="main" name="layout"/>
        <title>
            SALVE-LOGIN
        </title>
    </head>
    <body>
            <!-- view: /views/app/login.gsp -->
        <div class="row align-middle" id="div-page-login" style="display:none;">
            <div class="large-centered large-6 small-12 columns">
                <fieldset>
                    <legend>{{title}}</legend>
                    <br/>
                    <form action="${request.contextPath}/login" id="frmLogin" method="POST" name="frmLogin">
                        <div class="row">
                            <div class="small-12 columns">
                                %{-- Exibir o retorno do login que falhou --}%
                                <g:if test="${flash.error}">
                                    <div class="callout alert">
                                        <b>
                                            ${flash.error}
                                        </b>
                                    </div>
                                </g:if>
                                <g:else>
                                    %{-- mensagem erro --}%
                                    <div class="callout alert" id="msg" :class="{ hidden: !errorMsg }">
                                        <button @click="errorMsg=''" aria-label="Close alert" class="close-button" type="button">
                                            <span aria-hidden="true">
                                                ×
                                            </span>
                                        </button>
                                        <h5>
                                            Ops!!!
                                        </h5>
                                        <p>
                                            {{errorMsg}}
                                        </p>
                                    </div>
                                </g:else>

                                <!-- Email -->
                                <div v-if="view!='emailEnviado'">
                                    <label for="fldEmail">
                                        Email
                                    </label>
                                    <input id="fldEmail"
                                            value="${params.fldEmail}"
                                           @keyup="clearTimeout" name="fldEmail" id="fldEmail"
                                           placeholder="Informe email" required="" type="email"
                                           @change="errorMsg=''" v-model="fields.email"/>
                                </div>

                                <!-- senha     -->
                                <div v-if="view=='login'">
                                    <label for="fldSenha">
                                        Senha
                                    </label>
                                    <input id="fldSenha" @keyup="clearTimeout" name="fldSenha" required="" type="password" v-model="fields.senha"/>
                                    <!-- botoes -->
                                    <div class="row">
                                        <div class="small-12 columns">
                                            <button @click="login" class="primary button" style="width:250px">Entrar</button>
                                            <button @click="forgot" class="secondary button">Esqueci minha senha!</button>
                                        </div>
                                    </div>
                                </div>


                                %{-- Recuperação da senha --}%
                                <div v-if="view=='forgot'">
                                    <!-- botoes -->
                                    <div class="row">
                                        <div class="small-12 columns">
                                            <button @click="forgotPassword" class="primary button">Enviar Email</button>
                                            <button @click="voltar" class="button">Voltar</button>
                                        </div>
                                    </div>
                                </div>


                                %{-- Email Enviado --}%
                                <div v-if="view=='emailEnviado'">
                                    <div class="callout success">
                                        <h5>
                                            Acesse o email
                                            <b>
                                                {{fields.email}}
                                            </b>
                                            e clique no link para informar uma nova senha.
                                        </h5>
                                    </div>
                                    <!-- botoes -->
                                    <div class="row">
                                        <div class="small-12 columns">
                                            <button @click="sair" class="primary button">Sair</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </fieldset>
            </div>
        </div>
    </body>
</html>
<content tag="javascript">
    <script src="${assetPath(src: 'login.js')}" type="text/javascript">
    </script>
</content>
