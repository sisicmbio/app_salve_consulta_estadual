<!DOCTYPE html>
<html>
    <head>
        <meta content="main" name="layout"/>
        <title>
            SALVE-RECUPERAÇÃO SENHA
        </title>
    </head>
    <body>
            <!-- view: /views/app/resetPassword.gsp -->
        <div class="row align-middle" id="div-page-reset-password" style="display:none;">
            <div class="large-centered large-6 small-12 columns">
                <h3>
                    Definição de Nova Senha
                </h3>
                <div class="callout secondary">
                    <form id="frmResetPassword" name="frmResetPassword">
                        <div class="row">
                            <div class="small-12 columns">
                                %{-- mensagem erro --}%
                                <div class="callout" id="msg"  :class="{hidden:errorMsg=='',alert:resultadoAjax==1,success:resultadoAjax==0}">
                                    <button @click="errorMsg=''" aria-label="Close alert" class="close-button" type="button">
                                        <span aria-hidden="true">
                                            ×
                                        </span>
                                    </button>
                                    <p>
                                        {{errorMsg}}
                                    </p>
                                </div>

                                <!-- Campo com hash recebido -->
                                <input type="hidden" id="fldHash" name="fldHash" value="${hash}"/>

                                <!-- senhas -->
                                <div v-if="view=='formulario'">
                                    <label for="fldSenha">
                                        Nova Senha
                                    </label>
                                    <input id="fldSenha" name="fldSenha" required="" type="password" v-model="fields.senha"/>
                                    <label for="fldSenha">
                                        Redigite a Nova Senha
                                    </label>
                                    <input id="fldSenha2" name="fldSenha2" required="" type="password" v-model="fields.senha2" data-rule-equalto="#fldSenha"/>
                                    <!-- botoes -->
                                    <div class="row">
                                        <div class="small-12 columns">
                                            <button @click="save" class="primary button expanded">Gravar</button>
                                            <button @click="sair" class="alert button expanded">Sair</button>
                                        </div>
                                    </div>
                                </div>

                                <div v-if="view=='resultado'">
                                    <button @click="login" class="primary button expanded">Login</button>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
<content tag="javascript">
    <script src="${assetPath(src: 'resetPassword.js')}" type="text/javascript">
    </script>
</content>
