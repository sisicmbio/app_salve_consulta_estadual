<!DOCTYPE html>
<html>
    <head>
        <meta content="main" name="layout"/>
        <title>
            SALVE-Sobre
        </title>
    </head>
    <body>
        <!-- view: /views/app/sobre.gsp -->
        <div class="row align-middle">
            <div class="small-12 columns">
            <h4>Apresentação</h4>
            <p class="text-justify">O Sistema de Avaliação do Risco de Extinção da Biodiversidade – SALVE foi elaborado com o objetivo de facilitar o processo de avaliação da fauna, funcionando como uma base de dados para o armazenamento das informações sobre as espécies da fauna brasileira avaliadas e como uma ferramenta para o controle e o acompanhamento das diferentes etapas do processo de avaliação, desde o início da compilação de dados até a publicação das fichas.</p>
            <h4>Informações gerais</h4>
            <p class="text-justify">
O Sistema SALVE é de uso interno do Instituto, com permissões de acesso, em diferentes níveis, aos parceiros e colaboradores durante o processo de avaliação. Será elaborado também um módulo de acesso público para visualização das fichas finais das espécies avaliadas, contendo a categoria de risco de extinção e as informações que subsidiaram a categorização, além de um módulo de relatórios gerenciais, no qual as inúmeras informações do banco de dados podem ser extraídas. Deste modo, o SALVE atende ao disposto na Portaria MMA n° 43/2014, que institui o Programa
Nacional de Conservação das Espécies Ameaçadas de Extinção – Pró-Espécies e que estabelece como intrumentos do Programa bases de dados e sistemas para subsidiar avaliações de risco de extinção das espécies e para auxiliar o planejamento de ações de conservação. O Sistema foi elaborado a partir de discussões envolvendo a equipe responsável pelo processo de avaliação da DIBIO e servidores de alguns Centros com experiência no processo. Foram incluídos
todos os campos da ficha de avaliação utilizada no processo e adicionados alguns campos para detalhar informações que possam ser importantes. Nem todos os campos precisam ser preenchidos. Em alguns casos, podem não ser utilizados para a aplicação do método de avaliação em si, mas podem ser relevantes para análises específicas e para a extração de informações em formato de relatórios gerenciais.</p>
<p class="text-justify">
Algumas informações podem ser mais relevantes para um determinado grupo de espécies. Por exemplo, há campo específico para inserir interações com outras espécies, ou para detalhar mudança de hábitat em épocas diferentes ou em diferentes fases da vida do animal. Esse tipo de O processo de Avaliação da Fauna Brasileira
A Portaria MMA n° 43/2014 estabelece que o Instituto Chico Mendes é responsável pelo processo de Avaliação do Estado de Conservação da Fauna Brasileira, cujos resultados subsidiam o Ministério do Meio Ambiente na publicação das Listas Nacionais Oficiais de Espécies da Fauna Ameaçadas de Extinção. O Instituto Chico Mendes conduz o processo desde 2009, quando foi iniciado o primeiro ciclo de avaliação, tendo avaliado 12.254 táxons da fauna brasielira, incluindo todos os vertebrados conhecidos até 2014. O processo é normatizado pela Instrução Normativa n°
34/2013 do Instituto, que define as suas diretrizes gerais, a metodologia utilizada, os atores envolvidos e suas funções, as etapas e os prazos. O Sistema SALVE foi elaborado observando-se essas diretrizes, incluindo os atores e as etapas previstas no processo. Mais informações sobre o processo podem ser obtidas no “Roteiro Metodológico para avaliação do Estado de Conservação das Espécies da Fauna Brasielira”, disponível no site do Instituto
Chico Mendes ( <a target="_blank" href="http://www.icmbio.gov.br/portal/images/stories/biodiversidade/fauna-brasileira/avaliacao-do-risco/Roteiro_Metodologico_Avaliacao_Fauna_Brasileira_2014.pdf">http://www.icmbio.gov.br/portal/images/stories/biodiversidade/fauna-brasileira/avaliacao-do-risco/Roteiro_Metodologico_Avaliacao_Fauna_Brasileira_2014.pdf</a>).
</p>
            </div>
        </div>
    </body>
</html>
