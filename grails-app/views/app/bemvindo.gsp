<!DOCTYPE html>
<html>
    <head>
        <meta content="main" name="layout"/>
        <title>
            SALVE-BEM VINDO
        </title>
    </head>
    <body>
        <!-- view: /views/app/bemvindo.gsp -->
        <div class="row align-middle">
            <div class="large-centered large-6 small-12 columns">
                <div class="callout success">
                     <div class="row">
                         <div class="small-12 columns">

                         <h5>
                            Bem Vindo,${user?.pessoaFisica?.pessoa?.noPessoa}
                        </h5>

                         <h6>
                            <p>Seu cadastro foi ativado com SUCESSO!</p>
                         </h6>

                         <br>
                         <p><a class="primary button expanded" href="${request.contextPath}/login">Entrar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<content tag="javascript">
    <script src="${assetPath(src: 'login.js')}" type="text/javascript">
    </script>
</content>
