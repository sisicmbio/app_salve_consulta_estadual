<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>SALVE-REGISTRO</title>
</head>
<body>
<!-- view: /views/registrar/index.gsp -->
<div class="row align-middle" id="div-page-cadastrar" style="display:none;">
    <div class="large-centered large-8 small-12 columns">
        <fieldset>
            <legend>Registrar-se</legend>
            <div v-if="view=='formulario'">
                <form id="frmRegistroInicial" name="frmRegistroInicial" action="registrar/save">
                    %{-- MENSAGEM RETORNO --}%
                    <div class="row">
                        <div class="small-12 columns">
                            <div class="callout alert" :class="{ hidden: !errorMsg }" id="msg">
                                <button class="close-button" aria-label="Close alert" type="button" @click="errorMsg=''">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h5>Ops!!!</h5>
                                <p v-html="errorMsg"></p>
                            </div>
                        </div>
                    </div>

                    %{-- INSTITUICAO --}%
                    %{--
                    <div class="row" v-if="fields.noInstituicaoOutra==''">
                        <div class="small-12 columns">
                            <label for="fldInstituicao">Selecione uma instituição</label>
                            <v-select :options="institutionsList" id="fldInstituicao"
                                      :on-change="selectInstituicaoChange"
                                      :searchable="true"
                                      placeholder="Nome (min 3 caracteres para pesquisar)"
                                      :on-search="getInstitutions"></v-select>
                            <small style="font-size:0.7rem">Digite nome/sigla ou parte do nome da instituição e aguarde o carregamento da lista.</small>
                        </div>
                    </div>--}%
                    %{-- INSTITUICAO OUTRA --}%
                    <div class="row">
                        <div class="small-6 large-8 columns">
                            <label for="fldInstituicaoOutra">Instituição</label>
                            <input v-model="fields.noInstituicaoOutra" type="text" id="fldInstituicaoOutra" name="fldInstituicaoOutra">
                        </div>
                        <div class="small-6 large-4 columns">
                            <label for="fldInstituicaoSiglaOutra">Sigla</label>
                            <input v-model="fields.sgInstituicaoOutra" type="text" id="fldInstituicaoSiglaOutra" name="fldInstituicaoOutraSigla">
                        </div>
                    </div>

                    %{-- NOME COMPLETO --}%
                    <div class="row">
                        <div class="small-12 columns">
                            <label for="fldNome">Seu nome completo</label>
                            <input v-model="fields.nome" type="text" id="fldNome" name="fldNome" placeholder="" required data-msg="Nome não poder ficar em branco." data-rule-minlength="4">
                        </div>
                    </div>

                    %{-- EMAIL --}%
                    <div class="row">
                        <div class="small-12 columns">
                            <label for="fldEmail">Seu email</label>
                            <input v-model="fields.email" @blur="fldEmailExit" type="email" id="fldEmail" name="fldEmail" placeholder="Email será seu login!" required>
                        </div>
                    </div>

                    %{-- SENHA --}%
                    <div class="row">
                        <div class="small-12 large-6 columns">
                            <label for="fldSenha">Senha</label>
                            <input v-model="fields.senha" type="password" id="fldSenha" name="fldSenha" required data-rule-minlength="4">
                        </div>
                        <div class="small-12 large-6 columns">
                            <label for="fldSenha2">Redigite a Senha</label>
                            <input v-model="fields.senha2" type="password" id="fldSenha2" name="fldSenha2" required data-rule-equalTo="#fldSenha" data-msg-equalTo="As senhas informadas estão diferentes!">
                        </div>
                    </div>


                    <!-- BOTOES -->
                    <div class="row">
                        <div class="small-12 columns">
                            <button class="primary button" @click="save">Gravar</button>
                        </div>
                    </div>
                </form>
            </div>

            %{-- View resultado com email enviado --}%
            <div v-if="view=='resultado'">
                <div class="callout success text-justify">
                    <h3>Parabéns !!!</h3>
                    <p>
                        Seu registro foi realizado com sucesso mas ainda precisa ser ATIVADO.
                    </p>
                    <p>
                        Para ativá-lo, acesse o e-mail <b>{{fields.email}}</b> e clique no link <b>Ativar Registro</b>.
                    </p>
                    <p>A equipe do SALVE agradece a sua colaboração.</p>
                </div>
                <!-- botoes -->
                <div class="row">
                    <div class="small-12 columns">
                        <button class="primary button" @click="voltar">Ok, entendi!</button>
                    </div>
                </div>
            </div>

            %{-- View resultado com falha do email --}%
            <div v-if="view=='falhaEmail'">
                <div class="callout warning text-justify">
                    <h3>Atenção!</h3>
                    <p>
                        Seu registro foi realizado com sucesso mas ainda precisa ser ativado.
                    </p>
                    <p>
                        Não foi possível enviar o e-mail de ativação para o enderço <b>{{fields.email}}</b>.<br/>
                        Neste caso você poderá solicitar a ativação do registro para o e-mail <b>{{fields.email}}</b>, através do e-mail <b>salve@icmbio.gov.br</b>
                    </p>
                    <p>O Instituto Chico Mendes da Biodiversidade agradece sua colaboração.</p>
                </div>
                <!-- botoes -->
                <div class="row">
                    <div class="small-12 columns">
                        <button class="primary button" @click="voltar">Ok, entendi!</button>
                    </div>
                </div>
            </div>

        </fieldset>
    </div>
</div>
<div class="page-footer" style="min-height: 100px;"></div>
<content tag="javascript">
    <script type="text/javascript" src="${assetPath(src: 'registrar.js')}"></script>
</content>
</body>
</html>
