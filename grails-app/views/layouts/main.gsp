<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <g:set var="version" value="${grailsApplication.metadata['app.version']}"/>
    <g:if env="production">
        <g:set var="env" value=""/>
    </g:if>
    <g:else>
        <g:set var="env" value=" (DSV)"/>
    </g:else>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="Sistema de Avaliação do Risco de Extinção da Biodiversidade - SALVE - Estadual - Consulta Pública" />
    <meta name="description" content="Módulo de consulta pública do Sistema do Risco de Extinção da Biodiversidade - SALVE-Estadual" />
    <meta name="generator" content="Grails Framework" />
    <meta name="reply-to" content="salve@icmbio.gov.br">
    <meta name="author" content="Instituto Chico Mendes de Conservação da Biodiversidade - ICMBio">
    <meta name="rights" content="Instituto Chico Mendes de Conservação da Biodiversidade - ICMBio" />
    <meta name="keywords" content="Sistema Avaliação Risco Extinção Biodiversidade SALVE Consulta Estadual" />
    <meta name="robots" content="index,nofollow">
    <meta name="robots" content="noarchive">
    <meta name="googlebot" content="index,nofollow">

    %{-- titulo --}%
	<title><g:layoutTitle default="Salve"/>${env}</title>
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">

		%{-- css  --}%
	<asset:stylesheet src="application.css"/>
	<g:layoutHead/>
    <script>
        var webUserId="${session?.sicae?.user?.sqPessoa}";
    </script>
	</head>
	<body>
        <main class="wrapper">
            <!-- Start Top Bar -->
            <div class="row app-top-bar" id="app-top-bar">
                <div class="small-12 column">

                    %{-- Inicio Title Bar --}%
                    <div class="title-bar" data-responsive-toggle="top-bar-menu" data-hide-for="medium">
                      <button class="menu-icon" type="button" data-toggle="top-bar-menu"></button>
                      <div class="title-bar-title">Menu</div>
                    </div>
                    <div class="top-bar no-padding" id="top-bar-menu">
                        <div class="top-bar-left">
                            <ul class="dropdown menu" data-dropdown-menu>
                                <li><a href="${request.contextPath}/."><i style="font-size:1.4rem;" class="fi-home"></i>Início</a></li>
                            </ul>
                        </div>
                        <div class="top-bar-right">
                            <ul class="dropdown menu" data-dropdown-menu>
                                <li><a href="${request.contextPath}/faleConosco"><i style="font-size:1.4rem;" class="fi-mail"></i>Fale Conosco</a></li>
                                <g:if test="${!session?.sicae?.user}">
                                    <li><a href="${request.contextPath}/registrar"><i style="font-size:1.3rem;" class="fi-page-edit"></i>Registrar-se</a></li>
                                    <li><a href="${request.contextPath}/login"><i style="font-size:1.5rem;" class="fi-key"></i>Login</a></li>
                                </g:if>

                            <g:if test="${session?.sicae?.user}">
                                <li><a href="${request.contextPath}/#"><i style="font-size:1.5rem;" class="fi-torso"></i></a>
                                    <ul class="menu vertical">
                                        <li class="menu-text nowrap" id="li-full-name">
                                            ${br.gov.icmbio.Util.nomeAbreviado( session?.sicae?.user?.noPessoa ) }
                                        </li>
                                        <li><a href="${request.contextPath}/logout">Sair</a></li>
                                    </ul>
                                </li>
                                </g:if>
                            </ul>
                      </div>
                    </div>
                </div>
            </div>
            <!-- End Top Bar -->

            %{-- Imagem topo --}%
            <div class="row row-image-top">
                <div class="small-12 column">
                    <div class="panel hide-for-small-only" id="div-img-top"></div>
                    <div class="name-version">
                        <h3>Sistema de Avaliação do Risco de Extinção da Biodiversidade - SALVE-Estadual - <span> Consulta/Revisão</span> <small style="font-size:0.7rem;">v.${version}</small></h3>
                    </div>
                </div>
            </div>

            %{-- inicio conteudo --}%
            <g:layoutBody/>
            %{-- fim conteudo --}%

            <!-- This is the alert modal -->
            <div class="reveal" id="app-alert-modal" data-close-on-click="false" data-close-on-esc="true" data-multiple-opened="true" data-reveal>
                <h3></h3>
                <p class="lead"></p>
                <button class="close-button" data-close aria-label="Close reveal" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

		</main>
        <asset:javascript src="application.js"/>
        <g:javascript src="vuejs/vue230.min.js" />
        <g:javascript src="vuejs/vue-resource.min.js" />
        <g:pageProperty name="page.javascript"/>
    </body>
</html>
