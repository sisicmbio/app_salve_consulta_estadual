package br.gov.icmbio

import grails.transaction.Transactional
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.sql.Sql
import groovy.time.TimeCategory
import groovy.time.TimeDuration

// deve ser transacional para poder ser utilizado dentro da mesma sessão das classes da aplicacao
@Transactional
class SqlService {
    //static transactional = false
    protected Long UmDia = 60 * 60 * 24
    //protected Long defaultCacheTimeInMinutes = 20
    protected String lastError = null
    def dataSource
    def emailService
    def grailsApplication

    protected String _cacheDir() {
        return grailsApplication?.config?.cache?.dir ?:'/data/salve/temp/'
    }

    /**
     * executar sql
     * @param cmdSql
     * @param params
     * @return
     */
    List execSql( String cmdSql='', Map params=[:], Map debug=[:], Integer cacheMinutes=0 ) {
        lastError = null
        if( ! cmdSql ) {
            return []
        }
       debug = debug ?: [:]
       List rows = []
       Sql sql = new Sql(dataSource)
       try {
           Date start = new Date()
           if( cmdSql.trim() =~ /(?i)^(insert|delete|update)/) {
               if( cmdSql =~ /(?i)\sRETURNING\s/) {
                   rows = sql.rows(cmdSql, params )
                } else {
                   sql.executeUpdate(cmdSql , params )
               }
           }
           else {
               // ler dados do cache aqui...
               String hashString = cmdSql.replaceAll(/-- ?.+/,'').replaceAll(/\n(\s+)/,' ') + params.toString()
               String hashCode   = hashString.encodeAsMD5()
               String fileName = _cacheDir() + 'salve-query-' + hashCode +'.cache'
               File file = new File( fileName )
               if( cacheMinutes && file.exists() && ! _cacheExpired( file , cacheMinutes ) ) {
                   def jsonSlurper = new JsonSlurper()
                   rows = _parseRows( jsonSlurper.parse( file ) )
                   println ' '
                   println 'SALVE - sqlService - dados recuperados do cache de '+cacheMinutes+' minutos - ' + start.format('dd/MM/yyyy HH:mm:ss') + ' hash:' + hashCode
               } else {
                   rows = sql.rows(cmdSql, params)
                  if( cacheMinutes ){
                       try {
                           if (rows && rows.size() > 0) {
                               def json_str = JsonOutput.toJson(rows)
                               file.write(json_str)
                           }
                       } catch( Exception e ){}
                   }
               }
           }
           debug.seconds = debug.seconds ?: 40
           Date stop = new Date()
           TimeDuration td = TimeCategory.minus( stop, start )
           if( td.getSeconds() > debug.seconds.toInteger() ) {
               println ' '
               println cmdSql
               println params
               println 'SALVE sqlService() : duração > '+debug.seconds.toString()+'s : ' + td
               println '-'*80
               if( debug.email ) {
                   emailService.sendTo(debug.email, 'SALVE - SQL demorada ' + td, cmdSql + '<br>Parametros:<br>' + params.toString(), 'salve@icmbio.gov.br')
               }
           }
       } catch( Exception e) {
           lastError = e.getMessage()
           println ' '
           println 'Erro SqlSevice->execSql() em ' + new Date().format('dd/MM/yyyy HH:mm:ss')
           println cmdSql
           println params
           println ' '
           println e.getMessage()
           println ' '
           rows=null
       }
        if( sql ) {
            sql.close()
        }
       return rows
    }
    List execSqlCache( String cmdSql='', Map params=[:], Integer cacheMinutes=0i ) {
        return execSql( cmdSql, params, [:], cacheMinutes )
    }
    List execSqlCache( String cmdSql, Map params, Long cacheMinutes ) {
        return execSql( cmdSql, params, [:], cacheMinutes ? cacheMinutes.toInteger() : 0i)
    }


    /**
     * executar sql e criar os dados de paginação
     * @param cmdSql
     * @param params
     * @param pagination
     * @return
     */
    Map paginate( String cmdSql = '', Map qryParams = [:], Map requestParams = [:], Integer pageSize = 100, Integer totalButtons = 10, Integer cacheMinutes = 0, Map debug = [:] ) {
        pageSize = requestParams.paginationPageSize ? requestParams.paginationPageSize.toInteger() : pageSize
        Integer totalRecords = requestParams.paginationTotalRecords ? requestParams.paginationTotalRecords.toInteger() : 0
        // limitar máximo de 500 registros por página
        pageSize = Math.min(pageSize, 500)
        Integer pageNumber = requestParams.paginationPageNumber ? requestParams.paginationPageNumber.toInteger() : 1;
        Map pagination = [pageSize: pageSize, offset: 0, rowNum: 1, buttons: totalButtons, gridId: (requestParams.gridId ?: ''), pageNumber: pageNumber, totalRecords: totalRecords]
        List rows
        if( requestParams?.format?.toString()?.toLowerCase() == 'xls' ) {
            rows = execSql(cmdSql,qryParams,debug,cacheMinutes)
        } else {
            // fazer o controle do cache das paginas
            String fileName = ''
            if (cacheMinutes > 0) {
                //String rndId      = requestParams.rndIdFiltro ? '-' + requestParams.rndIdFiltro : ''
                String rndId = ''
                String hashString = cmdSql.replaceAll(/-- ?.+/, '').replaceAll(/\n(\s+)/, ' ') + qryParams.toString() + 'page-' + pageNumber
                String hashCode = hashString.encodeAsMD5()
                fileName = _cacheDir() + 'salve-pagination-' + hashCode + rndId + '-page-' + pageNumber + '.cache'
                Map cachedPage = _restorePageFromCache(fileName, cacheMinutes)
                if (cachedPage) {
                    println ' '
                    println 'SALVE - sqlService-paginate - pagina ' + pageNumber + ' recuperada do cache de ' + cacheMinutes + ' minutos - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
                    return cachedPage
                }
            }
            if (pagination.pageSize) {
                // se não informou o total de paginas, calcular...
                if (!requestParams.paginationTotalPages) {
                    rows = this.execSql(cmdSql, qryParams, debug, cacheMinutes)
                    if (rows) {
                        pagination.totalRecords = rows.size()
                        pagination.totalPages = Math.max(1, Math.ceil(pagination.totalRecords / pagination.pageSize)).toInteger()
                        pagination.pageNumber = Math.min(pagination.totalPages, pagination.pageNumber)
                        // recortar a página solicitada da lista de registros
                        rows = rows.collate(pagination.pageSize)[pagination.pageNumber - 1]
                        // calcular numero da linha
                        pagination.rowNum = ((pagination.pageNumber - 1) * pagination.pageSize) + 1
                    }
                } else {
                    // calcular o offset
                    pagination.totalPages = requestParams.paginationTotalPages.toInteger()
                    // pagina atual não pode ser maior que o total de paginas
                    pagination.pageNumber = Math.min(pagination.pageNumber.toInteger(), pagination.totalPages)
                    // calcular o offset
                    pagination.offset = (pagination.pageSize * (pagination.pageNumber - 1))
                    // adicionar parametros da paginação na query
                    cmdSql += "\n offset ${pagination.offset} LIMIT ${pagination.pageSize}"
                    rows = this.execSql(cmdSql, qryParams, debug, cacheMinutes)
                    // calcular numero da linha
                    pagination.rowNum = ((pagination.pageNumber - 1) * pagination.pageSize) + 1
                }
            } else {
                pagination.error = 'Please inform the param paginationPageSize=XX'
            }

            // executar a query
            /** /
             println ' '
             println 'SqlService.paginate() '
             println cmdSql
             println qryParams
             println 'Parametros Paginacao'
             println requestParams
             /**/

            if (fileName && !lastError) {
                _savePageToCache(fileName, [rows: rows, pagination: pagination])
            }
        }
        return [rows: rows, pagination: pagination,error:lastError]
    }

    /**
     * executar sql e criar os dados de paginação
     * @param cmdSql
     * @param params
     * @param pagination
     * @return
     */
    Map paginateOld( String cmdSql='', Map qryParams=[:], Map requestParams = [:], Integer pageSize = 100, Integer totalButtons = 10 ) {

        /** /
        println '-'*80
        println '-'*80
        println '-'*80
        println 'REQUEST PARAMS TO PAGINATE'
        println requestParams
        /**/

        Map pagination = [pageSize: pageSize, offset: 0, rowNum:1, buttons:totalButtons, gridId:(requestParams.gridId?:''),pageNumber:1,totalRecords:0]
        // solicitou uma pagina
        if( requestParams.paginationTotalRecords ) {
            pagination.totalRecords = requestParams.paginationTotalRecords.toInteger()
            if (requestParams?.paginationPageNumber) {
                pagination.pageNumber = requestParams.paginationPageNumber.toInteger()
            }
            // calcular o offset
            pagination.totalPages = Math.max(1, Math.ceil(pagination.totalRecords / pagination.pageSize))
            // pagina atual não pode ser maior que o total de paginas
            pagination.pageNumber = Math.min(pagination.pageNumber.toInteger(), pagination.totalPages.toInteger())
            // calcular o offset
            pagination.offset = (pagination.pageSize * (pagination.pageNumber - 1))
            if (pagination.offset >= pagination.totalRecords) {
                pagination.offset = Math.max(0, (pagination.totalRecords - pagination.pageSize) - 1)
            }
            pagination.offset = pagination.offset.toInteger()

            // adicionar parametros da paginação na query
            if( pagination.pageSize ) {
                cmdSql += "\n offset ${pagination.offset} LIMIT ${pagination.pageSize}"
            }
        }

        // executar a query
        /** /
        println ' '
        println 'SqlService.paginate() '
        println cmdSql
        println qryParams
        /**/
        List rows = this.execSql(cmdSql, qryParams)

        // calcular a paginação
        if( ! pagination.totalRecords )  {
            pagination.totalRecords = rows.size()
            pagination.totalPages   = Math.max(1, Math.ceil(pagination.totalRecords / pagination.pageSize))
            pagination.pageNumber   = 1
            pagination.rowNum       = 1
            rows = rows.collate( pagination.pageSize )[0]
        } else {
            pagination.rowNum = ( ( pagination.pageNumber - 1) * pagination.pageSize) + 1
        }

        return [ rows:rows?:[], pagination:pagination ]
    }


    List getAmeacas(){
        return execSql('select * from salve.vw_ameacas order by ordem')
    }

    List getUsos(){
        return execSql('select * from salve.vw_usos order by ordem')
    }

    List getAcoesConservacao(){
        return execSql('select * from salve.vw_acao_conservacao order by ordem')
    }

    List getConvencoes(){
        return execSql('select * from salve.vw_convencoes order by codigo')
    }

    List getRecursiveApoio( String tableName = ''){
        String qry="""with recursive arvore ( sq_dados_apoio
            , sq_dados_apoi_pai
            , cd_dados_apoio
            , ds_dados_apoio
            , de_dados_apoio_ordem
            , depth
            ) as
            (
            select da.sq_dados_apoio, da.sq_dados_apoio_pai, da.cd_dados_apoio , da.ds_dados_apoio, da.de_dados_apoio_ordem, 0 as depth
               from salve.dados_apoio da
            where da.cd_sistema = :tableName
            union all
            select dc.sq_dados_apoio, dc.sq_dados_apoio_pai, dc.cd_dados_apoio , dc.ds_dados_apoio, dc.de_dados_apoio_ordem, arvore.depth + 1
               from arvore
               inner join salve.dados_apoio dc on dc.sq_dados_apoio_pai = arvore.sq_dados_apoio
            )
            select arvore.depth as nivel, repeat('&nbsp;&nbsp;&nbsp;',arvore.depth-1)||arvore.cd_dados_apoio||'- '|| arvore.ds_dados_apoio as descricao, arvore.sq_dados_apoio as id from arvore
            where arvore.depth > 0
            order by arvore.de_dados_apoio_ordem
            """
        execSql( qry, ['tableName':tableName])
    }

    /**
     * gravar em cache a pagina
     * @param rows
     * @param pagination
     * @param hashCode
     * @param cacheMinutes
     * @param rndIdFiltro
     */
    protected void _savePageToCache(String fileName, Map data ){
        File file = new File( fileName )
        if( data && data.size() > 0 ) {
            def json_str = JsonOutput.toJson(data)
            //def json_beauty = JsonOutput.prettyPrint( json_str )
            file.write(json_str)
        }
    }
    /**
     * recuperar do cache a pagina
     * @param rows
     * @param pagination
     * @param hashCode
     * @param cacheMinutes
     * @param rndIdFiltro
     */
    protected Map _restorePageFromCache(String fileName, Integer cacheMinutes ) {
        File file = new File( fileName )
        if( !file.exists() || _cacheExpired( file, cacheMinutes ) ) {
            return null
        }
       def jsonSlurper = new JsonSlurper()
       Map cachedPage = jsonSlurper.parse( file )
        // é preciso retornar os valores numericos e data para seus tipos originais
       cachedPage.rows = _parseRows( cachedPage.rows )
       return cachedPage
    }
    /**
     * metodo para verificar se o arquivo em cache ja expirou
     * caso o tempo de vida tenha expirado, o arquivo é excluido
     * @param file
     * @return
     */
    protected boolean _cacheExpired( File file, Integer cacheMinutes) {
        Date modifiedAt = new Date( file.lastModified() )
        //int days = Util.dateDiff(modifiedAt, new Date(), 'D')
        int minutes = Util.dateDiff(modifiedAt, new Date(), 'M')
        //if( days >= cacheDays ) {
        if( minutes >= cacheMinutes ) {
            file.delete();
            return true;
        };
        return false
    }


    protected List _getDataFromCache( String cmd='', Map pars=[:], Integer cacheMinutes = 0 ) {
        String hashParams   = pars.hashCode()
        cmd = cmd.replaceAll(/-- ?.+/,'').replaceAll(/\n(\s+)/,' ')
        String hashSql      = cmd.hashCode()
        String cacheFileName= _cacheDir()+'public-' + hashParams+hashSql+'.cache'
        List data = []
        File file = new File( cacheFileName )
        if( file.exists() && ! _cacheExpired( file , cacheMinutes ) ) {
            d('Cache recuperado...')
            def jsonSlurper = new JsonSlurper()
            data = jsonSlurper.parse( file )
        }
        if( ! data || data.size() == 0 && cacheMinutes ) {
            try {
                data = execSql(cmd, pars)
                // só colocar no cache se a consulta retornou alguma linha
                if( data && data.size() > 0 ) {
                    def json_str = JsonOutput.toJson(data)
                    //def json_beauty = JsonOutput.prettyPrint( json_str )
                    file.write(json_str)
                }
            } catch( Exception e ) {
                throw new Exception( e.getMessage() );
            }
        }
        return data
    }

    /**
     * converter para os tipos originais os campos date e number depois de
     * recuperados do cache
     * @param rows
     * @return
     */
    protected List _parseRows( List rows = [] ){
        if( !rows ) {
            return []
        }
        rows.eachWithIndex { row, index ->
            row.each { key, value ->
                /** /
                 if( index == 0 ){println key+' = '+value}/**/
                try {
                    // converter campos data 2021-08-27T16:48:52+0000
                    if (key =~ /^dt_/) {
                        if (value =~ /^[0-9]{4}(-|\/)[0-9]{2}(-|\/)[0-9]{2}/) {
                            value = value.replaceAll(/\//, '-')
                            try {
                                row[key] = new Date().parse("yyyy-MM-dd'T'HH:mm:ss", value)
                            } catch (Exception e1) {
                                try {
                                    row[key] = new Date().parse("yyyy-MM-dd HH:mm:ss", value)
                                } catch (Exception e2) {
                                }
                            }
                        }
                    } else if (key =~ /^sq_/) {
                        // converter campos sequences
                        if (value.toString().isNumber()) {
                            row[key] = value.toString().toLong()
                        }
                    } else if (key =~ /^vl_/) {
                        // converter campos numericos
                        if (value.toString().isNumber()) {
                            row[key] = value.toString().toLong()
                        }
                    } else if (key =~ /^nu_/) {
                        // converter campos numericos
                        if (value.toString().isNumber()) {
                            row[key] = value.toString().toInteger()
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
        return rows
    }

    /**
     * método para recuperar item da tabela de apoio pelo codigo da tabela e codigo do item
     * @param tableCodigo
     * @param rowCodigo
     * @example getDadosApoio( 'TB_SITUACAO_FICHA' , 'AVALIADA' )
     * @return
     */
    Map getDadosApoio(String tableCodigo, String rowCodigo) {
        Map result = [:]
        if (tableCodigo) {
            List rowTable = execSqlCache('select * from salve.dados_apoio where cd_sistema = :tableCodigo and sq_dados_apoio_pai is null;', [tableCodigo: tableCodigo.toUpperCase()], UmDia)
            List rowItem
            if (rowTable) {
                result = rowTable[0]
                if (rowCodigo) {
                    Long sqDadosApoioPai = rowTable[0].sq_dados_apoio.toLong()
                    rowItem = execSqlCache('select * from salve.dados_apoio where sq_dados_apoio_pai = :sqDadosApoioPai and cd_sistema = :rowCodigo;', [sqDadosApoioPai: sqDadosApoioPai, rowCodigo: rowCodigo.toUpperCase()], UmDia)
                    //rowItem = execSql('select * from salve.dados_apoio where sq_dados_apoio_pai = :sqDadosApoioPai and cd_sistema = :rowCodigo;', [sqDadosApoioPai: sqDadosApoioPai, rowCodigo: rowCodigo.toUpperCase()])
                    if (rowItem) {
                        result = rowItem[0]
                    }
                }
            }
        }
        return result
    }

}
