package br.gov.icmbio

/*import java.awt.image.*
import java.awt.*
import static java.awt.RenderingHints.*
import javax.imageio.*
*/
import net.coobird.thumbnailator.Thumbnails
import net.coobird.thumbnailator.name.Rename

class ImageService
{

    // https://github.com/coobird/thumbnailator
    static String createThumb( String path, int newWidth=64, int newHeight=64 )
    {
        //path = path.replaceAll(/\\/,'/')
        String extension = Util.getFileExtension( path )
        if( ['png','jpg','jpeg','bmp','tiff','gif'].findAll { it == extension } )
        {
            Thumbnails.of(path)
                    .size(newWidth, newHeight)
                    .toFiles(Rename.PREFIX_DOT_THUMBNAIL)
        }
    }

    /*static String createThumb( String path, int newWidth=64, int newHeight=64 )
    {
        BufferedImage image = scaleTo(path,newWidth,newHeight)
        if( image )
        {
            String fileExtension = Util.getFileExtension(path)
            String thumbFileName = path.replace('.' + fileExtension, '.thumb.' + fileExtension)
            ImageIO.write(image, fileExtension, new File(thumbFileName))
            return thumbFileName
        }
        return ''
    }

    static BufferedImage scaleTo( String path, int newWidth, int newHeight )
    {
        File imgTemp = new File( path )
        BufferedImage image = null
        if( ! imgTemp )
        {
            return image
        }
        def hint = VALUE_INTERPOLATION_BILINEAR
        image = ImageIO.read( imgTemp )
        int type = ( image.getTransparency() == Transparency.OPAQUE ) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB
        int w = image.width
        int h = image.height
        boolean ended = false
        while( ! ended ) {
            if( w > newWidth ) {
                w /= 2
                if( w < newWidth ) {
                    w = newWidth
                }
            }
            if( h > newHeight ) {
                h /= 2
                if( h < newHeight ) {
                    h = newHeight
                }
            }
            image = new BufferedImage( w, h, type ).with { ni ->
                ni.createGraphics().with { g ->
                    g.setRenderingHint( KEY_INTERPOLATION, hint )
                    g.drawImage( image, 0, 0, w, h, null )
                    g.dispose()
                    ni
                }
            }
            if( w == newWidth || h == newHeight ) {
                ended=true
            }
        }
        return image
    }
    */
}