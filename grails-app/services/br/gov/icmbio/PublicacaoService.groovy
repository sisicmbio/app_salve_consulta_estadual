package br.gov.icmbio

import grails.transaction.Transactional

@Transactional
class PublicacaoService {

    List search(String q, String contexto = null, Boolean inListaOficialVigente = null )
    {
		q = q.trim();
        if (! q )
        {
           return []
        }
        // verificar se informou autor e ano
        List qs = q.split(' ');
        def cri = Publicacao.createCriteria()
        List lista = cri.list
        {
            qs.each { palavra ->

                palavra= URLDecoder.decode(palavra.toString(),"UTF-8")
                if ( palavra.isInteger() && palavra.toInteger() > 1500 )
                {
                    and {
                        eq('nuAnoPublicacao', palavra.toInteger())
                    }
                }
                else {
                    and {
                        or {
                            ilike("noAutor" ,'%'+palavra+'%')
                            ilike('deTitulo','%'+palavra+'%')
                        }
                    }
                }
            }

            if ( inListaOficialVigente != null ) {
                and {
                    eq('inListaOficialVigente', inListaOficialVigente )
                }

            }
            if (!contexto || contexto != 'ocorrencia') {
                List tiposInvalidos = TipoPublicacao.findAllByCoTipoPublicacaoInList(['MUSEU', 'BANCO_DADOS']);
                and {
                    not { 'in'("tipoPublicacao", tiposInvalidos) }
                }
            }
        }
		return lista;
    }
    //----------------------------------------------
}
