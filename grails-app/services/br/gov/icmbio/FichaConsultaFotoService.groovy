package br.gov.icmbio

import br.gov.icmbio.Util
import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import groovy.sql.Sql
import org.springframework.web.multipart.commons.CommonsMultipartFile


@Transactional
class FichaConsultaFotoService {

    def grailsApplication
    def dataSource
    def dadosApoioService
    def fichaMultimidiaService

    def salvar(Map params, GrailsHttpSession session)
    {
        Map res = [ status:0, msg:'', type:'error', errors:[], data:[] ]  
        
        if( !params.sqConsultaFicha ){            
            throw new Exception ( 'Ficha inválida!' )                
        }
        
        CicloConsultaFicha consultaFicha = CicloConsultaFicha.get(params.sqConsultaFicha)
        Ficha ficha = consultaFicha.ficha
        if( consultaFicha.cicloConsulta.tipoConsulta.codigoSistema != 'REVISAO_POS_OFICINA') {
            if (ficha.situacao.codigoSistema != 'CONSULTA' && ficha.situacao.codigoSistema != 'AVALIADA_EM_REVISAO') {                                
                throw new Exception ( 'Ficha não está em Consulta Ampla/Direta ou Avaliada em Revisão' )                
            }
        }

        // salva a foto enviada        
        DadosApoio tipoMultimidia = dadosApoioService.getByCodigo('TB_TIPO_MULTIMIDIA','IMAGEM')
        params.fotoAutor.eachWithIndex { it, i ->
            String index = i.toString()
            try {                           
                // se não existe a imagem ainda ou se ela existe e ainda não foi aprovada, pode ser alterada  
                FichaConsultaFoto fcf
                if (params.fotoId[index]) {
                    fcf = FichaConsultaFoto.get(params.fotoId[index].toLong())                                        
                }
     
                FichaMultimidia fm = fichaMultimidiaService.save(
                        !fcf ? null : fcf.fichaMultimidia.id
                        , consultaFicha.ficha.id.toLong()
                        // se for uma alteração, pode não ter a foto
                        , ( ( params.foto && params.foto[index] ) ? ((CommonsMultipartFile) params.foto[index]) : null)
                        , tipoMultimidia.id.toLong()
                        , null
                        , 'Colaboração externa'
                        , params.fotoDescricao[index].toString()
                        , params.fotoAutor[index].toString()
                        , params.fotoEmailAutor[index].toString() 
                        , new Date()
                        , false
                        , null
                        , null
                )

                if (fm.id) {                                        
                    Usuario webUsuario   = Usuario.get( session.sicae.user.sqPessoa.toLong() )

                    if(!fcf){
                        fcf = new FichaConsultaFoto()
                    }               
                    fcf.fichaMultimidia     = fm     
                    fcf.consultaFicha       = consultaFicha
                    fcf.usuario             = webUsuario
                    fcf.inTipoTermoResp     = params.fotoInTipoTermoResp[index].toString()                            
                    fcf.dtAceiteTermoResp   = new Date()                    
                    fcf.nuIpComputador      = params.ip_user
                    fcf.noNavegador         = params.user_agent
                    
                    fcf.save(flush: true)

                    if( ! fcf.id ){                        
                        throw new Exception ( 'Ops! Não foi possível salvar a foto. Favor tentar novamente.' )                                   
                    }               
                }                                                              
            } catch (Exception e) {                 
                res.errors.push( e.getMessage() )
                throw new Exception ( e.getMessage() )                
            }
        }   
        return res             
    }

    /**
    * Deleta a foto - FichaConsultaFoto e a FichaConsultaMultimidia
    */
    def delete(Long sqFichaConsultaFoto, GrailsHttpSession selfSession){                     
        // verificar se o id da foto foi informado
        if (!sqFichaConsultaFoto) {
            throw new Exception ( 'Id da Foto não informado!' )
        }
        
        FichaConsultaFoto foto = FichaConsultaFoto.get(sqFichaConsultaFoto)
        if (!foto) {
            throw new Exception ( 'Id da foto inexistente!' )
        }
        // verificar se a foto é do usuário logado
        if( foto.usuario.id.toLong() != selfSession.sicae.user.sqPessoa.toLong() )
        {
            throw new Exception ( 'Foto foi cadastrada por outra pessoa!' )
        }
        // Exclui apenas registro com situação diferente de APROVADO (1074)
        DadosApoio situacaoAprovada = dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA','APROVADA')
        if(foto.fichaMultimidia?.situacao == situacaoAprovada ){
            throw new Exception('Ops! A situação deste registro não permite sua exclusão.')
        }        

        String arquivoExcluir
        if(foto.fichaMultimidia.noArquivoDisco){                                
            arquivoExcluir = foto.fichaMultimidia.noArquivoDisco
        }
        // deleta a FichaConsultaFoto e a FichaConsultaMultimidia
        foto.fichaMultimidia.delete(flush:true)

        // Exclui os arquivos do disco            
        if(arquivoExcluir){   
            // Exclui do disco apenas se existir uma unica imagem com mesmo noArquivoDisco na base de dados
            String qry = """
            select count(no_arquivo_disco) from salve.ficha_multimidia 
            where no_arquivo_disco = '${arquivoExcluir}'   
            """        
            Integer total = 0
            def sql = new Sql( dataSource )
            sql.eachRow(qry) { it ->
                total = it.count            
            }
            if(total <= 1){
                File fileFoto   = new File(grailsApplication.config.multimidia.dir + foto.fichaMultimidia.noArquivoDisco)
                File fileThumb  = new File(grailsApplication.config.multimidia.dir + 'thumbnail.' + foto.fichaMultimidia.noArquivoDisco)                
                if (fileFoto.exists()) {            
                    fileFoto.delete()
                }
                if (fileThumb.exists()) {            
                    fileThumb.delete()
                }
            }                        
        }
    }

    /**
    * Retorna as colaborações de fotos do usuário logado
    */
    def minhasFotosColaboracao(Map params, GrailsHttpSession session) {                             
        Map data = [ status:0, msg:'', data:['fotosColaboracao':[] ] ]

        if( ! params.sqConsultaFicha ){
            data.status = 1
            data.msg = 'Ficha inválida!'
            return data
        }

        Ficha ficha        
        CicloConsultaFicha cicloConsultaFicha        
        
        cicloConsultaFicha = CicloConsultaFicha.get(params.sqConsultaFicha.toLong())
        
        ficha = cicloConsultaFicha?.ficha

        String qry = """
            select fcf.sq_ficha_consulta_foto, fcf.sq_ficha_multimidia, fcf.in_tipo_termo_resp, 
            fm.sq_situacao, fm.dt_elaboracao, fm.no_autor, fm.de_legenda, fm.tx_multimidia,
            da.ds_dados_apoio AS situacao
            from salve.ficha_consulta_foto fcf
            inner join salve.ficha_multimidia fm on fm.sq_ficha_multimidia  = fcf.sq_ficha_multimidia
            inner join salve.dados_apoio da on  da.sq_dados_apoio = fm.sq_situacao
            inner join salve.ciclo_consulta_ficha ccf on ccf.sq_ciclo_consulta_ficha = fcf.sq_ciclo_consulta_ficha
            inner join salve.ciclo_consulta cc on cc.sq_ciclo_consulta = ccf.sq_ciclo_consulta
            where fcf.sq_web_usuario = ${session.sicae.user.sqPessoa}
            and fm.sq_ficha = ${ficha.id}
            and cc.dt_fim >= CURRENT_DATE
            """  
        List itens = []                          
        def sql = new Sql( dataSource )
        sql.eachRow(qry) { it ->
            String desTermoAceito = ''
            if(it.in_tipo_termo_resp == 'USO_IRRESTRITO'){
                desTermoAceito = 'Uso amplo e irrestrito'
            }else{ // USO_RESTRITO_AVALIACAO
                desTermoAceito = 'Uso relacionado ao processo de avaliação do risco de extinção da fauna'
            }
            data.data.fotosColaboracao.push([id             : it.sq_ficha_consulta_foto
                                            , idEdit         : it.sq_ficha_consulta_foto
                                            , url            : 'getMultimidia/'+it.sq_ficha_multimidia.toString()+'/thumb'
                                            , inAprovada     : it.situacao 
                                            , dtFoto         : it.dt_elaboracao.format('dd/MM/yyyy')
                                            , hrFoto         : it.dt_elaboracao.format('hh:mm')
                                            , noAutorFoto    : it.no_autor
                                            //, deLegenda      : it.de_legenda  
                                            , descricao      : it.tx_multimidia
                                            , termoAceito    : desTermoAceito
            ])                                                                         
        }
        /*  // NÃO SERÁ USADO PAGINAÇÂO NO MOMENTO                  
        params.pageNumber = params.pageNumber ? params.pageNumber.toInteger() : 1
        // fazer a paginação dos registros
        Map pagination = [ pageSize:100
                ,pageNumber: params.pageNumber
                ,totalRecords :data.data.fotosColaboracao.size()
                ,totalPages : 1
                ,rowNumber : 1
        ]
        pagination.totalPages = Math.max(1, Math.ceil( pagination.totalRecords / pagination.pageSize) ).toInteger()
        pagination.pageNumber = Math.min( pagination.totalPages, pagination.pageNumber)

        // calcular numero da linha
        pagination.rowNum = ((pagination.pageNumber - 1) * pagination.pageSize) + 1

        // fatiar registros para pegar a pagina correta
        data.data.fotosColaboracao = data.data.fotosColaboracao.collate(pagination.pageSize)[pagination.pageNumber - 1]
        /*
        println' '
        println'PAGINAÇAO'
        println 'Página..............: ' + pagination.pageNumber.toString()
        println 'Total de Ocorrencias: ' + pagination.totalRecords.toString()
        println 'Registros por página: ' + pagination.pageSize.toString()
        println 'Total páginas.......: ' + pagination.totalPages.toString()
        println 'Linha numero........: ' + pagination.rowNum.toString()
        */
        //data.fotoColaboracaoPagination = pagination

        return data
    }
}
