package br.gov.icmbio

//import grails.transaction.Transactional

//@Transactional
class DadosApoioService {

 	public static List getTable( String table_name )
    {
		List pai = DadosApoio.findAllByCodigoSistema(table_name);
		if( pai )
		{
    		return DadosApoio.findAllByPai(pai)
		}
		return []
    }

	public static DadosApoio getByCodigo( String table_name, String codigoSistema )
    {
		List pai = DadosApoio.findAllByCodigoSistema(table_name);
		if( pai )
		{
    		return DadosApoio.findByPaiAndCodigoSistema(pai,codigoSistema)
		}
		return null
    }

    public static DadosApoio getByDescricao( String table_name, String descricao)
    {
        List pai = DadosApoio.findAllByCodigoSistema(table_name);
        if( pai )
        {
            return DadosApoio.findByPaiAndDescricaoIlike(pai,descricao)
        }
        return null
    }

    public static List getNodeTree(DadosApoio node)
    {
        List data = [];
        node.itens.each {
            data.push([id:it.id,title:it.descricao,icon:false,foder:false])
        }
        return data;
    }

}
