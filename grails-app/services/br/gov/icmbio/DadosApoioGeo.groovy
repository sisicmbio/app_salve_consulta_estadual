
package br.gov.icmbio
import com.vividsolutions.jts.geom.Geometry
import org.hibernate.spatial.GeometryType
class DadosApoioGeo {

    String cdGeo
    String txGeo
    Geometry geGeo

    static belongsTo = [dadosApoio:DadosApoio]

    static constraints = {
        cdGeo  nullable:false
        txGeo  nullable:true
        geGeo  nullable:false
    }

    static mapping = {
        version 	  false
        sort        txGeo: 'asc'
        table       name:'salve.dados_apoio_geo'
        id          column:'sq_dados_apoio_geo',generator:'identity'
        dadosApoio  column:'sq_dados_apoio'
        geGeo       type: GeometryType, sqlType: "GEOMETRY"
    }

    def beforeValidate() {
        // SRID padrão: 4674
        if (this.geGeo && this.geGeo?.SRID == 0) {
            geGeo.SRID = 4674
        }
    }
}
