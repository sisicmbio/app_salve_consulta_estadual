package br.gov.icmbio


import grails.transaction.Transactional
import grails.util.Holders
import org.springframework.context.ApplicationContext
import org.springframework.web.multipart.commons.CommonsMultipartFile

@Transactional
class ColaborarService {

    def dadosApoioService

    List getBreadcrumb(Ficha ficha )
    {
        List h = []
        ficha.taxon.structure.each { key, value ->
            if( key ==~ /^no.*/ && key != 'noCientifico')
            {
                if( key=='noEspecie' || key=='noSubespecie')
                {
                    value = value.toString().toLowerCase()
                }
                h.push( value )
            }
        }
        return h.reverse()
    }

    List getFichaImages(Ficha ficha=null, String contexto='')
    {
        List listArquivos = []
        String strTemp = ''
        File file
        DadosApoio apoioContexto = dadosApoioService.getByCodigo('TB_CONTEXTO_ANEXO', contexto)
        FichaAnexo.findAllByFichaAndContexto(ficha, apoioContexto).each {
            strTemp = it.deLocalArquivo.toString()
            file = new File(strTemp)
            if (!file.exists())
            {
                ApplicationContext ctx = Holders.grailsApplication.mainContext
                file = new File(ctx.grailsApplication?.config?.anexos?.dir + strTemp)
                ctx = null
            }
            if ( it.deLocalArquivo && file && file.exists())
            {
                listArquivos.push( [id:it.id, localArquivo:file.getAbsolutePath(), legenda:it.deLegenda] )
            }
        }
        return listArquivos
    }

     FichaConsultaAnexo saveFichaConsultaAnexo(
                Long sqFichaConsultaAnexo   = null
              , Long sqCicloConsultaFicha   = null
              , CommonsMultipartFile file   = null
              , Long sqWebUsuario           = null) {

         // ler contexto grailsApplication
         ApplicationContext ctx = Holders.grailsApplication.mainContext
        FichaConsultaAnexo fca
        DadosApoio situacao = null
         if ( !file || file.empty )
         {
             throw new Exception('Arquivo está vazio!')
         }
         if ( ! sqCicloConsultaFicha )
         {
             throw new Exception('Ciclo consulta não informado!')
         }
         CicloConsultaFicha cicloConsultaFicha = CicloConsultaFicha.get( sqCicloConsultaFicha )
         if ( !cicloConsultaFicha )
         {
             throw new Exception('Ciclo consulta inválido!')
         }
        String mmDir = ctx.grailsApplication.config.anexos.dir
        File savePath = new File(mmDir)
        if (!savePath.exists()) {
            if (!savePath.mkdirs()) {
                throw new Exception ( 'Não foi possivel criar o diretório ' + savePath.asString() )
            }
        }
        String fileName = file.getOriginalFilename()
        String fileExtension = Util.getFileExtension( fileName )
        String fileNameMd5 = file.inputStream.text.toString().encodeAsMD5() +'.'+fileExtension
        fileName = savePath.absolutePath + '/' + fileNameMd5
        // copiar o arquivo para o diretorio /anexo se o arquivo aind não existir
        if ( ! new File( fileName ).exists() ) {
            file.transferTo(new File(fileName) )
        }
        fileName = file.getOriginalFilename()
        situacao = dadosApoioService.getByCodigo('TB_SITUACAO_COLABORACAO', 'NAO_AVALIADA')
        Usuario webUsuario = Usuario.get( sqWebUsuario )

         if( ! sqFichaConsultaAnexo ) {
             // verificar se o arquivo ja foi enviado pelo usuario
             fca = FichaConsultaAnexo.createCriteria().get {
                 eq('deLocalArquivo', fileNameMd5)
                 eq('webUsuario', webUsuario)
                 eq('consultaFicha', cicloConsultaFicha)
                 maxResults(1)
             }
         } else {
             fca = FichaConsultaAnexo.get( sqFichaConsultaAnexo )
         }
         if( ! fca ) {
             fca = new FichaConsultaAnexo()
             fca.dtInclusao = new Date()
             fca.webUsuario = webUsuario
             fca.situacao = situacao
             fca.consultaFicha = cicloConsultaFicha
        }
        fca.deLocalArquivo = fileNameMd5
        fca.noArquivo = fileName
        fca.deTipoConteudo = fileExtension
        fca.validate()
        if ( fca.hasErrors() ) {
                println fca.getErrors()
                throw new Exception('Inconsistência na gravação do arquivo.')
        }
        fca.save(flush: true)
        return fca
    }


}
