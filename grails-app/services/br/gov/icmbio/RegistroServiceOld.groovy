package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry

/*import grails.transaction.Transactional
@Transactional
*/
class RegistroServiceOld {

    def sqlService
    /**
     * verificar se o ponto já existe e retornar o ID
     * @param geometry
     * @return
     */
    Long pointExists(Geometry geometry) {
        Map coords = Util.roundCoords(geometry.y, geometry.x)
        // criar o ponto no formato texto com SRID 4674
        String ponto = "SRID=4674;POINT(${coords.lonX.toString()} ${coords.latY.toString()})"
        String cmdSql = "select sq_registro from salve.registro where ge_registro = ST_GeomFromText('${ponto}') limit 1"
        List rows = sqlService.execSql(cmdSql)
        if (rows) {
            return rows[0].sq_registro.toLong();
        }
        return null
    }

    /**
     * criar o ponto e retornar o novo ID
     * @param geometry
     * @return
     */
    Long createPoint(Geometry geometry) {
        Long id = pointExists(geometry)
        if (!id) {
            Map coords = Util.roundCoords(geometry.y, geometry.x)
            // criar o ponto no formato texto com SRID 4674
            String ponto = "SRID=4674;POINT(${coords.lonX.toString()} ${coords.latY.toString()})"
            List rows = sqlService.execSql("insert into salve.registro ( ge_registro) values ( ST_GeomFromText('${ponto}')) returning sq_registro")
            if ( rows ) {
                // println ' - novo registro - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
                id = rows[0].sq_registro.toLong()
                // adicionar o Pais que o ponto intersecta
                addPais(id, geometry)
                // adicionar o Estado que o ponto insersecta
                addEstado(id, geometry)
                // adicionar o Municipio que o ponto intersecta
                addMunicipio(id, geometry)
                // adicionar o Bioma que o ponto intersecta
                addBioma(id, geometry)
                // adicionar o Bacia que o ponto intersecta
                addBacia(id, geometry)
                // adicionar o UCs Federais que o ponto intersecta
                addUcFederal(id, geometry)
                // adicionar o UC Estadual que o ponto intersecta
                addUcEstadual(id, geometry)
                // adicionar o RPPN  que o ponto intersecta
                addRppn(id, geometry)

            }
        }
        return id
    }

    /**
     * criar o registro na tabela Registro e ligar o ponto do SALVE ao registro criado
     * @param fo
     */
    void saveOcorrenciaSalve(FichaOcorrencia fo) {
        saveOcorrenciaSalve([ id:fo.id, geometry:fo.geometry] )
    }
    void saveOcorrenciaSalve(Map fo) {
         //println ' '
         //println 'SaveOcorrenciaSalve iniciado...'
        try {
            if (!fo || !fo.geometry || !fo.id) {
                println ' - ocorrencia sem id'
                return
            }
            // verificar se o ponto já existe e senão existir cria-lo e retornar no ID
            Long pointId = createPoint(fo.geometry)
            Map sqlParams=[:]
            if (pointId) {
                //println ' - registro encontrado: ' + pointId
                // verificar se a ocorrencia já está ligada a um registro e se tiver fazer somente um update
                sqlParams = [ sqOcorrencia:fo.id.toLong() ]
                List rows = sqlService.execSql("""select sq_ficha_ocorrencia_registro from salve.ficha_ocorrencia_registro
                        where sq_ficha_ocorrencia = :sqOcorrencia""",
                    sqlParams)
                if ( rows ) {
                    sqlParams = [sqRegistro:pointId, sqFichaOcorrenciaRegistro:rows[0].sq_ficha_ocorrencia_registro.toLong()]
                    sqlService.execSql("""update salve.ficha_ocorrencia_registro set sq_registro = :sqRegistro where sq_registro <> :sqRegistro and sq_ficha_ocorrencia_registro = :sqFichaOcorrenciaRegistro""", sqlParams)
                } else {
                    // fazer o relacionamento do REGISTRO X FICHA_OCORRENCIA
                    sqlParams = [sqOcorrencia: fo.id.toLong(), sqRegistro: pointId]
                        rows = sqlService.execSql("""select sq_ficha_ocorrencia_registro from salve.ficha_ocorrencia_registro
                        where sq_ficha_ocorrencia = :sqOcorrencia and sq_registro = :sqRegistro""",
                        sqlParams)
                    if (!rows) {
                        sqlService.execSql("""insert into salve.ficha_ocorrencia_registro (sq_ficha_ocorrencia,sq_registro) values (:sqOcorrencia,:sqRegistro)""",
                        sqlParams)
                        //println ' - relacionamento registro x ocorrencia salve criado'
                    }
                }

                // atualizar o Estado e o Municipio que são exibidos na aba 2.6.3
                FichaOcorrencia fichaOcorrencia = FichaOcorrencia.get( fo.id )
                if( fichaOcorrencia ) {
                    sqlService.execSql("""select re.sq_estado
                    from salve.registro_estado re
                    inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_registro = re.sq_registro
                    where foreg.sq_ficha_ocorrencia = ${fichaOcorrencia.id.toString()}""").each { row ->
                        fichaOcorrencia.estado = Estado.get(row.sq_estado.toLong())
                        fichaOcorrencia.save(flush: true);
                    }
                    sqlService.execSql("""select re.sq_municipio
                    from salve.registro_municipio re
                    inner join salve.ficha_ocorrencia_registro foreg on foreg.sq_registro = re.sq_registro
                    where foreg.sq_ficha_ocorrencia = ${fichaOcorrencia.id.toString()}""").each { row ->
                        fichaOcorrencia.municipio = Municipio.get(row.sq_municipio.toLong())
                        fichaOcorrencia.save(flush: true);
                    }
                }


            } else {
                println 'NAO FEZ NADA'
            }
        } catch (Exception e) {
            println ' '
            println 'Erro - RegistroService - saveOcorrenciaSalve() - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            println e.getMessage()
            println e.getProperties()
        }
        //println 'SaveOcorrenciaSalve finalizado'
    }

    /**
     * criar o registro na tabela Registro e ligar o ponto do PORTALBIO ao registro criado
     * @param fo
     */
    void saveOcorrenciaPortalbio(FichaOcorrenciaPortalbio fo) {
        saveOcorrenciaPortalbio([ id:fo.id, geometry:fo.geometry] )
    }
    void saveOcorrenciaPortalbio(Map fo) {
        //println ' '
        //println 'SaveOcorrenciaPortalbio iniciado...'
        try {
            if (!fo || !fo.geometry || !fo.id) {
                println ' - ocorrencia sem id'
                return
            }
            // verificar se o ponto já existe e senão existir cria-lo e retornar no ID
            Long pointId = createPoint(fo.geometry)
            if (pointId) {
                //println ' - registro encontrado: ' + pointId

                // fazer o relacionamento do REGISTRO X FICHA_OCORRENCIA
                Map sqlParams = [sqOcorrencia: fo.id.toLong(), sqRegistro: pointId]
                //println sqlParams
                List rows = sqlService.execSql("""select sq_ficha_ocorrencia_portalbio_reg from salve.ficha_ocorrencia_portalbio_reg
                        where sq_ficha_ocorrencia_portalbio = :sqOcorrencia and sq_registro = :sqRegistro""",
                    sqlParams)
                if ( ! rows ) {
                    sqlService.execSql("""insert into salve.ficha_ocorrencia_portalbio_reg (sq_ficha_ocorrencia_portalbio,sq_registro) values (:sqOcorrencia,:sqRegistro)""", sqlParams)
                    //println ' - relacionamento registro x ocorrencia portalbio criado'
                }
            }
        } catch (Exception e) {
            println ' '
            println 'Erro - RegistroService - saveOcorrenciaPortalbio() - ' + new Date().format('dd/MM/yyyy HH:mm:ss')
            println e.getMessage()
            println e.getProperties()
        }
        //println 'SaveOcorrenciaPortalbio finalizado'
    }

    /**
     * Adicionar o Pais que o ponto intersecta
     * @param idRegistro
     * @param geometry
     */
    void addPais(Long sqRegistro, Geometry geometry) {
        Map coords = Util.roundCoords(geometry.y, geometry.x)
        String ponto = "SRID=4674;POINT(${coords.lonX.toString()} ${coords.latY.toString()})"
        List rows = sqlService.execSql("select sq_pais from corporativo.vw_pais where the_geom is not null and st_intersects( '${ponto}'::geometry , the_geom  ) limit 1")
        if (rows) {
            Map sqlParams = [sqRegistro: sqRegistro, sqPais: rows[0].sq_pais.toLong()]
            sqlService.execSql("insert into salve.registro_pais (sq_registro, sq_pais) values (:sqRegistro,:sqPais)", sqlParams)
        }
        return
    }

    /**
     * Adicionar o Estado que o ponto intersecta
     * @param idRegistro
     * @param geometry
     */
    void addEstado(Long sqRegistro, Geometry geometry) {
        Map coords = Util.roundCoords(geometry.y, geometry.x)
        String ponto = "SRID=4674;POINT(${coords.lonX.toString()} ${coords.latY.toString()})"
        List rows = sqlService.execSql("select sq_estado from corporativo.vw_estado where the_geom is not null and st_intersects( '${ponto}'::geometry , the_geom  ) limit 1")
        if (rows) {
            Map sqlParams = [sqRegistro: sqRegistro, sqEstado: rows[0].sq_estado.toLong()]
            sqlService.execSql("insert into salve.registro_estado (sq_registro, sq_estado) values (:sqRegistro,:sqEstado)", sqlParams)
        }
        return
    }

    /**
     * Adicionar o Municipio que o ponto intersecta
     * @param idRegistro
     * @param geometry
     */
    void addMunicipio(Long sqRegistro, Geometry geometry) {
        Map coords = Util.roundCoords(geometry.y, geometry.x)
        String ponto = "SRID=4674;POINT(${coords.lonX.toString()} ${coords.latY.toString()})"
        List rows = sqlService.execSql("select sq_municipio from corporativo.vw_municipio where the_geom is not null and st_intersects( '${ponto}'::geometry , the_geom  ) limit 1")
        if (rows) {
            Map sqlParams = [sqRegistro: sqRegistro, sqMunicipio: rows[0].sq_municipio.toLong()]
            sqlService.execSql("insert into salve.registro_municipio (sq_registro, sq_municipio) values (:sqRegistro,:sqMunicipio)", sqlParams)
        }
        return
    }

    /**
     * Adicionar o Bioma que o ponto intersecta
     * @param idRegistro
     * @param geometry
     */
    void addBioma(Long sqRegistro, Geometry geometry) {
        Map coords = Util.roundCoords(geometry.y, geometry.x)
        String ponto = "SRID=4674;POINT(${coords.lonX.toString()} ${coords.latY.toString()})"
        List rows = sqlService.execSql("select sq_bioma from corporativo.vw_bioma where the_geom is not null and st_intersects( '${ponto}'::geometry , the_geom  ) limit 1")
        if (rows) {
            Map sqlParams = [sqRegistro: sqRegistro, sqBioma: rows[0].sq_bioma.toLong()]
            sqlService.execSql("insert into salve.registro_bioma (sq_registro, sq_bioma) values (:sqRegistro,:sqBioma)", sqlParams)
        }
        return
    }

    /**
     * Adicionar a Bacia que o ponto intersecta
     * @param idRegistro
     * @param geometry
     */
    void addBacia(Long sqRegistro, Geometry geometry) {
        Map coords = Util.roundCoords(geometry.y, geometry.x)
        String ponto = "SRID=4674;POINT(${coords.lonX.toString()} ${coords.latY.toString()})"
        List rows = sqlService.execSql("""select geo.sq_dados_apoio_geo as sq_bacia_geo
                    from salve.vw_bacia_hidrografica as vw_bacia
                    inner join salve.dados_apoio_geo as geo on geo.sq_dados_apoio = vw_bacia.id
                    where st_intersects( '${ponto}'::geometry , geo.ge_geo ) limit 1""")
        if (rows) {
            Map sqlParams = [sqRegistro: sqRegistro, sqBaciaGeo: rows[0].sq_bacia_geo.toLong()]
            sqlService.execSql("insert into salve.registro_bacia (sq_registro, sq_bacia_geo) values (:sqRegistro,:sqBaciaGeo)", sqlParams)
        }
        return
    }

    /**
     * Adicionar as UCs federais que o ponto intersecta
     * @param idRegistro
     * @param geometry
     */
    void addUcFederal(Long sqRegistro, Geometry geometry) {
        Map coords = Util.roundCoords(geometry.y, geometry.x)
        String ponto = "SRID=4674;POINT(${coords.lonX.toString()} ${coords.latY.toString()})"
        List rows = sqlService.execSql("""select sq_pessoa as id from corporativo.vw_unidade_org where the_geom is not null and st_intersects('${ponto}'::geometry, ST_CollectionExtract( the_geom,3) ) offset 0 limit 2""")
        rows.each{ row->
            Map sqlParams = [sqRegistro: sqRegistro, sqUc: row.id.toLong()]
            sqlService.execSql("insert into salve.registro_uc_federal (sq_registro, sq_uc_federal) values (:sqRegistro,:sqUc)", sqlParams)
        }
        return
    }

    /**
     * Adicionar a UC estadual que o ponto intersecta
     * @param idRegistro
     * @param geometry
     */
    void addUcEstadual(Long sqRegistro, Geometry geometry) {
        Map coords = Util.roundCoords(geometry.y, geometry.x)
        String ponto = "SRID=4674;POINT(${coords.lonX.toString()} ${coords.latY.toString()})"
        List rows = sqlService.execSql("""select gid as id from geo.vw_uc_estadual_geo where the_geom is not null and st_intersects('${ponto}'::geometry, ST_CollectionExtract( the_geom,3) )""")
        rows.each{ row->
            Map sqlParams = [sqRegistro: sqRegistro, sqUc: row.id.toLong()]
            sqlService.execSql("insert into salve.registro_uc_estadual (sq_registro, sq_uc_estadual) values (:sqRegistro,:sqUc)", sqlParams)
        }
        return
    }

    /**
     * Adicionar a RPPN que o ponto intersecta
     * @param idRegistro
     * @param geometry
     */
    void addRppn(Long sqRegistro, Geometry geometry) {
        Map coords = Util.roundCoords(geometry.y, geometry.x)
        String ponto = "SRID=4674;POINT(${coords.lonX.toString()} ${coords.latY.toString()})"
        List rows = sqlService.execSql("""select sq_pessoa from corporativo.vw_rppn where the_geom is not null and st_intersects('${ponto}'::geometry, ST_CollectionExtract( the_geom,3) ) offset 0 limit 1""")
        rows.each{ row->
            Map sqlParams = [sqRegistro: sqRegistro, sqRppn: row.sq_pessoa.toLong()]
            sqlService.execSql("insert into salve.registro_rppn (sq_registro, sq_rppn) values (:sqRegistro,:sqRppn)", sqlParams)
        }
        return
    }
    // -- fim
}
