package br.gov.icmbio

import grails.transaction.Transactional
import grails.validation.ValidationErrors
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap

@Transactional
class RegistrarService {

    def messageSource
    def emailService
    Locale locale=Locale.getDefault()

    private res = [status:0,errors:[],msg:'',data:[:]]

    private Boolean hasError( ValidationErrors errors, String errorMessage='') {
        // TODO - IMPLEMENTAR SALVE ESTADUAL
        /*
        String msg
        errors.allErrors.each {
             msg=messageSource.getMessage(it, locale )
             res.errors.push( msg )
             log.error( msg )
        }
        if( errors.allErrors.size() > 0 )
        {
            if( errorMessage )
            {
                throw new Exception( errorMessage )
            }
            return true
        }*/
        return false
    }

    Map save( GrailsParameterMap params ) {

        res = [status:1,errors:[],msg:'',data:[:] ]
        try {

            if( ! params?.senha ){
                res.errors.push('Senha é obrigatória!');
            }
            else if( ( params?.senha != params?.senha2 ) ){
                res.errors.push('Senhas não conferem');
            }

            if( !params.email ) {
                res.errors.push('Email é obrigatório!')
            } else {
                params.email = params.email.toLowerCase()

                if (! Util.isEmail(params.email) ) {
                    res.errors.push('Email inválido!')
                }

                Pessoa pessoa = Pessoa.findByDeEmail(params.email)
                if (pessoa) {
                    res.errors.push('Email <b>' + params.email + '</b> já está cadastrado!')
                }
            }

            if( !params.nome ) {
                res.errors.push('Nome é obrigatório!')
            }

            if( !params.sqInstituicao && !params.noOutraInstituicao ) {
                res.errors.push('Instituição é obrigatória!')
            }

            if( res.errors ) {
                throw new Exception('Erro(s) Encontrado(s)')
            }


            // criar webUser com a conta desativada
            Pessoa pessoa = new Pessoa()
            pessoa.deEmail = params.email.toLowerCase()
            pessoa.noPessoa = params.nome
            pessoa.save()
            PessoaFisica pf = new PessoaFisica()
            pf.pessoa = pessoa
            pf.save()

            Usuario usuario = new Usuario()
            usuario.pessoaFisica = pf
            usuario.deSenha = Util.md5(params.senha.toString())
            usuario.noInstituicao = params.noInstituicaoOutra ?: null
            usuario.sgInstituicao = params.sgInstituicaoOutra ?: null
            usuario.stAtivo = false
            usuario.deHash = usuario.getHash()
            usuario.save()
            // enviar email para o usuario
            //emailService.sendConfirmRegistro(user.email, user.fullName, user.hashAtivacao)
            if( emailService.sendEmailAtivacaoConta(usuario) ) {
                res.status=0
            }
            else {
                res.msg='Falha no envio do e-mail.'
                res.status=1
            }
            return res
        } catch( Exception e) {
            res.status = 1
            res.msg = 'Falha ao efetuar o registro'
            log.error(e.getMessage())
            res.errors.push( e.getMessage() )
        }
        return res

        /*
        sleep(100)
        res = [status:1,errors:[],msg:'',data:[:]]
        try {

            res = [status:0,errors:[],msg:'',data:[:]]

            if( ! params?.senha )
            {
                res.errors.push('Senha é obrigatória!');
            }
            else if( ( params?.senha != params?.senha2 ) )
            {
                res.errors.push('Senhas não conferem');
            }

            if( !params.email )
            {
                res.errors.push('Email é obrigatório!')
            }
            Integer qtd = WebUsuario.countByUsername( params.email )
            if( qtd > 0 )
            {
                res.errors.push('Email <b>' + params.email+'</b> já está cadastrado!')
            }
            if( !params.nome )
            {
                res.errors.push('Nome é obrigatório!')
            }
            if( !params.sqInstituicao && !params.noOutraInstituicao )
            {
                res.errors.push('Instituição é obrigatória!')
            }
            if( res.errors )
            {
                throw new Exception('Erro(s) Encontrado(s)')
            }

            WebInstituicao inst
            // verificar a instituição informada, se não existir, criar.
            if( params?.sqInstituicao?.toInteger() > 0  )
            {
                inst = WebInstituicao.get( params?.sqInstituicao?.toInteger() )
            }
            else
            {
                inst = WebInstituicao.findByNoInstituicao( params?.noInstituicaoOutra )
                if( !inst )
                {
                    inst = new WebInstituicao(noInstituicao:params.noInstituicaoOutra,sgInstituicao: params?.sgInstituicaoOutra).save(flush:true)
                }
            }
            if( ! inst )
            {
                throw new Exception('Instituição informada é inválida!')
            }

            // ler o perfil ROLE_USER
            WebRole role = WebRole.findByAuthority('ROLE_USER')
            if( !role )
            {
                throw new Exception('Permissão USER não cadastrada!')
            }

            // criar webUser com a conta desativada
            WebUsuario user = new WebUsuario(noUsuario:params.nome, username:params.email, password:params.senha, enabled:false,instituicao:inst)
            if( !user )
            {
                throw new Exception('Erro na criação do usuário!')
            }
            // user.deHashAtivacao = springSecurityService.encodePassword( user.id + params.email + new Date().format('ddMMyyyyhhmmssS') )
            user.validate();
            if( ! hasError( user.errors , 'Erro na validação dos dados do usuário!') )
            {
                user.save(flush:true)
                // adicionar o perfil ao usuario webUserRole
                WebUsuarioRole userRole = WebUsuarioRole.create(user,role,true)
            }
            // enviar email para o usuario
            //emailService.sendConfirmRegistro(user.email, user.fullName, user.hashAtivacao)
            if( emailService.sendEmailAtivacaoConta(user) )
            {
                res.status=0
            }
            else
            {
                res.msg='Falha no envio do e-mail.'
                res.status=1
            }
            return res
        }
        catch( Exception e)
        {
                res.status=1
                log.error( e.getMessage() )
                res.errors.plus(0,e.getMessage() ) // adiciona a mensagem no iniico do array
                return res
                //e.printStackTrace()
        }*/
    }
}
