package br.gov.icmbio

import br.gov.icmbio.Util
import grails.converters.JSON
import grails.transaction.Transactional
import groovyx.net.http.*
import org.apache.http.conn.scheme.Scheme
import org.apache.http.conn.ssl.SSLSocketFactory
import org.codehaus.groovy.grails.web.json.JSONElement

// necessário para o trust manager - HTTPS

import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import java.security.cert.X509Certificate

@Transactional
class RequestService {

    def grailsApplication;

    /**
    * Método utilizado para fazer requisições POST
    *  @params mixQueryParams - recebe os parametros de pesquisa nos formatos  [p1:1,p2:3]  ou  p1=2&p2=3
    *
    **/
    public def doPost(String strUrl="", def mixQueryParams="", String strReturnContentType = "", Map mapCookies = [:] )
    {
        def sslContext    = null;
        def socketFactory = null;
        def httpsScheme   = null;
        ContentType contentType = null;

        if( strUrl.indexOf('?') > -1 )
        {
            Map getParams = urlParams2Map( strUrl);
            mixQueryParams = getParams;
            strUrl = strUrl.substring( 0, strUrl.indexOf('?' ) );
        }

        if( strUrl.isEmpty() )
        {
            return null;
        }

        //formato [p1:1,p2:3]
        if( mixQueryParams.getClass() == Map)
        {
                mixQueryParams = mixQueryParams.toString();
        }
        // formato p1=2&p2=3....
        if( mixQueryParams.getClass() == String )
        {
            mixQueryParams = mixQueryParams.split('&').inject([:] )
            { map, token ->
                try {

                    token.split('=').with {
                        if( map[ it[0] ]  != null )
                        {
                            map[ it[0] ].push(URLDecoder.decode(it[1],"UTF-8"))
                        }
                        else
                        {
                            map[ it[0] ] = [URLDecoder.decode(it[1],"UTF-8") ]
                        }
                    }
                } catch( Exception e) {}
                map
            }
        }
        if( strUrl.indexOf('https://') == 0 )
        {
            TrustManager[] trustAllCerts = [
              new X509TrustManager()
                {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null}
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                }
            ]
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            socketFactory = new SSLSocketFactory(sslContext, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            httpsScheme = new Scheme("https", socketFactory, 443);
        }

        try
        {
            HTTPBuilder http = new HTTPBuilder( strUrl );
            if( httpsScheme != null )
            {
                http.client.connectionManager.schemeRegistry.register( httpsScheme );
            }

            contentType = ( strReturnContentType == 'image/jpg' ? ContentType.BINARY: ContentType.TEXT);

            //http://javadox.com/org.codehaus.groovy.modules.http-builder/http-builder/0.6/groovyx/net/http/ContentType.html
            return http.request( strUrl, Method.POST, contentType ) {
                // uri.path = strPath
                requestContentType = ContentType.URLENC
                body = mixQueryParams
                headers.'User-Agent' = "Mozilla/5.0 Firefox/3.0.4"
                if( mapCookies )
                {
                    headers['Cookie'] = mapCookies.collect { k,v -> "$k=$v" }.join(';');
                    Util.d('Cookies: ' + headers['Cookie'] );
                }
                headers.Accept = '*/*,application/octet-stream,application/json,application/javascript,text/javascript,text/plain,application/x-www-form-urlencoded,application/xml,text/xml,application/xhtml+xml,application/atom+xml'
                contentType = contentType // */* application/octet-stream application/json application/javascript text/javascript text/plain application/x-www-form-urlencoded application/xml text/xml application/xhtml+xml application/atom+xml
                response.success = { resp, reader ->
                    if( contentType == ContentType.BINARY )
                    {
                        reader.bytes
                    }
                    else
                    {
                        reader.text;
                    }
                }
                response.'404' = {
                    Util.d('404 - DoPost() não encontrou a url: ' + strUrl );
                }
                response.'503' = {
                    Util.d('503 - DoPost() servico temporariamente indisponível na url: ' + strUrl );
                }
                response.'301' = {
                    Util.d('301 - DoPost() URL has been permanently redirected to another URL. (' + strUrl+')' );
                }
                response.failure = { resp, reader ->
                	String erro;
                    erro = resp.statusLine+'\n';
                    erro += reader.text
                    erro += "\n---------------------------------------\n";
                    erro += 'DoPost() falhou url: ' + strUrl+'\n';
                    erro += 'Parametros: ' + mixQueryParams+'\n';
                    erro += 'Cookies...: ' + mapCookies.toString()+'\n';
                    erro += response.toString() +'\n';
                    erro += '---------------------------------------';
                    d( erro );
                }
            }
        }
        catch( Exception e )
        {
            Util.d( 'ERROR - Método doPost() chamando a url: '+strUrl);
            Util.d( e.getMessage() );
        }
    }

    /**
     * Método utilizado para fazer requisições GET que retornam JSON
     *
     * @param url
     * @return
     */
    public JSONElement doGetJson(String url) {
        url = url.replaceAll(' ','+');
        def conn = new URL(url).openConnection()
        conn = this.setProxy( conn );
        try {
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);
            def json = conn.content.text;
            return JSON.parse(json);
        } catch (Exception e) {
            Util.d("doGetJson() não conseguiu buscar o JSON em (${url}). ${e.getClass()} ${e.getMessage()}, ${e}");
            return  e.getMessage()
        }
    }
    /**
     * Método utilizado para fazer requisições GET que retornam texto puro
     *
     * @param url
     * @return
     */
    public String doGet(String requestUrl='', Map requestParams=[:], String returnType='text') {
        String params = requestParams.collect { it }.join('&');
        requestUrl = requestUrl.replaceAll(' ','+');
        if( requestParams )
        {
            requestUrl = "${requestUrl}?${params}"
        }
        def conn = new URL(requestUrl).openConnection()
        conn = this.setProxy( conn );
        try {
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);
            String result = conn.content.text;
            if( returnType.toLowerCase() == 'json')
            {   if( result == 'null' )
                {
                    return ( [:] as JSON ).toString();
                }
            }
            return result;
        } catch (Exception e) {
            Util.d("doGet() não conseguiu buscar dados na url (${requestUrl}). ${e.getClass()} ${e.getMessage()}, ${e}");
            if( returnType.toLowerCase() == 'json')
            {
                Map result = [error:e.getMessage()];
                return (result as JSON).toString();
            }
            return e.getMessage()
        }
    }
    /**
     * [setProxy description]
     * @param  conn [description]
     * @return      [description]
     */
    private HttpURLConnection setProxy( HttpURLConnection conn )
    {
        if( System.getProperty("encoded") )
        {
            println 'Requisicao autentidada (' + System.getProperty("encoded") + ')';
            conn.setRequestProperty("Proxy-Authorization", "Basic " + System.getProperty("encoded") );
        }
        return conn;
    }
}
