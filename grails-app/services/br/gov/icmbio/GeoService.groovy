package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import groovy.sql.Sql

import grails.transaction.Transactional

@Transactional
class GeoService {

	static dataSource

    // executer comandos sqls diretamente
    static java.util.ArrayList execSql( String query, List parametros = null ) {
        query  = query.trim()
        Sql sql = new Sql( dataSource )
        def rows
        try
        {
            rows = sql.rows( query, parametros )
        }catch(Exception e ){
            println e.getMessage()
        }
        sql.close()
        return rows
    }

    static String getSrid(String esquemaName, String tableName, String columnName) {
        try {
            String query = "SELECT Find_SRID( ?, ?, ?) as srid"
            def rows = execSql( query,[esquemaName, tableName, columnName] )
            if( rows )
            {
                 return rows[0].srid ? rows[0].srid.toString() : '0'
            }
        } catch ( Exception e){ println e.getMessage()}
        return '4674'
    }

    /**
     * retornar o centroide da unidade de conservacao:Federal, Estadual ou Rppn e tambem
     * o Estado, Municipio e bioma do centroide
     * @param uc
     * @return
     */
     static Map getCentroide( Uc uc ) {
         String tableName
         String idColumnName
         String geoColumnName

         Map data   = [:]

        java.util.ArrayList rows
    	if( ! uc )
    	{
    		return data
    	}

         tableName = 'salve.unidade_conservacao'
         idColumnName = 'sq_pessoa'
         geoColumnName = 'ge_uc'

        if( tableName && uc.sqUc ) {
            String sql= """with CTE as (
                select ${idColumnName}, ST_centroid( ST_ExteriorRing( st_asEWKT( ( st_dump( ${geoColumnName} ) ).geom )::geometry) ) as centroide,
                st_asText( ST_PointOnSurface( ST_ExteriorRing( st_asEWKT( ( st_dump( the_geom ) ).geom )::geometry) ) ) as pontoSuperficie
                from ${tableName} where ${idColumnName} = ?
                )
                select st_asText( tmp.centroide ) as centroide, st_x( tmp.centroide ) as x, st_y( tmp.centroide) as y from (
                    select case when a.${idColumnName}::text is null then cte.pontoSuperficie::geometry else cte.centroide::geometry end as centroide
                    from CTE
                    left join ${tableName} a on st_intersects( cte.centroide, a.${geoColumnName}) and a.${idColumnName} = cte.${idColumnName}
                ) tmp
                """
            rows = execSql(sql, [uc.sqUc]);
            if (rows) {
                data = ['text'         : rows[0].centroide
                        , 'x'          : null
                        , 'y'          : null
                        , 'sqEstado'   : null
                        , 'noEstado'   : null
                        , 'sgEstado'   : null
                        , 'sqMunicipio': null
                        , 'noMunicipio': null
                        , 'sqBioma'    : null
                        , 'noBioma'    : null
                ]
                if ( data.text ) {
                    data.x = rows[0].x
                    data.y = rows[0].y
                    // localizar Estado
                    Estado uf = getUfByCoord(data.y.toBigDecimal(), data.x.toBigDecimal())

                    if (uf) {
                        data.sqEstado = uf.id
                        data.noEstado = uf.noEstado
                        data.sgEstado = uf.sgEstado
                        data.sqPais = uf.pais.id
                        data.noPais = uf.pais.noPais

                        // procurar o municipio
                        Municipio municipio = getMunicipioByCoord(data.y.toBigDecimal(), data.x.toBigDecimal(), uf)
                        if (municipio) {
                            data.sqMunicipio = municipio.id
                            data.noMunicipio = municipio.noMunicipio
                        }

                        // localizar bioma
                        Bioma bioma = getBiomaByCoord(data.y.toBigDecimal(), data.x.toBigDecimal())
                        if (bioma) {
                            data.sqBioma = bioma.id;
                            data.noBioma = bioma.noBioma;
                                                                                                                                                                                                                               }
                    }
                }
            }
        }
        return data
    }

    /**
     * localizar o centroide do estado pelas coordenadas ou id do Estado
     * @param latitude
     * @param longitude
     * @param sqEstado
     * @return
     */
    static Map getCentroideEstado( BigDecimal latitude = null, BigDecimal longitude = null, Long sqEstado = null ) {
        Map data   = [:]
        String sql
        java.util.ArrayList rows
        if( sqEstado ) {
            sql = 'select tmp.no_estado, st_asText( tmp.centroide ) as centroide, st_x( tmp.centroide) as x, st_y( tmp.centroide) as y from ( select no_estado, ST_Centroid( the_geom ) as centroide from salve.estado where sq_estado = ? ) tmp'
            rows = execSql( sql,[sqEstado])
        } else if( latitude && longitude )  {
            String ponto = 'SRID=4674;POINT('+longitude+' '+latitude+')'
            sql = 'select tmp.no_estado, st_asText( tmp.centroide ) as centroide, st_x( tmp.centroide) as x, st_y( tmp.centroide) as y from ( select no_estado, ST_Centroid( the_geom ) as centroide from salve.estado where ( the_geom is not null and st_intersects( ?, the_geom ) ) limit 1) tmp'
            rows = execSql( sql,[ponto] )
        }
        if( rows ) {
            data.text       = rows[0].centroide
            data.noEstado   = rows[0].no_estado
            data.x          = rows[0].x
            data.y          = rows[0].y
        }
        return data
    }

    /**
     * Encontrar o centoide do municipio pelas coordenadas ou id do municipio
     * @param latitude
     * @param longitude
     * @param sqMunicipio
     * @return
     */
    static Map getCentroideMunicipio( BigDecimal latitude = null, BigDecimal longitude = null, Long sqMunicipio = null) {
        Map data   = [:]
        String sql
        java.util.ArrayList rows
        if( sqMunicipio )
        {
            sql = 'select tmp.no_municipio,st_asText( tmp.centroide) as centroide,  st_x( tmp.centroide) as x, st_y( tmp.centroide) as y from ( select no_municipio, ST_Centroid( the_geom ) as centroide from salve.municipio where sq_municipio = ?) tmp'
            rows = execSql( sql,[sqMunicipio])
        }
        else if( latitude && longitude ) {
           String ponto = 'SRID=4674;POINT('+longitude+' '+latitude+')'
           sql = 'select tmp.no_municipio,st_asText( tmp.centroide ) as centroide, st_x( tmp.centroide) as x, st_y( tmp.centroide) as y from ( select no_municipio, ST_Centroid( the_geom ) as centroide from salve.municipio where ( the_geom is not null and st_intersects( ?, the_geom ) ) limit 1) tmp'
           sql = 'select tmp.no_municipio,st_asText( tmp.centroide ) as centroide, st_x( tmp.centroide) as x, st_y( tmp.centroide) as y from ( select no_municipio, ST_Centroid( ST_CollectionExtract(the_geom,3) ) as centroide from salve.municipio where ( the_geom is not null and st_intersects( ?, the_geom ) ) limit 1) tmp'
           //sql = 'select tmp.no_municipio,st_asText( tmp.centroide ) as centroide, st_x( tmp.centroide) as x, st_y( tmp.centroide) as y from ( select no_municipio, ST_PointOnSurface( the_geom ) as centroide from salve.municipio where ( the_geom is not null and st_intersects( ?, the_geom ) ) limit 1) tmp'
           rows = execSql( sql, [ponto] )
        }
        if( rows ) {
            data.text       = rows[0].centroide
            data.noMunicipio= rows[0].no_municipio
            data.x          = rows[0].x
            data.y          = rows[0].y
        }
        return data
    }

    /**
     * localizar a UC em um determinado ponto seguindo a ordem de prioridade: Federal, Estadual ou RPPN, a que for encontrada primeiro
     * @param latitude
     * @param longitude
     * @return
     */
    static Uc getUcByCoord( BigDecimal latitude, BigDecimal longitude ){
        Uc uc
        Map data   = [status:0]
        java.util.ArrayList rows
        String ponto = 'SRID=4674;POINT('+longitude+' '+latitude+')'

        // procurar nas ucs federais - considerar os PARNAs quando o ponto cair em uma APA e um PARNA
        //rows = execSql( 'select sq_pessoa as id, sg_instituicao as sgUnidade, instituicao.sg_instituicao as noUc from salve.unidade_conservacao, salve.instituicao where unidade_conservacao.sq_pessoa = instituicao.sq_pessoa and ( ge_uc is not null and st_intersects( ?, ST_CollectionExtract( ge_uc,3 ) ) ) order by instituicao.sg_instituicao desc limit 1',[ponto])
        rows = execSql("""select case when esfera.cd_sistema = 'FEDERAL' then 1 else  case when esfera.cd_sistema = 'ESTADUAL' THEN 2 ELSE 3 END END as de_ordem_esfera,
                                       case when tipo.cd_sistema = 'PARNA' then 1 else 2 END as de_ordem_tipo,
                                       unidade_conservacao.sq_pessoa as id, instituicao.sg_instituicao as sgUnidade, instituicao.sg_instituicao as noUc
                                from salve.unidade_conservacao, salve.instituicao, salve.dados_apoio as esfera,salve.dados_apoio as tipo
                               where unidade_conservacao.sq_pessoa = instituicao.sq_pessoa and esfera.sq_dados_apoio = unidade_conservacao.sq_esfera
                                 and tipo.sq_dados_apoio = unidade_conservacao.sq_tipo
                                 and ( ge_uc is not null and st_intersects( ?, ST_CollectionExtract( ge_uc,3 ) ) )
                                order by 1,2,instituicao.sg_instituicao limit 1
                            """,[ponto])

        if( rows ) {
            uc = Uc.get( rows[0].id )
        }
        return  uc
    }

    static Map getUcsByCoord( BigDecimal latitude, BigDecimal longitude ){
        Map resultado  = ['FEDERAIS':[],'ESTADUAIS':[],'RPPNS':[]]
        String ponto = 'SRID=4674;POINT('+longitude+' '+latitude+')'
        execSql("""select esfera.cd_sistema as cd_esfera,
                           unidade_conservacao.sq_pessoa as id
                               ,instituicao.sg_instituicao as sgUnidade
                               ,instituicao.sg_instituicao as noUc
                                from salve.unidade_conservacao, salve.instituicao, salve.dados_apoio as esfera
                               where unidade_conservacao.sq_pessoa = instituicao.sq_pessoa 
                                 and esfera.sq_dados_apoio = unidade_conservacao.sq_esfera
                                 and ( ge_uc is not null and st_intersects( ?, ST_CollectionExtract( ge_uc,3 ) ) )                                
                            """,[ponto]).each{ row ->

            Map map = ['id':row.id, 'sigla':row.sgUnidade, 'nome':row.noUc, 'esfera':'']
            if( row.cd_esfera == 'FEDERAL') {
                map.esfera ='F'
                resultado.FEDERAIS.push( map )
            } else if( row.cd_esfera == 'ESTADUAL') {
                map.esfera ='E'
                resultado.ESTADUAIS.push( map )
            } else if( row.cd_esfera == 'RPPN') {
                map.esfera ='R'
                resultado.RPPNS.push( map )
            }
        }


        /*
        // procurar nas ucs federais
        //execSql( 'select sq_pessoa as id, sg_unidade_org as sgUnidade, no_pessoa as noUc from salve.instituicao where ( the_geom is not null and st_intersects( ?, ST_CollectionExtract(the_geom,3) ) ) limit 2',[ponto]).each{ row->
        execSql( 'select sq_pessoa as id, sg_unidade_org as sgUnidade, no_pessoa as noUc from salve.instituicao where ( the_geom is not null and st_intersects( ?, ST_CollectionExtract( the_geom,3) ) ) limit 2',[ponto]).each{ row->
            Map map = ['id':row.id, 'sigla':row.sgUnidade, 'nome':row.noUc, 'esfera':'F']
            resultado.FEDERAIS.push( map )
        }

        // procurar ucs estaduais
        execSql( 'select gid as id, nome_uc1 as sgUnidade, nome_uc1 as noUc from geo.vw_uc_estadual_geo where ( the_geom is not null and st_intersects( ?, ST_CollectionExtract(the_geom,3) ) ) limit 2',[ponto] ).each{ row ->
            Map map = ['id':row.id, 'sigla':row.sgUnidade, 'nome':row.noUc, 'esfera':'E']
            resultado.ESTADUAIS.push( map )
        }
        // procurar ucs rppns
        execSql( 'select ogc_fid as id, nome as sgUnidade, nome as noUc from geo.vw_rppn_geo where ( the_geom is not null and st_intersects( ?, ST_CollectionExtract( the_geom,3) ) ) limit 2',[ponto]).each{ row ->
            Map map = ['id':row.id, 'sigla':row.sgUnidade, 'nome':row.noUc, 'esfera':'R']
            resultado.RPPNS.push( map )
        }*/
        return resultado
    }

    static UnidadeConservacao getRppnCorporativoByCoord(BigDecimal latitude, BigDecimal longitude ){
        String ponto = 'SRID=4674;POINT('+longitude+' '+latitude+')'
        UnidadeConservacao rppn
        // procurar na tabela corporativo.rppn primeiro
        ArrayList rows  = execSql( """select sq_pessoa from salve.unidade_conservacao, salve.dados_apoio as esfera
                                            where unidade_conservacao.sq_esfera = esfera.sq_dados_apoio
                                              and esfera.cd_sistema = 'RPPN'
                                              and st_intersects(?, ST_CollectionExtract(ge_uc,3)::geometry ) LIMIT 1""",[ponto])
        if( rows ){
            rppn = UnidadeConservacao.get( rows[0].sq_pessoa.toLong() )
        }
        return rppn
    }

    static Pais getPaisByCoord(BigDecimal latitude, BigDecimal longitude ){
        Pais pais
        String ponto = 'SRID=4674;POINT('+longitude+' '+latitude+')'
        ArrayList rows = execSql( 'select sq_pais from salve.pais where ( ge_pais is not null and st_intersects( ? , ST_CollectionExtract( ge_pais,3) ) ) limit 1',[ponto])
        if( rows )
        {
            pais = Pais.get( rows[0].sq_pais.toInteger() )
        }
        return pais
    }

    static Estado getUfByCoord(BigDecimal latitude, BigDecimal longitude ) {
        Estado uf;
        String ponto = 'SRID=4674;POINT('+longitude+' '+latitude+')'
        //ArrayList rows = execSql( 'select sq_estado from salve.estado where ( the_geom is not null and st_intersects( ? , st_asText( the_geom ) ) ) limit 1',[ponto])
        ArrayList rows = execSql( 'select sq_estado from salve.estado where ge_estado is not null and st_intersects( ? , ge_estado  ) limit 1',[ponto])
        if( rows )
        {
            uf = Estado.get( rows[0].sq_estado.toInteger() )
        }
        return uf;
    }

    static Municipio getMunicipioByCoord(BigDecimal latitude, BigDecimal longitude, Estado estado = null){
        Municipio municipio
        String ponto    = 'SRID=4674;POINT('+longitude+' '+latitude+')'
        String sql      = "select sq_municipio from salve.municipio where "+( estado ? ' sq_estado = ? and ':'')+" ge_municipio is not null and st_intersects(?, ge_municipio ) limit 1"
        List params     = []
        if( estado )
        {
            params.push(estado.id)
        }
        params.push( ponto )
        ArrayList rows = execSql( sql, params )
        if( rows )
        {
            municipio = Municipio.get( rows[0].sq_municipio.toInteger())
        }
        return municipio
    }

    /**
     * * retorna o bioma cadastrado na tabela coorporativa Bioma
     * @param latitude
     * @param longitude
     * @return
     */
    static Bioma getBiomaByCoord(BigDecimal latitude, BigDecimal longitude ){
        //String srid = getSrid( 'corporativo','bioma','the_geom')
        Bioma bioma
        String ponto    = 'SRID=4674;POINT('+longitude+' '+latitude+')'
        String sql      = "select sq_bioma from salve.bioma where ge_bioma is not null and st_intersects(?,ge_bioma ) limit 1"
        ArrayList rows = execSql( sql, [ponto] )
        if( rows )
        {
            bioma = Bioma.get( rows[0].sq_bioma.toInteger())
        }
        return bioma
    }

    /**
     * retorna o bioma cadastrado na tabela de apoio do salve
     * @param latitude
     * @param longitude
     * @return
     */
    static DadosApoio getBiomaSalveByCoord( BigDecimal latitude, BigDecimal longitude ) {
        Bioma bioma = getBiomaByCoord( latitude, longitude )
        if( bioma ) {
            DadosApoio tabelaBiomas = DadosApoio.findByCodigoSistema('TB_BIOMA')
            return DadosApoio.findByPaiAndDescricaoIlike(tabelaBiomas, '%'+bioma.noBioma+'%')
        }
        return null
    }

    // tabela dados_apoio
    static DadosApoio getBaciaByCoord( BigDecimal latitude, BigDecimal longitude ) {
        DadosApoio bacia
        String ponto = 'SRID=4674;POINT(' + longitude.toString() + ' ' + latitude.toString() + ')'
        ArrayList rows = execSql("select geo.sq_dados_apoio from salve.dados_apoio_geo geo where st_intersects( ?, geo.ge_geo ) limit 1",[ponto])
        if (rows) {
            bacia = DadosApoio.get( rows[0]?.sq_dados_apoio?.toLong() )
        }
        return bacia
    }
    // tabela dados_apoio_geo
    static DadosApoioGeo getBaciaGeoByCoord( BigDecimal latitude, BigDecimal longitude ) {
        DadosApoioGeo baciaGeo
        String ponto = 'SRID=4674;POINT(' + longitude.toString() + ' ' + latitude.toString() + ')'
        ArrayList rows = execSql("select geo.sq_dados_apoio_geo from salve.dados_apoio_geo geo where st_intersects( ?, geo.ge_geo ) limit 1",[ponto])
        if (rows) {
            baciaGeo = DadosApoioGeo.get( rows[0]?.sq_dados_apoio_geo?.toLong() )
        }
        return baciaGeo
    }

    /**
     * Verificar se a coordenada esta dentro da uf
     * @param uf
     * @param geometry
     * @return
     */
    static boolean chkUf(Estado uf, Geometry geometry) {
        java.util.ArrayList rows
        String ponto = 'SRID=4674;POINT('+geometry.x+' '+geometry.y+')'
        rows = execSql( 'select sq_estado from salve.estado where sq_estado = ? and ge_estado is not null and st_intersects( ?, ge_estado ) limit 1',[ uf.id, ponto ])
        return (rows && rows.size > 0)
    }

    /**
     * Verificar se a coordenada esta dentro do municipio
     * @param municipio
     * @param geometry
     * @return
     */
    static boolean chkMunicipio(Municipio municipio, Geometry geometry) {

        java.util.ArrayList rows
        String ponto = 'SRID=4674;POINT('+geometry.x+' '+geometry.y+')'
        rows = execSql( 'select sq_municipio from salve.municipio where sq_municipio = ? and ( ge_municipio is null or st_intersects( ?, ge_municipio ) ) limit 1',[ municipio.id, ponto ])
        return ( rows && rows.size>0)
    }

    /**
     * Verificar se a coordenada esta dentro do pais
     * @param Pais
     * @param geometry
     * @return
     */
    static boolean chkPais(Pais pais, Geometry geometry){

        java.util.ArrayList rows
        String ponto = 'SRID=4674;POINT('+geometry.x+' '+geometry.y+')'
        rows = execSql( 'select sq_pais from salve.pais where sq_pais = ? and ( ge_pais is null or st_intersects( ?, ge_pais ) ) limit 1',[ pais.id, ponto ])
        return  ( rows && rows?.size > 0 )
    }

    /**
     * Verificar se a coordenada esta dentro da Unidade de Conservação
     * @param Uc and Geometry
     * @param geometry
     * @return
     */
    static boolean chkUc(Uc uc, Geometry geometry){
        java.util.ArrayList rows
        String ponto = 'SRID=4674;POINT('+geometry.x+' '+geometry.y+')'
        //rows = execSql( 'select sq_pessoa from salve.instituicao where sq_pessoa = ? and ( the_geom is null or st_intersects( ?, ST_CollectionExtract(the_geom,3) ) ) limit 1',[ uc.sqUc, ponto])
        rows = execSql( 'select sq_pessoa from salve.unidade_conservacao where sq_pessoa = ? and ( ge_uc is null or st_intersects( ?, ST_CollectionExtract( ge_uc,3) ) ) limit 1',[ uc.sqUc, ponto])
        return ( rows && rows.size > 0 )
    }


    /**
     * Verificar se o ponto pertence ao Sistema Costeiro-Marinho e se a espécie pode estar neste bioma
     * @param sqFicha
     * @param latY
     * @param lonX
     * @return
     */
    static String validarBiomaGrupo( Long sqFicha = 0, BigDecimal latY = 0, BigDecimal lonX = 0 ) {
        String result = ''
        if( sqFicha ) {
            DadosApoio  biomaSalve = getBiomaSalveByCoord( latY, lonX )
            if ( biomaSalve && ( biomaSalve.codigoSistema == 'MARINHO_COSTEIRO' ) ) {
                VwFicha vwFicha = VwFicha.get( sqFicha )
                DadosApoio grupo = vwFicha.grupo
                if( grupo.codigoSistema =~ /^(COLEMBOLAS|ANFIBIOS|EFEMEROPTERAS|ODONATAS|ABELHAS|ARACNIDEOS|COLEMBOLAS|CEFALOPODAS|FORMIGAS|INVERTEBRADOS_TERRESTRES|BORBOLETAS|MARIPOSAS|MIRIAPODAS|OLIGOQUETAS|ONICOFORAS|MOLUSCOS_TERRESTRES|CARNIVOROS_TERRESTRES|CERVIDEOS|MARSUPIAIS|MORCEGOS|PERISSODACTILAS|PRIMATAS|ROEDORES|TAIASSUIDAES|XENARTRAS|CROCODILIANOS|LAGARTOS|ANFISBENAS|SERPENTES|LAGOMORFAS)$/){
                    result = 'O local do ponto pertence ao <b>Sistema Costeiro-Marinho</b> o qual não é compatível com espécies do grupo <b>'+grupo.descricao+'</b>.'
                }
            }
        }
        return result
    }
}
