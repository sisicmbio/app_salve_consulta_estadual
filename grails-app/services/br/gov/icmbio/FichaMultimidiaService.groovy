package br.gov.icmbio

import com.vividsolutions.jts.geom.Geometry
import grails.transaction.Transactional
import grails.util.Holders
import org.springframework.context.ApplicationContext
import org.springframework.web.multipart.commons.CommonsMultipartFile

//import org.hibernate.spatial.GeometryType

@Transactional
class FichaMultimidiaService {

    def dadosApoioService
    def imageService


    FichaMultimidia save(
               Long id              = null
             , Long sqFicha         = null
             , CommonsMultipartFile file = null
             , Long sqTipo          = null
             , Long sqSituacao      = null
             , String deLegenda     = null
             , String txMultimidia  = null
             , String noAutor       = null
             , String deEmailAutor  = null
             , Date dtElaboracao    = null
             , Boolean inPrincipal  = null
             , Long sqDatum         = null
             , Geometry geometry    = null
             ) {

        FichaMultimidia fm
        DadosApoio datum = null

        // ler contexto grailsApplication
        ApplicationContext ctx = Holders.grailsApplication.mainContext

        if ( sqDatum ) {
            datum = DadosApoio.get( sqDatum )
            if ( ! datum) {
                throw new Exception('Datum inválido!')
            }
        }

        if (id) {
            fm = FichaMultimidia.get(id)
        } else {
            if (!sqFicha) {
                throw new Exception('Fichn não informada!')
            }
            fm = new FichaMultimidia()
            fm.ficha = Ficha.get(sqFicha)
            if (!fm.ficha) {
                throw new Exception('Ficha inválida!')
            }
            if (!file || file.empty) {
                throw new Exception('Foto não informada!')
            }
        }

        if( !fm ) {
            throw new Exception('Id inválido!')
        }
        if (file && ! file.isEmpty()) {
            String mmDir = ctx.grailsApplication.config.multimidia.dir
            File savePath = new File(mmDir)
            if (!savePath.exists()) {
                if (!savePath.mkdirs()) {
                    throw new Exception('Não foi possivel criar o diretório ' + savePath.asString())
                }
            }
            // remover os acentos e caracteres improprios do nome da foto
            String fileName = Util.removeAccents( file.getOriginalFilename().toString() ).replaceAll(/(?i)[^a-z0-9\s\.]/,'')
            String fileExtension = Util.getFileExtension(fileName)
            String fileNameMd5 = file.inputStream.text.toString().encodeAsMD5() + '.' + fileExtension
            fileName = savePath.absolutePath + '/' + fileNameMd5
            // copiar o arquivo para o diretorio /anexo se o arquivo aind não existir
            if (!new File(fileName).exists()) {
                file.transferTo(new File(fileName))
            }
            imageService.createThumb(fileName)
            fileName = file.getOriginalFilename()
            FichaMultimidia fmExistente = FichaMultimidia.findByFichaAndNoArquivoDiscoAndIdNotEqual(fm.ficha, fileNameMd5, fm.id ?: 0)
            // não permitir gravar o mesmo arquivo na mesma ficha
            if (fmExistente) {                
                throw new Exception('Foto já cadastrada!')
            }
            DadosApoio situacao = dadosApoioService.getByCodigo('TB_SITUACAO_MULTIMIDIA', 'PENDENTE_APROVACAO')
            fm.noArquivoDisco = fileNameMd5
            fm.noArquivo = fileName
            fm.tipo = DadosApoio.get(sqTipo)
            fm.situacao = situacao
        }

        fm.dtElaboracao = dtElaboracao
        fm.inPrincipal = inPrincipal
        fm.noAutor = noAutor
        fm.deEmailAutor = deEmailAutor
        fm.deLegenda = deLegenda
        fm.txMultimidia = txMultimidia
        fm.datum = datum
        fm.geometry = geometry
        fm.validate()
        if (fm.hasErrors()) {
            println fm.getErrors()
            throw new Exception('Inconsistência na gravação do arquivo.')
        }
        // só pode haver uma principal por ficha
        if (fm.inPrincipal) {
            FichaMultimidia.findAllByFicha(fm.ficha).each {
                if (fm.id && it.id != fm.id) {
                    it.inPrincipal = false
                    it.save(flush: true)
                }
            }
        }
        fm.save(flush: true)
        return fm

    } // fim

}
