package br.gov.icmbio

import grails.transaction.Transactional
import org.apache.commons.mail.HtmlEmail
import org.apache.commons.mail.SimpleEmail

// import org.springframework.web.util.WebUtils

@Transactional
class EmailService {

    def grailsApplication

    private List caracteresEspeciais = ["Ç|&Ccedil;", "ç|&ccedil;", "Á|&Aacute;", "Â|&Acirc;", "Ã|&Atilde;", "É|&Eacute;", "Ê|&Ecirc;", "Í|&Iacute;", "Ô|&Ocirc;", "Õ|&Otilde;", "Ó|&Oacute;", "Ú|&Uacute;", "á|&aacute;", "â|&acirc;", "ã|&atilde;", "é|&eacute;", "ê|&ecirc;", "í|&iacute;", "ô|&ocirc;", "õ|&otilde;", "ó|&oacute;", "ú|&uacute;"]

    private String getBaseUrl() {

        String url = grailsApplication.config.url.salve.consulta + '/'
        url = url.replaceAll(/\/\/$/, '/')
        return url
        /*
        def request = RequestContextHolder.currentRequestAttributes().request
        //def request = WebUtils.retrieveGrailsWebRequest().getCurrentRequest()
        String sistema = grails.util.Metadata.current.getApplicationName()
        String requestUrl = request.getRequestURL()
        String protocolo =  ( request.isSecure() ? 'https://' : 'http://' )

        if( requestUrl.indexOf('.icmbio.gov.br') > 0 )
        {
            requestUrl = requestUrl.replaceAll(':8080','')
        }

        List partes = requestUrl.split( '/'+sistema )
        println 'getBaseUrl()'
        println 'Sistema: ' + sistema
        println 'RequestUrl: '+ requestUrl
        println 'Partes: ' + partes
        println 'baseUrl = ' + partes[0]+'/'+sistema+'/'
        println 'protocolo = ' + protocolo
        return partes[0]+'/'+sistema+'/'
        */
    }

    // Alternative ports: 8025, 587, 80 or 25. TLS is available on the same ports. SSL is available on ports 465, 8465 and 443.

    boolean sendTo(String strTo, String strSubject, String strMsg, String emailFrom = '') {
        boolean transactional = false
        String host = grailsApplication.config.email.host
        String username = grailsApplication.config.email.username
        String password = grailsApplication.config.email.password
        String from = emailFrom ?: grailsApplication.config.email.from
        String port = grailsApplication.config.email.port

        if (!host || !strTo || !strMsg || !strSubject) {
            log.info('Email não enviado, parâmetros incompletos!')
            return false;
        }
        try {

            println '--------------------------------------------------------------------------------------'
            println 'Email formato TEXT selecionado'
            println 'host:' + host
            println 'username:' + username
            println 'password:' + password
            println 'from:' + from
            println 'to:' + strTo
            println 'port:' + port
            println 'Subject:' + strSubject
            println '--------------------------------------------------------------------------------------'
            println ' '
            // enviar no formato texto simples
            if (strMsg.indexOf('</') == -1) {
                SimpleEmail email = new SimpleEmail()
                email.setHostName(host)
                email.addTo(strTo)
                email.setFrom(from)
                email.setSubject(strSubject)
                email.setMsg(strMsg)
                if (username && password) {
                    email.setAuthentication(username, password)
                }
                //email.setSSLOnConnect(true)
                ///email.setSslSmtpPort( "465" )
                email.setSmtpPort(port.toInteger())
                email.send()
            } else {
                println 'Email formato HTML selecionado'

                // converter os acentos para formato html
                strMsg = str2html(strMsg)

                // enviar no formato HTML
                HtmlEmail htmlEmail = new HtmlEmail()
                //htmlEmail.setSSLOnConnect(true)
                //htmlEmail.setSslSmtpPort( "465" )
                htmlEmail.setHostName(host)
                //htmlEmail.setAuthenticator( new DefaultAuthenticator( username , password ) );
                htmlEmail.setAuthentication(username, password)
                htmlEmail.setSmtpPort(port.toInteger())
                htmlEmail.setFrom(from, "Sistema Salve")
                htmlEmail.addTo(strTo)
                htmlEmail.setDebug(false)
                htmlEmail.setSubject(strSubject)
                /*StringBuilder builder = new StringBuilder();
                builder.append("<h1>Um titulo</h1>");
                builder.append("<p>Lorem ipsum dolor sit amet, <b>consectetur adipiscing elit</b>. Duis nec aliquam tortor. Sed dignissim dolor ac est consequat egestas. Praesent adipiscing dolor in consectetur fringilla.</p>");
                builder.append("<a href=\"http://wwww.botecodigital.info\">Boteco Digital</a> <br> ");
                builder.append("<img src=\"http://www.botecodigital.info/wp-content/themes/boteco/img/logo.png\">");
                */

                htmlEmail.setHtmlMsg(strMsg.toString())
                htmlEmail.send()
            }
            return true
        }
        catch (Exception e) {
            //e.printStackTrace()
            println 'Erro sendTo()'
            log.error(e.getMessage())
        }
        return false
    }

    boolean sendEmailAtivacaoConta(Usuario user) {
        //user.refreshHash()
        String baseUrl = getBaseUrl()
        String msg = """<!DOCTYPE html>
        <html lang=\"pt-BR\" dir=\"ltr\">
        <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
        </head>
        <body>
        <h2>Sistema de Avaliação de Espécies - SALVE</h2>
        <p>Parabéns, seu registro foi realizado com SUCESSO!</p>
        <p>Ainda é necessário liberação do acesso. Para isso, clique no link abaixo.</p>
        <p><a title=\"Sistema Salve Estadual Consulta - link para confirmação do cadastro.\"target=\"_blank\" href=\"${baseUrl}app/ativar/${user.deHash}\">Ativar Registro</a></p>
        </body>
        </html>
        """
        try {

            if (this.sendTo(user.email, 'Ativação do Registro no Sistema SALVE', msg)) {
                println 'Email envido com sucesso para ' + user.email
                return true
            }
        }
        catch (Exception e) {
            println 'Erro sendEmailAtivacaoConta()'
        }
        return false;
    }

    boolean sendEmailForgotPassword(Usuario user) {
        user.refreshHash()
        String baseUrl = getBaseUrl()
        String msg = """<!DOCTYPE html>
        <html lang=\"pt-BR\" dir=\"ltr\">
        <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
        </head>
        <body>
        <h2>Sistema de Avaliação do Risco de Extinção da Biodiversidade - SALVE - Estadual</h2>

        <p>Clique no link abaixo para alterar sua senha.</p>
        <p><a target=\"_blank\" href=\"${baseUrl}app/resetPassword/${user.deHash}\">Alterar Senha</a></p>
        </body>
        </html>
        """
        try {
            if (this.sendTo(user.email, 'Recuperação de Senha', msg)) {
                println 'Email envido com sucesso para ' + user.email
                return true
            }
        }
        catch (Exception e) {
            println 'Erro sendEmailForgotPassword()'
        }
        return false
    }

    boolean sendFaleConosco(String msg, String emailFrom) {
        emailFrom = emailFrom ?: 'salve-estadual.faleconosco@icmbio.gov.br'
        return this.sendTo('salve-estadual@icmbio.gov.br', 'Fale Conosco', msg, emailFrom)
    }

    /**
     * método para converter acentos no formato html ex: ç = &ccedil
     * @param texto [description]
     * @return [description]
     */
    String str2html(String texto = "") {
        this.caracteresEspeciais.each {
            List a = it.split(/\|/)
            texto = texto.replace(a[0], a[1])
        }
        return texto
    }

    /**
     * método para converter carcteres especiais html para acentos normais ex: &ccedil = ç
     * @param texto [description]
     * @return [description]
     */
    String html2str(String texto = "") {
        this.caracteresEspeciais.each {
            List a = it.split(/\|/)
            texto = texto.replace(a[1], a[0])
        }
        return texto
    }

}
