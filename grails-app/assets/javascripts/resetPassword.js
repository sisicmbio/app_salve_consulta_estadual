Vue.http.options.emulateJSON = true;
var page = new Vue({
    el: '#div-page-reset-password',
    data: {
        resultadoAjax:-1,
        view:'',
        errorMsg: '',
        form: null,
        fields: {
            senha  : '',
            senha2 : '',
            hash   :''
        }
    },
    created: function() {},
    mounted: function() {
        this.form = $("#frmResetPassword");
        this.form.validate();
        $("#div-page-reset-password").show(function(){
            page.showView('formulario');
            $("#fldSenha").focus();
        });
    },
    methods: {
        showView:function(view)
        {
            this.view = view;
        },
        save: function(e) {
            this.errorMsg = "";
            e.preventDefault();
            if (!this.form.valid()) {
                return;
            }
            if ( ! this.fields.senha )
            {
                this.errorMsg = "Nova senha não informada!";
                return;
            }
            if ( this.fields.senha != this.fields.senha2 ) {
                this.errorMsg = "Senhas informadas devem ser iguais!";
                setFocus('fldSenha2');
                return;
            }

            blockUi('Gravando a nova senha...');
            var data = $(this.form).serializeArray();
            //let data = JSON.stringify(this.fields);
            this.$http.post(baseUrl +'resetPassword', data ).then( function(response) {
                unblockUi();

                var res = response.body;
                page.resultadoAjax = res.status;

                if( res.msg )
                {
                    page.errorMsg = res.msg;
                }
                if( res.errors.length > 0 )
                {
                    page.errorMsg = res.errors.join('<br/>');
                }
                else if( res.status == 0)
                {
                    $("#msg").addClass('success');
                    this.showView('resultado');
                }
        }, function(response) {
            unblockUi();
            if( ! response.ok )
            {
                this.errorMsg = 'Servidor está fora do ar. Tente novamente mais tarde!';
            }
            else if( response.status == 401)
            {
                this.errorMsg = 'Página não autorizada!'
            }
            else if( response.status == 404)
            {
                this.errorMsg = 'Página não encontrada!'
            }
            else
            {
                this.errorMsg = 'Erro nº ' + response.status + ' - '+response.statusText
            }
          });

        },
        login:function(e)
        {
            e.preventDefault();
            window.location.replace(baseUrl+ 'login');
        },
        sair:function(e)
        {
            e.preventDefault();
            window.location.replace(baseUrl);
        }
    }
});
