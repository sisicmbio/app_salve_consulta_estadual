//# sourceURL=colaborarFicha.js

// variaveis globais
var ultimaJustificativa="" ; // referenciada em ol3map.js
var hex2rgba = function(x, a) {
    a = (a == undefined) ? 1 : a;
    var r = x.replace('#', '').match(/../g),
        g = [],
        i;
    for (i in r) {
        g.push(parseInt(r[i], 16));
    }
    g.push( a );
    return 'rgba(' + g.join() + ')';
}

// adicionar a div do popup no mapa
/*var popup1 = $('#pointPopup1');
var popup2 = $('#pointPopup2');
var ovlPopup1 = new ol.Overlay({
    element: popup1[0],
    stopEvent: false,
    offset: [10, 0],
    positioning: 'top-center',
});
var ovlPopup2 = new ol.Overlay({
    element: popup2[0],
    stopEvent: false,
    offset: [10, 0],
    positioning: 'top-center',
});
*/
var newPointColor = '';
/*
var osmSource = new ol.source.OSM();
osmSource.firstLoad = true; // não exibir mensagem de atualização sempre
var totalLendo = 0;
var totalLido = 0;

osmSource.on('tileloadstart', function() {

    if (totalLendo == 0 && osmSource.firstLoad) {
        var divMap = 'map-ocorrencias';
        // detectar qual mapa está visivel
        if ($("#divFrmOcorrencia").is(':visible')) {
            divMap = 'map-form-ocorrencias';
        }
        $('#' + divMap).block({
            message: '<h5>Atualizando o mapa...</h5>',
            showOverlay: false,
            css: {
                borderRadius: '10px',
                paddingTop: '10px',
                width: '80%',
                border: '1px solid #000'
            }
        });
    }
    totalLendo++
});

osmSource.on('tileloadend', function() {
    totalLido++
    if (totalLendo == totalLido) {
        totalLendo = 0;
        totalLido = 0;
        $('#map-ocorrencias').unblock();
        $('#map-form-ocorrencias').unblock();
        if (osmSource.firstLoad) {
            //var extent = pontosLayer.getSource().getExtent();
            //page.map.getView().fit(extent, page.map.getSize());
        }
        osmSource.firstLoad = false;

    }
});


var osmLayer = new ol.layer.Tile({
    source: osmSource,
    visible: true,
    zIndex: 1,
    name: 'osm',
    tipo: 'baseLayer',
});
*/

/*
// adicionar layers ao mapa
var bingLayer = new ol.layer.Tile({
    source: new ol.source.BingMaps({
        key: 'AnLGl1Vfs_oXbalB22KQnOifIy3w8KLQfRGMAkZ4LkxJmjeS_1VFhlxyCSiOOdVx',
        imagerySet: 'AerialWithLabels',
        //imagerySet: bingLayers[i],
        maxZoom: 19
    }),
    visible: false,
    tipo: 'baseLayer',
    name: 'bingmaps'
});
*/

/*
// layer dos pontos salvos: Portalbio, salve e salve consulta
var pontosLayer = new ol.layer.Vector({
    //source: new ol.source.Vector(),
    source: new ol.source.Vector({
        //url: 'https://openlayers.org/en/v4.0.0/examples/data/geojson/countries.geojson', // exemplo de paises
        url: baseUrl + 'ficha/getPontos?sqConsultaFicha=' + $("#sqConsultaFicha").val(),
        format: new ol.format.GeoJSON()
    }),
    zIndex: 2,
    visible: true,
    name: 'pontos',
    tipo: 'overlay',
    renderBuffer: 200,
    attributions: [
        new ol.Attribution({
            html: 'ICMBio - Sistema SALVE'
        })
    ],
    style: function(feature, resolution) {
        if (feature.get('color')) {
            color = feature.get('color');
            var legend = feature.get('legend');
            if (!color) {
                color = '#FFB90C';
            }
            if (legend && color) {
                // if (!oLegend[legend]) {
                //     oLegend[legend] = color;
                // }
                //
                oLegend[legend] = color;
                updateLegend();
            }
            // if (feature.get('bd') == 'salve-consulta' && newPointColor == '') {
            //     newPointColor = color;
            // }
            //
            //console.log( feature.get('bd')+':' + color )
            return new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 4.5,
                    stroke: new ol.style.Stroke({
                        color: '#000000',
                        width: 0.5,
                    }),
                    fill: new ol.style.Fill({
                        color: hex2rgba(color, "0.85"),
                    })
                })
            })
        }
    }
});
*/

/*var updateLegend = function() {
    var $div = $("#map-ocorrencias-legend #conteudo")
    var itens = []
    for (key in oLegend) {
        var cor = oLegend[key];
        var legenda = key;
        //itens.push('<i class="glyphicon glyphicon-minus-sign" style="color:' + cor + ';background-color:' + cor + ';"></i><span>&nbsp;' + legenda + '</span>');
        itens.push('<i class="fa fa-circle" aria-hidden="true" style="color:' + cor + ';"></i><span>&nbsp;' + legenda + '</span>');
    }
    $div.html(itens.join('&nbsp;&nbsp;&nbsp;&nbsp;'))
}
*/
/*
// layer genérico para desenhos e pontos novos
var drawLayer = new ol.layer.Vector({
    source: new ol.source.Vector({
        features: []
    }),
    name: 'drawLayer',
    visible: true,
    zIndex: 3,
    attributions: [
        new ol.Attribution({
            html: 'Sistema de Avaliação - Salve - ICMBio'
        })
    ],
});
*/

// capturuar click no mapa
var mapSingleClick = function(evt) {
    //evt.pixel[0]=parseInt( evt.pixel[0])
    //evt.pixel[1]=parseInt( evt.pixel[1])
    // log( evt.coordinate);
    // log( ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326'));
    var mapName = evt.target.getProperties().target; // evt.target.get('target')
    if (evt && evt.coordinate && evt.originalEvent.ctrlKey && mapName == "map-form-ocorrencias") {
        var coords = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
        var datumSirgas = $("#sqDatum option[data-codigo=SIRGAS_2000]").val();
        modalOcorrencia.vlLon = coords[0];
        modalOcorrencia.vlLat = coords[1];
        modalOcorrencia.sqDatum = datumSirgas;
        modalOcorrencia.setMarkerPosition(false, true); // não centralizar e atualizar info localidade
        return;
    }
    return true;
};

/*
var refreshMap = function(map) {
    window.setTimeout(function() {
        var target = map.getTarget()
        map.setTarget(null);
        map.setTarget(target);
    }, 100);
}
*/

// configuração padrão do editor tinymce
//var default_editor_style =  '.mce-content-body { text-align:justify;margin:0px 10px;font-size:14pt;font-family:"Times New Roman", Times, serif;}'
var default_editor_style =  '.mce-content-body {margin:0px 10px;font-size:14pt;font-family:open_sansregular,"Open Sans","Times New Roman", Times, serif;}'

// configurações padrão do editor de texto rico wysiwyg
var default_editor_options = {
    mode: 'specific_textareas',
    content_style: default_editor_style,
    elementpath: false,
    menubar: false,
    statusbar: false,
    skin: "icmbio", //http://skin.tinyMCE.com/
    language: 'pt_BR',
    height: 200,
    min_height: 200,
    autoresize_min_height: 200,
    autoresize_max_height: 400,
    browser_spellcheck: true,
    plugins: "textcolor,link,lists,autoresize,fullscreen,paste",
    toolbar: 'undo redo | bold italic underline subscript superscript | alignleft aligncenter alignright alignjustify | forecolor backcolor | bullist numlist | link unlink openlink | outdent indent | removeformat | fullscreen | selectall',
    inline: false,
    autoresize_bottom_margin: 0,
    paste_retain_style_properties: "color background i b u",
    paste_merge_formats: true,
    paste_webkit_styles: "all",
    paste_as_text: true,
    /*paste_preprocess: function(plugin, args) {
        //console.log('antes', args.content);
        // https://github.com/cure53/DOMPurify
        // var clean = DOMPurify.sanitize(args.content,{ALLOWED_TAGS: ['a','b', 'i', 'p','em'], ALLOWED_ATTR: ['style']});
        // limpar font
        args.content = args.content.replace(/\n/g,' ');
        args.content = args.content.replace(/ ?-webkit-[a-z0-9-:'"!% ,]+(;|")/ig,'');
        args.content = args.content.replace(/(<meta[^>]*>)/ig, '');
        // tag font
        args.content = args.content.replace(/font-color:/ig,'xont-color:');
        args.content = args.content.replace(/<font color="/ig,'<xont color="');
        args.content = args.content.replace(/ ?font-[a-z0-9-:'"!% ,]+(;|")/ig,'').replace(/(<font[^>]*>)|(<\/font>)/ig, '');
        args.content = args.content.replace(/xont-color:/,'font-color:');
        args.content = args.content.replace(/<xont color="/ig,'<font color="');

        // tags de form
        args.content = args.content.replace(/(<form[^>]*>)|(<\/form>)/ig, '');
        args.content = args.content.replace(/(<input[^>]*>)|(<\/input>)/ig, '');
        args.content = args.content.replace(/(<button[^>]*>)|(<\/button>)/ig, '');
        args.content = args.content.replace(/(<select[^>]*>)|(<\/select>)/ig, '');
        args.content = args.content.replace(/(<textarea[^>]*>)|(<\/textarea>)/ig, '');
        args.content = args.content.replace(/(<iframe[^>]*>)|(<\/iframe>)/ig, '');

        // remover <h1><h2>...<hn>
        args.content = args.content.replace(/ ?<\/?h[1-7]>/g,'');
        args.content = args.content.replace(/ ?text-[a-z0-9-:'"!% ,]+(;|")/ig,'');
        args.content = args.content.replace(/ ?letter-[a-z0-9-:'"!% ,]+(;|")/ig,'');
        args.content = args.content.replace(/ ?white-[a-z0-9-:'"!% ,]+(;|")/ig,'');
        args.content = args.content.replace(/ ?(line|word|margin)-[a-z0-9-:'"!% ,]+(;|")/ig,'');
        args.content = args.content.replace(/ ?(float|widows|orphans|display):[a-z0-9-'"!% ,]+(;|")/ig,'');
        //args.content = args.content.replace(/ ?style=">/ig,'>');
        args.content = args.content.replace(/ ?style=""?>/ig,'>');
        //console.log(' ')
        //console.log('depois', args.content);


        //log( args.content);
        //args.content = args.content.replace(/font-[a-zA-Z-]+:.+(;|"|')/ig,'').replace(/(<font[^>]*>)|(<\/font>)/ig, '');
        //args.content = args.content.replace(/line-height\:[^;]+;?|text-align\:[^;]+;?|white-space\:[^;]+;?|margin\:[^;]+;?|box-sizing\:[^;]+;?|border\:[^;]+;?|padding\:[^;]+;?|text-indent\:[^;]+;?|windows\:[^;]+;?|outline\:[^;]+;?|position\:[^;]+;?|z-índex\:[^;]+;?|<\/?h[1-7]>|<\/?strong>/g, '');
        //args.content = args.content.replace(/&nbsp;/g, ' ').replace(/> +/g,'>').trim();
        //log( args.content)
    }*/

};

/*
 Metodo para ativar o editor de texto nos campos textareas com a classe wysiwyg
 */
var initEditor = function() {
    var selector = '.wysiwyg';
    // limpar array de editores
    tinyMCE.editors = [];
    tinyMCE.remove(selector);
    tinyMCE.remove(selector + 'RO');

    // configurações do editor
    var editorOptions = $.extend({},default_editor_options,{
        selector: selector,
        plugins: default_editor_options.plugins+',save',
        toolbar: 'save | '+default_editor_options.toolbar,
        setup: function( editor ) {

            //$(tinymce.activeEditor.theme.panel.find('toolbar *')[1].getEl()).css('background-color','red')
            editor.on("init", function() {
                var id = editor.targetElm.id;
                var conteudo = trim(editor.getContent());
                var original = trim($('#' + id + 'OriginalValue').html());
                if ($(editor.targetElm).prop("readonly") === true) {
                    $(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").hide();
                    editor.readonly = true;
                    this.readonly = true;
                } else if ($(editor.targetElm).hasClass('no-bar')) {
                    //$(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").hide();
                    editor.toolbar = 'save'
                }

                if (typeof page.fields[id] != 'undefined') {
                    page.fields[id].edited = (conteudo != original && conteudo != '')
                }

                // adicionar a fonte padrão Open Sans
                var text = editor.getContent()
                if( /Times New Roman/.test( text ) ) {
                    text = text.replace(/Times New Roman/g,'open_sansregular');
                    $(editor.targetElm).html(text);
                    editor.setContent(text, {format : 'html'});
                }

            });
            editor.on('Dirty', function(e) {
                e.target.theme.panel.find('toolbar *')[1].getEl().style.backgroundColor = '#EFB8B8'
                //$(tinymce.activeEditor.theme.panel.find('toolbar *')[1].getEl()).css('background-color','red')
                //$(e.theme.panel.find('toolbar *')[1].getEl()).css('background-color','red')

            });
        },
        save_onsavecallback: function(e) {

            var id = e.id
            if (!page.fields[id]) {
                return;
            }
            var editorRefBib = tinymce.get(id+'_ref');
            if( editorRefBib.isDirty() )
            {
                editorRefBib.save();
                //console.log( 'salvar a ref. bib');
            }
            var valor = tinyMCE.trim(tinyMCE.activeEditor.getContent({
                format: 'html'
            }).replace(/<p>&nbsp;<\/p>/g, ''));
            if ($("#" + id + '_ref').size() == 1 && $("#" + id + '_ref').val().trim() == '' && valor) {
                e.setDirty(true);
                alert('Para salvar é necessário informar uma referência bibliográfica!', '', 'error')
                return;
            }
            tinyMCE.activeEditor.setContent(valor);
            var refBibValue = '';

            // gravar tambem as referências
            var editorRefBib = tinyMCE.get(id + '_ref');
            if (editorRefBib) {
                refBibValue = tinyMCE.trim(editorRefBib.getContent({
                    format: 'html'
                }).replace(/<p>&nbsp;<\/p>/g, ''));
                editorRefBib.setContent(refBibValue);
            }
            if (page.fields[id].value != valor || (editorRefBib && editorRefBib.isDirty())) {
                var data = {
                    sqConsultaFicha: $("#sqConsultaFicha").val(),
                    sqTipoConsulta: $("#sqTipoConsulta").val(),
                    fieldName: id,
                    fieldValue: valor,
                    refBibValue: refBibValue,
                    fieldLabel: $("#legend_" + id).data('label').trim()
                }
                if( ! data.fieldValue )
                {
                    data.refBibValue = '';
                    if( editorRefBib)
                    {
                        editorRefBib.setContent('');
                    }
                }
                doPost(baseUrl + 'ficha/salvarColaboracao', data, function(res) {
                    $(tinymce.activeEditor.theme.panel.find('toolbar *')[1].getEl()).css('background-color', 'transparent');
                    page.fields[id].value = valor; // evitar salvamento seguidos sem alteração
                    if (editorRefBib) {
                        editorRefBib.save();
                        editorRefBib.nodeChanged();
                    }
                });
            }
            //tinyMCE.activeEditor.focus(); // esta posicionando o cursor no inicio do texto sempre
        }
    });

    // criar o editor rico
    tinyMCE.init(editorOptions);

    editorOptions = $.extend({},default_editor_options, {
        selector: '.wysiwyg-ref-bib',
        toolbar: 'mybuttonSave,mybuttonSelect,mybuttonComPess',
        setup: function (editor) {
            // console.log( tinyMCE.trim(editor.getContent({ format: 'raw' })) ) ;
            editor.on('change', function (ed) {
                if (editor.saveButton) {
                    editor.saveButton.disabled(false);
                }
            });

            // http://www.logistikcluster-regionbasel.ch/public/js/tinyMCE-4.0.12/skins/lightgray/Icons.Ie7.less
            editor.addButton('mybuttonSave', {
                text: 'Salvar',
                icon: 'mce-ico mce-i-save',
                class: 'red',
                tooltip: 'Salvar alterações',
                disabled: true,
                context: 'file',
                onPostRender: function () {
                    var self = this;
                    editor.on('nodeChange', function () {
                        self.disabled(!editor.isDirty());
                        if (editor.isDirty()) {
                            $(self.getEl()).css({
                                'background-color': '#EFB8B8'
                            })
                        } else {
                            $(self.getEl()).css({
                                'background-color': 'transparent'
                            })
                        }
                    });
                },

                //cmd     : 'mceSave'
                onclick: function (e) {
                    if (editor.isDirty()) {
                        //editor.saveButton = this;
                        //this.disabled(true);
                        var id = editor.targetElm.id.replace(/_ref$/, '');
                        var fieldName = id;
                        var fieldLabel = $("#legend_" + fieldName).data('label').trim();
                        var refBibValue = tinyMCE.trim(editor.getContent({
                            format: 'html'
                        }).replace(/<p>&nbsp;<\/p>/, '').replace(/<hr \/>$/,''));
                        editor.setContent(refBibValue);
                        //editor.focus();
                        this.disabled(true);
                        // tinyMCE.activeEditor.theme.panel.find('toolbar *')[1].disabled(false)

                        // salvar tambem o texto da colaboração
                        var editorColaboracao = tinyMCE.get(id);
                        if (editorColaboracao.isDirty()) {
                            // salvar os dois texto
                            editorColaboracao.execCommand('mceSave', true);
                        }
                        else
                        {
                            // salvar somente a ref bib
                            modalRefBib.saveRefBib(fieldName, fieldLabel, refBibValue);
                            editor.save();
                        }
                    }
                    $(tinymce.activeEditor.theme.panel.find('toolbar *')[1].getEl()).css('background-color', 'transparent')
                }
            });

            editor.addButton('mybuttonSelect', {
                text: 'Pesquisar',
                tooltip: 'Pesquisar as referências cadastradas.',
                icon: 'mce-ico mce-i-searchreplace',
                // image: '/salve-consulta/assets/save.png',
                onclick: function (e) {
                    var fieldName = editor.targetElm.id.replace(/_ref$/, '');
                    modalRefBib.show(fieldName);
                }
            });


            editor.addButton('mybuttonComPess', {
                text: 'Comunicação Pessoal',
                tooltip: 'Inserir comunicação pessoal.',
                icon: 'mce-ico mce-i-user',
                // image: '/salve-consulta/assets/save.png',
                onclick: function (e) {
                    var fieldName = editor.targetElm.id;
                    var nomeUsuario = capitalize($("#li-full-name").text().trim());
                    var comPess = nomeUsuario.split(' ');
                    if (comPess.legend_ < 2) {
                        alert('Nome ' + nomeUsuario + ' inválido para gerar com. Pess');
                        return;
                    }
                    comPess = page.generateComPessoal();
                    if (!comPess) {
                        return;
                    }
                    var curEditor = tinyMCE.get(fieldName);
                    if (curEditor) {
                        var currentHtml = tinyMCE.trim(curEditor.getContent({
                            format: 'html'
                        }).replace(/<p>&nbsp;<\/p>/g, '').replace(/<hr \/>$/,''));
                        var currentText = tinyMCE.trim(curEditor.getContent({
                            format: 'text'
                        }).replace(/<p>&nbsp;<\/p>/g, '').replace(/<hr \/>$/,''));
                        curEditor.setContent(comPess);
                        comPess = tinyMCE.trim(comPess.replace(/<p>&nbsp;<\/p>/g, ''))
                        var currentValue = curEditor.getContent({
                            format: 'text'
                        });
                        if (currentText.indexOf(currentValue) > -1) {
                            curEditor.setContent(currentHtml);
                        } else {
                            curEditor.setContent(currentHtml + comPess + '<hr/>');
                            curEditor.focus()
                        }
                        curEditor.setDirty(true);
                        // encontrar o botão salvar ( primeiro da toolbar )
                        $(curEditor.theme.panel.find('toolbar *')[1].getEl()).click();

                        // salvar
                        /*fieldName = fieldName.replace(/_ref$/, '');
                        curEditor = tinymce.get( fieldName);
                        if( curEditor )
                        {
                            curEditor.setDirty(true);
                            $(curEditor.theme.panel.find('toolbar *')[1].getEl()).click();
                        }
                        */
                    }
                }
            });

        }, // fim setup
    } );

    // limpar array de editores
    tinyMCE.remove(selector);
    tinyMCE.init( editorOptions );
};

Vue.http.options.emulateJSON = true;
Vue.component('v-select', VueSelect.VueSelect);
Vue.filter('trim', function(value) {
    return trim(value.replace(/<p>&nbsp;<\/p>/g, ''));
});

Vue.component('ficha-colaboracao', {
    template: '#_ficha_colaboracao',
    props: ['dataTopic', 'dataFieldLabel', 'dataFieldName', 'dataValue', 'dataRefBib', 'dataOriginalValue', 'dataVisible', 'dataHelp'],
    computed: {
        originalValueTrim: function() {
            return String(this.dataOriginalValue).replace(/<p>&nbsp;<\/p>$/, '');
        },
        valueTrim: function() {
            return String(this.dataValue).replace(/<p>&nbsp;<\/p>$/, '');
        }
    },
    created: function() {
        page.fields[this.dataFieldName].value = trim(this.dataValue);
        page.fields[this.dataFieldName].originalValue = trim(this.dataOriginalValue);
    }
});

// INICIAR APP PRINCIPAL
var page = new Vue({
    el: '#div-page-colaboracao',
    dataTableRegistros:null,
    data: {
        map: null,
        oMap:null,
        selectedPoint: {},
        visible: false,
        ocorrenciasVisible: false,
        ocorrenciasPagination: { totalPages:0,pageNumber:1,pageSize:0,rowNumber:1,totalRecords:0 },        
        minhasOcorrenciasVisible: false,
        minhasOcorrencias: [],
        refBibList: [],
        currentField: '',
        currentRefBib: {},
        valido: '' , // opinião do colaborador se o ponto é ou não válido
        obsValidacao: '',
        ocorrenciaEditando: null,
        currentSN:'', // guardar o valor atual dos combos S/N do grid de ocorrências ao abrir a popup para justificativa quando não concordar com o registro
        fields: {
            'dsNotasTaxonomicas': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsDiagnosticoMorfologico': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'nomesComuns': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'sinonimias': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsDistribuicaoGeoNacional': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsDistribuicaoGeoGlobal': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'estados': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'biomas': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'bacias': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'areasRelevantes': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsHistoriaNatural': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsHabitoAlimentar': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsUsoHabitat': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsEspecialistaMicroHabitat': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsInteracao': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsReproducao': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsPopulacao': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsCaracteristicaGenetica': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsAmeaca': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsUso': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsAcaoConservacao': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsPresencaUc': {
                value: '',
                edited: false,
                originalValue: ''
            },
            'dsPesquisaExistNecessaria': {
                value: '',
                edited: false,
                originalValue: ''
            },
        },
        // dados da planilha enviada com os registros de ocorrencias
        selectedFilePlanilha:null,
        sqFichaConsultaAnexo:null,
        noFichaConsultaAnexo:null,

        fotosColaboracaoVidible: false,        
        resFotosColaboracao: [],
    },
    created: function() {

    },

    mounted: function() {
        this.visible = true;
        this.$nextTick(function(){
            page.toggleOcorrencias();
            $(document).scrollTop(0);
            // iniciar o tinyMCE
            initEditor();
        });

        // adicionar evento on close nas modais
        $(window).on('closed.zf.reveal',function(evt){
            if( evt.target.id == 'frmRefBib')
            {
                if( modalRefBib.opener )
                {
                    $("#" + modalRefBib.opener).foundation('open');
                    modalRefBib.opener=null;
                    setFocus('vlLat');
                }
            } else if( evt.target.id == 'divFrmOcorrencia' ) {
                page.getMinhasOcorrencias();
            }
        });

        window.setTimeout(function() {
            // ler informações da planilha de ocorrencias anexada ja cadastrada se existir
            var $inputPlanilha = $("#inputPlanilha");
            if( $inputPlanilha.length == 1 ) {
                if( $inputPlanilha.data('currentId') ) {
                    page.sqFichaConsultaAnexo = $inputPlanilha.data('currentId');
                    page.noFichaConsultaAnexo = $inputPlanilha.data('currentFile');
                }
            }

            // http://iamceege.github.io/tooltipster/#triggers
            $('.tooltipster').tooltipster({
                theme: 'tooltipster-green',
                contentAsHTML: true,
                contentCloning: true,
                maxWidth: 800,
                animation: 'grow',
                animationDuration: 200,
                trigger: 'click',
                interactive: true,
            }).on('click', function(e) {
                if ($(this).tooltipster('status').state == 'closed') {
                    $(this).tooltipster('open');
                } else if ($(this).tooltipster('status').state == 'stable') {
                    $(this).tooltipster('close');
                }
                e.stopPropagation();
            });
        }, 300);
      

    },
    methods: {
        downloadPlanilhaRegistro:function(){
            var url = baseUrl + 'ficha/downloadPlanilhaModeloOcorrencias';
            window.open(url, '_self', "width=0,height=0");
        },
        // excluir a planilha anexada
        deletePlanilhaRegistroEnviada : function() {
            if ( confirm('Confirma Exclusão da planilha?') ) {
                doPost(baseUrl + 'ficha/excluirPlanilhaOcorrencia', {sqFichaConsultaAnexo: page.sqFichaConsultaAnexo }, function (res) {
                    if (res.status == 0) {
                        page.selectedFilePlanilha=null;
                        page.sqFichaConsultaAnexo=null;
                        page.noFichaConsultaAnexo=null;
                        if( res.msg ) {
                            notify( res.msg,null,'success');
                        }
                    }
                });
            }
        },
        // ao selecionar a planilha de ocorrencias para envio
        onFileSelectedPlanilha:function( event ) {
            this.selectedFilePlanilha=null;
            var input = event.target;
            if ( input.files && input.files[0]) {
                this.selectedFilePlanilha = event.target.files[0]
                if( ! (/.+\.zip$/.test( this.selectedFilePlanilha.name) ) ) {
                    this.selectedFilePlanilha = null;
                    alert('Arquivo inválido.<br>Selecione um arquivo compactado no formato zip.<br>Extensão .zip');
                } else {
                    // gravar anexo no banco de dados
                    //console.log( this.selectedFilePlanilha );
                    var fd = new FormData();
                    if( this.sqFichaConsultaAnexo ) {
                        fd.append('sqFichaConsultaAnexo', this.sqFichaConsultaAnexo )
                    }
                    fd.append('sqCicloConsulta',  $("#sqConsultaFicha").val() )
                    fd.append('arquivo', this.selectedFilePlanilha, this.selectedFilePlanilha.name);
                    blockUi('Enviando arquivo..');
                    doPost(baseUrl + 'ficha/salvarPlanilhaOcorrencias', fd, function(res) {
                        unblockUi()
                        if( res.errors.length > 0 ) {
                            alert(res.errors.join('<br/>'), 'Atenção', 'error');
                        } else {
                            page.sqFichaConsultaAnexo = res.data.sqFichaConsultaAnexo
                            page.noFichaConsultaAnexo = page.selectedFilePlanilha.name
                            if( res.msg ) {
                                notify( res.msg,null,'success');
                            }
                        }
                    });
                }
            }
        },
        getMinhasOcorrencias: function() {
            var vm=this;
            doPost(baseUrl + 'ficha/getMinhasOcorrencias', {
                pageNumber:vm.ocorrenciasPagination.pageNumber,
                sqConsultaFicha: $("#sqConsultaFicha").val()
            }, function(res) {
                if (res.data) {
                    vm.ocorrenciasPagination = res.pagination;
                    var $table = $("#grideRegistros");
                    $table.floatThead('destroy');
                    blockUi('Aguarde...');
                    //page.dataTableRegistros=null;
                    /*if ( page.dataTableRegistros )
                    {
                        page.dataTableRegistros.fnDestroy();
                    }*/
                    //page.ocorrenciasPagination = res.pagination;
                    vm.minhasOcorrencias = res.data.ocorrencias;
                    // aplicar floatHead na table
                    window.setTimeout(function(){
                        $table.floatThead({scrollContainer: true,
                            autoReflow: true});
                        /*var nonOrderableCols = [];
                        $("#grideRegistros").find('thead th').each(function(key,value){
                            nonOrderableCols.push( key );
                        });
                        page.dataTableRegistros = $("#grideRegistros").dataTable( $.extend({},default_datatable_options,{
                            "columnDefs" : [
                                {"orderable": false,"targets": nonOrderableCols },
                            ],
                        }));*/
                        //console.log( res.pagination );
                        unblockUi();
                    },2000);
                } else {
                    page.minhasOcoorrencias = [];
                }
            });
        },
        ocorrenciasPaginationChange:function(){
            this.getMinhasOcorrencias();
        },
        editMinhaOcorrencia: function(id) {
            modalOcorrencia.show(id);
        },

        deleteMinhaOcorrencia: function(id) {
            modalOcorrencia.delete(id);
        },
        toggleOcorrencias: function() {
            this.ocorrenciasVisible = !this.ocorrenciasVisible
            if( this.ocorrenciasVisible ){

            }
        },
        toggleMinhasOcorrencias: function() {
            this.minhasOcorrenciasVisible = !this.minhasOcorrenciasVisible
            if (this.minhasOcorrencias.length == 0) {
                this.getMinhasOcorrencias();
            }
        },
        initMap: function() {
            $('#divGridOcorrencia').width($('#map-ocorrencia-container').width());
            if (this.oMap) {
                //refreshMap(this.map);
                this.oMap.refresh();
                return;
            }
            var urlApiSalve = baseUrl.replace('/salve-consulta/', '/salve-estadual/') + 'api/ocorrencias/';
            if (baseUrl.indexOf('8090') > -1) {
                urlApiSalve = urlApiSalve.replace('8090', '8080');
            }
            else if (baseUrl.indexOf('8080') > -1) {
                urlApiSalve = urlApiSalve.replace('8080', '8090');
            }
             this.oMap = new Ol3Map({
                el: 'map-ocorrencias'
                , eooUsedPoints:false
                 ,contexto:'salveConsulta'
                , height: 670
                , gridId: 'divGridOcorrencia'
                , readOnly: true
                , showToolbar: true
                , sqFicha: $("#sqFicha").val()
                , urlOcorrencias: urlApiSalve
                , showAdicionadosAposAvaliacao:false
                , showAoo : false
            });
            this.oMap.run();

            // já exibir o gride com as ocorrências
            this.toggleMinhasOcorrencias();

        },
      updatePonto: function(id) {

      },
      generateComPessoal: function() {
            var nomeUsuario = capitalize($("#li-full-name").text().trim());
            var comPess = nomeUsuario.split(' ');
            if (comPess.legend_ < 2) {
                alert('Nome ' + nomeUsuario + ' inválido para gerar comunicação pessoal.');
                return '';
            }
            // Pegar inicial do primeiro nome+ponto+espaço+último nome. +com. pess. +ano vigente
            return comPess[0].substring(0, 1).toUpperCase() + '. ' + comPess[comPess.length - 1] + '. com. pess. ' + new Date().getFullYear()
        },
        abrirModalObservacao: function(ocorrencia) {
            // guardar o valor atual do select se ainda não tiver sido guardado
            if( !ocorrencia.currentValue || ocorrencia.currentValue == '' ) {
                ocorrencia.currentValue = ocorrencia.valido;
            }
            this.ocorrenciaEditando = ocorrencia;
            $("#txObsValicaoOcorrencia").val(ocorrencia.obs);
            $("#divFrmObsValidacaoOcorrencia").foundation('open').off('closed.zf.reveal').on('closed.zf.reveal',function(){
                if( ocorrencia.valido != ocorrencia.currentValue ) {
                    ocorrencia.valido = ocorrencia.currentValue;
                }
            });
            window.setTimeout(function() {
                $("#txObsValicaoOcorrencia").focus();
            }, 600);
        },
        validarRegistro: function(ocorrencia, evt) {
            ocorrencia.currentValue = ocorrencia.valido;
            ocorrencia.valido = evt.target.value;
            $(evt.target).next('i').addClass('hidden');
            if (ocorrencia.valido == 'S') {
                $(evt.target).addClass('bgGreen')
                this.ocorrenciaEditando = ocorrencia;
                $("#txObsValicaoOcorrencia").val(ocorrencia.obs);
                page.gravarValidacaoOcorrencia();
            } else if (ocorrencia.valido == 'N') {
                $(evt.target).addClass('bgRed');
                $(evt.target).next('i').removeClass('hidden');
                this.abrirModalObservacao(ocorrencia);
            } else {
                $(evt.target).removeClass('bgRed bgGreen');
                this.ocorrenciaEditando = ocorrencia;
                //$("#txObsValicaoOcorrencia").val('');
                page.gravarValidacaoOcorrencia();
            }

        },
        gravarValidacaoOcorrencia: function() {
            var ocorrencia = this.ocorrenciaEditando;
            ocorrencia.obs = $("#txObsValicaoOcorrencia").val();
            var data = {
                id: ocorrencia.id,
                idConsulta: ocorrencia.idConsulta, // ou da revisão
                valido: ocorrencia.valido,
                fonte: ocorrencia.fonte,
                obs: $.trim(ocorrencia.obs)
            }

            // não permitir discordar sem uma justificativa
            if( data.valido =='N' && data.obs == '' ){
                alert('Favor, nos diga porque você não concorda.');
                $("#txObsValicaoOcorrencia").focus();
                return;
            }
            $("#divFrmObsValidacaoOcorrencia").foundation('close');
            doPost(baseUrl + 'ficha/validarRegistro', data,
                function(res) {
                    if (!res.status == 0) {
                        evt.target.value = '';
                    } else {
                        page.ocorrenciaEditando.currentValue = data.valido;
                        page.ocorrenciaEditando.valido = data.valido;
                    }
                });
        },

        identificarPonto: function(params) {
            if (!params.id) {
                return;
            }
            params.center = params.center == false ? false : true
            // os pontos no mapa a fonte é SALVE ou PORTALBIO
            params.fonte = params.fonte =='salve-consulta' ? 'salve' : params.fonte;
            //console.log( params )
            page.oMap.identifyPoint({id:params.id,bd:params.fonte}, params.center)
        },
        viewFoto:function( objFoto ) {
            var noCache = objFoto.noArquivo ? String( objFoto.noArquivo).replace(/[^a-z0-9]/gi,'') : randomString(4);
            $("#divViewImage").foundation('open');
            $("#imgViewFoto").attr('src',objFoto.url.replace('/thumb','')+'?noCache='+noCache );
        },     

    },
    watch: {
        ocorrenciasVisible: function() {
            if (!this.map || this.ocorrenciasVisible) {
                // criar o mapa
                window.setTimeout(function() {
                    page.initMap();
                }, 500);
            }
        },
    },

});

// FIM APP PRINICIPAL

// MODAL REFERENCIA BIBLIOGRAFICA
//----------------------------------------------------------------------------------
var modalRefBib = new Vue({
    el: '#frmRefBib',
    data: {
        'refBibList': [],
        'currentField': '',
        'current': null,
        'opener': null,
        'callback': null,
        'institutionsList': [],
    },
    mounted: function() {

        // inicializar select2 da ref. bibliográfica
        //placeholder= "Nome (min 3 letras)"
        //                  allowClear: true
        $("#frmRefBib #fldRefBib").select2({
            placeholder: "Nome (min 3 letras)",
            allowClear: true,
            minimumInputLength: 3,
            //minimumResultsForSearch: Infinity, // não exibir o campo search dentro da lista de opções
            //theme: "classic",
            ajax: {
                url: baseUrl + 'ajax/getRefBib',
                dataType: 'json',
                delay: 800,
                cache: true,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    return {
                        results: data.items
                    };
                },
            },
            templateResult: function(item) {
                if (!item.id || item.loading) {
                    return item.text;
                }
                return $('<div style="display:block;border-bottom:1px dashed gray;">' +
                    '<b>' + item.titulo + '</b><br/>' +
                    item.autor + '/' + item.ano + '</div>');
            },
            templateSelection: function(item) {
                var text = $.trim(item['descricao'] || item['text']);
                // quando existir codigo html no texto tem que retornar como objeto jquery
                if (text.indexOf('</') > -1) {
                    return $('<span>' + text + '</span>');
                }
                return text;
            },
        }).on("select2:select", function(e) {
            var data = $("#frmRefBib #fldRefBib").select2('data');
            if (data && data.length > 0) {
                modalRefBib.selectRefBibChange(data[0]);
            }
        }).on('select2:open', function(e) {
            var maxH = ($(window).height() / 2)
            $("#frmRefBib #div-page-ref-bib").css({
                height: String(maxH + 180) + 'px'
            });
            $('ul.select2-results__options').css({
                'max-height': maxH + 'px',
                'height': maxH + 'px'
            });
            $("#fldRefBib input[type='search']").focus();
        });
        // fim select2
    },
    methods: {
        show: function(field, opener, callback) {
            this.currentField = field;
            this.opener = opener;
            this.callback = callback;
            $("#frmRefBib").foundation('open');
            // existe evento closed.zf.reveal geral declarado acima
            /*$('#frmRefBib').unBind('closed.zf.reveal').bind('closed.zf.reveal', function() {
                // abre de volta a tela de cadastro da ocorrencia
                //$("#" + this.opener).foundation('open');
                modalRefBib.opener = null;
            });
            */
            window.setTimeout(function() {
                $("#frmRefBib #fldRefBib").select2("open");
            }, 1000);
        },
        //------------------------------------------------------
        // Autocomplete ref bibs
        //----------------------------------------------------
        selectRefBibChange: function(val) {
            if (val && this.opener) {
                $("#frmRefBib").foundation('close');
                if (this.callback) {
                    this.callback(val);
                }
                this.current = '';
            } else if (val && this.currentField) {
                val = val.citacao;
                var editor = tinyMCE.get(this.currentField + '_ref');
                if (editor) {
                    var currentHtml = tinyMCE.trim(editor.getContent({
                        format: 'html'
                    }).replace(/<p>&nbsp;<\/p>/g, '').replace(/<hr \/>$/,''));
                    var currentText = tinyMCE.trim(editor.getContent({
                        format: 'text'
                    }).replace(/<p>&nbsp;<\/p>/g, '').replace(/<hr \/>$/,''));
                    editor.setContent(val);
                    val = tinyMCE.trim(val.replace(/<p>&nbsp;<\/p>/g, '').replace(/<hr \/>$/,''))
                    var currentValue = editor.getContent({
                        format: 'text'
                    });
                    if (currentText.indexOf(currentValue) > -1) {
                        editor.setContent(currentHtml);
                    } else {
                        editor.setContent(currentHtml + val + '<hr/>');
                        $("#frmRefBib").foundation('close');
                        $("#frmRefBib #fldRefBib").empty();
                        var fieldLabel = $("#legend_" + this.currentField).data('label').trim();
                        editor.setDirty( true );
                        //editor.save();
                        editor.focus();
                    }
                    this.current = val;
                }
            }

        },
        saveRefBib: function(fieldName, fieldLabel, refBibValue) {
            // se a colaboração tiver alterada, basta salva-la para salvar tambem a ref bib
            var editorColaboracao = tinyMCE.get(fieldName.replace('_ref$',''));
            if (editorColaboracao.isDirty()) {
                // salvar os dois texto
                editorColaboracao.execCommand('mceSave', true);
            }
            else {
                var data = {
                    sqConsultaFicha: $("#sqConsultaFicha").val(),
                    sqTipoConsulta: $("#sqTipoConsulta").val(),
                    fieldName: fieldName,
                    fieldLabel: fieldLabel,
                    fieldValue: editorColaboracao.getContent(),
                    refBibValue: refBibValue
                }
                doPost(baseUrl + 'ficha/salvarColaboracao', data, function (res) {
                });
            };
        },
    }
});

// MODAL OCORRÊNCIAS
//------------------------------------------------------------------------------------
var modalOcorrencia = new Vue({
    el: '#divFrmOcorrencia',
    data: {
        map: null,
        oMap : null,
        marker: null,
        dragLayer: null,
        sqFichaOcorrenciaConsulta: '',
        sqPublicacao: '',
        txRefBib: '',
        deComunicacaoPessoal: '',
        vlLat: '',
        vlLon: '',
        nuDiaRegistro: '',
        nuMesRegistro: '',
        nuAnoRegistro: '',
        hrRegistro: '',
        inDataDesconhecida: false,
        sqDatum: '',
        sqPrecisaoCoordenada: '',
        sqRefAproximacao: '',
        noLocalidade: '',
        txObservacao: '',
        sqPrazoCarencia:'',
        nominatim: {},
        showReferenciaCoordenada: false,
        // dados da foto
        rowEditing:-1,
        idFoto:'',
        dtFoto:'',
        inAprovada:'',
        hrFoto:'',
        noAutorFoto:'',
        deFoto:'',
        minhasFotos:[],
        selectedFile:null,
    },
    beforeMount: function() {
        this.noAutorFoto = $("#noAutorFoto").val();
    },
    mounted: function() {
        $("#dtFoto").mask('00/00/0000');
        $("#hrFoto").mask('00:00');
    },
    methods: {
        show: function(id) {
            modalOcorrencia.reset(); // limpar o formulário
            this.sqFichaOcorrenciaConsulta = id || '';
            $("#divFrmOcorrencia").foundation('open');
            window.setTimeout(function() {
                modalOcorrencia.initMap();
                page.minhasOcorrencias = [];
            }, 500);
            if (this.sqFichaOcorrenciaConsulta) {

                // ler o registro do banco de dados
                doPost(baseUrl + 'ficha/editarOcorrencia', {
                    sqFichaOcorrenciaConsulta: this.sqFichaOcorrenciaConsulta
                }, function(res) {

                    if (res.sqFichaOcorrenciaConsulta) {
                        modalOcorrencia.sqFichaOcorrenciaConsulta = res.sqFichaOcorrenciaConsulta;
                        modalOcorrencia.sqPublicacao = res.sqPublicacao;
                        modalOcorrencia.txRefBib = res.txRefBib;
                        modalOcorrencia.deComunicacaoPessoal = res.deComunicacaoPessoal;
                        modalOcorrencia.vlLat = res.vlLat;
                        modalOcorrencia.vlLon = res.vlLon;
                        modalOcorrencia.sqDatum = res.sqDatum;
                        modalOcorrencia.sqPrecisaoCoordenada = res.sqPrecisaoCoordenada;
                        modalOcorrencia.sqRefAproximacao = res.sqRefAproximacao;
                        modalOcorrencia.noLocalidade = res.noLocalidade;
                        modalOcorrencia.nuDiaRegistro = res.nuDiaRegistro;
                        modalOcorrencia.nuMesRegistro = res.nuMesRegistro;
                        modalOcorrencia.nuAnoRegistro = res.nuAnoRegistro;
                        modalOcorrencia.hrRegistro = res.hrRegistro;
                        modalOcorrencia.inDataDesconhecida = (res.inDataDesconhecida == 'S');

                        modalOcorrencia.txObservacao = res.txObservacao;
                        modalOcorrencia.sqPrazoCarencia = res.sqPrazoCarencia;

                        if (modalOcorrencia.marker) {
                            modalOcorrencia.setMarkerPosition();
                            modalOcorrencia.oMap.setZoom(10);
                        }
                    }
                    // carregar as fotos ligadas ao registro de ocorrencia
                    modalOcorrencia.minhasFotos = res.fotos;
                });
            }
        },
        onFileSelected:function(event)
        {
            var input=event.target;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imgFoto').attr('src', e.target.result );
                };
                reader.readAsDataURL(input.files[0]);
            };
            this.selectedFile = event.target.files[0]
        },
        addFoto : function( file )
        {
            if( !this.dtFoto ||!this.noAutorFoto || !this.deFoto )
            {
                alert('Campos obrigatórios devem ser preenchidos!');
                return;
            }
            if( !this.idFoto && !this.selectedFile )
            {
                alert('Nenhuma foto selecionada!');
                return;
            }
            // validar data e hora
            if( ! isValidDate( this.dtFoto+' '+this.hrFoto) )
            {
                return;
            }

            if( this.rowEditing > -1 )
            {
                this.minhasFotos[this.rowEditing].idFoto = this.idFoto;
                this.minhasFotos[this.rowEditing].dtFoto = this.dtFoto;
                this.minhasFotos[this.rowEditing].hrFoto = this.hrFoto;
                this.minhasFotos[this.rowEditing].noAutorFoto =  this.noAutorFoto;
                this.minhasFotos[this.rowEditing].deFoto = this.deFoto;
                this.minhasFotos[this.rowEditing].inAprovada = ''
                if( this.selectedFile ) {
                    this.minhasFotos[this.rowEditing].noArquivo = this.selectedFile.name;
                    this.minhasFotos[this.rowEditing].file = this.selectedFile;
                    this.minhasFotos[this.rowEditing].url = URL.createObjectURL(this.selectedFile);
                }
            }
            else
            {
                if( this.minhasFotos.length > 4 )
                {
                    alert('Máximo 5 fotos!');
                    return;
                }

                if( modalOcorrencia.minhasFotos.filter(function(foto){
                    return foto.noArquivo == modalOcorrencia.selectedFile.name;
                }).length > 0) {
                    alert('Foto já cadastrada!');
                    return;
                }


                this.minhasFotos.push({
                      idFoto     : this.idFoto
                    , dtFoto     : this.dtFoto
                    , hrFoto     : this.hrFoto
                    , noAutorFoto: this.noAutorFoto
                    , deFoto     : this.deFoto
                    , noArquivo  : this.selectedFile.name
                    , file       : this.selectedFile
                    , url        : URL.createObjectURL(this.selectedFile)
                    , inAprovada : 'N' // valor padrão, não aprovada
                });
            }
            this.resetFoto();
            notify( 'Foto adicionada com SUCESSO! Clique no botão "Gravar" para concluir o registro.');
        },
        editMinhaFoto:function(foto, row)
        {
            if( foto.inAprovada == 'S')
            {
                alert('Foto já foi aprovada pela equipe SALVE.');
                return;
            }
            this.rowEditing = row;
            this.dtFoto = foto.dtFoto;
            this.hrFoto = foto.hrFoto;
            this.noAutorFoto = foto.noAutorFoto;
            this.deFoto = foto.deFoto;
            this.idFoto = foto.idFoto;
            this.selectedFile = foto.file;
            this.inAprovada = foto.inAprovada;
            $('#imgFoto').attr('src', foto.url );
            setFocus( 'dtFoto');
        },
        deleteMinhaFoto:function(foto,i )
        {
            var that=this;
            if ( confirm('Confirma Exclusão da foto '+( !foto.file ? foto.noArquivo : foto.file.name)+'?')) {
                if( foto.idFoto ) {
                    doPost(baseUrl + 'ficha/excluirMinhaFoto', {sqFichaConsultaMultimidia: foto.idFoto }, function (res) {
                        if (res.status == 0) {
                            that.minhasFotos.splice(i, 1);
                        }
                    });
                } else {
                    that.minhasFotos.splice(i,1);
                }
            }
        },
        resetFoto:function()
        {
            this.rowEditing = -1;
            this.dtFoto     = '';
            this.hrFoto     = '';
            this.deFoto     = '';
            this.idFoto     = '';
            this.inAprovada = 'N';
            this.selectedFile = null;
            $('#imgFoto').attr('src','//via.placeholder.com/350x220');
            setFocus('dtFoto');
            var inputFoto = $("#inputFoto");
            inputFoto.replaceWith( inputFoto.val('').clone(true) );
        },
        getRefBib: $.debounce(1000, function(q, loading) {
            loading(true);
            doPost(baseUrl + 'ajax/getRefBib', {
                q: q
            }, function(res) {

                loading(false);
                modalOcorrencia.refBibList = res;
            });
        }),
        selectRefBibChange: function(val) {
            this.txRefBib = '';
            if (val) {
                this.sqPublicacao = val.id;
                this.txRefBib = val.citacao + '&nbsp;<i title="Excluir" onClick="modalOcorrencia.deleteRefBib(' + val.id + ');" class="fi-minus fi-minus-delete"></i>';
            }
            $("#fldRefBib").val([]).trigger('change');
        },
        saveOcorrencia: function() {
            //event.preventDefault();
            //var data = $("frmOcorrencia").serializeArray();
            var scrollTopOnError = 59.7;

            if (!this.showReferenciaCoordenada) {
                this.sqRefAproximacao = '';
            } else {
                if (!this.sqRefAproximacao) {
                    alert('Referência da coordenada deve ser informada!', 'Atenção', 'error')
                    $("#divFrmOcorrencia").parent().scrollTop(scrollTopOnError);
                    return;
                }
            }
            if( this.selectedFile || this.rowEditing > -1 )
            {
                alert('Existe foto selecionada, clique no botão Incluir/Gravar foto ou Limpar antes de salvar o registro da ocorrência.');
                $("#divFrmOcorrencia").parent().scrollTop(scrollTopOnError);
                return;
            }
            this.vlLat = this.vlLat.replace(',','.');
            this.vlLon = this.vlLon.replace(',','.');
            // validar a lat e lon
            // lat deve estar entre -90 a +90
            // lon deve estar entre -180 a +180
            if( this.vlLat < -90 || this.vlLat > 90 ) {
                alert('Latitude inválida. A latitude deve estar entre -90 e +90.');
                return;
            }

            if( this.vlLon < -180 || this.vlLon > 180 ) {
                alert('Longitude inválida. A latitude deve estar entre -180 e +180.');
                return;
            }

            var data = {
                sqConsultaFicha: $("#sqConsultaFicha").val(),
                sqTipoConsulta: $("#sqTipoConsulta").val(),
                sqFichaOcorrenciaConsulta: this.sqFichaOcorrenciaConsulta,
                sqPublicacao: this.sqPublicacao,
                txRefBib: this.txRefBib,
                deComunicacaoPessoal: this.deComunicacaoPessoal,
                vlLat: this.vlLat,
                vlLon: this.vlLon,
                nuDiaRegistro: this.nuDiaRegistro,
                nuMesRegistro: this.nuMesRegistro,
                nuAnoRegistro: this.nuAnoRegistro,
                hrRegistro: this.hrRegistro,
                inDataDesconhecida: (this.inDataDesconhecida ? 'S' : 'N'),
                sqDatum: this.sqDatum,
                sqPrecisaoCoordenada: this.sqPrecisaoCoordenada,
                sqRefAproximacao: this.sqRefAproximacao,
                noLocalidade: this.noLocalidade,
                txObservacao: this.txObservacao,
                refBibHtml  : this.sqPublicacao ? this.txRefBib : this.deComunicacaoPessoal,
                sqPrazoCarencia: this.sqPrazoCarencia,
            };

            if( !data.nuAnoRegistro || !data.vlLat || !data.vlLon || !data.sqDatum || !data.sqPrecisaoCoordenada || !data.sqPrazoCarencia )
            {
                alert('Campos obrigatórios não preenchidos!');
                $("#divFrmOcorrencia").parent().scrollTop(scrollTopOnError);
                return;
            }
            if( !data.deComunicacaoPessoal && !data.sqPublicacao )
            {
                alert('Referência bibliográfica ou comunicação pessoal deve ser informada!');
                $("#divFrmOcorrencia").parent().scrollTop(scrollTopOnError);
                return;
            }
            var fd = new FormData();
            for( key in data )
            {
                if( data[ key ] ) {
                    fd.append(key, data[key] )
                }
            }

            if( this.minhasFotos.length) {
                var index = 0;
                this.minhasFotos.map(function (item, i) {

                    fd.append('fotoId.' + (index), item.idFoto);
                    if (item.file) {
                        fd.append('foto.' + (index), item.file, item.file.name);
                    }
                    fd.append('fotoData.' + (index), item.dtFoto);
                    fd.append('fotoHora.' + (index), item.hrFoto);
                    fd.append('fotoAutor.' + (index), item.noAutorFoto);
                    fd.append('fotoDescricao.' + (index), item.deFoto.trim());
                    index++;
                });
            }

            //doPost(baseUrl + 'ficha/salvarOcorrencia', data, function(res) {
            blockUi('Gravando...');
            doPost(baseUrl + 'ficha/salvarOcorrencia', fd, function(res) {
                if( res.msg ) {
                    notify(res.msg, '', res.type);
                    res.msg = ''; // evitar reesibição da mensagem
                }
                if (res.errors && res.errors.length > 0 && res.type) {
                    alert(res.errors.join('<br/>'), 'Atenção', 'error');
                }
                if (res.data.idEdit) {
                    var index = page.minhasOcorrencias.map(function(item) {
                        return item.idEdit;
                    }).indexOf(res.data.idEdit);
                    if (index == -1) {
                        page.minhasOcorrencias.push({
                            id: res.data.id,
                            idEdit: res.data.idEdit,
                            dataHora: res.data.dataHora,
                            refBib: res.data.refBibHtml,
                            lat: modalOcorrencia.vlLat,
                            lon: modalOcorrencia.vlLon,
                            baseDados: 'minha-colaboracao',
                            fonte: 'salve-consulta',
                            datum: $("#sqDatum option:selected").val() ? $("#sqDatum option:selected").text() : '',
                            precisao: $("#sqPrecisaoCoordenada option:selected").val() ? $("#sqPrecisaoCoordenada option:selected").text() : '',
                            refAproximacao: $("#sqRefAproximacao option:selected").val() ? $("#sqRefAproximacao option:selected").text() : '',
                            localidade: modalOcorrencia.noLocalidade,
                            fotos:modalOcorrencia.minhasFotos,

                            //refBib: modalOcorrencia.sqPublicacao ? modalOcorrencia.txRefBib : modalOcorrencia.deComunicacaoPessoal,
                        });
                    } else {
                        page.minhasOcorrencias[index].lat = modalOcorrencia.vlLat;
                        page.minhasOcorrencias[index].lon = modalOcorrencia.vlLon;
                        //page.minhasOcorrencias[index].nuMesRegistro = $("#nuMesRegistro option:selected").val() ? $("#nuMesRegistro option:selected").text() : '';
                        page.minhasOcorrencias[index].datum = $("#sqDatum option:selected").val() ? $("#sqDatum option:selected").text() : '';
                        page.minhasOcorrencias[index].precisao = $("#sqPrecisaoCoordenada option:selected").val() ? $("#sqPrecisaoCoordenada option:selected").text() : '';
                        page.minhasOcorrencias[index].refAproximacao = $("#sqRefAproximacao option:selected").val() ? $("#sqRefAproximacao option:selected").text() : '';
                        page.minhasOcorrencias[index].localidade = modalOcorrencia.noLocalidade;
                        page.minhasOcorrencias[index].refBib = res.data.refBibHtml;
                        page.minhasOcorrencias[index].carencia = res.data.carencia;
                        page.minhasOcorrencias[index].fotos = modalOcorrencia.minhasFotos;
                        $("#divFrmOcorrencia").foundation('close'); // fechar apos editar
                        //page.minhasOcorrencias[index].refBib = modalOcorrencia.sqPublicacao ? modalOcorrencia.txRefBib : modalOcorrencia.deComunicacaoPessoal;
                    }
                    // atualizar os pontos
                    modalOcorrencia.reset(); // limpar formulário
                    modalOcorrencia.oMap.refresh(); // atualizar pontos no mapa
                    page.oMap.refresh(); // atualizar pontos no mapa

                    //page.updatePonto(res.data.idEdit);
                    if( !res.errors || res.errors.length == 0 )
                    {
                        notify('Registro gravado com SUCESSO!');
                    }
                    //page.pontosLayer.getSource().clear();
                    // ou pontosLayer.changed()
                }
                // atualizar os ids das fotos
                if ( res.data.fotos && modalOcorrencia.minhasFotos.length > 0 )
                {
                    res.data.fotos.forEach(function(item,i){
                       var index = parseInt(item.index);
                       var arquivo = item.arquivo;
                       if( modalOcorrencia.minhasFotos[index].file.name == arquivo )
                       {
                           modalOcorrencia.minhasFotos[index].idFoto = parseInt(item.id);
                       }
                    });
                }
            });
        },
        deleteRefBib: function(id) {
            // limpar div da referencia bibliográfica da ocorrencia
            this.sqPublicacao = '';
            this.txRefBib = '';
        },
        delete: function(id) {
            var self = this;
            if (id) {
                this.sqPublicacao = '';
                this.txRefBib = '';
                if (!confirm('Confirma Exclusão?')) {
                    return;
                }
                var data = {
                    sqFichaOcorrenciaConsulta: id
                };
                doPost(baseUrl + 'ficha/excluirOcorrencia', data, function(res) {
                    if (res.status == 0) {
                        modalOcorrencia.reset();
                        try{ page.oMap.refresh();} catch(e){};
                        try{ self.oMap.refresh();} catch(e){};
                        //pontosLayer.getSource().clear();
                        //drawLayer.getSource().clear();
                        page.minhasOcorrencias = page.minhasOcorrencias.filter(function(item) {
                            return !item.idEdit || item.idEdit != id;
                        });
                    }

                });
            }
        },
        reset: function() {
            this.sqFichaColaboracaoOcorrencia = '';
            this.sqPublicacao = '';
            this.txRefBib = '';
            this.deComunicacaoPessoal = '';
            this.vlLat = '';
            this.vlLon = '';
            this.nuDiaRegistro = '';
            this.nuMesRegistro = '';
            this.nuAnoRegistro = '';
            this.hrRegistro = '';
            this.inDataDesconhecida = false;
            this.sqDatum = '';
            this.sqPrecisaoCoordenada = '';
            this.sqRefAproximacao = '';
            this.noLocalidade = '';
            this.txObservacao = '';
            this.resetFoto();
            this.minhasFotos=[];
            if (this.map) {
                this.map.getView().setCenter(this.marker.getGeometry().getCoordinates());
            }
        },
        initMap: function() {
            if (this.oMap) {
                this.oMap.refresh();
                return;
            }
            var urlApiSalve = baseUrl.replace('/salve-consulta/','/salve-estadual/') + 'api/ocorrencias/';
            if( baseUrl.indexOf('8090') > -1 )
            {
                urlApiSalve = urlApiSalve.replace('8090','8080');
            }
            else if( baseUrl.indexOf('8080') > -1 )
            {
                urlApiSalve = urlApiSalve.replace('8080','8090');
            }
            this.oMap = new Ol3Map({
                el: 'map-form-ocorrencias'
                , height: 670
                , contexto:'salveConsulta'
                , eooUsedPoints:false
                , readOnly: true
                , showToolbar: true
                , sqFicha: $("#sqFicha").val()
                , showExportButton:false
                , showAdicionadosAposAvaliacao:false
                , urlOcorrencias: urlApiSalve
                , showAdicionadosAposAvaliacao:false
                , showAoo : false
            });
            this.oMap.run();

            // criar um layer para mostrar a marker
            // criar marker
            this.marker = new ol.Feature(new ol.geom.Point([-5328210.013394036, -1781534.300990922]));
            this.dragLayer = new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: [this.marker]
                }),
                id:'dragLayer',
                //name:'Drag Layer'
                zIndex: 4,
                visible: true,
                //title: 'Clique e arraste para selecionar a lat e lon.',
                style: new ol.style.Style({
                    image: new ol.style.Icon( ({
                        anchor: [16, 32],
                        anchorXUnits: 'pixels',
                        anchorYUnits: 'pixels',
                        opacity: 0.65,
                        src: '/salve-consulta/assets/marker-my-sight.png',
                    })),
                    stroke: new ol.style.Stroke({
                        width: 3,
                        color: [255, 0, 0, 1]
                    }),
                    fill: new ol.style.Fill({
                        color: [0, 0, 255, 0.6]
                    })
                })
            });
            this.oMap.map.addLayer(this.dragLayer);

            // capturuar click no mapa
            this.oMap.map.on('singleclick', mapSingleClick);

            // se for edição posicionar a marker no ponto editado
            if( modalOcorrencia.sqFichaOcorrenciaConsulta )
            {
                modalOcorrencia.setMarkerPosition()
            }

            /*
            this.map = new ol.Map({
                target: 'map-form-ocorrencias',
                interactions: ol.interaction.defaults().extend([new oldd.Drag()]),
                layers: [osmLayer, pontosLayer, drawLayer],
                controls: ol.control.defaults({
                    attributionOptions: ({
                        collapsible: false
                    })
                }).extend([
                    new ol.control.FullScreen(),
                    new ol.control.ScaleLine(),
                    new ol.control.ZoomSlider(),
                    new ol.control.MousePosition({
                        projection: 'EPSG:4326',
                        coordinateFormat: ol.coordinate.createStringXY(10),
                        undefinedHTML: 'X: Y:'
                    }),
                    //new ol.control.OverviewMap({ className: 'ol-overviewmap ol-custom-overviewmap'}),
                ]),
                view: new ol.View({
                    center: [-5328210.013394036, -1781534.300990922],
                    zoom: 5
                })
            }); // fim new map


            this.map.addOverlay(ovlPopup2);

            // capturuar click no mapa
            this.map.on('singleclick', mapSingleClick);

            // trocar o cursor para pointer ao passar sobre alguma feature
            this.map.on('pointermove', function(e) {
                if (e.map) {
                    var pixel = e.map.getEventPixel(e.originalEvent);
                    var hit = e.map.hasFeatureAtPixel(pixel);
                    e.map.getViewport().style.cursor = hit ? 'pointer' : '';
                }

            });
            */

            // criar marker
            /*this.marker = new ol.Feature(new ol.geom.Point([-5328210.013394036, -1781534.300990922]));
            this.dragLayer = new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: [this.marker]
                }),
                zIndex: 4,
                visible: true,
                title: 'Clique e arraste para selecionar a lat e lon.',
                style: new ol.style.Style({
                    image: new ol.style.Icon( ({
                        anchor: [16, 32],
                        anchorXUnits: 'pixels',
                        anchorYUnits: 'pixels',
                        opacity: 0.65,
                        src: '/salve-consulta/assets/marker-my-sight.png',
                    })),
                    stroke: new ol.style.Stroke({
                        width: 3,
                        color: [255, 0, 0, 1]
                    }),
                    fill: new ol.style.Fill({
                        color: [0, 0, 255, 0.6]
                    })
                })
            });
            this.map.addLayer(this.dragLayer);
            this.map.once('postrender', function(event) {
                modalOcorrencia.setMarkerPosition();
                // quando iniciar para edição, definir o zoom em 10
                if (modalOcorrencia.sqFichaColaboracaoOcorrencia > 0) {
                    modalOcorrencia.map.getView().setZoom(10);
                }
            });
            */

        },
        getPointInfo: function() {
            /*
            Utilizar o serviço nominatim
            Exemplo: https://nominatim.openstreetmap.org/reverse.php?format=html&lat=-15.673287710389&lon=-47.995834350586&zoom=6
            Exemplo requisição: https://nominatim.openstreetmap.org/reverse.php?format=html&lat=-15.673287710389&lon=-47.995834350586&zoom=6
            */
            var params = '?format=json&lat=' + this.vlLat + '&lon=' + this.vlLon + '&zoom=18';

            $('#noLocalidade').parent().block({
                message: '<h4>Atualizando...</h4>',
                showOverlay: true,
                css: {
                    borderRadius: '10px',
                    'paddingTop': '10px',
                    width: '80%',
                    border: '1px solid #000'
                }
            });
            $.ajax({
                type: 'GET',
                url: 'https://nominatim.openstreetmap.org/reverse.php' + params
            }).done(function(res) {
                //console.log( res );
                if (typeof res.display_name != 'undefined') {
                    modalOcorrencia.nominatim = res;
                    modalOcorrencia.noLocalidade = 'Estado:' + res.address.state + "\n" + res.display_name;
                }
            }).fail(function(jqXHR, status, message) {
                alert(status + '<br>' + message, 'Erro', 'error');
            }).always(function() {
                $('#noLocalidade').parent().unblock();
            });
        },
        addPoint: function(lat, lon, color, content, radius, transparency, borderWidth) {
            //console.log('AddPoint chamado, fazer tratramento')

            /*color = color || '#FB4DFF';
            radius = radius || 4.5;
            content = content || '';
            transparency = transparency || 0.85;
            borderWidth = borderWidth || 0.5;
            //var position =  ol.proj.transform( [lon,lat], 'EPSG:4326','EPSG:3857' )
            var position = ol.proj.fromLonLat([lon, lat]);
            var style = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: radius,
                    stroke: new ol.style.Stroke({
                        color: '#000000',
                        width: borderWidth,
                    }),
                    fill: new ol.style.Fill({
                        color: hex2rgba(color, String(transparency)),
                    })
                })
            });

            var ponto = new ol.Feature({
                geometry: new ol.geom.Point(position),
                text: content,
            });
            ponto.setStyle(style);
            drawLayer.getSource().addFeature(ponto);
            //map.getView().setCenter(position);
            //map.getView().setResolution(2.388657133911758);// é o mesmo que setZoom(x)
            return ponto;
            */
        },
        onMarkerMoved: function() {
            var latLon = ol.proj.transform(this.marker.getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326');
            this.vlLat = latLon[1].toFixed(11);
            this.vlLon = latLon[0].toFixed(11);
            this.getPointInfo();
        },
        setMarkerPosition: function(boolCenter, boolUpdateInfo) {
            this.vlLon = String(this.vlLon).replace(',','.').replace(/[^0-9\.-]/g,'');
            this.vlLat = String(this.vlLat).replace(',','.').replace(/[^0-9\.-]/g,'');
            boolCenter = (boolCenter == false ? false : true);
            boolUpdateInfo = (boolUpdateInfo == true ? true : false);
            if (this.marker && this.vlLat != '' && this.vlLon != '') {
                this.vlLat = String(parseFloat(String(this.vlLat).replace(/\,/, '.')));
                this.vlLon = String(parseFloat(String(this.vlLon).replace(/\,/, '.')));
                var coords = ol.proj.transform([parseFloat(this.vlLon), parseFloat(this.vlLat)], 'EPSG:4326', 'EPSG:3857');
                this.marker.getGeometry().setCoordinates(coords);
                if (boolCenter) {
                    this.oMap.map.getView().setCenter(coords);
                }
                if (boolUpdateInfo) {
                    this.onMarkerMoved(); // ler dados da localidade
                }
            }
        },
        centerMarker: function() {
            //this.marker.getGeometry().setCoordinates(this.map.getView().getCenter())
        },
        inserirComPessoal: function(fld) {
            this.deComunicacaoPessoal = page.generateComPessoal();
        },
        updateGridFoto : function()
        {
            var data = { sqConsultaFicha: $("#sqConsultaFicha").val(),};

            doPost(baseUrl + 'ficha/getGridFotos', data, function(res) {
                if (res.status == 0) {

                }
            });
        }


        //------------------------------------------ fim methods
    },
    watch: {
        sqPrecisaoCoordenada: function() {
            var self = this;
            window.setTimeout(function() {
                self.showReferenciaCoordenada = ($("#sqPrecisaoCoordenada option:selected").data('codigo') == 'APROXIMADA');
            }, 1000)
        },
        inDataDesconhecida: function() {
            if (this.inDataDesconhecida) {
                this.nuDiaRegistro = '';
                this.nuMesRegistro = '';
                this.nuAnoRegistro = '';
                this.hrRegistro = '';
            }
        },
    },
});


// FOTO COLABORAÇÃO (Diferente da foto ocorrência)
//------------------------------------------------------------------------------------
var vmFotoColaboracao = new Vue({
    el: '#divFrmFotoColaboracao',
    data: {
        sqFichaConsultaFoto: '',      
        fotosPagination: { totalPages:0,pageNumber:1,pageSize:0,rowNumber:1,totalRecords:0 },
        // dados da foto
        idFoto:'',
        noAutorFoto:'',
        deEmailAutor:'',
        deFoto:'',
        inTipoTermoResp: '',
        postFotosColaboracao:[],
        selectedFileFotoColab:null,
        isEdit: false,
    },
    beforeMount: function() {
        this.noAutorFoto = $("#noAutorFoto").val();
        this.deEmailAutor = $("#deEmailAutor").val();        
    },
    mounted: function() {
        this.updateGridFotoColaboracao()                
    },
    methods: {
        show: function(sqFichaConsultaFoto) {
            vmFotoColaboracao.isEdit = false
            $('#imgFotoColaboracao').show()
            $('#btnSelectFotoColaborador').show()
            $('#conteinerImgFotoColaboracao').html('<img id="imgFotoColaboracao" src="//via.placeholder.com/350x220" style="width: 100%;height: auto;" title="imagem"/>');
            
            vmFotoColaboracao.inTipoTermoResp = ''
            
            vmFotoColaboracao.resetFotoColaboracao(); // limpar o formulário
            this.sqFichaConsultaFoto = sqFichaConsultaFoto || '';
            $("#divFrmFotoColaboracao").foundation('open');
            window.setTimeout(function() {
               vmFotoColaboracao.postFotosColaboracao = [];
            }, 500);    
                        
            if (this.sqFichaConsultaFoto) {
                vmFotoColaboracao.isEdit = true

                $('#imgFotoColaboracao').hide()
                
                // se for edição mostra minhatura da foto
                $('#imgFotoColaboracao').src = $('#img_grid_colab_'+this.sqFichaConsultaFoto).attr('src');
                imageTemp = new Image();                
                imageTemp.src = $('#img_grid_colab_'+this.sqFichaConsultaFoto).attr('src');
                imageTemp.width = '200';                
                imageTemp.title = 'Clique para ampliar'; 
                imageTemp.style = 'cursor: pointer;';                                
                $(imageTemp).on( "click", function() {
                    vmFotoColaboracao.viewFotoColaboracao(this.src)
                });
                document.getElementById("conteinerImgFotoColaboracao").appendChild(imageTemp);    

                $('#btnSelectFotoColaborador').hide()
                // ler o registro do banco de dados
                doPost(baseUrl + 'ficha/editarFotoColaboracao', {
                    sqFichaConsultaFoto: this.sqFichaConsultaFoto
                }, function(res) {
                    vmFotoColaboracao.idFoto         = res.sqFichaConsultaFoto
                    vmFotoColaboracao.noAutorFoto    = res.noAutor
                    vmFotoColaboracao.deEmailAutor   = res.deEmailAutor
                    vmFotoColaboracao.deFoto         = res.txMultimidia
                    vmFotoColaboracao.inTipoTermoResp= res.inTipoTermoResp

                });                
            }
        },
        onFileSelectedFotoColaboracao:function(event)
        {
            var input = null;
            input = event.target;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imgFotoColaboracao').attr('src', e.target.result );
                };
                reader.readAsDataURL(input.files[0]);
            };
            this.selectedFileFotoColab = event.target.files[0]
        },
        addFotoColaboracao : function()
        {
            this.postFotosColaboracao = []
            if(  $('input[name=in_tipo_termo_resp]:checked', '#frmFoto').val() ){
                this.inTipoTermoResp = $('input[name=in_tipo_termo_resp]:checked', '#frmFoto').val()           
            }else{
                return alert('Para prosseguir precisa aceitar o termo de uso!');                
            }
     
            if( !this.noAutorFoto || !this.deFoto || !this.deEmailAutor )
            {
                return alert('Campos obrigatórios devem ser preenchidos!');                
            }
            if( !this.idFoto && !this.selectedFileFotoColab )
            {                
                return alert('Nenhuma foto selecionada!');                
            }            
                
            var vNoArquivo  = ''
            var vFile       = ''
            var vUrl        = ''
            if(this.selectedFileFotoColab){
                vNoArquivo  = this.selectedFileFotoColab.name        
                vFile       = this.selectedFileFotoColab
                vUrl        = URL.createObjectURL(this.selectedFileFotoColab)
            }
            this.postFotosColaboracao.push({
                  idFoto        : this.idFoto                                  
                , noAutorFoto   : this.noAutorFoto
                , deEmailAutor  : this.deEmailAutor
                , deFoto        : this.deFoto                
                , noArquivo     : vNoArquivo
                , file          : vFile
                , url           : vUrl
                , inTipoTermoResp:this.inTipoTermoResp
            });

            var data = {
                sqConsultaFicha : $("#sqConsultaFicha").val(),               
            };
      
            var fd = new FormData();
            for( key in data )
            {
                if( data[ key ] ) {
                    fd.append(key, data[key] )
                }
            }            
            
            var index = 0;                
            fd.append('fotoId.'         + (index), this.postFotosColaboracao[0].idFoto);
            if (this.postFotosColaboracao[0].file) {
                fd.append('foto.'       + (index), this.postFotosColaboracao[0].file, this.postFotosColaboracao[0].file.name);
            }                
            fd.append('fotoAutor.'      + (index), this.postFotosColaboracao[0].noAutorFoto);
            fd.append('fotoEmailAutor.' + (index), this.postFotosColaboracao[0].deEmailAutor);
            fd.append('fotoDescricao.'  + (index), this.postFotosColaboracao[0].deFoto.trim());
            fd.append('fotoInTipoTermoResp.'+ (index), this.postFotosColaboracao[0].inTipoTermoResp.trim());
            index++;
            
            blockUi('Gravando...');

            doPost(baseUrl + 'ficha/salvarFotoColaboracao', fd, function(res) {
                if( res.msg ) {
                    notify(res.msg, '', res.type);
                    res.msg = ''; // evitar reesibição da mensagem
                }
                if (res.errors && res.errors.length > 0 && res.type) {
                    alert(res.errors.join('<br/>'), 'Atenção', 'error');
                }else{               
                    vmFotoColaboracao.resetFotoColaboracao(); // limpar formulário
                    $("#divFrmFotoColaboracao").foundation('close'); // fechar apos editar                        
                    notify('Registro gravado com SUCESSO!');                    
                    vmFotoColaboracao.updateGridFotoColaboracao(); // carrega o grid com fotos do colaborador
                    page.fotosColaboracaoVidible = true
                }                
                this.postFotosColaboracao  = [];
                
            });
            
        },
        deleteFotoColaboracao: function(sqFichaConsultaFoto) {
            if (sqFichaConsultaFoto) {
                if (!confirm('Confirma Exclusão?')) {
                    return;
                }
                var data = {
                    sqFichaConsultaFoto: sqFichaConsultaFoto
                };
                doPost(baseUrl + 'ficha/excluirFotoColaboracao', data, function(res) {
                    if (res.status == 0) {
                        var tr = document.querySelectorAll('[data-foto-colaboracao="'+sqFichaConsultaFoto+'"]')
                        $(tr).closest('tr').remove();
                    }
                });
            }
        },        
        resetFotoColaboracao:function()
        {       
            this.idFoto         = '';
            this.deFoto         = '';                        
            this.postFotosColaboracao   = [];
            $('#imgFotoColaboracao').attr('src','//via.placeholder.com/350x220');
            var inputFotoColab = $("#imgFotoColaboracao");
            inputFotoColab.replaceWith( inputFotoColab.val('').clone(true) );
        },          
        viewFotoColaboracao:function( urlFoto ) {
            var noCache = randomString(4);
            $("#divViewImage").foundation('open');
            $("#imgViewFoto").attr('src',urlFoto.replace('/thumb','')+'?noCache='+noCache );
        },
        updateGridFotoColaboracao: function(){                        
            blockUi('Aguarde...');                        
            page.resFotosColaboracao = []  
            try{
                doPost(baseUrl + 'ficha/minhasFotosColaboracao', {                       
                    sqConsultaFicha: $("#sqConsultaFicha").val()
                }, function(res) {         
                    if (res.data) {                                                                                    
                        page.resFotosColaboracao = res.data.fotosColaboracao;                                                   
                    }
                });                        
            }catch(e){
                alert('Ocorreu um erro. Favor recarregue a página! Erro: '+e.message)
            }finally{
                unblockUi();
            }                               
        },
        toggleFotoColaboracao: function() {
            page.fotosColaboracaoVidible = !page.fotosColaboracaoVidible
        },

        //------------------------------------------ fim methods
    },
    watch: {

    },
});
