// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require axios/axios.min
//= require jquery/v224/jquery.min
//= require foundation/js/vendor/foundation.min
//= require foundation/js/vendor/what-input

// DATATABLE
//= require jquery/plugins/datatable/datatables.min

//= require jquery/plugins/blockUI/blockUI.min
//= require jquery/plugins/validation/jquery.validate.min
//  require jquery/plugins/validation/additional-methods.min
//= require jquery/plugins/validation/localization/messages_pt_BR.min
//  require jquery/plugins/validation/localization/methods_pt_BR
//= require jquery/plugins/debounce/jquery-debounce.min // https://github.com/cowboy/jquery-throttle-debounce/
//= require jquery/plugins/mask/jquery.mask.min          // site http://igorescobar.github.io/jQuery-Mask-Plugin/ ou site: http://www.igorescobar.com/blog/2012/03/13/mascaras-com-jquery-mask-plugin/

//= require toastr/toastr.min  // site: https://github.com/CodeSeven/toastr

//= require vue/select2/vue-select.js
//= require jquery/plugins/tooltipster/js/tooltipster.bundle.min
//= require jquery/plugins/jspanel/jquery.jspanel-compiled.min
//= require select2/js/select2.full.min
//= require select2/js/i18n/pt-BR
//= require FileSaver.min
// require jquery/plugins/filestyle/jquery-filestyle.min.js // http://markusslima.github.io/jquery-filestyle/#Options

//  require_tree .
//= require_self


var webProtocol = window.location.protocol;
// encontrar a url base
var aTemp = String(webProtocol + '//' + window.location.host + window.location.pathname).split('/salve-consulta');
var baseUrl = aTemp[0] + '/salve-consulta/';
var appPing;
var mapAnimateFeatureRunning = false;


/*$(document).on('closed.zf.reveal', '#app-alert-modal', function()
  {

    }
);
*/
var default_datatable_options = {
    "paging"            : true,
    "pageLength"        : 50,
    "scrollY"           : 500,
    "scrollX"           : false,
    "lengthMenu"        : [[5, 10, 20, 50, 100, -1], [5, 10, 20, 50,100,"Todos"]],
    "scrollCollapse"    : true,
    "order": [],
    "buttons": [],
    "searching": true,
    "retrieve": true, // evitar erro se inicializar mais de uma vez seguida
    "language": {
        "search": "Pesquisar:",
        "zeroRecords": "<b>Nenhum registro encontrado!</b>",
        "info": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "infoEmpty": "Mostrando 0 até 0 de 0 registros",
        "infoFiltered": " filtrados do total de _MAX_.",
        "enfoPostFix": "",
        "enfoThousands": ".",
        "lengthMenu": "_MENU_ registros por página",
        "loadingRecords": "Carregando...",
        "processing": "Processando...",
        "zeroRecords": "Nenhum registro encontrado",
        "paginate": {
            "next": "Próxima",
            "previous": "Anterior",
            "first": "Primeira",
            "last": "Última"
        },
        "aria": {
            "sortAscending": ": Ordenar colunas de forma ascendente",
            "sortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    initComplete: function (datatable) {
        datatable.filters=[],
       // numerar as linhas de 1..n
       this.on( 'order.dt search.dt', function () {
            var columns = $(this).dataTable().api().columns(0);
            columns.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                var value = cell.innerHTML;
                var pos = value.indexOf('&nbsp;-&nbsp;');
                if( pos > -1 && pos < 10 )
                {
                    value = (i+1) + value.substring(pos);
                }
                cell.innerHTML = value;
            });
        } );
    }
};
window.alert = function(msg, title, type) {
    var win = $("#app-alert-modal");
    if (type == 'error') {
        win.css({
            'background-color': '#8f0606',
            'color': '#ffff05',
            'border': 'none'
        });
    } else if (type == 'success') {
        win.css({
            'background-color': '#C3E8C9',
            'color': '#000000',
            'border': 'none'
        });
    } else if (type == 'info') {
        win.css({
            'background-color': '#468df3',
            'color': '#ffffff',
            'border': 'none'
        });
    } else {
        win.css({
            'background-color': '#ffffff',
            'color': '#000000',
            'border': 'none'
        });
    }
    win.find("h3").html(title || '');
    win.find('.lead').html(msg.replace(/\n/g, '<br/>') || '');
    win.foundation('open');
}


/**
 * função para exibir mensagens no canto superior direito da tela
 * @site  - http://codeseven.github.io/toastr/demo.html
 * @example notify('Mensagem','Titulo da Janela','success');
 *
 * @param message - texto par exibição
 * @param title - titulo da janela
 * @param type - success, danger, error, info
 * @param delay - tempo para fechamento automático da mensagem. 0 = não fechar
 * @returns $.notify
 */
var notify = function( message, title, type, delay) {
    // default params
    title = ( title==null ? '' : title);
    title = ( title != '' ? '<strong>'+title+'</strong><hr>' : title );
    type  = type  || 'success';
    delay = ( delay == null ? 3000 : delay );
    if( ! message )
    {
        return;
    }
    toastr[type](message, title, {timeOut: delay,positionClass: "toast-top-full-width"} );
};


var trim = function(texto) {
    texto = $.trim(texto);
    texto = texto.replace(/<p>&nbsp;<\/p>$/, '');
    return texto;
};

var randomString = function(len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
};


var sliderStart = function() {
    var imgNum = Math.floor((Math.random() * 4) + 1);
    $("#div-img-top").css({
        'background-image': 'url("/salve-consulta/assets/slider/imagem' + String(imgNum) + '.jpg")'
    })

    window.setTimeout(function() {
        sliderStart();
    }, 60000)
};

if (typeof jQuery !== 'undefined') {

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    $(document).ready(function($) {

        /*
                // evitar ctrl-s e ctrl-w
                $(document).bind('keydown', function(e) {
                    e = e || window.event;//Get event
                    if (e.ctrlKey) {
                        var c = e.which || e.keyCode;//Get key code
                        switch (c) {
                            case 83://Block Ctrl+S
                            case 87://Block Ctrl+W --Not work in Chrome
                                e.preventDefault();
                                e.stopPropagation();
                                break;
                        }
                    }
                });
        */

        // escutar as teclas pressionadas e tratar a tecla Enter
        $(document).keypress(function(e) {
            if (e.which == 13) {
                if (e.target && e.target.tagName == 'INPUT') {
                    e.preventDefault(); // evitar que a tecla enter submeta o formulário
                }
            }
        });

        $(document).foundation();
        sliderStart();
        $('#primary-menu').on(
            'show.zf.dropdownmenu',
            function() {
                var dropdown = $(this).find('.is-dropdown-submenu');
                dropdown.css('display', 'none');
                dropdown.fadeIn('slow');
            });
        $('#primary-menu').on(
            'hide.zf.dropdownmenu',
            function() {
                var dropdown = $(this).find('.is-dropdown-submenu');
                dropdown.css('display', 'inherit');
                dropdown.fadeOut('slow');
            });
    });

    // evitar que a sessão caia por inatividade
    appPing = window.setInterval(function() {
        //console.log( 'Ping...');
        $.post(baseUrl + 'ajax/ping', function(res) {
            if (res != '') {
                //console.log(res);
                window.clearInterval(appPing);
                alert(res, 'Mensagem');
            }
        }).error(function(xhr, status, exception) {
            /*console.log(xhr.status)
            console.log(status)
            console.log(exception)*/
            window.clearInterval(appPing);
            if (xhr.status == 0) {
                alert('A sessão expirou ou o sistema está off-line.<br/><br/>Pressione F5 para recarregar a página.', 'Ops!!', function() {
                    window.location.replace(baseUrl);
                });
            }
        });
    }, 50000);
};

var blockUi = function(strMsg, callback) {
    $.blockUI({
        message: strMsg,
        css: {
            'font-size': '2rem',
            'border-radius': '10px',
            minHeight: '50px',
            border: '1px solid #000',
            backgroundColor: '#efefef',
            color: '#000'
        }
    });
    if( callback ){
        setTimeout(function(){
            try{ callback(); } catch( e ) {}
        },2000);
    }
};

var unblockUi = function() {
    $.unblockUI();
};

var doPost = function(strUrl, objData, fncCallback) {
    if( ! ( objData instanceof FormData ) )
    {
        $.when($.post(strUrl, objData).then(
            function (res) {
                unblockUi();
                if (res.msg && trim(res.msg) != '' && res.type)
                {
                    notify(res.msg, '', res.type);
                    res.msg = ''; // evitar reesibição da mensagem
                }
                if (res.errors && res.errors.length > 0 && res.type)
                {
                    alert(res.errors.join('<br/>'), 'Atenção', 'error');
                }
                if (fncCallback)
                {
                    fncCallback(res);
                }
            },
            function (objResponse, textStatus, jqXHR) {
                unblockUi();
                if (objResponse.status == 404)
                {
                    alert(strUrl + ' não existe!', 'Erro', 'error');
                }
                else if (objResponse.status == 401)
                {
                    alert('Sem permissão de acesso a ' + strUrl, 'Erro', 'error');
                }
                else
                {
                    alert(textStatus, 'Erro', 'error');
                }
            }));
    }
    else
    {
        axios.post( strUrl, objData ).then(function(res){
            unblockUi();
            if ( fncCallback )
            {
                if( res.data ) {
                    fncCallback(res.data);
                }
                else {
                    fncCallback( res );
                }
            }
        });
    }
};

var setFocus = function(id) {
    window.setTimeout(function() {
        $("#" + id.replace(/^#/g, '')).focus();
    }, 500);
};

var openModal = function(strTitle, urlAjax, data, callback, intWidth, intHeight) {
    intWidth = intWidth || 800;
    intHeight = intWidth || 600;
    strTitle = strTitle || '';
    data = data || {}
    id = 'win_' + (data.fieldName ? data.fieldName : randomString(5));

    $.jsPanel({
        id: id,
        //position:    {my: "center-top", at: "center-top", offsetY: 15},
        //paneltype:   'modal',
        theme: "silver",
        contentSize: {
            width: intWidth,
            height: intHeight
        },
        headerTitle: strTitle,
        contentAjax: {
            url: baseUrl + urlAjax,
            data: data,
            autoload: true,
            contentSize: 'auto auto',
        },
        content: "",
        callback: function() {
            this.content.css("padding", "15px");
            if (callback) {
                callback();
            }
        },
    });
};

var capitalize = function(str, force) {
    force = force == false ? false : true
    str = force ? str.toLowerCase() : str;
    return str.replace(/(\b)([a-zA-Z])/g,
        function(firstLetter) {
            return firstLetter.toUpperCase();
        });
};

var mapAnimateFeature = function(map, vectorLayer, feature, pulsateCount, center) {
    if (mapAnimateFeatureRunning) {
        return;
    }
    mapAnimateFeatureRunning = true;
    pulsateCount = pulsateCount || 5;
    center = (center == true ? true : false);
    var style, image, r, currR, maxR, sign;
    var geomTemp = feature.getGeometry();
    var mapExtent = map.getView().calculateExtent(map.getSize())
    var pontoTemp = new ol.Feature({
        geometry: geomTemp,
        name: 'pulse-' + new Date()
    });
    vectorLayer.getSource().addFeature(pontoTemp);
    pontoTemp.setStyle(new ol.style.Style({
        image: new ol.style.Circle({
            radius: 5,
            fill: new ol.style.Fill({
                color: 'rgba(0,0,0,0)'
            }),
            stroke: new ol.style.Stroke({
                color: 'rgba(255,0,0,0.6)',
                width: 2
            })
        })
    }));
    if (!center && !ol.extent.containsCoordinate(mapExtent, feature.getGeometry().getCoordinates())) {
        mapAnimateFeatureRunning = false;
        return;
    }
    //map.getView().setCenter(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'))
    if (center) {
        map.getView().setCenter(pontoTemp.getGeometry().getCoordinates());
    }
    image = pontoTemp.getStyle().getImage();
    style = pontoTemp.getStyle();
    image = style.getImage();
    r = image.getRadius();
    currR = r;
    maxR = 4 * r;
    sign = 1;
    var pulsate = function(event) {
        var vectorContext = event.vectorContext;
        if (currR > maxR) {
            sign = -1;
            pulsateCount--;
        } else if (currR < r) {
            sign = 1;
            if (!pulsateCount) {
                map.un('postcompose', pulsate);
                vectorLayer.getSource().removeFeature(pontoTemp);
                mapAnimateFeatureRunning = false;
                return;
            }
        }
        currR += sign * 1.5;
        vectorContext.drawFeature(pontoTemp, new ol.style.Style({
            image: new ol.style.Circle({
                radius: currR,
                fill: image.getFill(),
                stroke: image.getStroke()
            })
        }));
        map.render();
    };
    map.on('postcompose', pulsate);
};

var getCookie = function(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

Number.prototype.formatMoney = function(c, d, t){
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t)
        + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

var isDate = function(y, m, d){
    if(typeof y == "string" && m instanceof RegExp && d){
        if(!m.test(y)) return 1;
        y = RegExp["$" + d.y], m = RegExp["$" + d.m], d = RegExp["$" + d.d];
    }
    d = Math.abs(d) || 0, m = Math.abs(m) || 0, y = Math.abs(y) || 0;
    return !(arguments.length != 3 ? 1 : d < 1 || d > 31 ? 2 : m < 1 || m > 12 ? 3 : /4|6|9|11/.test(m) && d == 31 ? 4
        : m == 2 && (d > ((y = !(y % 4) && (y % 1e2) || !(y % 4e2)) ? 29 : 28)) ? 5 + !!y : 0);
};

var isValidDate = function( dateTime){
    var strDate = dateTime.split(' ')[0];
    var strTime = dateTime.split(' ')[1];
    var isOk = true;
    if( strDate )
    {
        // regular expression to match required date format
        re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        if (strDate != '' && !strDate.match(re))
        {
            alert("Data inválida: " + strDate);
            return false;
        }
        var parts = strDate.split('/');
        if( ! isDate(parts[2],parts[1],parts[0]) )
        {
            alert("Data inválida: " + strDate);
            return false;
        }

    }
    if( strTime )
    {
        try
        {
            // regular expression to match required time format
            re = /^\d{1,2}:\d{2}([ap]m)?$/;
            if (strTime != '' && !strTime.match(re))
            {
                alert("Hora inválida: " + strTime);
                return false;
            }
            var h = strTime.split(':');
            isOk = true;
            if (parseInt('0' + h[0]) > 23)
            {
                isOk = false;
            }
            if (parseInt('0' + h[1]) > 59)
            {
                isOk = false;
            }
            if (!isOk)
            {
                alert("Hora inválida: " + strTime);
                return false;
            }
        } catch( e )
        {
            alert('Data/hora inválida!');
            return false;
        }
    }
    return true;
};

$.fn.dataTable.ext.search.push( function (settings, searchData, index, rowData, counter) {
    var linhaValida = true;
    var data        = '';
    var q = '';
    if( page && page.filtroDeNivel ) {
        q = page.filtroDeNivel.toLowerCase().trim().replace(/[^A-Za-z\s]/g, '');
    }
    if( settings.sInstance == 'tableFichasConsultaAmpla') {
        data = page.listaFichasConsultaAmpla[index];
        // filtrar o gride da pesquisa ampla
        linhaValida = String(rowData[0].toLowerCase() + ' ' + rowData[1].toLowerCase()).indexOf(page.filtroPesquisaAmpla.trim().toLowerCase()) > -1;
    }else if( settings.sInstance == 'tableMinhasFichas') {
        data = page.listaFichasConsultaDireta[index];
        // filtrar o gride da pesquisa direta
        linhaValida = String(rowData[0].toLowerCase() + ' ' + rowData[1].toLowerCase()).indexOf(page.filtroPesquisaDireta.trim().toLowerCase()) > -1;
    }else if( settings.sInstance == 'tableFichasRevisao') {
        data = page.listaFichasConsultaDireta[index];
        // filtrar o gride da pesquisa revisao
        linhaValida = String(rowData[0].toLowerCase() + ' ' + rowData[1].toLowerCase()).indexOf(page.filtroPesquisaRevisao.trim().toLowerCase()) > -1;
    }

    // verificar se tem filtro por grupo e subgrupo
    if( page.filtroSqGrupo ){
        linhaValida = ( parseInt(data.sqGrupo) == parseInt( page.filtroSqGrupo ) )
        if( linhaValida && page.filtroSqSubgrupo ){
            linhaValida = ( parseInt(data.sqSubgrupo) == parseInt( page.filtroSqSubgrupo ) )
        }
    }
    // filtrar pela consulta selecionada
    if( settings.sInstance != 'tableFichasRevisao') {
        if (page && page.filtroConsulta && page.filtroConsulta.sqCicloConsulta) {
            linhaValida = (parseInt(data.sqCicloConsulta) == parseInt(page.filtroConsulta.sqCicloConsulta))
        }
    } else  if( settings.sInstance == 'tableFichasRevisao') {
        if (page && page.filtroConsulta && page.filtroConsulta.sqCicloConsulta) {
            console.log( data.sqCicloConsulta,page.filtroConsulta.sqCicloConsulta)
            linhaValida = (parseInt(data.sqCicloConsulta) == parseInt(page.filtroConsulta.sqCicloConsulta))
        }
    }

    // verificar se tem filtro por nivel taxonomico selecionado
    if ( linhaValida && q && data ) {
        try {
            var noNivel = page.filtroNoNivel;
            if (/noEspecie|noSubEspecie/.test(noNivel)) {
                noNivel = 'especie';
            } else {
                noNivel = noNivel.substring(2).toLowerCase();
            }
            //var data = page.listaFichasConsultaAmpla[index];
            var jsonData = JSON.parse(data.arvoreTaxonomica.toLowerCase());
            var noTaxon = jsonData[noNivel].no_taxon;
            linhaValida = (noTaxon == q || noTaxon.indexOf(q) > -1);
        } catch (e) {
            linhaValida = true
        }
    }
    return linhaValida;
});


var ajaxJson = function( url, data, msg, callback) {
    if( msg )
    {
        app.blockUI(msg);
    }
    $.ajax({
            url: url
            , type: 'POST'
            , cache: false
            , timeout: 0
            //, contentType : "application/json; charset=utf-8"
            , dataType: 'json'
            , data: { data:JSON.stringify(data) }
            , error: function (xhr, message, error) {
                alert(message + 'requisição ajax\nCódigo: ' + xhr.status + ': ' + error + '\nUrl: ' + url );
            },
        }
    ).done(function (res) {
        if( res && res.msg )
        {
            res.type = (res.type ? res.type : 'success');
            if( res.type == 'error' ) {
                app.alertError( res.msg );
            }
            else {
                app.alertInfo(res.msg);
            }
            res.msg='';
        }
        callback && callback( res );
    }).always(function(res){
        if( msg )
        {
            app.unblockUI();
        }
    } )
}


// funcoes app para compatibilizar com SALVE
var ajaxPost = function( url, data, msg, callback,contentType) {
    contentType = contentType || 'application/x-www-form-urlencoded; charset=UTF-8'; // 'application/json; charset=utf-8'
    if( msg ) { app.blockUI( msg); }
    $.ajax({
            url: url
            , type: 'POST'
            , cache: false
            , timeout: 0
            , contentType : contentType
            , data: data
            , error: function (xhr, message, error) {
                alert(message + 'requisição ajax\nCódigo: ' + xhr.status + ': ' + error + '\nUrl: ' + url );
            },
        }
    ).done(function (res) {
        if( res && res.msg ){
            res.type = (res.type ? res.type : 'success');
            if( res.type == 'error' ) {
                app.alertError( res.msg );
            }
            else {
                app.alertInfo(res.msg);
            }
            res.msg='';
        }
        callback && callback( res );
    }).always(function(res){
        if( msg ){
            app.unblockUI();
        }
    } )
};

var app = ( function() {
    var _params2data = function (params, data, container) {
        var fld;
        params = params || {};
        params = params.data ? params.data : params;
        if (params) {
            if (typeof (params) === 'object') {
                for (key in params) {
                    if (key === 'params') {
                        _params2data(params[key], data, container);
                    } else {
                        if (!/tooltipster/.test(key)) {
                            data[key] = (params[key] || params[key] === false ? params[key] : $("#" + params[key]).val());
                        }
                    }
                }
            } else {
                var aTemp;
                params.split(',').map(function (v) {
                    v = v.split(':');
                    if (!v[1]) {
                        aTemp = v[0].split('|');
                        v[0] = aTemp[0];
                        if (!container) {
                            if ($("#" + v[0]).size() > 0) {
                                v[1] = $("#" + v[0]).val(); // pegar pelo id
                            } else {
                                v[1] = $("*[name='" + v[0] + "']").val(); // pegar pelo name
                            }
                        } else {
                            container = "#" + container.replace(/^#/, '');
                            fld = $(container + ' [name=' + v[0] + ']').get(0) || $(container + ' [id=' + v[0] + ']'); // pegar pelo name dentro do conatainer
                            if (fld) {
                                v[1] = $(fld).val();
                            }
                        }
                        if (aTemp[1]) {
                            v[0] = aTemp[1];
                        }
                    } else {
                        try {
                            v[1] = eval(v[1]);
                        } catch (e) {
                            v[1] = v[1];
                        }
                    }
                    if (v[1]) {
                        var aTemp = v[0].split('|');
                        v[0] = aTemp[0];
                        try {
                            data[v[0]] = v[1]
                        } catch (e) {
                        }
                        ;
                    }
                });
            }
        }
        // ajustar o parametro
        for (key in data) {
            if (!isNaN(key)) {
                if (data[key].hasOwnProperty('name') && data[key].hasOwnProperty('value')) {
                    data[data[key].name] = data[key].value;
                }
            }
        }
        return data;
    }

    var _blockUI = function (msg,callback) {
        if (msg) {
            blockUi( msg, callback );
        }
    }

    var _unblockUI = function () {
        return unblockUi();
    };

    var _alertInfo = function (msg) {
        alert(msg, 'Mensagem', 'info')
    }

    var _alertError = function (msg) {
        alert(msg, 'Atenção', 'error')
    }


    return {
        params2data: _params2data,
        blockUI: _blockUI,
        unblockUI: _unblockUI,
        alertInfo: _alertInfo,
        alertError: _alertError
    }

})();
