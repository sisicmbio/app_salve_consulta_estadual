var page = new Vue({
    el: '#div-page-login',
    data: {
        timeout:null,
        title:'Autenticar',
        view:'login',
        errorMsg: '',
        form: null,
        fields: {
            email: '',
            senha: '',
        }
    },
    created: function() {},
    mounted: function() {
        this.form = $("#frmLogin");
        this.form.validate();
        $("#div-page-login").show(function(){
            $("#fldEmail").focus();
        });

        // manter o e-mail digitado em caso de erro no form
        this.$data.fields.email = $("#fldEmail").attr('value');

        // se não fizer nada em 10 segundos, redirecionar para a pagina inicial
        this.timeout = window.setTimeout(function(){
            //window.location.replace(baseUrl);
        },10000)
    },
    methods: {
        clearTimeout:function(e){

            if( this.timeout )
            {
                window.clearTimeout(this.timeout);
                this.timeout=null;
            }
            if( e && e.key && e.key=='Enter') {
                this.login(e);
            }
        },
        showView:function( view )
        {
            this.view=view;
            if( this.view == 'login' )
            {
                this.title='Autenticar'
            }
            else if( this.view=='forgot')
            {
                this.title='Esqueci Minha Senha'
            }
            else if( this.view=='emailEnviado')
            {
                this.title='Email Enviado'
            }
        },
        forgotPassword:function(e)
        {
            this.clearTimeout();
            e.preventDefault();
            if( ! this.fields.email )
            {
                this.errorMsg='Informe o email!'
                return;
            }
            blockUi('Enviando Email...');
            doPost( baseUrl+'forgotPassword',{email:this.fields.email},function(res){

                if( res.msg )
                {
                    page.errorMsg = res.msg;
                }
                if( res.status == 0 )
                {
                    page.showView('emailEnviado')
                }
            })
        },
        login: function(e) {
            this.clearTimeout()
            this.errorMsg = "";
            e.preventDefault();
            if (!this.form.valid()) {
                return;
            }
            // tranformar a senha para minúsculas sempre
            this.form.find('input#fldSenha').val(this.form.find('input#fldSenha').val().toLowerCase() );
            var data = this.form.serializeArray();
            blockUi('Validando...')
            window.setTimeout(function(form){
                form.submit();
            },500,this.form)
        },
        sair: function(e)
        {
            this.clearTimeout()
            e.preventDefault();
            window.location.replace(baseUrl);
        },
        voltar: function( e )
        {
            this.clearTimeout()
            e.preventDefault();
            this.showView('login');
        },
        forgot: function( e )
        {
            this.clearTimeout();
            this.errorMsg="";
            e.preventDefault();
            this.showView('forgot');
        }

    }
});
