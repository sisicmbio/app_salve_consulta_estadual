/**
 *
 *
 */
Vue.http.options.emulateJSON = true;
var page = new Vue({
    el: '#div-wrap',
    data: {
        dataTableConsultaAmpla:null,
        dataTableConsultaDireta:null,
        dataTableConsultaRevisao:null,
        qtdFichasAmpla: 0,

        filtroConsulta: {},

        filtroNoCientifico: '',
        filtroNoNivel: '',
        filtroDeNivel: '',

        filtroPesquisaAmpla:'',
        filtroPesquisaDireta:'',
        filtroPesquisaRevisao:'',

        filtroSqGrupo:0,
        filtroSqSubgrupo:0,
        filtroSqSubgrupoTemp:0,

        pesquisaVisible: false,
        pesquisaAvancadaVisible: false,
        fichasConsultaDiretaRevisaoVisible: false,
        fichasConsultaDiretaVisible: false,
        fichasConsultaRevisaoVisible: false,
        fichasConsultaAmplaVisible: false,
        listaConsultas:[],
        listaFichasConsultaDireta: [],
        listaFichasConsultaAmpla: [],
        listaGruposAvaliados: [{sqGrupo:0,dsGrupo:'-- todos --'}],
        listaSubgruposAvaliados: [{sqSubgrupo:0,dsSubgrupo:'-- todos --'}]
    },
    created: function() {},
    mounted: function() {
        this.getFichasConsultaAmpla();
        this.getFichasConsultaDireta();
        this.getGruposAvaliados();
        this.listaConsultas.push( {id:1,nome:''}
                ,{sqCicloConsulta:2,sgConsulta:''}
                ,{sqCicloConsulta:3,sgConsulta:''}
                ,{sqCicloConsulta:4,sgConsulta:''}
                ,{sqCicloConsulta:5,sgConsulta:''}
                ,{sqCicloConsulta:6,sgConsulta:''}
                );
    },
    methods: {
        filtrarEspeciesConsulta:function(consulta) {
            //if( consulta.cdSistema =='CONSULTA_AMPLA') {
                if (page.filtroConsulta == consulta) {
                    page.filtroConsulta = {}
                } else {
                    page.filtroConsulta = consulta;
                }
                page.aplicarFiltro();
                document.location.href = '#fsConsultas';
            //}
        },
        getFichasConsultaAmpla: function() {
            doPost(baseUrl + 'ficha/consultaAmpla', null, function(res) {
                page.listaFichasConsultaAmpla = [];
                page.listaConsultas = res.consultas;
                if (res.data.data.length > 0) {
                    blockUi('Aguarde...');
                    page.listaFichasConsultaAmpla = res.data.data;
                    // aplicar datatable na tabela
                    // exemplo de componente para testar: https://willvincent.com/2016/04/08/making-vuejs-and-datatables-play-nice/
                    page.fichasConsultaAmplaVisible=true;

                    window.setTimeout(function(){
                        var nonOrderableCols = [];
                        $("#tableFichasConsultaAmpla").find('thead th').each(function(key,value){
                           nonOrderableCols.push( key );
                        });

                        page.dataTableConsultaAmpla = $("#tableFichasConsultaAmpla").DataTable( $.extend({},default_datatable_options,{
                            "columnDefs" : [
                                {"orderable": false,"targets": nonOrderableCols },
                            ],
                        }));
                        // esconder campo de pesquisa padrão do datatable
                        $("#tableFichasConsultaAmpla_filter").hide();
                        unblockUi();
                        var booleanAplicarFiltro=false;
                        try {
                            var filtrosSalvos = {}
                            if( $("#li-full-name").text() != "" ) {
                                filtrosSalvos = JSON.parse(localStorage.getItem('salve_consulta_filtros'));
                            }
                            if( filtrosSalvos.filtroPesquisaAmpla ) {
                                page.filtroPesquisaAmpla = filtrosSalvos.filtroPesquisaAmpla
                            }
                            if( filtrosSalvos.filtroPesquisaDireta ) {
                                page.filtroPesquisaDireta = filtrosSalvos.filtroPesquisaDireta
                            }
                            if( filtrosSalvos.filtroPesquisaRevisao ) {
                                page.filtroPesquisaRevisao = filtrosSalvos.filtroPesquisaRevisao
                            }
                            if( filtrosSalvos.filtroNoNivel ) {
                                page.filtroNoNivel = filtrosSalvos.filtroNoNivel
                            }

                            if( filtrosSalvos.filtroSqSubgrupo ) {
                                // da tempo dos subgrupos serem crregados via ajax
                                page.filtroSqSubgrupoTemp = filtrosSalvos.filtroSqSubgrupo;
                            }

                            if( filtrosSalvos.filtroSqGrupo ) {
                                page.filtroSqGrupo = filtrosSalvos.filtroSqGrupo;
                                page.pesquisaAvancadaVisible=true;
                                page.$nextTick(function(){
                                    page.updateSubgrupos();
                                })
                            }

                            if( filtrosSalvos.filtroDeNivel ){
                                page.filtroDeNivel = filtrosSalvos.filtroDeNivel
                                page.pesquisaAvancadaVisible=true;
                                booleanAplicarFiltro = true;
                            }

                            if( filtrosSalvos.filtroConsulta ){
                                page.filtroConsulta = filtrosSalvos.filtroConsulta;
                                booleanAplicarFiltro = true;
                            }

                            if( booleanAplicarFiltro ) {
                                page.aplicarFiltro();
                            }

                            if( filtrosSalvos.scrollTop ) {
                                $(document).scrollTop(filtrosSalvos.scrollTop);
                            }
                        } catch( e ) {}
                    },1000);

                } else {
                    $("#div_loading_fichas span").html('<br><h5 style="color:red;">Nenhuma ficha em consulta ampla!</h5>')
                }
            });
        },
        getFichasConsultaDireta: function() {
            doPost(baseUrl + 'ficha/consultaDireta', null, function(res) {
                page.listaFichasConsultaDireta = [];
                page.fichasConsultaDiretaRevisaoVisible=true
                if (res.data.itens.length > 0) {
                    page.listaFichasConsultaDireta = res.data.itens;
                    //page.pesquisaVisible = true;
                    // aplicar datatable na tabela de fichas em consulta direta
                    // exemplo de componente para testar: https://willvincent.com/2016/04/08/making-vuejs-and-datatables-play-nice/

                    window.setTimeout(function(){


                        var nonOrderableCols = [];
                        $("#tableMinhasFichas").find('thead th').each(function(key,value){
                            nonOrderableCols.push( key );
                        });

                        page.dataTableConsultaDireta = $("#tableMinhasFichas").DataTable( $.extend({},default_datatable_options,{
                            "columnDefs" : [
                                {"orderable": false,"targets": nonOrderableCols },
                            ],
                        }));
                        // esconder campo de pesquisa padrão do datatable
                        $("#tableMinhasFichas_filter").hide();

                        // aplicar datatable na tabela das fichas em revisão pos-oficina
                        nonOrderableCols=[];
                        $("#tableFichasRevisao").find('thead th').each(function(key,value){
                            nonOrderableCols.push( key );
                        });

                        page.dataTableConsultaRevisao = $("#tableFichasRevisao").DataTable($.extend({}, default_datatable_options, {
                                "columnDefs": [
                                    {"orderable": false, "targets": nonOrderableCols},
                                ],
                            }));
                            // esconder campo de pesquisa padrão do datatable
                            $("#tableFichasRevisao_filter").hide();

                        // fechar o grupo minhas fichas
                        // page.fichasConsultaDiretaRevisaoVisible=false;
                    },1000);
                } else {
                    window.setTimeout(function(){
                        page.fichasConsultaDiretaRevisaoVisible=false;
                        page.listaFichasConsultaDireta=[];
                    },1000);

                }
            });
        },
        getGruposAvaliados: function() {
            doPost(baseUrl + 'ficha/gruposAvaliados', null, function(res) {
                if (res.data.itens.length > 0) {
                    page.listaGruposAvaliados=res.data.itens;
                }
            });
        },
        updateSubgrupos : function() {
            this.listaSubgruposAvaliados = [{sqSubgrupo:0,dsSubgrupo:'-- todos --'}];
            this.filtroSqSubgrupo=0;
            if( this.filtroSqGrupo ) {
                doPost( baseUrl + 'ficha/subgruposAvaliados', {sqGrupo: this.filtroSqGrupo}, function (res) {
                    if (res.data.itens.length > 0) {
                        page.listaSubgruposAvaliados = res.data.itens;
                        if( page.filtroSqSubgrupoTemp > 0 ) {
                            page.filtroSqSubgrupo = page.filtroSqSubgrupoTemp;
                            page.filtroSqSubgrupoTemp = 0;
                        }
                    }
                });
            }
            this.aplicarFiltro();
        },


        colaborarFicha: function(item) {
            this.salvarFiltrosAtivos();
            $("#sqConsultaFicha").val(item.sqConsultaFicha);
            $("#codigoSistema").val(item.codigoSistema);
            blockUi('Carregando a ficha...');
            window.setTimeout(function(){
                $("#frmPost").submit();
            },500)
        },
        toggleFichasConsultaAmpla: function() {
            this.fichasConsultaAmplaVisible = !this.fichasConsultaAmplaVisible;
            if ( this.fichasConsultaAmplaVisible ) {
                /*window.setTimeout(function() {
                    document.location.href = "#rowFiltro";
                }, 500);
                 */
            }
        },
        cancelarFiltro: function(){
          // quando limpar o campo nome cientifico da consulta avançada
          if( ! this.filtroDeNivel )
          {
              this.aplicarFiltro();
          }
        },
        aplicarFiltro : function( e ){
            if( page.dataTableConsultaAmpla ) {
                page.dataTableConsultaAmpla.search(this.filtroPesquisaAmpla).draw();
            }

            if( ! page.filtroConsulta.cdSistema || page.filtroConsulta.cdSistema =='REVISAO_POS_OFICINA') {
                if (page.dataTableConsultaRevisao) {
                    page.dataTableConsultaRevisao.search(this.filtroPesquisaRevisao).draw();
                }
            }
            /* Aplicar os filtros avancados somente na consulta ampla
            if( page.dataTableConsultaDireta ) {
                page.dataTableConsultaDireta.search(this.filtroPesquisaDireta).draw();
            }
            if( page.dataTableConsultaRevisao ) {
                page.dataTableConsultaRevisao.search(this.filtroPesquisaRevisao).draw();
            }*/
        },
        salvarFiltrosAtivos:function() {
            try {
                if( $("#li-full-name").text() != "" ) {
                    var filtrosAtivos = {
                        scrollTop: $(document).scrollTop(),
                        filtroPesquisaAmpla: this.filtroPesquisaAmpla,
                        filtroNoNivel: this.filtroNoNivel,
                        filtroDeNivel: this.filtroDeNivel,
                        filtroPesquisaDireta: this.filtroPesquisaDireta,
                        filtroPesquisaRevisao: this.filtroPesquisaRevisao,
                        filtroSqGrupo: this.filtroSqGrupo,
                        filtroSqSubgrupo: this.filtroSqSubgrupo,
                        filtroConsulta : this.filtroConsulta,
                    }
                    localStorage.setItem('salve_consulta_filtros', JSON.stringify(filtrosAtivos));
                }
            } catch( e ) {}
        }
    },
    computed: {
        filtrarLista: function(e) {
            var lista = this.listaFichasConsultaAmpla;
            page.qtdFichasAmpla = lista.length;
            this.pesquisaVisible = true;
            this.fichasConsultaAmplaVisible = true;
            return lista;
        },

        filtrarListaDireta: function(e) {
            var lista = this.listaFichasConsultaDireta.filter(function(item){
                return item.codigoSistema == 'CONSULTA_DIRETA';
            });
            if( lista.length > 0 )
            {
                this.fichasConsultaDiretaRevisaoVisible = true;
                this.fichasConsultaDiretaVisible = true;
            }
            return lista;
        },
        filtrarListaRevisao: function(e) {
            var lista = this.listaFichasConsultaDireta.filter(function(item){
                return item.codigoSistema == 'REVISAO_POS_OFICINA';
            });
            if( lista.length > 0 )
            {
                this.fichasConsultaRevisaoVisible = true;
            }
            return lista;
        }
    },
    watch: {
        filtroNoCientifico: function(val, oldValue) {
            if (!this.fichasConsultaAmplaVisible) {
                this.fichasConsultaAmplaVisible = (val != '');
            }
            if (!this.fichasConsultaDiretaVisible) {
                this.fichasConsultaDiretaVisible = (val != '');
            }
        },
        filtroPesquisaAmpla:function( val, oldValue )
        {
            page.dataTableConsultaAmpla.search( val ).draw();
        },

        filtroPesquisaDireta:function( val, oldValue )
        {
            page.dataTableConsultaDireta.search( val ).draw();
        },

        filtroPesquisaRevisao:function( val, oldValue )
        {
            page.dataTableConsultaRevisao.search( val ).draw();
        },
    },
    components: {}
});
