Vue.http.options.emulateJSON = true;
var page = new Vue({
    el: '#div-page-faleconosco',
    data: {
        title:'Fale Conosco',
        errorMsg: '',
        form: null,
        fields: {
            email: '',
            mensagem: '',
        }
    },
    created: function() {},
    mounted: function() {
        this.form = $("#frmFaleConosco");
        this.form.validate();
        $("#div-page-faleconosco").show(function(){
            $("#fldMensagem").focus();
        });
    },
    methods: {
        sair: function(e)
        {
           if( typeof e != 'undefined' ){ e.preventDefault();}
            window.location.replace(baseUrl);
        },
        enviar:function(e)
        {
            e.preventDefault();
            if( ! page.form.valid() )
            {
                return;
            }

            // enviar mensagem
            blockUi('Enviando mensagem...');
            var data = $(this.form).serializeArray();
            this.$http.post(baseUrl + 'faleConosco/save', data ).then( function( response ) {
                unblockUi();
                var res = response.body
                if( res.msg )
                {
                    if( res.status == 0 )
                    {
                        $("#divMsg").removeClass('alert');
                        page.fields.mensagem = '';
                        window.setTimeout( function() {
                            page.sair(e);
                        },3000);
                    }
                    else
                    {
                        $("#divMsg").addClass('alert');
                    }
                    page.errorMsg = res.msg;
                }
                if( res.errors.length > 0 )
                {
                    $("#divMsg").addClass('alert');
                    page.errorMsg = res.errors.join('<br/>');
                }
        }, function(response) {
            unblockUi();
            if( ! response.ok )
            {
                this.errorMsg = 'Servidor está fora do ar. Tente novamente mais tarde!';
            }
            else if( response.status == 401)
            {
                this.errorMsg = 'Página não autorizada!'
            }
            else if( response.status == 404)
            {
                this.errorMsg = 'Página não encontrada!'
            }
            else
            {
                this.errorMsg = 'Erro nº ' + response.status + ' - '+response.statusText
            }
          });
        } //------------------------------
    }
});