Vue.http.options.emulateJSON = true;
Vue.component('v-select', VueSelect.VueSelect);
var page = new Vue({
    el: '#div-page-cadastrar',
    data: {
        view         : 'formulario',
        errorMsg     : '',
        form         : null,
        noInstituicao: '',
        institutionsList:[],
        fields       : {
            email                : '',
            senha                : '',
            senha2               : '',
            nome                 : '',
            sqInstituicao        : 0,
            noInstituicao        : '',
            sgInstituicao        : '',
            noInstituicaoOutra   : '',
            sgInstituicaoOutra   : ''
        }
    },
    created: function() {},
    mounted: function() {
        this.form = $("#frmRegistroInicial");
        //this.form.validate();
        $("#div-page-cadastrar").show(function(){
            $("#fldInstituicaoOutra").focus();
        });
    },
    methods: {
        showView:function(view)
        {
            this.view = view;
        },
        save: function(e) {
            this.errorMsg = "";
            e.preventDefault();
            if (!this.form.valid()) {
                return;
            }
            // se não tiver o id da instituição, enviar o nome
            if (!this.fields.sqInstituicao) {
                if (!this.fields.noInstituicaoOutra) {
                    this.errorMsg = 'Necessário selecionar ou informar outra instituição!';
                    return;
                }
            }
            if (!confirm('Confirma dados cadastrais?')) {
                return;
            }
            blockUi('Efetuando o registro...');
            this.$http.post(baseUrl + this.form.attr('action'), this.fields).then(function (response) {
                unblockUi();
                var res = response.body
                if (res.msg) {
                    page.errorMsg = res.msg;
                }
                if (res.errors.length > 0) {
                    page.errorMsg = res.errors.join('<br/>');
                } else if (res.status == 0) {
                    this.showView('resultado')
                } else if (res.status == 1) {
                    this.showView('falhaEmail');
                }
            }, function (response) {
                unblockUi();
                if (!response.ok) {
                    this.errorMsg = 'Servidor está fora do ar. Tente novamente mais tarde!';
                } else if (response.status == 401) {
                    this.errorMsg = 'Página não autorizada!'
                } else if (response.status == 404) {
                    this.errorMsg = 'Página não encontrada!'
                } else {
                    this.errorMsg = 'Erro nº ' + response.status + ' - ' + response.statusText
                }
            })

        },
        fldEmailExit: function(e) {
            if ( e.target.value )
            {
                doPost( baseUrl + 'ajax/checkEmail',{email:page.fields.email},
                    function(res)
                    {
                        page.errorMsg='';
                        if( res.msg )
                        {
                            // page.fields.email='';
                            page.errorMsg = res.msg;
                            $("#fldInstituicaoOutra").focus();
                        }

                    }
                );
            }
        },
        voltar:function(e)
        {
            window.location.replace(baseUrl);
        },
        //------------------------------------------------------
        // Autocomplete
        //----------------------------------------------------
        selectInstituicaoChange : function(val) {
            if( val )
            {
                this.fields.sqInstituicao = val.value;
                this.fields.sgInstituicao = val.sigla;
                this.fields.noInstituicaoOutra = '';
                this.fields.sgInstituicaoOutra = '';
            }
            else
            {
                this.fields.sqInstituicao = '';
                this.fields.sgInstituicao = '';
            }
            //console.log( 'mudou');
            //console.dir(JSON.stringify(val))
        },
        getInstitutions : $.debounce(1500,function(q,loading)
        {
            q = trim( q )
            if( q && q.length > 2 )
            {
                loading(true);
                doPost(baseUrl + 'ajax/getInstitutions', {q: q}, function (res)
                {
                    page.noInstituicao = q;
                    loading(false);
                    page.institutionsList = res;
                });
            }
            else
            {
                page.institutionsList = []
            }
        }),
    },
    components: {

    }
});
