package br.gov.icmbio

import grails.util.Environment
import org.jsoup.Jsoup

import javax.swing.text.MaskFormatter
import java.security.*
import java.text.DecimalFormat
import java.text.Normalizer
import java.util.regex.Matcher
import java.util.regex.Pattern

class Util {

	static int dateDiff( String startDate, Date endDate, String what )
    {
        endDate = endDate ?: new Date();
        Date d2 = Date.parse("dd/MM/yyyy HH:mm:ss", endDate.getDateTimeString() );
        Date d1 = Date.parse("dd/MM/yyyy HH:mm:ss",startDate);
        Calendar dataInicial = Calendar.getInstance();
        Calendar dataFinal = Calendar.getInstance();
        dataInicial.setTime( d1 );
        dataFinal.setTime( d2 );
        long millis = dataFinal.getTimeInMillis() - dataInicial.getTimeInMillis();
        long seconds = millis / 1000;
        long minutes = ( (seconds / 60) < 0 ) ? 0 : ( seconds / 60 );
        long hours  = ( (minutes / 60) < 0 ) ? 0 : ( minutes / 60 );
        long days  = ( (hours / 24) < 0 ) ? 0 : ( hours / 24 );
      /*  println '############################'
        println d2;
        println d1;
        println "dias: " + days.toString();
        println "Horas: " + hours.toString();
        println "Minutos: " + minutes.toString();
        println "Segundos: " + seconds.toString();
        println "Milisegundos:" + millis.toString();
        println '############################'
        */
        switch( what.toUpperCase() )
        {
            case 'D':
                return days;
            case 'H':
                return hours;
            case 'M':
               return minutes;
            case 'S':
                return seconds;
        }
        return 0;
    }


	/**
     * Método para remover os acentos para caractere sem acento correspondente.
     * @param  String - palavra com acentos
     * @return String
     */
    public static String removeAccents(String str)
    {
        return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }

    static String getHash( String txt='', String hashType='') {
    try {
                MessageDigest md = MessageDigest.getInstance(hashType);
                byte[] array = md.digest(txt.getBytes());
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < array.length; ++i) {
                    sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
             }
                return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            //error action
        }
        return null;
	}

    public static String md5(String txt = '' ) {
        return getHash(txt, 'MD5');
    }

    public static String sha1(String txt) {
        return getHash(txt, "SHA1");
    }

    public static void d( Object value ){

        value = ( value == null ? 'null' : value );
        //def msg = Environment.current.toString() +' ['+this.getClass().toString().replace('class br.gov.icmbio.','')+'] ' + value.toString();

        String msg = value.toString() +' in ' + Environment.current.toString();

        if (Environment.current == Environment.DEVELOPMENT) {
            println msg;
        }

        //log.info( msg );

    }

    /**
     * Retorna a data atual sem as horas
     * @return
     */
    static Date hoje() {
        return Date.parse("dd/MM/yyyy", new Date().format('dd/MM/yyyy'))
    }

    /**
     * Método para formatar strings
     * @url http://docs.oracle.com/javase/6/docs/api/javax/swing/text/MaskFormatter.html
     * @param  String value - string a ser formatada
     * @param  String pattern - máscara de formatação
     * @return String
    */
    public static String formatStr(String value, String pattern)
    {
        MaskFormatter mask;
        try
        {
            mask = new MaskFormatter(pattern);
            mask.setValueContainsLiteralCharacters(false);
            return mask.valueToString(value);
        }
        catch (Exception e)
        {
            return value;
        }
    }

    public static String formatCpf(String cpf)
    {
		return formatStr(cpf,'###.###.###-##');
    }

    public static String formatNumber(def valor, def mascara)
    {
       def f  = new DecimalFormat(mascara);
       return f.format(valor);
    }

    public static String nome2citacao( def nome )
    {
        List partes = nome.toString().split(' ')
        String resultado = partes.pop() + ' ' // ultimo  nome
        partes.each {
            if(  ! (it.toLowerCase() ==~ /^d.s?$/ || it.trim() == '' ) )
            {
                resultado += it.substring(0,1)+'. '
            }
        }
        return resultado
    }

   public static String getRandomColor() {
         String[] letters = ["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"];
         String color = "#";
         for (int i = 0; i < 6; i++ ) {
            color += letters[(int) Math.round(Math.random() * 15)];
         }
         return color;
    }

    static String getFileExtension(String fileName) {
        if (!fileName) {
            return ''
        }
        Integer pos = fileName.lastIndexOf('.')
        if (pos > 0) {
            return fileName.substring(pos + 1)
        }
        return ''
    }

    static String trimEditor(String texto) {
        if (texto) {
            return texto.replaceAll(/<p>&nbsp;<\/p>/, '').trim()
        }
        return texto
    }

    static String stripTags(String texto = '' ) {
        //texto =  texto?.replaceAll("<(.|\n)*?>", '')
        //texto = Jsoup.clean( texto, Whitelist.none().addTags('i','b','a').addAttributes("a", "href","style") )
        //Document doc = Jsoup.parse( texto )
        //return doc.outerHtml()
        return Jsoup.parse( trimEditor( texto ) ).text().replaceAll(/ /,' ').trim()

    }

    /**
     * metodo para colocar o nome cientifico em itálico
     * @param nomeCientifico
     * @return
     */
    static String ncItalico( String nomeCientifico = '', Boolean semAutor=false)
    {
        String resultado=''
        try {
            nomeCientifico = nomeCientifico.replaceAll(/(?i)(<i>|<\/i>)/, '')
            nomeCientifico = nomeCientifico.replaceAll(/'/, '`')
            for (int i = 1; i <= nomeCientifico.length() - 1; i++) {
                if (resultado == '' && (Character.isUpperCase(nomeCientifico.charAt(i)) || !(nomeCientifico.charAt(i) ==~ /[A-Za-z\s]/))) {
                    resultado = '<i>' + nomeCientifico.substring(0, i).trim() + '</i>'+ (semAutor ? '':'&nbsp;<span>'+nomeCientifico.substring(i).trim() + '</span>')
                }
            }
        } catch( Exception e ) {}
        if( !resultado )
        {
            resultado = nomeCientifico ? '<i>'+nomeCientifico+'</i>' : ''
        }
        return resultado
    }

    static boolean isValidDate(String date = '')
    {
        if( date.trim() )
        {
            return true
        }
        if( date )
        {
            try {
                def dateParser = new java.text.SimpleDateFormat("dd/MM/yyyy")
                dateParser.lenient = false
                dateParser.parse( date )
            } catch( Exception e )
            {
                return false
            }
            return true
        }
    }

    static boolean isValidTime(String time = '')
    {
        if( time.trim() )
        {
            return true
        }
        if( time )
        {
            try {
                def dateParser = new java.text.SimpleDateFormat("HH:mm")
                dateParser.lenient = false
                dateParser.parse( time )
            } catch( Exception e )
            {
                return false
            }
            return true
        }
    }

    static Date strToDate( String dataHora = null )
    {
        if( !dataHora)
        {
            return null
        }
        try
        {
            if( dataHora.length() == 19)
            {
                return Date.parse("dd/MM/yyyy HH:mm:ss", dataHora)
            }
            else if( dataHora.length() == 16)
            {
                return Date.parse("dd/MM/yyyy HH:mm", dataHora)
            }
            else if( dataHora.length() == 10)
            {
                return Date.parse("dd/MM/yyyy", dataHora)
            }
        } catch( Exception e ){}
        return null
    }

    /**
     * Padronizar as coordenadas geográficas com 6 casas decimais
     * @param latY
     * @param lonX
     * @return Map
     */
    static Map roundCoords( double latY, double  lonX ) {
        Map result= [latY:latY, lonX:lonX, hash:'']
        try {
            lonX = Math.round(lonX * 100000000) / 100000000
            latY = Math.round(latY * 100000000) / 100000000
            result.hash = (lonX.toString() + '/' + latY.toString()).encodeAsMD5()
        } catch (Exception e) {}
        return result
    }

    /**
     * Abreviar nomes para que o tamanho nao ultrapasse 20 caracteres
     * Exemplo: Joaquim Jose da Silva -> Joaquim J. da Silva
     * @param text
     * @return String
     */

    static String nomeAbreviado(String text = null) {
        Integer maxLengh = 20
        if ( ! text ) {
            return ''
        }
        try {
            text = capitalize(text)
            List listNames = text?.split(' ')
            if (text.length() <= maxLengh) {
                return text;
            }
            listNames.eachWithIndex { name, index ->
                if (index > 0 && index < (listNames.size() - 1)) {
                    if (name.toString().trim() != '' && !(name.toString().toLowerCase() =~ /^d.s?$/)) {
                        String nomeTemp = text.replace(' ' + name, ' ' + name.toString().substring(0, 1) + '.')
                        if (nomeTemp.length() > maxLengh) {
                            text = nomeTemp
                        }
                    }
                }
            }
        } catch (Exception e) {
        };
        return text
    }

    /**
     * validar e-mail
     * @param email
     * @return
     */
    static boolean isEmail( String email = '' ){
        boolean isEmailIdValid = false;
        if (email != null && email.length() > 0) {
            String expression = '^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$'
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
            if (matcher.matches()) {
                isEmailIdValid = true;
            }
        }
        return isEmailIdValid;
    }

}
