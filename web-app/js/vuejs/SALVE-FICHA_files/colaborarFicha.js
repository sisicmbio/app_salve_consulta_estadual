
/*
 Metodo para ativar o editor de texto nos campos textareas com a classe wysiwyg
 */
var initEditor = function()
{
    var selector = '.wysiwyg';
    // limpar array de editores
    tinyMCE.editors = [];
    tinymce.remove(selector);
    tinymce.remove(selector + 'RO');
    tinymce.init({
        mode              : 'specific_textareas',
        selector          : selector,
        content_style     : ".mce-content-body {font-size:15px;font-family:Verdana,Arial,sans-serif;}",
        elementpath       : false,
        menubar           : false,
        statusbar         : false,
        padding_bottom      : 0,
        skin              : "icmbio",
        language          : 'pt_BR',
        browser_spellcheck: true,
        plugins           : "textcolor,link,lists,fullscreen",
        // https://www.tinymce.com/docs/advanced/editor-control-identifiers/#toolbarcontrols
        toolbar           : 'undo redo | bold italic underline subscript superscript | forecolor backcolor | bullist numlist | link unlink openlink | outdent indent | removeformat | fullscreen ',
        //preview_styles:'color background-color'
        //removed_menuitems: 'undo, redo',
        //content_css : 'css/tinymce.css',
        // sincronizar o texto com o campo para não precisar chamar   tinyMCE.triggerSave(); antes de salvar o formulário
        setup             : function (editor)
        {
            editor.on('change', function (e)
            {
                $("#"+e.target.id+'Button').attr('disabled',false).addClass('success');

                //  tinyMCE.activeEditor.getContent();
                //  log( editor.getContent() );
                editor.save();
            });

            editor.on("keyup", function (e)
            {
                if( e.ctrlKey && e.keyCode == 83 )
                {
                    e.preventDefault();
                    e.preventBubble();
                    console.log( 'Gravar');
                }
            });

            editor.on("init", function ()
            {
                if ($(editor.targetElm).prop("readonly") === true)
                {
                    $(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").hide();
                    this.readonly = true;
                    editor.readonly = true;
                }
            });
        },
    });
}

Vue.http.options.emulateJSON = true;
/*
Comecei a fazer o componente mas não sei como inicializar os valores
 */
Vue.component('ficha-colaboracao',{
    template:'#_ficha_colaboracao',
    props:['dataTopic','dataFieldName','dataValue','dataOriginalValue','dataVisible'],
    data: function() {
        return  {
            value:''
        }
    },
    methods:{
        gravarColaboracao:function()
        {
            if( this.value != tinyMCE.activeEditor.getContent() )
            {
                this.value = tinyMCE.activeEditor.getContent();
                $("#" + this.dataFieldName + 'Button').removeClass('success').attr('disabled', true);
                this.value = tinyMCE.activeEditor.getContent();
                var data = {sqFicha: $("#sqFicha").val(), fieldName: this.dataFieldName, fieldValue: this.value}
                doPost(baseUrl + 'ficha/salvarColaboracao', data, function (res)
                {
                    tinyMCE.activeEditor.focus();
                })
            }
        }
    },
})

var page = new Vue({
    el  : '#div-page-colaborar',
    data: {
        visible:false,
        fields:{'notasTaxonomicas'      :{visible:true,value:'',originalValue:''},
                'diagnosticoMorfologico':{visible:true,value:'',originalValue:''}},

    },
    created: function() {},
    mounted: function() {
        this.visible=true;
        // iniciar o tinyMce
        initEditor();
        window.setTimeout(function(){
            for( key in page.fields )
            {
                console.log( key );
                page.fields[key].originalValue = $("#valor_original_"+key).html();
            }
        },100);
    },
    methods:{
        gravarColaboracao:function(fieldName)
        {
            alert('gravar');
            return;
            $("#"+fieldName+'Button').removeClass('success').attr('disabled',true);
            this.fields[fieldName].value = tinyMCE.activeEditor.getContent();
            console.log( 'Campo: ' + this.fields[fieldName]);
            console.log( 'Valor original: ' + this.fields[fieldName].originalValue);
            console.log( 'Valor Atual :'  + this.fields[fieldName].value);
            var data = {sqFicha:$("#sqFicha").val(), fieldName:fieldName, fieldValue:this.fields[fieldName].value}
            doPost(baseUrl+'ficha/salvarColaboracao',data,function(res){

            })
        }
    },
    components:{}
});