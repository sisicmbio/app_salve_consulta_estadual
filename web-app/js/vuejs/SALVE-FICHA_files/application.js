// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require axios/axios.min
//= require jquery/v224/jquery.min
//= require foundation/js/vendor/foundation.min
//= require foundation/js/vendor/what-input
//= require jquery/plugins/blockUI/blockUI.min
//= require jquery/plugins/validation/jquery.validate.min
//  require jquery/plugins/validation/additional-methods.min
//= require jquery/plugins/validation/localization/messages_pt_BR.min
//  require jquery/plugins/validation/localization/methods_pt_BR.js
//= require jquery/plugins/debounce/jquery-debounce.min // https://github.com/cowboy/jquery-throttle-debounce/
//= require vue/select2/vue-select.js

//  require_tree .
//= require_self


var webProtocol = window.location.protocol;
// encontrar a url base
var aTemp   = String( webProtocol +'//'+ window.location.host + window.location.pathname).split( '/salve-consulta');
var baseUrl =  aTemp[0]+'/salve-consulta/';


sliderStart = function()
{
    var imgNum = Math.floor( ( Math.random() * 6) +1);
    $("#div-img-top").css({'background-image':'url("/salve-consulta/assets/slider/imagem' + String(imgNum) + '.jpg")'})

    window.setTimeout( function(){
      sliderStart();
    },60000)
}

if (typeof jQuery !== 'undefined') {

    $(document).ready(function()
    {
        $(document).foundation();
        // escutar as teclas pressionadas e tratar a tecla Enter
        $(document).keypress(function (e)
        {
            console.log( e );
            if (e.which == 13)
            {
                if (e.target && e.target.tagName == 'INPUT')
                {
                    e.preventDefault(); // evitar que a tecla enter submeta o formulário
                }
            }
        });
        sliderStart();
        $('#primary-menu').on(
            'show.zf.dropdownmenu', function ()
            {
                var dropdown = $(this).find('.is-dropdown-submenu');
                dropdown.css('display', 'none');
                dropdown.fadeIn('slow');
            });
        $('#primary-menu').on(
            'hide.zf.dropdownmenu', function ()
            {
                var dropdown = $(this).find('.is-dropdown-submenu');
                dropdown.css('display', 'inherit');
                dropdown.fadeOut('slow');
            });
    });
}

blockUi = function( strMsg )
{
    $.blockUI({ message:strMsg,css: { 'font-size':'2rem','border-radius':'10px', minHeight:'50px', border:'1px solid #000',backgroundColor: '#efefef', color: '#000'} });
}

unblockUi = function()
{
    $.unblockUI();
}

doPost = function( strUrl, objData, fncCallback )
{
    $.when( $.post( strUrl,objData).then(
        function( res )
        {
            unblockUi();
            if( res.msg )
            {
                alert( res.msg );
            }
            if( fncCallback )
            {
                fncCallback(res);
            }
        },function( objResponse, textStatus, jqXHR ){
            unblockUi();
            if( objResponse.status == 404)
            {
                alert(strUrl + ' não existe!')
            }
            else if( objResponse.status == 401)
            {
                alert('Sem permissão de acesso a ' + strUrl)
            }
            else
            {
                alert( textStatus )
            }
        })
    );
}

setFocus = function( id )
{
    window.setTimeout(function(){
     $("#"+id.replace('#','') ).focus();
    },100);
}