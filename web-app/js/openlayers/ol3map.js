/**
/**
 * Classe para gerar o mapa utilizando OpenLayers3 com controles customizados
 * de desenhar, arrastar, adicionar camadas WMS
 *
 * Autor: Luis Eugênio - 2018
 */

/**
 * Teste de Servicos utilizados
 * - procurar localidade pelo nome ou endereco
 *   https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?f=json&singleline=Instituto%20chico%20mendes,%20brasilia%20df
 *
 * Utilizar Serviço https://nominatim.openstreetmap.org/ui/search.html?q=Goi%C3%A2nia%2CGO
 * https://nominatim.openstreetmap.org/search.php?q=Goiânia,GO&polygon_geojson=1&format=jsonv2
 * ou
 * servico do INDE para localização de lugares pelo nome. Exemplo:
 *  const res = await fetch("https://visualizador.inde.gov.br/api/buscaLocalidade?busca=brasilia", {
 * 	method: "GET",
 * 	headers: { "Content-Type": "application/json" }
 * });
 *
 * console.log(await res.json())
 *
 * - exemplo de como fazer um tooltip para exibir informações do ponto ao passar o mouse
 *   https://openlayers.org/en/v4.6.5/examples/measure.html?q=draw
 *
 * - exemplo como criar camada geojson com  src.getState()
 * http://astuntechnology.github.io/osgis-ol3-leaflet/ol3/02-GEOJSON.html
 *
 *
 *  - tentar implementar a camada Toponimia MMA
 *     http://mapas.mma.gov.br/i3geo/mashups/openlayers.php?temas=undefined&layers=undefined&mapext=-76.5125927,-39.3925675209,-29.5851853,9.49014852081&botoes=pan,zoombox,zoomtot,identifica
 *  -  http://mapas.mma.gov.br/cgi-bin/mapserv?map=/opt/www/html/webservices/baseref.map&LAYERS=base&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&SRS=EPSG%3A4326&BBOX=-48.3875927,-28.1425675209,-42.7625927,-22.5175675209&WIDTH=256&HEIGHT=256
 *  -  I3GEO - http://mapas.mma.gov.br/i3geo/classesphp/mapa_openlayers.php?g_sid=ttf86le6vcmtjnkd128k73b904&TIPOIMAGEM=nenhum&layer=zee&r=0.14294386850663532&LAYERS=zee&MAP_IMAGETYPE=AGG_Q&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image%2Fjpeg&SRS=EPSG%3A4326&BBOX=-34.589827806803,-31.835931122721,-20.048810587483,-17.294913903401&WIDTH=256&HEIGHT=256
 */


/*
  DONE - Criar legenda automaticamente das camadas do portalbio e salve
  DONE - Criar janela popup se a feature clicada possuir dados
  DONE - Criar função para identificar o ponto no mapa e substituir a app.mapAnimateFeature()
  TODO - Criar função para retornar o layer pelo nome
  TODO - Criar função para geoLocation do usuário
  TODO - Criar função para adicionar marker ( ver olApi.addMarker(ocorrencia.oMap.map, 'drawlayer', coordinates[1], coordinates[0], window.grails.markerDotRed); )
  TODO - Criar botão no layerswitch para exibir/esconder
  TODO - Criar camadas do sigeo na tela de adicionar camada WMS utilizando abas

*/

//var oldHandleEvent; // utilizado no controle do zoom com a tecla control pressionada
var baseUrl = baseUrl ? baseUrl : 'https://salve.icmbio.gov.br/salve-consulta/';
var styleCache = {};
var loadingOccurrences = false;

// tirar depois estas variaveis de debub
function Ol3Map(opt_options) {
    var self            = this;
    var styleCache      = {};

    var urlSalve        = ( baseUrl ? baseUrl.replace('/salve-consulta/','/salve-estadual/') : '/salve-estadual/');
        urlSalve        = urlSalve.replace(':8090',':8080')

    // flag para habilitar alguma funcionalidades apenas para usuários do CBC
    //var isCbc           =  /CBC\sBrasília-DF$/.test( $("#spanLoginUserName").prop('title'))
    var turfPoligonoBrasil = null
    var turfPoligonoZee = null
    var running = false; // evitar chamadas recursivas durante o processamento
    var hydroshedLoaded = {id:'',features:[], selectedFeatures:[] };
    var clusterDistance = 15;
    var iconSize =12;
    var fitFilterPolygonAfterDraw=true; // ajustar o poligono do filtro no viewport do mapa apos finalizar o desenho
    var default_options = { showToolbar: true
        , showGeocoder:true
        , sqFicha:0
        , readOnly:false
        , urlOcorrencias:urlSalve+'api/ocorrencias/'
        , showExportButton:true
        , showBioma:false
        , showUcFederal:false
        , showUcEstadual:false
        , showZee:false
        , showEoo:true
        , showBacias:true
        , showEcorregioesAguaticas:true
        , showAdicionadosAposAvaliacao:true
        , showAoo:true
        , eooUsedPoints:true
        , expandLayerSwitcher:true
    };

    // camadas dos registros de ocorrencias com novo padrão de cor: Vermelha, Amarela e Verde apenas
    var occurrenceLayers = [
        {
            id      : "regNaoUtilizados",
            name    : 'Não utilizados',// aparecer no layer switch
            color   : "#FF0000", // VERMELHO -  utilizado pelo cluster
            options : { legend : {  image:'ponto_vermelho_12x12.png'} },
            zIndex  : 6,
            hint    : 'Registros Não utilizados na avaliação',
            cluster : true,
            filter  : true
        },{
            id      : "regNaoConfirmados",
            name    : 'A confirmar',// aparecer no layer switch
            color   : "#FFFF00", // AMARELO - utilizado pelo cluster
            options : { legend : {  image:'ponto_amarelo_12x12.png'} },
            zIndex  : 5,
            hint    : 'Registros que precisam ser analisados',
            cluster : true,
            filter  : true
        },{
            id      : "regUtilizados",
            name    : 'Utilizados',// aparecer no layer switch
            color   : "#00FF00", // VERDE - utilizado pelo cluster
            options : { legend : {  image:'ponto_verde_12x12.png'} },
            zIndex  : 9,
            hint    : 'Registros utilizados na avaliação',
            cluster : true,
            filter  : true
        },{
            id      : "regUtilizadoHistorico",
            name    : 'Históricos',// aparecer no layer switch
            color   : "#00FF00", // VERDE - utilizado pelo cluster
            options : { legend : {  image:'ponto_verde_12x12_T.png'} },
            zIndex  : 8,
            hint    : 'Registros sem presença atual na coordenada',
            cluster : true,
            filter  : true
        },{
            id      : "regConfirmadoAdicionadoAposAvaliacao",
            name    : 'Confirmados e adicionados após avaliação',// aparecer no layer switch
            color   : "#00FF00", // VERDE - utilizado pelo cluster
            options : { legend : {  image:'ponto_verde_12x12_X.png'} },
            zIndex  : 7,
            hint    : 'Registros estão confirmados mas foram adicionados após a avaliação',
            cluster : true,
            filter  : true
        }
    ];


    // gerar um webUserId ou utilizar a variavel global sessionId definada pela aplicação
    this.webUserId = 'webUserId_' + Math.floor(Math.random() * 1000000) + 1;
    if( typeof( webUserId ) == 'string' && webUserId != '')
    {
        this.webUserId = webUserId;
    }
    this.options            = opt_options || {};
    this.options            = $.extend({},default_options,this.options);
    this.expandLayerSwitcher= this.options.expandLayerSwitcher == false ? false : true;
    this.overlayPopup       = null; // camada para exibição da janela popup ao clicar sobre um ponto
    this.filters            = []; // filtros geo
    this.filtersMetadata    = []; // filtros por metadados do ponto
    this.filteredIds        = []; // ids dos pontos filtrados para agilizar a atualização do gride
    this.sqFicha            = this.options.sqFicha; // para exibição das camdas de ocorrências
    this.contexto           = this.options.contexto||''; // contexto para fltrar ocorrencias utilizadas e nao utilizadas
    this.legendItems        = {}; // objeto contendo nome e cor para criação automática da legenda
    this.animatedPoint      = null; // ponto selecionado para efeito de animate ao mover o mapa
    this.animating          = false; // não aplicar a animação repetidamente
    this.gridId             = this.options.gridId; // id do grid para mostrar/esconder as linhas ao filtrar/desfiltrar os pontos
    this.callbackActions    = this.options.callbackActions; // método chamado após o retorno ajax
    this.urlAction          = this.options.urlAction; // url da ação ajax que será chamada para executar alguma ação no banco de dados
    this.readOnly           = this.options.readOnly; // habilitar/desabilitar ações que alteram banco de dados
    this.contextMenuItems   = [];
    this.urlOcorrencias     = this.options.urlOcorrencias.replace(/\/\/?$/,'') + '/' + this.webUserId + '/';
    this.showExportButton   = this.options.showExportButton;
    this.file               = null; // objeto arquivo atual do upload sheapeFile
    this.geocoder           = null;
    this.showZee            = this.options.showZee;
    this.showEoo            = ( this.options.showEoo && this.options.sqFicha ); // se não tem ficha não tem como calcular eoo dos pontos de ocorrencias da espécie
    this.showBacias         = this.options.showBacias;
    this.showEcorregioesAguaticas = this.options.showEcorregioesAguaticas;
    this.showAoo            = ( this.options.showAoo && this.options.sqFicha);
    this.vlEoo              = 0; // guardar o valor atual da EOO
    this.vlAoo              = 0; // guardar o valor atual da AOO
    this.editingEoo         = false;
    this.editingAoo         = false;
    this.eooUsedPoints      = this.options.eooUsedPoints;
    this.eooFeature         = null;
    this.showUcFederal      = this.options.showUcFederal;
    this.showUcEstadual     = this.options.showUcEstadual;
    this.showBioma          = this.options.showBioma;

    // estilo para desenhos selecionados no mapa
    this.featureSelectedStyle =  new ol.style.Style({
        stroke: new ol.style.Stroke({
            //color: 'rgba(65,160,210, 0.8)', // azul piscina
            color: '#ff0000',
            width: 1,
        }),
        fill: new ol.style.Fill({
            color: 'rgba(6, 44, 176, 0.15)'
        }),
        zIndex:3
    });

    // estilo para desenhos NÃO selecionados no mapa
    this.featureUnSelectedStyle =  new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: '#3399cc',
            width: 1
        }),
        fill: new ol.style.Fill({
            //color: 'rgba(0, 0, 255, 0.025)'
            color: 'rgba(6, 44, 176, 0.05)'
        }),
        zIndex:1
    });

    // distâncias de 2km em epsg:3587, para localizar o square onde o ponto está
    this.x2km = 2081.5665851693425;
    this.y2km = 2002.2377318836736;

    // ferramenta de medição
    this.sketch                = null; // feature criada ao inicar draw para medir distância
    this.sketchListener        = null; // evento change da linha de medição
    //----------------------------------//

    // poligono atualmente selecionado
    this.currentSelectedDraw=null;

    // creates unique id's
    this.uid = function() {
        return 'feature_' + Math.floor(Math.random() * 10000) + 1;
    }

    if ( ! this.options.el) {
        if( $("div#map").size() == 1 ) {
            this.options.el = 'map'
        } else {
            // criar div para exibição do mapa se não for informado o el
            this.options.el = 'map-' + this.uid().replace(/[^0-9]/g, '');
            $('<div id="' + this.options.el + '" style="width:800px;height:600px;border:1px solid silver;"></div>').appendTo('body');
        }
    }
    this.el = this.options.el.trim().replace(/#/g); // id do elemento html onde será criado o mapa
    this.map = null; // instancia do mapa ( ol3 )

    if( this.options && this.options.height)
    {
        $("#"+this.el).height( this.options.height );
    }
    $("#"+this.el).css('background-color','#f0f8ff');
    // serviço de mapa padrão Open Street Map
    this.layers = [];

    this.addMarker = function(lon,lat,text,secondsToRemove, marker){
        var position = [lon, lat]
        imagePath = marker ? marker: window.grails.markerPinBlue;

        // definir o estilo do layer
        var iconStyle = new ol.style.Style({
            image: new ol.style.Icon(({
                anchor: [0.5, 128],
                anchorXUnits: 'fraction',
                anchorYUnits: 'pixels',
                opacity: 1,
                src: imagePath,
                scale:0.35,
                //anchor: [0.6, 20],
                //anchor: [60,145],
                //img: $('<img src="'+imagePath+'"/>')[0],
                //imgSize:[37,55],
                //size:[37,55],


            }))
        });
        // criar a feature
        var iconFeature = new ol.Feature({
            geometry: new ol.geom.Point(position),
            text: ( text ? text : ''),
        });
        iconFeature.setStyle(iconStyle);
        self.drawLayer.getSource().addFeature(iconFeature);
        // remover marker depois de 10 segundos
        if( secondsToRemove ) {
            window.setTimeout(function () {
                self.drawLayer.getSource().removeFeature(iconFeature);
            }, secondsToRemove * 1000)
        }
        return iconFeature;
    }

    this.addLayer = function(layer) {
        try {
            if (layer && layer.getSource()) {
                this.layers.push(layer);
                if (this.map) {
                    this.map.addLayer(layer);
                    if( layer.get('name') ) {
                        this.updateLayerSwitch();
                    }
                }
            } else {
                console.log('layer inválido!')
            }
        } catch (e) {
            console.log(e.message);
        }
    }

    // camada base OPEN STREET MAP
    var osm = new ol.layer.Tile({
        source: new ol.source.OSM({
            maxZoom: 19,
            crossOrigin: 'anonymous'
        }),
        visible: true,
        type: 'baselayer',
        name: 'Ruas (OSM)',
        id: 'osm',
        zIndex: 1
    });
    this.addLayer(osm);
    /*
    osm.getSource().on('tileloadstart', function(event) {
        //replace with your custom action
        console.log('osm carregando...');
    });

    osm.getSource().on('tileloadend', function(event) {
        //replace with your custom action
        console.log('osm terminou');
    });
    osm.getSource().on('tileloaderror', function(event) {
        //replace with your custom action
        console.log('osm erro');
    });*/

    /*
    // camada BING MAP
    var bingLayer = new ol.layer.Tile({
        source: new ol.source.BingMaps({
            key: 'AnLGl1Vfs_oXbalB22KQnOifIy3w8KLQfRGMAkZ4LkxJmjeS_1VFhlxyCSiOOdVx',
            imagerySet: 'AerialWithLabels',
            preload: Infinity,
            //imagerySet: bingLayers[i],
            maxZoom: 19,
            crossOrigin: 'anonymous'
        }),
        visible: false,
        type: 'baseLayer',
        name: 'Bing Map',
        id: 'bingmap',
        zIndex: 1
    });
    this.addLayer(bingLayer);
    */


    // camada base ESRI
    var esriTopografia = new ol.layer.Tile({
        source: new ol.source.XYZ({
            attributions: [
                new ol.Attribution({
                    html: 'WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, increment P Corp.&nbsp;<a target="_blank" href="http://services.arcgisonline.com/ArcGIS/' +
                        'rest/services/World_Topo_Map/MapServer">ESRI</a>'
                })
            ],
            url: 'https://server.arcgisonline.com/ArcGIS/rest/services/' +
                'World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
            crossOrigin: 'anonymous'
        }),
        visible: false,
        type: 'baseLayer',
        name: 'Topografia Mundial (Esri)',
        id: 'esri-topografia',
        zIndex: 1
    });
    this.addLayer(esriTopografia);

    var esriGeografia = new ol.layer.Tile({
        source: new ol.source.XYZ({
            attributions: [
                new ol.Attribution({
                    html: 'WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, increment P Corp.&nbsp;<a target="_blank" href="http://services.arcgisonline.com/ArcGIS/' +
                        'rest/services/NatGeo_World_Map/MapServer">ESRI</a>'
                })
            ],
            url: 'https://server.arcgisonline.com/ArcGIS/rest/services/' +
                'NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}',
            crossOrigin: 'anonymous'
        }),
        visible: false,
        type: 'baseLayer',
        name: 'Geografia Nacional (Esri)',
        id: 'esri-geografia',
        zIndex: 1
    });
    this.addLayer(esriGeografia);

    var esriSatelite = new ol.layer.Tile({
        source: new ol.source.XYZ({
            attributions: [
                new ol.Attribution({
                    html: 'DigitalGlobe, GeoEye, CNES/Airbus DS&nbsp;<a target="_blank" href="http://services.arcgisonline.com/ArcGIS/' +
                        'rest/services/World_Imagery/MapServer">ESRI</a>'
                })
            ],
            url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
            crossOrigin: 'anonymous'
        }),
        visible: false,
        type: 'baseLayer',
        name: 'Satélite (Esri)',
        id: 'esri-satelite',
        zIndex: 1
    });
    this.addLayer( esriSatelite );

    // adicionar camadas em que os arquivos shps ficam em disco
    if( typeof( shp ) !='undefined' ) {
        // camada batimetria
        /**/
        var batimetriaLayer = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: new ol.style.Style({
                stroke: new ol.style.Stroke({
                    //color: 'rgba(65,160,210, 0.8)', // azul piscina
                    color: '#2e79ba', // azul piscina
                    width: 1
                }),
            }),
            visible: false,
            id: 'Batimetria',
            name: 'Batimetria',
            type: 'vector',
            edit: false,
            zIndex: 1
        });
        self.addLayer(batimetriaLayer);
        var urlBatimetria = urlSalve + 'api/shapefile/batimetria';
        shp(urlBatimetria).then(function ( geojson ) {
            var features = self.getGeojsonValidFeatures( geojson );
            batimetriaLayer.setSource(new ol.source.Vector({
                features: features //(new ol.format.GeoJSON()).readFeatures(geojson, {featureProjection: 'EPSG:3857'})
            }));
        });
        /**/

        /**/
        // bacias hidrográficas
        if ( self.options.showBacias ) {
            var baciaHidrograficaLayer = new ol.layer.Vector({
                source: new ol.source.Vector(),
                style: new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        //color: 'rgba(65,160,210, 0.8)', // azul piscina
                        color: '#2e79ba', // azul piscina
                        width: 1
                    }),
                }),
                visible: false,
                id: 'BaciaHidrografica',
                name: 'Bacias Hidrográficas',
                type: 'vector',
                edit: false,
                zIndex: 1
            });
            self.addLayer(baciaHidrograficaLayer);
            //var urlBatimetria = urlSalve + 'api/shapefile/baciaHidrografica';
            var urlBaciaHidrografica = urlSalve + 'api/shapefile/baciaHidrografica';
            shp(urlBaciaHidrografica).then(function (geojson) {
                var features = self.getGeojsonValidFeatures(geojson);
                features.map(function (f, i) {
                    var PS1_NM = f.get('PS1_NM');
                    var RHI = f.get('RHI');
                    var text = 'Sub-bacia Hidrográfica';
                    if( RHI ) {
                        text += '<br>Região: ' + RHI;
                    }
                    if( PS1_NM) {
                        text += '<br>Nome: '  + PS1_NM;
                    }
                    f.set('text', text);
                });
                baciaHidrograficaLayer.setSource(new ol.source.Vector({
                    features: features //(new ol.format.GeoJSON()).readFeatures(geojson, {featureProjection: 'EPSG:3857'})

                }));
            });
        }
         /**/

        // camada ecorregioes aquaticas continentais x brasil
        /** /
        if ( self.options.showEcorregioesAguaticas ) {
            var ecorregioesAguaticasContinentais = new ol.layer.Vector({
                source: new ol.source.Vector(),
                style: new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        //color: 'rgba(65,160,210, 0.8)', // azul piscina
                        color: '#0e7b27', // verde
                        width: 1
                    }),
                }),
                visible: false,
                id: 'EcorregiaoAguaticaContinenental',
                name: 'Ecoregioes Aquáticas',
                type: 'vector',
                edit: false,
                zIndex: 1
            });
            self.addLayer(ecorregioesAguaticasContinentais);
            //var urlBatimetria = urlSalve + 'api/shapefile/baciaHidrografica';
            var urlShapeEcorregiao = urlSalve + 'api/shapefile/Ecorregioes_aquaticas_x_Brasil';
            shp( urlShapeEcorregiao ).then(function (geojson) {
                var featuresEcoregiao = self.getGeojsonValidFeatures(geojson);
                featuresEcoregiao.map(function (f, i) {
                    var AREA_SQKM   = f.get('AREA_SQKM');
                    var ECOREGION   = f.get('ECOREGION');
                    var ECO_ID      = f.get('ECO_ID');
                    if( ECOREGION ) {
                        var text = '<b>'+ECOREGION+'</b>'
                        if (AREA_SQKM) {
                            text += '<br>'+AREA_SQKM+' Km&sup2;';
                        }
                        if (ECO_ID) {
                            text += '<br>ID:' + ECO_ID;
                        }
                        f.set('text', text);
                    }
                });
                ecorregioesAguaticasContinentais.setSource(new ol.source.Vector({
                    features: featuresEcoregiao //(new ol.format.GeoJSON()).readFeatures(geojson, {featureProjection: 'EPSG:3857'})

                }));
            });
        }
        /**/
    }

    // camada BIOMAS - IBGE
    /*var biomas = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            //url:urlSalve+'api/getLayer',
            //params: { 'layer': 'biomas', 'debug':false },
            url:'https://geoservicos.ibge.gov.br/geoserver/wms',
            params: {'LAYERS':'CREN:biomas_5000'
                ,'SERVICE':'WMS'
                ,'VERSION':'1.1.0'
                ,'STYLES':''
                ,'NAME':'Biomas'
            },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: this.showBioma,
        id: 'biomas',
        name: 'Biomas',
        type: 'wms',
        zIndex: 1
    });
    */

    // camada BIOMAS - SIGEO2
    var biomas = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            //url:urlSalve+'api/getLayer',
            //params: { 'layer': 'biomas', 'debug':false },
            url:'https://mapproxy2.icmbio.gov.br/service',
            params: {'LAYERS':'3_biomas'
                ,'SERVICE':'WMS'
                ,'VERSION':'1.3.0'
                ,'STYLES':''
                ,'NAME':'Biomas'
                ,'id':'3'
            },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: this.showBioma,
        id: 'biomas',
        name: 'Biomas',
        type: 'wms',
        zIndex: 1
    });

/*
    // mapas icmbio
    var biomas = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            //url:urlSalve+'api/getLayer',
            //params: { 'layer': 'biomas', 'debug':false },
            url:'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/biomas.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png',
            params: { 'LAYERS': 'biomas' },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: this.showBioma,
        id: 'biomas',
        name: 'Biomas',
        type: 'wms',
        zIndex: 1
    });
*/
    //biomas.getSource().setTileLoadFunction( getTile );
    this.addLayer(biomas);


    //  uc: http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/uc_Federal.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=uc_federal&WIDTH=230&HEIGHT=230&CRS=EPSG%3A3857&STYLES=&MAP_RESOLUTION=80.99999785423279&BBOX=-7514065.628545966%2C-7514065.628545966%2C-5009377.08569731%2C-5009377.08569731
    //  uc: http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/uc_Federal.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=uc_federal&WIDTH=230&HEIGHT=230&CRS=EPSG%3A3857&STYLES=&MAP_RESOLUTION=80.99999785423279&BBOX=-7514065.628545966%2C-7514065.628545966%2C-5009377.08569731%2C-5009377.08569731
    // legenda biomas: http://mapas.mma.gov.br:80/i3geo/ogc.php?tema=bioma&layer=bioma&request=getlegendgraphic&service=wms&format=image/jpeg
    // http://mapas.mma.gov.br:80/i3geo/ogc.php?tema=bioma&layer=bioma&request=getlegendgraphic&service=wms&format=image/jpeg
    // http://mapas.mma.gov.br/cgi-bin/mapserv?map=/opt/www/html/webservices/biorregioes.map&layers=biomas&style=default&r=0.8384542120079235&SRS=EPSG:4326&CRS=EPSG:4326&FORMAT=image%2Fpng&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&BBOX=-90.738954487784,-30.492636325189,-60.985272650378,-0.73895448778376&WIDTH=256&HEIGHT=256
    // &version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png
    // http://mapas.mma.gov.br/cgi-bin/mapserv?map=/opt/www/html/webservices/biorregioes.map&layers=biomas&style=default&r=0.8384542120079235&SRS=EPSG:4326&CRS=EPSG:4326&FORMAT=image%2Fpng&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&BBOX=-90.738954487784,-30.492636325189,-60.985272650378,-0.73895448778376&WIDTH=256&HEIGHT=256
    // camada biomas
    /*
    var biomas = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            //url:'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/biomas.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png',
            // cgi-bin mma
            // url:'http://mapas.mma.gov.br/cgi-bin/mapserv?map=/opt/www/html/webservices/biorregioes.map',
            // params: { 'LAYERS': 'biomas','SRS':'EPSG:4326','CRS':'EPSG:4326' },
            // params: { 'LAYERS': 'biomas','VERSION':'1.1.1'},
            // i3geo mma
            //http://mapas.mma.gov.br/i3geo/ogc.php?tema=ujFggztAkm
            //http://mapas.mma.gov.br/cgi-bin/mapserv?map=/opt/www/html/webservices/biorregioes.map&layers=biomas&style=default&r=0.48125924970552303&SRS=EPSG:4326&CRS=EPSG:4326&&FORMAT=image%2Fpng&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&BBOX=-55.661416178782,-21.61377889833,-49.444486987721,-15.396849707269&WIDTH=256&HEIGHT=256
            // http://mapas.mma.gov.br/i3geo/ogc.php?tema=bioma&FORMAT=image/png&transparent=true&service=wms&layers=bioma&version=1.1.1&srs=EPSG:3857&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&CRS=EPSG%3A3857&STYLES=&WIDTH=1071&HEIGHT=1005&BBOX=-11256422.533388197%2C-7734204.270007273%2C-777823.1998299537%2C2098655.0485977996
            // http://mapas.mma.gov.br/i3geo/ogc.php?tema=bioma&FORMAT=image/png&transparent=true&service=wms&layers=bioma&version=1.1.1&srs=EPSG:3857&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&CRS=EPSG%3A3857&STYLES=&WIDTH=1071&HEIGHT=1005&BBOX=-11256422.533388197%2C-7734204.270007273%2C-777823.1998299537%2C2098655.0485977996
            url:'http://mapas.mma.gov.br/i3geo/ogc.php?tema=bioma&FORMAT=image/png&transparent=true&service=wms&layers=bioma&version=1.1.1&srs=EPSG:3857',
            params: { 'layers': 'bioma','version':'1.1.1'},
            serverType: 'mapserver',
            projection:'EPSG:3857',
            crossOrigin: 'anonymous',
            //tileLoadFunction:function(imageTile, src){
                // imageTile.getImage().src = src;
            //    console.log( imageTile )
             //   console.log( src )
             //   console.log('--------------------')
           // },
        }),
        visible: this.showBioma,
        id: 'biomas',
        name: 'Biomas',
        type: 'wms',
        zIndex: 1
    });*/

/*
    // camada BIOMAS XYZ
    var biomas = new ol.layer.Tile({
        source: new ol.source.XYZ({
            //url: 'http://mapas.mma.gov.br/i3geo/ogc.php/{z}/{y}/{x}?tema=bioma&LAYERS=bioma&version=1.1.1&request=GetMap&FORMAT=image/png&transparent=true&service=wms&&version=1.1.1&srs=EPSG:3857',
            crossOrigin: 'anonymous'
        }),
        visible: false,
        type: 'wms',
        name: 'Biomas',
        id: 'biomas',
        zIndex: 1
    });
    this.addLayer(biomas);
*/

    // CAMADA MAPA DO BRASIL
    if( typeof(brasilGeoJson) != 'undefined' ) {
        var brasilLayer = new ol.layer.Vector({
            source: new ol.source.Vector({
                features: (new ol.format.GeoJSON()).readFeatures(brasilGeoJson, {featureProjection: 'EPSG:3857'})
            }),
            style: new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'blue',
                    //lineDash: [1],
                    width: 1
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(0, 0, 255, 0.1)'
                })
            }),
            visible: false,
            id: 'Brasil',
            name: 'Brasil',
            type: 'vector',
            edit: false,
            zIndex: 1
        });
        this.addLayer(brasilLayer);
    }

    // camada ZEE
    var zee = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url:'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/ibge_zee.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png',
            params: { 'LAYERS': 'zee' },
            serverType: 'mapserver',
            crossOrigin: ''
        }),
        visible: this.showZee,
        id: 'zee',
        name: 'ZEE',
        type: 'wms',
        zIndex: 1
    });
    this.addLayer(zee);// camada ZEE

    // camada TERRAS INDÍGENAS
    if( typeof( shp ) != 'undefined' ) {
        var terrasIndigenasLayer = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'rgba(203,70,19,0.8)', // azul piscina
                    width: 1
                }),
            }),
            visible: false,
            id: 'TerrrasIndigenas',
            name: 'Terras indígenas',
            type: 'vector',
            edit: false,
            zIndex: 1
        });
        self.addLayer(terrasIndigenasLayer);
        //var urlTerrasIndigenas = baseUrl.replace('/salve-consulta/', '/salve-estadual/') + 'api/shapefile/ti_sirgas2000';
        var urlTerrasIndigenas = urlSalve + 'api/shapefile/ti_sirgas2000';
        shp(urlTerrasIndigenas).then(function (geojson) {
            var features = self.getGeojsonValidFeatures( geojson );
            terrasIndigenasLayer.setSource(new ol.source.Vector({
                features: features//(new ol.format.GeoJSON()).readFeatures(geojson, {featureProjection: 'EPSG:3857'})
            }));
        });
    }
    /*
    // serivço de mapas icmbio antes do SIGEO2
    var ucFederal = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            //url:urlSalve+'api/getLayer',
            //params: { 'layer': 'uc', 'debug':false, cache:true },
            //url:'http://mapas.icmbio.gov.br/mapproxy/service',
            url:'https://mapas2.icmbio.gov.br/fcgi-bin/mapserv?map=/var/www/sigeo/storage/app/uploads/icmbio/unidades_de_conservacao.map',
            //https://mapas2.icmbio.gov.br/fcgi-bin/mapserv?map=/var/www/sigeo/storage/app/uploads/icmbio/unidades_de_conservacao.map&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=1_unidades_de_conservacao&TILED=true&id=1&hash=120e1580d5bde809266b2c17b8566674b0f4d7b97d8eb4ec4ae1cd0c1c3dbb91&hash_user=95dae17ffbca20685b3ba2751f2571836b81bb821cd038e13201f1732e4204e5&name=Unidades%20de%20Conserva%C3%A7%C3%A3o&time=1633441289818&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&STYLES=&BBOX=-10018754.171394622%2C0%2C-7514065.628545966%2C2504688.5428486555
            //params: { 'LAYERS':'uc','SERVICE':'WMS','VERSION':'1.3.0','STYLES':''
            params: { 'LAYERS':'1_unidades_de_conservacao'
                ,'SERVICE':'WMS'
                ,'VERSION':'1.3.0'
                ,'STYLES':''
                ,'TILED':true
                ,'id':1
                ,'name':'UC Federal'
                ,'hash':'120e1580d5bde809266b2c17b8566674b0f4d7b97d8eb4ec4ae1cd0c1c3dbb91'
                ,'hash_user':'95dae17ffbca20685b3ba2751f2571836b81bb821cd038e13201f1732e4204e5'
                ,'time': '1633441289818'

    // ,'SLD':'http://ogc.bgs.ac.uk/sld/20130628_1372432351566_OpenLayers.Layer.WMS_991.sld'
            },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: this.showUcFederal,
        id: 'uc_federal',
        name: 'UC Federal',
        type: 'wms',
        zIndex: 1
    });

    */

    // Camada UCs FEDERAIS - SIGEO2
    var ucFederal = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url:'https://mapas2.icmbio.gov.br/fcgi-bin/mapserv?map=/var/www/sigeo/storage/app/uploads/icmbio/unidades_de_conservacao.map',
            params: { 'LAYERS':'1_unidades_de_conservacao'
                ,'SERVICE':'WMS'
                ,'VERSION':'1.3.0'
                ,'STYLES':''
                ,'TILED':'true'
                ,'id':'1'

                // ,'SLD':'http://ogc.bgs.ac.uk/sld/20130628_1372432351566_OpenLayers.Layer.WMS_991.sld'
            },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: this.showUcFederal,
        id: 'uc_federal',
        name: 'UC Federal',
        type: 'wms',
        zIndex: 1
    });

    this.addLayer(ucFederal);

    /*
    // SEM UTILIZAR MAPPROXY
    var ucFederal = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url:'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/uc_Federal.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png',
            params: { 'LAYERS': 'uc_federal' },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: this.showUcFederal,
        id: 'uc_federal',
        name: 'UC Federal',
        type: 'wms',
        zIndex: 1
    });
    this.addLayer(ucFederal);*/

    // camada UC ESTADUAL
    var ucEstadual = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url:'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/mma_UcEstadual.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png',
            params: { 'LAYERS': 'uc_estadual' },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: this.showUcEstadual,
        id: 'uc_estadual',
        name: 'UC Estadual',
        type: 'wms',
        zIndex: 1
    });
    this.addLayer(ucEstadual);



    // https://sigel.aneel.gov.br/Down/
    // http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/aneel_usinas_hidreletricas.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image/png&TRANSPARENT=true&LAYERS=aneel_usinas_hidreletricas&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&STYLES=&BBOX=-7514065.628545966%2C-2504688.542848654%2C-5009377.08569731%2C1.3969838619232178e-9
    /*
    // camada USINAS HIDRELETRICAS - icmbio
    var usinasHidreletricas = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url:'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/aneel_usinas_hidreletricas.map&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png',
            params: { 'LAYERS': 'aneel_usinas_hidreletricas' },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: false,
        id: 'usinas_hidreletricas',
        name: 'Usinas Hidreléticas',
        type: 'wms',
        zIndex: 1
    });
    this.addLayer(usinasHidreletricas);
     */


    // https://sigel.aneel.gov.br/arcgis/services/PORTAL/Camadas/MapServer/WmsServer?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=0%2C10&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&STYLES=&BBOX=-7514065.628545966%2C-2504688.542848654%2C-5009377.08569731%2C1.3969838619232178e-9
    // https://sigel.aneel.gov.br/arcgis/services/PORTAL/Camadas/MapServer/WmsServer?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=0%2C7&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&STYLES=&BBOX=-7514065.628545966%2C-2504688.542848654%2C-5009377.08569731%2C1.3969838619232178e-9
    /*
    // camada USINAS HIDRELETRICAS ANEEL
    var usinasHidreletricas = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url:'https://sigel.aneel.gov.br/arcgis/services/PORTAL/Camadas/MapServer/WmsServer',
            params: { 'LAYERS': '0,10' },
            serverType: 'mapserver',
            crossOrigin: 'anonymous'
        }),
        visible: false,
        id: 'usinas_hidreletricas',
        name: 'Usinas Hidreléticas',
        type: 'wms',
        zIndex: 1
    });
    this.addLayer(usinasHidreletricas);
     */

   // camada overlay do POPUP
   // site: https://openlayers.org/en/latest/examples/popup.html
    this.overlayPopup = new ol.Overlay({
        //element: $('<div id="overlay-' + this.el + '" style="z-index:1;max-height:500px;max-width:400px;height:auto;overflow:hidden;overflow-y:auto;background-color: white; border-radius: 5px; border: 1px solid black; padding: 5px 10px;">').appendTo('body')[0],
        autoPan:true,
        autoPanAnimation: {
            duration: 500,
        },
        // fazer a paginação de itens dentro da popup
        currentIndex:0,
        totalItens:0,
        itens:[],
        element: $('<div id="popup-'+this.el+'" class="ol3-popup">'+
            '<a href="javascript:void(0);" id="popup-closer" class="ol3-popup-closer"></a>'+
            '<div id="popup-content" class="ol3-popup-content"></div>'+
            '</div>').appendTo('body')[0],
        //positioning: 'center-center'
    });



    /*
    // capturar o evento loaded do mapa
    this.layers[0].getSource().on('tileloadstart', function(event) {
        console.log(self.el+' iniciada carga do mapa...')
    });
    this.layers[0].getSource().on('tileloadend', function(event) {
        console.log(self.el+' mapa base carregado.')
    });
    this.layers[0].getSource().on('tileloaderror', function(event) {
       console.log(self.el+' mapa base carregado com erro.')
    });
    */

    this.center = [-6019690.043300373, -2814310.6931742462]; // Brasil
    this.zoom = 4;


    this.interactionMouseWheelZoom; // interação para aplicar zoom com a roda do mouse + telcla CTRL
    this.mouseWhellAlert = false;

    this.drawLayer; // camada para fazer os desenhosv
    this.eooLayer; // camada para fazer o calculo da Extensão de Ocorrência - EOO
    this.aooLayer; // camada para fazer a grade 2x2km para calculo da Area de ocupação (AOO)
    this.estadosCentroideLayer; // camada para exibir o Estado rachurado quando possuir apenas um ponto e for um centroide
    this.interactionSelect; // interação para selecionar um desenho
    this.interactionTranslate; // interatividade de arrastar
    this.currentDrawInteraction; // modo de desenho que está selecionado no momento
    this.hiddenStyle = new ol.style.Style({ image: '' });
    this.contextMenu = null;
    this.drawing = false;
    this.clickedFeature = null;

    /**
    Método para inicialização do mapa na página
    */
    this.run = function() {

        var self = this;
        // não permitir criar o mapa mais de uma vez
        if (self.map) {
            return;
        }

        // filtrar layer ocorrencias
        if( ! self.showAdicionadosAposAvaliacao ) {
            occurrenceLayers = occurrenceLayers.filter(function(layer){
                return layer.id != 'regConfirmadoAdicionadoAposAvaliacao';
            })
        }


        // Camada para  desenhos e animate de identificação no mapa
        self.drawLayer = drawLayer = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: self.drawStyle,
            visible: true,
            type: 'vector',
            name: 'Desenhos', // nome utilizado no cadastro de ocorrencia para desenha a marker ao informar uma coordenada.
            id: 'drawlayer',
            filter: true, // retirar depois, é só para testar o filtro por intercecção
            zIndex: 4
        });
        self.addLayer(drawLayer);// adicionar o layer para desenhos e animate de identificação

        // Camada para cálculo da EOO
        if ( self.showEoo ) {
            self.eooLayer = new ol.layer.Vector({
                source  : new ol.source.Vector(),
                style   : self.drawStyle,
                id      : "calcEOOlayer",
                name    : 'EOO',// aparecer no layer switch
                visible :  true,
                type    : 'vector',
                filter  : false,
                hint    : 'Extensão de Ocorrência',
                zIndex  : 5
            });
            self.addLayer(self.eooLayer);
            // carregar a EOO salva
            self.eooLayer.on('change:visible', function(e){
                if( e.target.getVisible() && !self.eooFeature ) {
                    self.loadEoo();
                }
            });
        }

        // Camada para cálculo da AOO
        if (self.showAoo) {
            var url = urlSalve + 'api/layer/aooFicha/' + self.sqFicha
            self.aooLayer = self.createLayer({
                type: 'geojson'
                , name: 'AOO'
                , visible: true
                , url: url
                , style: self.drawStyle
                //, color: '#714b4b'
                , filter: false
                , zIndex: 3
                , hint: 'Área de Ocupação'
            });
            this.aooLayerChange = function (evt) {
                //console.log('AOO LAYER CHANGE ' + self.aooLayer.getSource().getState());
                if (self.aooLayer.getSource().getState() == 'ready') {
                    self.updateAooValue();
                    //console.log('AOO LAYER LOADED');
                    // CONVERTER O MULTIPOLYGON EM POLYGONS
                    var polygons = [];
                    self.aooLayer.getSource().getFeatures().map(function (feature, i) {
                        if (feature.getGeometry().getType().toLowerCase() == 'multipolygon') {
                            feature.getGeometry().getPolygons().map(function (p) {
                                polygons.push(p);
                            })
                            self.aooLayer.getSource().removeFeature(feature);
                        }
                    });
                    // recriar os squares no layer AOO
                    if (polygons.length > 0) {
                        // desabilitar o evento change durante a criacao dos poligonos
                        self.aooLayer.getSource().un('change', self.aooLayerChange);
                        polygons.map(function (p) {
                            var coordinates = p.getCoordinates();
                            var feature = new ol.Feature({
                                geometry: new ol.geom.Polygon(coordinates),
                                id: 'calc-aoo'
                            });
                            self.aooLayer.getSource().addFeature(feature);
                        });
                        self.aooLayer.getSource().on('change', self.aooLayerChange);
                        self.updateAooValue();
                    }
                }
            }
            self.aooLayer.getSource().on('change', self.aooLayerChange);
        }

        // criar as camadas salve e portalbio ( clusters )
        if( false &&  self.sqFicha ) {
            this.clusters.forEach(function (cluster) {
                var showCluster = true;
                if( self.contexto == 'validacao'){
                    // layers que não devem aparecer no mapa do módulo validação
                     showCluster = ( /salveutilizadas|portalbioUtilizadas|salveHistorico/i.test( cluster.id ) );
                }
                if( showCluster ) {
                    self.createLayer({
                        type: 'geojson'
                        , name: cluster.name
                        , id: cluster.id
                        , visible: cluster.visible
                        , url: cluster.url
                        , style: self.occurrenceStyle
                        , color: cluster.color
                        , filter: cluster.filter
                        , zIndex: cluster.zIndex
                        , cluster: true
                        , title: cluster.title
                    });
                }
            });
        }

        // INTERAÇÕES PADRÃO COM O MAPA
        this.interactionMouseWheelZoom = new ol.interaction.MouseWheelZoom();

        this.interactionMouseWheelZoom.handleEvent = function(e) {
            var type = e.type;
            if ( type !== "wheel" && type !== "wheel" ) {
                return true;
            }
            if ( e.originalEvent.ctrlKey || e.originalEvent.metaKey ) {
                return true
            }

            if( ! self.mouseWhellAlert ) {
                self.mouseWhellAlert=true;
                var msg = '<b>Pressione a tecla CTRL ou CMD para aumentar/diminuir o zoom do mapa ao utilizar a roda do mouse.</b>';
                self.alert( msg,10,'warning')
            }
            return false;
            //oldHandleEvent.call(this,e);
        };

        // criar as interações para possibilitar selecionar e arrastar
        this.interactionSelect = new ol.interaction.Select({style: this.selectedStyle,
            filter:function(layer) {
                // não permitir que a ferramenta de desenho selecione feature da camada de calculo da AOO / EOO
                return  ! layer.get('bd')
                        && (layer.get('id')
                        //&& ! (/eoo|aoo/i.match( layer.get('id') ) )
                    );
            },
        });

        this.interactionTranslate = new ol.interaction.Translate({
            features: this.interactionSelect.getFeatures(),
            layers: function (layer) {
                return (layer.get('id') && layer.get('id').toLowerCase() == 'drawlayer');
            }
        });

        this.interactionTranslate.on('translateend', function (evt) {
            evt.features.forEach(function (feat) {
                if( self.filters.length > 0 ){
                    if( self.filters[0].polygon == feat ){
                        self.updateFilter();
                    }
                }
            })
        });

        this.interactionSelect.getFeatures().on('remove', function (event) {
            if (!self.drawing) {
            }
        });

        this.interactionSelect.getFeatures().on('add', function (event) {
            var feature = event.element;
        });

        // criação do mapa
        self.map = new ol.Map({
            layers: self.layers,
            target: self.el,
            controls: ol.control.defaults({
                attributionOptions: ({collapsible: true})
            }).extend([
                new ol.control.FullScreen()
                , new ol.control.ScaleLine()
                , new ol.control.ZoomSlider()
                , new ol.control.MousePosition({
                    projection: 'EPSG:4326',
                    //coordinateFormat: ol.coordinate.createStringXY(6),
                    coordinateFormat: function( coordinate ) {
                        return ol.coordinate.format(coordinate, 'y {y}, x {x}', 6);
                    },
                    undefinedHTML: 'Lat:? Lon:?'
                })
                //,new generateEditToolbar({ self:self })
                //,new generateLayerSwitch({ self:self })
                //,new generateDialogAddWms({ self:self })
            ]),
            interactions: ol.interaction.defaults().extend([ self.interactionMouseWheelZoom
                , self.interactionSelect
                , self.interactionTranslate]),
            view: new ol.View({
                center: self.center,
                zoom: self.zoom,
                projection: new ol.proj.Projection({
                    code: 'EPSG:3857',
                    units: 'm'
                }),
            })
        });

        // adicionar o overlay popup das informações do ponto clicado
        self.map.addOverlay( self.overlayPopup );
        $(self.overlayPopup.getElement()).find("a#popup-closer").off('click').on('click',function(e){
            e.preventDefault();
            self.overlayPopup.setPosition(undefined);
            self.clickedFeature=null;
            // colocar o mapa na posição original antes da abertura do popup
            if( self.overlayPopup.currentMapPosition ) {
                var degrees = ol.proj.transform(self.overlayPopup.currentMapPosition, 'EPSG:3857', 'EPSG:4326');
                self.overlayPopup.currentMapPosition=null;
                self.overlayPopup.coord=null;
                self.centerMap(degrees[0],degrees[1]);
            }
            //self.showPopup(null);
            return false;
        });



        // evento executado uma únca vez ao carregar o mapa
        self.map.once('postrender', function (event) {

            if( self.showEoo && !self.eooFeature ) {
                self.loadEoo();
            }
            // aplicar css padrão no mouse position
            $('#' + self.el + ' .ol-mouse-position').css({
                'padding': '3px',
                'font-size': '16px',
                'background-color': '#7c99bd',
                'color': '#fff',
                'min-width': '140px',
                'max-height': '30px',
                'border-radius': '4px',
                'text-align': 'center',
                'top': '10px',
                'bottom': 'auto',
                'left': 'auto',
                'right': '47px',
                'opacity': '0.7'
            });
            // atualizar os inputs no layer switcher
            self.addCustomControls(); // toolbar, layer switcher, add wms
            self.updateLayerSwitch();
            if( typeof(Geocoder) != 'undefined') {
                // adicionar geocoder
                self.geocoder = new Geocoder('nominatim', {
                    provider: 'osm',
                    //key: '__some_key__',
                    lang: 'pt-BR', //en-US, fr-FR
                    countrycodes: 'br',
                    placeholder: 'Informe a localidade e pressione <Enter>.',
                    targetType: 'glass-button',//'text-input',
                    limit: 15,
                    keepOpen: false,
                    preventDefault: true // não criar a marker padrão
                });
                // ao selecionar uma localidade
                self.geocoder.on('addresschosen', function (evt,o) {
                    self.map.getView().setCenter(evt.coordinate);
                    self.addMarker(evt.coordinate[0],evt.coordinate[1], evt.address.details.name + ' / ' + evt.address.details.state, 20)
                    self.setZoom( 16 );
                });
                self.map.addControl(self.geocoder);
                $("#gcd-button-control").attr('title', 'Encontrar local pelo nome ou parte do nome!');
                $("#gcd-button-control").parent().find('input').addClass('ignoreChange');
            }
        });


        // ao passar o mouse sobre alguma feature
        self.map.on('pointermove', function (event) {
            if (event.dragging) {
                return;
            }
            var pixel = self.map.getEventPixel(event.originalEvent);
            var hit = self.map.hasFeatureAtPixel(pixel);
            /*
            // se precisar saber qual layer está a feature encontrada
            var hit = map.forEachFeatureAtPixel(pixel, function (feature, layer) {
                return true;
            });
            */
            self.map.getViewport().style.cursor = ( hit ? 'pointer' : '');
        });

        // adicionar os eventos padrão ao mapa
        /*self.map.getView().on('propertychange', function(e) {
            switch (e.key) {
                case 'resolution':
                    self.filteredIds = [];
                    break;
            }
        });*/

        // ao clicar uma vez no mapa
        self.map.on('singleclick', function (event) {
            if (self.drawing || self.sketch ) {
                return false;
            }
            // se for linha não precisa detectar pontos abaixo ou dentro dela
            var otherFeature = null;
            var featureDeleted=false;
            var ctrlKeyPressed = ( event.originalEvent.ctrlKey || event.originalEvent.metaKey );

            // na tela de cadastro de ponto não mostrar popup com as informações da camada ou clicar no mapa
            if( ctrlKeyPressed && self.el=="mapTabOcorrencia"){
                return true;
            }

            // esconder o popup aberto
            var popup = $( self.overlayPopup.getElement() );
            var popupContent = popup.find('div#popup-content');
            var layerClicked=null
            popupContent.html('');
            popup.hide();
            // colocar o mapa na posição original antes da abertura do popup
            if( self.overlayPopup.currentMapPosition ) {
                var degrees = ol.proj.transform(self.overlayPopup.currentMapPosition, 'EPSG:3857', 'EPSG:4326');
                self.overlayPopup.currentMapPosition=null;
                self.overlayPopup.coord=null;
                self.centerMap(degrees[0],degrees[1]);
            }

            self.clickedFeature = this.forEachFeatureAtPixel(event.pixel, function (feature, layer) {
                    layerClicked=layer;
                    if (feature.getGeometry().getType().toLowerCase() == 'point') {
                            return feature;
                    }
                    else if( !otherFeature && feature.get('PROFUNDIDA') && !feature.get('text') )
                    {
                        feature.set('text','Profundidade: ' + feature.get('PROFUNDIDA'));
                    }
                    else if( !otherFeature && feature.get('PROFUNDIDADE') && !feature.get('text') )
                    {
                        feature.set('text','Profundidade: ' + feature.get('PROFUNDIDADE'));
                    }
                    else if( !otherFeature && feature.get('etnia_nome') && !feature.get('text') )
                    {
                        var metadados = [{'name':'terrai_nom'   ,'label':'Nome:'}
                                       , {'name':'etnia_nome'   ,'label':'Etnia:'}
                                       , {'name':'fase_ti'      ,'label':'Fase:'}
                                       , {'name':'modalidade'   ,'label':'Modalidade:'}
                                       , {'name':'municipio_'   ,'label':'Municipio:'}
                                       , {'name':'superficie'   ,'label':'Superficie:'}
                                       , {'name':'undadm_nom'   ,'label':'Und. Adm:'}
                                       , {'name':'undadm_sig'   ,'label':'Sigla:'}
                        ];
                        var itens = [];

                        $.each( metadados, function(index,item ) {
                            itens.push( item.label+' <b>'+feature.get( item.name )+'</b>' );
                        });
                        itens.push('<div style="margin-top:5px; color:gray;border-top:1px solid black;font-size:10px;font-weight: bold;">Fonte: <a href="http://www.funai.gov.br/index.php/shape" target="_blank">Funai</a></div>');

                        feature.set('text','<div style="margin-bottom:5px;border-bottom:1px solid black;font-size:16px;font-weight: bold;">Terra Indigena</div>'+itens.join('<br>'));
                    } else if( layer.get('id') == 'aooLayer' ) {
                        if ( event.originalEvent.ctrlKey || event.originalEvent.metaKey ) {
                            // permitir remover o square somente se ele não tiver ponto dentro.
                            //if ( ! feature.get('hasOccurrence') ) {
                                self.aooLayer.getSource().removeFeature(feature);
                                featureDeleted = true;
                            //}
                        }
                    } else if( !otherFeature && layer.get('id') == 'calcEOOlayer' ) {
                        if ( event.originalEvent.ctrlKey || event.originalEvent.metaKey ) {
                            if ( feature != self.eooFeature ) {
                                if (feature.get('selected')) {
                                    feature.set('selected', false);
                                    feature.setStyle(self.featureUnSelectedStyle);
                                } else {
                                    feature.set('selected', true);
                                    feature.setStyle(self.featureSelectedStyle);
                                }
                                otherFeature = feature;
                                setTimeout(function(){
                                    self.calcularEooShp();
                                    self.cancelDrawInteraction();
                                },100);

                            }
                        } else if(feature.getGeometry().getType() == 'Polygon' ) {
                            if( feature.get('id') == 'calc-eoo-pontos') {
                                otherFeature = feature;
                            }
                        }

                        // não exibir pop-up quando clicar sobre o poligono da hydroshead
                        /*else {
                            if ( ! feature.get('text') ) {
                                self.setTextFromProperties(feature);
                            }
                        }*/
                    }
                    if( !otherFeature && feature.get('text') ){
                        otherFeature = feature;
                    //} else if( !otherFeature && layerClicked.get('id') == 'estadosCentroide' ){
                    } else if( ! otherFeature && feature.get('type') == 'centroide' ) {
                        otherFeature = feature;
                    } else {
                        // não permitir selecionar o poligono da EOO
                        if ( feature.get('name') == 'EOO' || feature.get('id') == 'calc-eoo' || feature.get('id') == 'calc-aoo' ) {
                            layerClicked=null;
                            return null;
                        }
                    }
                });
            // se não for clicado em cima de um ponto retornar qualquer outra feature
            if( !self.clickedFeature && otherFeature ) {
                if( ( !self.editingAoo && !self.editingAoo ) || ! ctrlKeyPressed ) {
                    self.clickedFeature = otherFeature;
                }
            }

            //if( self.clickedFeature && ( self.clickedFeature.N.features instanceof Array ) ) {
            // verificar se o ponto é um cluster e pegar o primeiro ponto
            /*
            if( self.clickedFeature && self.clickedFeature.get('features') && ( self.clickedFeature.get('features').length > 0 ) ) {
                self.clickedFeature = self.clickedFeature.N.features[0];
            }
            */

            // se foi clicado no mapa e a layer de cálculo da AOO estive visivel, desenhar o square do ponto clicado
            if( ! self.clickedFeature ) {
                if( ! featureDeleted && self.editingAoo && self.aooLayer.getVisible() ){
                    if ( ctrlKeyPressed ) {
                        var coords = event.coordinate;
                        var feature = self.createSquare(coords[0], coords[1], false);
                        if ( feature ) {
                            // feature criada sem ter uma ocorrencia dentro
                            feature.set('hasOccurrence', false);
                        } else {
                            self.deleteSquare(coords[0], coords[1],false);
                        }
                    }
                }
            }

            // remover depois
            /*if (self.clickedFeature) {
                console.log(self.clickedFeature.get('id'))
            } else {
                console.log('Nenhuma feature detectada neste ponto')
            }
            */
            if( layerClicked ) {
                if( layerClicked.get('id') != 'calcEOOlayer') {
                    self.showPopup( event );
                }

                if( layerClicked.get('id') == 'estadosCentroide' ) {
                    // evitar que a ferramenta draw selecione o mapa clicado
                    layerClicked = false
                    otherFeature = false;
                }
            }
            if( otherFeature ){
                return true;
            }

            return ( ! self.clickedFeature && (layerClicked || ctrlKeyPressed) ? true : false );
        });

        self.map.on('moveend', function (evt) {
            // identificar ponto selecionado se houver
            self.identifyPoint();
        });

        /**
         * processar os filtros ativos e atualizar os ids selecionados.
         */
        this.updateFilter = function() {
            //console.log('atualizando filtro..');
             // array populado na funcao  self.occurrenceStyle ao pintar o ponto no mapa
            if( this.filters.length == 0 && this.filtersMetadata.length == 0) {
                 if( this.filteredIds.length > 0 ){
                     self.refresh();
                 }
                this.filteredIds = [];
                return false;
            }
            /*
            // ler todas as features sem o style hidden
            for( keyLayer in self.layers ) {
                var layer = self.layers[keyLayer];
                var options = layer.get('options');
                if (options && options.filter && layer.getVisible()) {
                    layer.getSource().changed();
                }
            }*/
            for( keyLayer in self.layers ) {
                var layer = self.layers[keyLayer];
                var options = layer.get('options');
                var layerId = layer.get('id') || '';
                // atualizar os estilos dos pontos
                if (options && options.filter && layer.getVisible()) {
                    //layer.getSource().changed();
                    var layerFeatures
                    if( layer.get('options').cluster ){
                        layerFeatures = layer.getSource().getSource().getFeatures()
                    } else {
                        layerFeatures = layer.getSource().getFeatures()
                    }
                    layerFeatures.forEach(function (featureFound) {
                        if (self.isFeatureInFilter(featureFound)) {
                            /*if (featureFound.get('features')) {
                                // pontos com cluster
                                featureFound.get('features').forEach(function (feat) {
                                    var featId = feat.get('id') || feat.get('id_registro') || feat.getId();
                                    if ( featId ) {
                                        self.filteredIds.push({
                                            id: featId
                                            , bd: feat.get('bd') || feat.get('no_base_dados')
                                            , layerId: layerId
                                        });
                                    }
                                });
                            } else {*/
                                // ponto sem cluster
                                if (featureFound.get('id')) {
                                    self.filteredIds.push({
                                        id: featureFound.get('id')
                                        , bd: featureFound.get('bd')
                                        , layerId: layerId
                                    });
                                }
                            //}
                        }
                    });
                }
            }
            // centralizar no poligono do filtro e atualizar os pontos no map
            // senão tem que executar o self.refresh() para atualizar os pontos no mapa
            if( this.filters.length > 0 ) {
                var extent = this.filters[0].polygon.getGeometry().getExtent();
                var center = ol.proj.transform(ol.extent.getCenter(extent), 'EPSG:3857', 'EPSG:4326');
                if (fitFilterPolygonAfterDraw) {
                    self.map.getView().fit(extent, self.map.getSize());
                    fitFilterPolygonAfterDraw = false;
                }
                self.flyTo(ol.proj.fromLonLat(center), function () {});
            } else {
                self.refresh();
            }
            /*
            // verificar quais feature estão dentro da área desenhada
            for( keyLayer in self.layers )
            {
                var layer = self.layers[keyLayer];
                var options = layer.get('options');

                if( options && options.filter && layer.getVisible() )
                {
                    var layerId = layer.get('id') || '';
                    // ler todos os pontos do layer
                    var features = [];
                    var options = layer.get('options');
                    features = layer.getSource().getFeatures()
                    features.forEach( function(featureFound ) {
                        if( self.filters.length > 0  )
                        {
                            var coords = featureFound.getGeometry().getCoordinates();
                            self.filters.map( function( filtro,i )
                            {
                                var filterGeometry = Object.values( filtro )[0].getGeometry();
                                if ( filterGeometry.intersectsCoordinate( coords ) ) {
                                    //featureFound.setStyle(self.occurrenceStyle);
                                    // verificar se o ponto é um cluster
                                    if( featureFound.get('features') ){
                                        featureFound.get('features').forEach(function( feat ){
                                            if (feat.get('id')) {
                                                self.filteredIds.push({id: feat.get('id')
                                                    , bd: feat.get('bd')
                                                    , layerId: layerId
                                                });
                                            }
                                        });
                                    } else {
                                        if (featureFound.get('id')) {
                                            self.filteredIds.push({id: featureFound.get('id')
                                                , bd: featureFound.get('bd')
                                                , layerId: layerId
                                            });
                                        }
                                    }
                                }
                                else {
                                    //featureFound.setStyle(self.hiddenStyle);
                                }
                            })
                        }
                        else
                        {
                            //featureFound.setStyle(null);
                            //if( featureFound.getId() ) {
                            //    self.filteredIds.push({id:featureFound.getId(), bd:featureFound.get('bd') } );
                            //}

                        }
                    });
                }

            }
            */

            // mostrar/esconder as linhas do grid
            this.filterGrid();
            return true;
        };

        // adicionar o menu de contexto
        // https://github.com/jonataswalker/ol3-contextmenu
        if( typeof( ContextMenu ) != 'undefined') {
            this.contextMenu = new ContextMenu({
                width: 'auto',
                defaultItems: false, // defaultItems are (for now) Zoom In/Zoom Out
                items: []
            });
            /**
             * ativar o menu de contexto ao clicar com o botão direito do mouse em um desenho
             */
            this.map.addControl(this.contextMenu);

            // exibir o menu de contexto somente se clicar sobre um desenho
            this.contextMenu.on('beforeopen', function (evt) {
                var layer = null;
                var isCluster=false;
                var isPoint  = true;
                var isEstadoCentroide = false;
                var feature = evt.target.getMap().forEachFeatureAtPixel(evt.pixel, function (ft, l) {
                    layer = l;
                    return ft;
                });

                //self.drawLayer.getSource().addFeature(feature);
                self.contextMenu.data = {feature: null, ids: []}
                if (!feature) {
                    self.contextMenu.disable();
                } else {
                    isEstadoCentroide = layer.get('id') == 'estadosCentroide'

                    // não exibir menu de contexto para linhas de medição de distância ( measure )
                    if (feature.get('measure')) {
                        self.contextMenu.disable();
                        return;
                    }
                    //var type = feature.getGeometry().getType().toLowerCase();
                    self.contextMenu.clear();
                    self.contextMenu.data.feature = feature;

                    // guardar o(s) id(s) na varialve data do menu ao abrir para envio via ajax
                    if( isEstadoCentroide ){
                        self.contextMenu.data.ids = [{id: feature.get('sqOcorrencia')
                            , bd: feature.get('origem')
                            , sistema: feature.get('sistema')
                            , nome : feature.get('nome')
                            , sigla: feature.get('sigla')
                        }];
                    }
                    else if ( feature.getGeometry().getType().toLowerCase() == 'point' ) {

                        // verificar se é um ponto do cluster
                        if( feature.get('features') ) {
                            isCluster=true;
                            self.contextMenu.data.feature=null
                            feature.get('features').forEach(function( feat ){
                                var featureId = feat.get('id') || feat.get('id_registro') || feat.getId();
                                if ( featureId ) {
                                    // ler o primeiro ponto da lista ( o que foi clicado )
                                    if( ! self.contextMenu.data.feature ) {
                                        self.contextMenu.data.feature = feat; // utilizar uma das features
                                    }
                                    var bd = feat.get('bd') || feat.get('no_base_dados')
                                    self.contextMenu.data.ids.push( {id: featureId, bd: bd });
                                }
                            })
                            feature = self.contextMenu.data.feature;
                        } else {
                            if ( feature.get('id') ) {
                                var bd = feature.get('bd') || feature.get('no_base_dados')
                                self.contextMenu.data.ids = [{id: feature.get('id'), bd: bd }];
                            }
                        }
                    } else {
                        isPoint=false; // clicou com o botão direito do mouse sobre um poligono
                        // selecionar somente os ids das camadas que estiverem visiveis
                        self.contextMenu.data.ids = self.filteredIds;
                        if( self.filteredIds.length > 0 ) {
                            var mapLayers={}
                            self.map.getLayers().getArray().map( function(layer){
                                var options = layer.get('options');
                                if( options && options.filter  ) {
                                    mapLayers[ layer.get('id') ] = layer.getVisible();
                                }
                            });
                            self.contextMenu.data.ids = self.contextMenu.data.ids.filter(function (item) {
                                return mapLayers[item.layerId];
                            });
                        }
                    }
                    // filtrar as opções de menu de acordo com o tipo da feature clicada
                    var filteredItems = self.contextMenuItems.filter(function (item, i) {
                        var name = item.name;
                        var mostrar = false;
                        var bd = feature.get('no_base_dados') || feature.get('bd');
                        //console.log( name );
                        // nenhum id selecionado pelo filtro
                        if ( self.contextMenu.data.ids.length == 0 ) {
                            mostrar = item.featureType.toLowerCase() == 'polygon';
                        } else {
                            mostrar = item.featureType.toLowerCase() != 'polygon';
                        }

                        if ( mostrar ) {
                            if (name == 'CALCULAR_AREA' && feature.getGeometry().measureTooltipElement ) {
                                mostrar = false;
                            }

                            if( name =='LOCALIZAR_GRIDE' && ( self.contextMenu.data.ids.length > 1 || ! isPoint ) ) {
                                mostrar = false;
                            }
                            if( name == 'TRANSFERIR_REGISTRO' ) {
                                //mostrar = self.contextMenu.data.ids.length > 0;
                                var utilizadoAvaliacao = feature.get('utilizadoAvaliacao') || feature.get('in_utilizado_avaliacao') || '';
                                if( ( isPoint && feature.get('utilizadoAvaliacao') && /^s/i.test( utilizadoAvaliacao ) ) ) {
                                    mostrar = false;
                                }
                            }

                            if( isEstadoCentroide ){
                                if ( /LOCALIZAR_GRIDE|TRANSFERIR_REGISTRO/.test(name) ) {
                                    mostrar = true;
                                } else {
                                    mostrar = false;
                                }
                            }
                        }

                        if (name == 'MARCAR_DESMARCAR_REGISTRO_HISTORICO') {
                            //if( feature.get('bd') && feature.get('bd') != 'salve') {
                            // permitir somente um ponto de cada vez
                            /*if (!bd || bd != 'salve') {
                                mostrar = false;
                            }*/
                        }
                        if( name == 'CALCULAR_AOO_FEATURE'){
                            if( ! self.editingAoo || ! self.aooLayer.getVisible() ) {
                                mostrar=false;
                            }
                        }

                        return mostrar

                        //return item.featureType.toLowerCase() == type || ! item.featureType
                    });
                    if (filteredItems.length > 0) {
                        self.contextMenu.extend(filteredItems);
                        self.contextMenu.enable();
                    } else {
                        self.contextMenu.disable();
                    }

                }
            });
        }

        // criar as opções do menu de contexto ao clicar sobre um ponto ou desenho com o mouse direito
        this.addContextMenuItem('Polygon','fa-filter','Filtrar registros','',function (e) {
                var tempObj = {}
                var id = 'polygon';//self.contextMenu.data.feature.getId() || 'shp';
                if( id ) {
                    tempObj[ id ] = self.contextMenu.data.feature;
                    self.filters.push(tempObj);
                    // atualizar os pontos em função dos filtros ativos em self.filters
                    self.updateFilter();
                }
            },'FILTRAR_AREA');

        // criar as opções do menu de contexto para medir area do poligono
        this.addContextMenuItem('Polygon','fa-object-ungroup','Calcular a área','',function (evt) {
              self.calcArea( self.contextMenu.data.feature, evt );
            },'CALCULAR_AREA');


        /**/
        // criar menu de contexto para adicionar squares na AOO
        // aoo
        if ( self.showAoo ) {
             this.addContextMenuItem('Polygon', 'fa-object-group', 'Calcular AOO', '', function (evt) {
                 var feature = self.contextMenu.data.feature;
                 var area = self.formatArea(feature.getGeometry());
                 var areaFloat = parseFloat( area.split(' ')[0].replace(/[^0-9]/g,'') );
                 if (areaFloat >= 3000) {
                     self.alert('Para o cálculo da AOO, a área deve ser menor que 3.000 km<sup>2</sup><br>Área atual = ' + area, 7);
                     return;
                 }
                 app.blockUI('Calculando a AOO.<br>Dependendo do tamanho da área, esta operação<br>poderá levar alguns minutos. Aguarde...', function () {
                     setTimeout(function () {
                         self.createSquareUsingFeature(self.contextMenu.data.feature, evt);
                         app.unblockUI();
                         if( self.zoom < 10 ) {
                             self.map.getView().fit( self.aooLayer.getSource().getExtent(), self.map.getSize() );
                         }
                     }, 2000)
                 });
             }, 'CALCULAR_AOO_FEATURE');
         }
        /**/

        if( ! this.readOnly ) {
            this.addContextMenuItem('', 'fa-chain', 'Utilizar na avaliação', '', function (e) {
                //console.log('Atualizar pontos para utilizados na avaliação.');
                //var inUtilizadoAvaliacao= self.contextMenu.data.feature.get('noLayer') =='regUtilizados'  ? 's' :'n';
                var inUtilizadoAvaliacao = self.contextMenu.data.feature.get('inUtilizadoAvaliacao');
                self.contextMenuAction({action:'UTILIZAR_AVALIACAO', ids:self.contextMenu.data.ids,inUtilizadoAvaliacao:inUtilizadoAvaliacao } )
                /*self.ajaxAction({action:'utilizarNaAvaliacao',sn:'S',ids:self.contextMenu.data.ids}, function(){
                    //self.refresh(); // atualizar mapa
                    self.reloadLayers(); // recarregar os pontos
                });*/
            },'UTILIZAR_AVALIACAO');


            this.addContextMenuItem('', 'fa-chain-broken', 'Não utilizar na avaliação', '#ff0000', function (e) {
                //console.log('Atualizar pontos para NÃO utilizados na avaliação.');
                //var inUtilizadoAvaliacao= self.contextMenu.data.feature.get('noLayer') =='regUtilizados'  ? 's' :'n';
                var inUtilizadoAvaliacao = self.contextMenu.data.feature.get('inUtilizadoAvaliacao');
                var textoNaoUtilizado    = self.contextMenu.data.feature.get('txNaoUtilizadoAvaliacao') || '';
                var sqMotivoNaoUtilizado = self.contextMenu.data.feature.get('sqMotivoNaoUtilizadoAvaliacao') || '';
                self.contextMenuAction( { action:'NAO_UTILIZAR_AVALIACAO'
                    ,inUtilizadoAvaliacao : inUtilizadoAvaliacao
                    ,textoNaoUtilizado      :textoNaoUtilizado
                    ,sqMotivoNaoUtilizado   :sqMotivoNaoUtilizado
                    ,ids                    :self.contextMenu.data.ids
                });
            },'NAO_UTILIZAR_AVALIACAO');

            this.addContextMenuItem('', 'fa-check', 'Marcar/Desmacar como registro histórico', '', function (e) {
                //console.log('Atualizar pontos para utilizados na avaliação.');
                self.contextMenuAction({action:'MARCAR_DESMARCAR_REGISTRO_HISTORICO',ids:self.contextMenu.data.ids});
                /*self.ajaxAction({action:'marcarDesmarcarRegistroHistorico',ids:self.contextMenu.data.ids}, function(){
                    //self.refresh(); // atualizar mapa
                    self.reloadLayers(); // recarregar os pontos
                });*/
            },'MARCAR_DESMARCAR_REGISTRO_HISTORICO','Alternar SIM/NÃO o valor do campo &quot;Presença atual na coordenada&quot;');

            this.addContextMenuItem('', 'fa-flag', 'Marcar/Desmacar como sensível', '#4884f1', function (e) {
                //console.log('Atualizar pontos para utilizados na avaliação.');
                self.contextMenuAction({action:'MARCAR_DESMARCAR_REGISTRO_SENSIVEL',ids:self.contextMenu.data.ids});
                /*self.ajaxAction({action:'marcarDesmarcarRegistroSensivel',ids:self.contextMenu.data.ids}, function(){
                    //self.refresh(); // atualizar mapa
                    self.reloadLayers(); // recarregar os pontos
                });*/
            },'MARCAR_DESMARCAR_REGISTRO_SENSIVEL','Marcar/desmarcar o ponto como sensível para publicação.');

            this.addContextMenuItem('', 'fa-trash', 'Excluir ou Marcar como excluído (Portalbio)', '#ff0000', function (e) {
                self.contextMenuAction({action:'EXCLUIR',ids:self.contextMenu.data.ids});
                /*app.confirm('Confirma a exclusão do(s) registro(s)?<br><br><small>obs:registros do SALVE serão excluídos da base de dados e registros do PortalBio serão apenas marcados como excluídos.</small>', function(){
                    self.ajaxAction({action:'excluir',sn:'E',ids:self.contextMenu.data.ids},function(){
                        // self.refresh();// atualizar mapa
                        self.reloadLayers(); // recarregar os pontos
                    });
                });*/
            },'EXCLUIR');

            this.addContextMenuItem('', 'fa-history', 'Restaurar registro excluído (Portalbio)', '#1ba11b', function (e) {
                self.contextMenuAction({action:'RESTAURAR',ids:self.contextMenu.data.ids});
                /*app.confirm('Confirma a recuperação do registros ?.</small><br><small>Obs:válido apenas para os registros do portalbio que foram excluídos.</small>', function(){
                    self.ajaxAction({action:'restaurar',sn:'R',ids:self.contextMenu.data.ids},function(){
                        //self.refresh();// atualizar mapa
                        self.reloadLayers(); // recarregar os pontos
                    });
                    //console.log('Excluir pontos.');
                });*/
            },'RESTAURAR');

            if( self.callbackActions ) {
                this.addContextMenuItem('', 'fa-exchange', 'Transferir registro(s) para outra ficha', '#c2206d', function (e) {
                    //var inUtilizadoAvaliacao= self.contextMenu.data.feature.get('noLayer') =='regUtilizados'  ? 's' :'n';
                    var inUtilizadoAvaliacao = self.contextMenu.data.feature.get('inUtilizadoAvaliacao');
                    self.contextMenuAction({action:'TRANSFERIR_REGISTRO',ids:self.contextMenu.data.ids,inUtilizadoAvaliacao:inUtilizadoAvaliacao});
                    //self.callbackActions( {ids: self.contextMenu.data.ids} , { sqFicha:self.sqFicha, action: 'transferir_registro'} );
                }, 'TRANSFERIR_REGISTRO');
            }
        }

        if( self.gridId ) {
            this.addContextMenuItem('', 'fa-bars', 'Localizar no gride', 'orange', function (e) {
                self.contextMenuAction({action:'LOCALIZAR_GRIDE',ids:self.contextMenu.data.ids});
                //self.findGridRow(self.contextMenu.data.ids[0],true)
            },'LOCALIZAR_GRIDE');
        }
        // fim contextmenu

        // criar as camadas das ocorrências
        this.createLayersOccurrences()

    } // fim run

    this.alert = function( msg, seconds, style ) {
        seconds = seconds || 3;
        seconds = (seconds > 999 ? parseInt(seconds/1000) : seconds );
        seconds = (seconds == 0 ? 3 : seconds );
        style  = style || 'error'; // warning, info, success
        if( typeof app != 'undefined' && typeof app.growl== 'function') {
            app.growl(msg, seconds, '', 'tc','large',style);
        }
        else
        {
            if( typeof notify == 'function')
            {
                notify( msg,null,style, (seconds*1000) );
            }
            else
            {
                alert( msg );
            }
        }
    };

    this.setZoom = function(intZoom) {
        this.zoom = intZoom
        this.map.getView().setZoom(this.zoom);
    };

    this.setCenter = function(x, y, in4326 ) {
        var xy=[x,y];
        if ( in4326 ) {
            xy = ol.proj.transform(xy, 'EPSG:4326', 'EPSG:3857');
        }
        this.map.getView().setCenter(xy);
    };


    // gerador de estilos para desenhos ( cor, borda, preenchimento, transparência etc)
    this.drawStyle = function(feature) {
        var style = null;
        var featureId = feature.get('id');
        // ler o id da feature das duas maneiras
        featureId = ( featureId ? featureId : feature.getId() );
        switch( featureId ) {
            case 'calcEOOlayer':
            case 'calc-eoo':
            case 'calc-eoo-pontos':
                style = new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: self.makePattern('rgba(0, 0, 255, 0.5)')
                    }),
                    stroke: new ol.style.Stroke({
                        color: '#094696',
                        width: 1
                    })
                });
                break;

            /*case 'calc-aoo':
                style = new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: self.makePattern('rgba(255, 0, 0, 0.5)')
                    }),
                    stroke: new ol.style.Stroke({
                        color: '#be507e',
                        width: 1
                    })
                });
                break;*/
            default :
                style = new ol.style.Style({
                    // cor preenchimento
                    fill: new ol.style.Fill({
                        color: 'rgba(100, 255, 255, 0.2)'
                    }),
                    // cor da borda
                    stroke: new ol.style.Stroke({
                        color: '#094696',
                        width: 1
                    }),

                    // cor e preenchimento quando o desenho for um ponto
                    image: new ol.style.Circle({
                        radius: 7,
                        fill: new ol.style.Fill({
                            color: '#ffcc33'
                        })
                    })
                });
        }
        return style;
    };

    // estilo do desenho selecionado
    this.selectedStyle = new ol.style.Style({
        // cor preenchimento
        fill: new ol.style.Fill({
            color: 'rgba(100, 255, 255, 0.2)'
        }),
        // cor da borda
        stroke: new ol.style.Stroke({
            color: '#ff0000',
            width: 2
        }),
        // cor e preenchimento quando o desenho for um ponto
        image: new ol.style.Circle({
            radius: 5,
            fill: new ol.style.Fill({
                color: '#ffcc33'
            })
        })
    });

    // desligar modo desenho
    this.cancelDrawInteraction = function() {

        // toggle se tiver ativa entao desativar e virse versa
        if ( this.currentDrawInteraction ) {
            this.map.removeInteraction(this.currentDrawInteraction);
            this.currentDrawInteraction = null;
            // cancelar a seleção do item se estiver selecionado
            self.interactionSelect.getFeatures().forEach(function (selected_feature) {
                self.interactionSelect.getFeatures().remove(selected_feature);
            });
            return true;
        }
        return false;
    }

    // função generica para adicionar interações de desenho no mapa. Pode ser: Polygon, Circle, Line
    this.addDrawInteraction = function(interactionType) {
        var self = this;

        if (interactionType && /^(Polygon|Circle)$/.test(interactionType)) {

            // limpar desenho anterior
            self.clearAllDraw();
            // desativar modo desenho
            self.cancelDrawInteraction();
            self.currentDrawInteraction = new ol.interaction.Draw({
                source: self.drawLayer.getSource(),
                type: interactionType
            });
            // when a new feature has been drawn...
            self.currentDrawInteraction.on('drawstart', function (event) {
                //console.log('Draw start');
                self.drawing = true;
            });
            self.currentDrawInteraction.on('drawend', function (event) {
                //console.log('Draw end');
                self.drawing = false;
                // criar um id para o desenho para excluí-lo se for necessário
                event.feature.setId(self.uid());
                event.feature.set('id', self.uid());
                event.feature.set('edit', true); // pode ser editada
                self.cancelDrawInteraction(); // retirar modo de desenho ao fechar o poligono
            });
            self.map.addInteraction(self.currentDrawInteraction);

        } else if (interactionType.toLowerCase() == 'measure-area' || interactionType.toLowerCase() == 'measure-distance') {

            // self.createMeasureTooltip();
            if (interactionType.toLowerCase() == 'measure-distance') {
                interactionType = 'LineString'; // Polygon para m2
            }
            else {
                interactionType = 'Polygon'; // Polygon para m2
            }
            self.cancelDrawInteraction();
            self.currentDrawInteraction = new ol.interaction.Draw({
                source: self.drawLayer.getSource(),
                type: interactionType
            });
            self.currentDrawInteraction.on('drawstart', function (event) {
                //console.log('Draw measure start');
                self.sketch = event.feature;
                self.sketch.setId(self.uid());
                self.sketch.set('id', self.uid());
                self.sketch.set('edit', true); // pode ser editada
                self.sketch.set('measure', true); // diferenciar a feature das features de desenho
                self.createMeasureTooltip(self.sketch);
                var tooltipCoord = event.coordinate;
                self.sketchListener = self.sketch.getGeometry().on('change', function (evt) {
                    var geom = evt.target;
                    var output;
                    if (geom instanceof ol.geom.Polygon) {
                        output = self.formatArea(geom);
                        tooltipCoord = geom.getInteriorPoint().getCoordinates();
                    } else if (geom instanceof ol.geom.LineString) {
                        output = self.formatLength(geom);
                        tooltipCoord = geom.getLastCoordinate();
                    }
                    geom.measureTooltipElement.innerHTML = output;
                    geom.measureTooltip.setPosition(tooltipCoord);
                });
            });
            self.currentDrawInteraction.on('drawend', function (event) {
                //console.log('Draw measure end');
                self.sketch = null;
                //self.measureTooltip.feature = event.feature
                self.cancelDrawInteraction(); // retirar modo de desenho ao fechar o poligono
            });
            self.map.addInteraction(self.currentDrawInteraction);
        } else if (interactionType.toLowerCase() == 'area') {
            // limpar desenho anterior
            self.clearAllDraw();
            // a DragBox interaction used to select features by drawing boxes
            self.cancelDrawInteraction();
            //self.currentDrawInteraction = new ol.interaction.DragBox({ condition: ol.events.condition.platformModifierKeyOnly });
            self.currentDrawInteraction = new ol.interaction.DragBox();
            self.currentDrawInteraction.on('boxstart', function () {
                self.drawing = true;
                //console.log('Drag start.')
            });
            self.currentDrawInteraction.on('boxend', function (event) {
                self.drawing = false;
                //console.log('Drag end.');

                // gEvent = event;
                // console.log( event.target );
                // var extent = self.currentDrawInteraction.getGeometry().getExtent();
                // console.log( 'Área do desenho:' + extent )
                // criar o desenho na camada drawLayer
                var feature = new ol.Feature({
                    geometry: event.target.getGeometry()
                });
                feature.setId(self.uid());
                feature.set('id', self.uid());
                feature.set('edit', true); // pode ser editada

                self.drawLayer.getSource().addFeature(feature);
                self.cancelDrawInteraction();
                //verificar quais feature estão dentro da área desenhada
                //self.drawLayer.getSource().forEachFeatureIntersectingExtent(extent, function(feature) {
                //    console.log( feature.get('name') );
                //});
            });
            self.map.addInteraction(self.currentDrawInteraction);
        } else if (interactionType.toLowerCase() == 'edit') {
            self.drawEdit();
        } else if (interactionType.toLowerCase() == 'stop') {
            self.cancelDrawInteraction();
            // remover poligono selecionado se tiver
            self.interactionSelect.getFeatures().forEach(function (selected_feature) {
                self.interactionSelect.getFeatures().remove(selected_feature);
            });
        } else if (interactionType.toLowerCase() == 'delete') {
            self.deleteDraw();
        } else if (interactionType.toLowerCase() == 'addwms') {
            var $div = $("#dlg-add-wms-" + self.el);
            $("#" + self.el).find('.ol-dialog').hide(); // fechar outras janelas
            $div.show({
                duration: 1,
                complete: function () {
                    $div.find('input:first').focus();
                }
            });
        } else if (interactionType.toLowerCase() == 'addshp') {
            var $div = $("#dlg-add-shp-" + self.el);
            $("#" + self.el).find('.ol-dialog').hide(); // fechar outras janelas
            $div.show({
                duration: 1,
                complete: function () {
                    $div.find('input:first').focus();
                }
            });
        } else if (interactionType.toLowerCase() == 'refresh') {
            self.refresh();
        } else if (interactionType.toLowerCase() == 'reload') {
            self.clearAllDraw();
            self.reloadLayers();
        }
        else if (interactionType.toLowerCase() == 'calcular-eoo') {
            //self.calcularEoo();
            self.toggleBoxEoo();
        }
        else if (interactionType.toLowerCase() == 'calcular-aoo') {
            self.toggleBoxAoo();
        } else if (interactionType.toLowerCase() == 'dialogmapfilters') {
            self.toggleDialogoFilterMaps();
        }
    };

    // ativar edição do desenho
    this.drawEdit = function() {
        // desativar o modo de desenho
        if( this.cancelDrawInteraction() )
        {
            return;
        }
        var allFeatures = this.interactionSelect.getFeatures();
        if (allFeatures.getLength() == 0) {
            self.alert('Nenhum desenho selecionado.');
            return;
        }
        var edit = false;
        allFeatures.forEach(function(selected_feature) {
            // remover a copia do desenho da interactionSelect
            if (selected_feature.get('edit')) {
                edit = true;
            }
        });
        if (!edit) {
            self.alert('Polígono não editável.');
            return;
        }

        // adicionar a interação de edição dos desenhos ao mapa
        // create the modify interaction
        this.currentDrawInteraction = new ol.interaction.Modify({
            features: allFeatures,
            // delete vertices by pressing the SHIFT key
            deleteCondition: function(event) {
                return ol.events.condition.shiftKeyOnly(event) && ol.events.condition.singleClick(event);
            },
        });
        self.currentDrawInteraction.on('modifystart', function(event) {
            //console.log('Modify start.');
            //console.log( event );
        });
        self.currentDrawInteraction.on('modifyend', function(event) {
            //var features = event.features.getArray()
            self.updateFilter();
        });

        // add it to the map
        this.map.addInteraction(this.currentDrawInteraction);
    }

    this.deleteDraw = function() {
        var self = this;
        this.cancelDrawInteraction();
        var olCollection = this.interactionSelect.getFeatures();
        var selectedId = null;
        var updateFilters=false;
        // remove all selected features from select_interaction and my_vectorlayer
        olCollection.forEach(function(selected_feature) {
            // remover a copia do desenho da interactionSelect
            selectedId = selected_feature.get('id');
            if( selected_feature.getGeometry().measureTooltip )
            {
                self.map.removeOverlay( selected_feature.getGeometry().measureTooltip );
            } else {
                if (self.filters.length) {
                    if (self.filters[0].polygon.get('id') == selected_feature.get('id')) {
                        self.filters = [];
                        updateFilters=true;
                    }
                }
            }
            olCollection.remove(selected_feature);
        });

        if (selectedId) {
            self.cancelDrawInteraction();
            self.drawLayer.getSource().getFeatures().forEach(function(source_feature) {
                //self.drawLayer.getSource().removeFeature(source_feature);
                if (selectedId === source_feature.get('id') ) {
                    // remove from my_vectorlayer
                    if( source_feature.getGeometry().measureTooltip )
                    {
                        self.map.removeOverlay( source_feature.getGeometry().measureTooltip );
                    }
                    self.drawLayer.getSource().removeFeature(source_feature);

                    // remover do array de filtros a feature excluida
                    /*self.filters = self.filters.filter( function( item,i){
                        return Object.keys(item)[0] != selectedId;
                    })
                    // VOLTAR AQUI
                    self.updateFilter();
                     */
                    /*if (self.filters[ selectedId ]) {
                        delete self.filters[ selectedId ];
                        self.updateFilter()
                    }
                    */

                    // se for excluida uma feature de medição ( measure ), excluir tambem a tooltip com a medida
                    if( source_feature.getGeometry().measureTooltipElement )
                    {
                        source_feature.getGeometry().measureTooltipElement.parentNode.removeChild( source_feature.getGeometry().measureTooltipElement );
                    }
                }
            });
        }
        if( updateFilters ){
            self.updateFilter();
            self.refresh()
        }
    }

    /**
     * remover todos os desenhos - limpar draw layer
     **/
    this.clearAllDraw = function() {
        this.cancelDrawInteraction();
        // limpar os tooltips
        self.map.getOverlays().getArray().slice(0).forEach(function(overlay) {
            // não remover o popu de informacoes do ponto clicado
            if( overlay.getElement().id != 'popup-'+self.el ) {
                self.map.removeOverlay(overlay);
            }
        });
        self.interactionSelect.getFeatures().clear();
        self.drawLayer.getSource().clear();
        if( self.filters.length > 0 ){
            self.filters=[];
            self.updateFilter();
            self.refresh()
            fitFilterPolygonAfterDraw=true;
        }
    }

    /**
     * criar camadas on-line no mapa
     */
    this.createLayer = function(options) {
        var newLayer = null;
        var self=this;
        options = options || {};
        options.id = ( options.id ? options.id : 'layer-' + this.uid() );
        options.layerId = ( options.layerId ? options.layerId : options.id );
        options.visible = options.visible == false ? false : true;
        options.filter = options.filter == true ? true : false; // layer será ignorado para aplicação de filtros
        options.edit   = options.edit == true ? true : false;
        options.zIndex = options.zIndex ? options.zIndex : 3;
        options.title = options.title ? options.title : '';
        options.type  = options.type ? options.type : 'geojson';
        if (!options.name) {
            options.name = 'Nova camada';
        }

        if( options.cluster ) {
            newLayer = new ol.layer.Vector({
                source: new ol.source.Cluster({
                    distance: parseInt(clusterDistance, 10),
                    source: new ol.source.Vector({
                        url:options.url,
                        format: new ol.format.GeoJSON()
                    }),
                }),
                type  : 'vector',
                id    : options.id,
                name  : options.name,
                edit  : options.edit,
                filter: options.filter,
                zIndex: options.zIndex,
                color : options.color,
                style : ( options.style ? options.style : self.occurrenceStyle),
                visible: options.visible
            });
            delete options.style;
            newLayer.set('options',options);
        }
        else if ( options.type.toLowerCase() == 'geojson') {
            if (options.url) {
                if( options.shp ){
                    options.epsg = options.epsg || 'EPSG:3857';
                    var color = options.color = options.color || 'rgba(203,70,19,0.8)';
                    newLayer = new ol.layer.Vector({
                        source: new ol.source.Vector(),
                        style: new ol.style.Style({
                            stroke: new ol.style.Stroke({
                                color: color,
                                width: 1
                            }),
                        }),
                        visible: options.visible,
                        id: options.id,
                        name: options.name,
                        type: 'vector',
                        edit: false,
                        zIndex: options.zIndex,
                    });

                    //self.addLayer( terrasIndigenasLayer );
                    // importar o shp e transformar em GeoJson
                    var urlShp = options.url.replace('/salve-consulta/','/salve-estadual/');
                    urlShp = urlShp.replace(/salve\/\//,'salve/');
                    shp( urlShp ).then( function( geojson ) {
                        var validFeatures = self.getGeojsonValidFeatures( geojson );
                        newLayer.setSource( new ol.source.Vector({
                            features: validFeatures //(new ol.format.GeoJSON()).readFeatures(geojson, { featureProjection: options.epsg })
                        }));
                    });
                } else {
                    newLayer = new ol.layer.Vector({
                        source: new ol.source.Vector({
                            url: options.url,
                            format: new ol.format.GeoJSON()
                        }),
                        visible: options.visible,
                        id: options.id,
                        name: options.name,
                        edit: false,
                        type: 'vector',
                        zIndex: options.zIndex,
                        //,style  : new ol.style.Style({})
                    });
                    if (options.style) {
                        newLayer.setStyle(options.style);
                        delete options.style;
                    }
                }
                // definir a propriedade options do layer para futuras interações com a camada
                newLayer.set('options',options);
            }
            else if( options.data ) {
                // criar a propriedade text nas features se não existir
                try {
                    var data = options.data;
                    data.features.forEach(function (feature) {
                        if( ! feature.properties.text) {
                            var text = ['<b>Informações'+(options.shp ? ' (shapefile)':'')+'</b>'];
                            for ( key in feature.properties ) {
                                var prop = feature.properties[key];
                                prop = self.sanitizeHtml( prop );
                                text.push('<span style="color:#777;">' + key +'</span> = <b>' + prop + '</b>');
                            }
                            feature.properties.text = text.join('<br>');
                        }
                    });
                } catch(e){}
                var validFeatures = self.getGeojsonValidFeatures( options.data );
                newLayer = new ol.layer.Vector({
                    source: new ol.source.Vector({
                        //format: new ol.format.GeoJSON(),
                        features : validFeatures // options.data //(new ol.format.GeoJSON() ).readFeatures( options.data, { featureProjection: 'EPSG:3857' }),
                    }),
                    visible: options.visible,
                    id: options.id,
                    name: options.name,
                    type: 'vector',
                    zIndex: options.zIndex,
                });
            } else {
                newLayer = new ol.layer.Vector({
                    source: new ol.source.Vector(),
                    visible: options.visible,
                    id: options.id,
                    name: options.name,
                    type: 'vector',
                    zIndex: options.zIndex,
                });
                if (options.style) {
                    newLayer.setStyle(options.style);
                    delete options.style;
                }
                delete options.style;
                newLayer.set('options',options);
            }
        }
        else if (options.type.toLowerCase() == 'wms') {
            if (options.url && options.layer && options.server) {
                newLayer = new ol.layer.Tile({
                    source: new ol.source.TileWMS({
                        url: options.url,
                        params: { 'LAYERS': options.layer },
                        serverType: options.server,
                        crossOrigin: ''
                        //crossOrigin: 'anonymous'
                    }),
                    visible: options.visible,
                    id: options.id,
                    name: options.name,
                    type: 'vector',
                    zIndex: options.zIndex,
                });
                if (options.style) {
                    newLayer.setStyle(options.style);
                    delete options.style;
                }
                // definir a propriedade options do layer para futuras interações com a camada
                newLayer.set('options',options);
            }
        }
        else if( options.type.toLowerCase() == 'shapefile') {
            options.color = options.color ? options.color : '#ffffff';
            newLayer = new ol.layer.Vector({
                source: new ol.source.Vector(),
                style: new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        //color: 'rgba(65,160,210, 0.8)', // azul piscina
                        color: options.color,
                        width: 1
                    }),
                }),
                visible: options.visible,
                id: options.id,
                name: options.name,
                type: 'vector',
                edit: false,
                zIndex: options.zIndex
            });
            var urlLayer = urlSalve + 'api/shapefile/'+options.layer;
            shp( urlLayer ).then(function (geojson) {
                var featuresLayer = self.getGeojsonValidFeatures(geojson);
                // criar a propriedade text que será exibida ao  clicar sobre o pológigono
                featuresLayer.map(function (f, i) {
                    try {
                        var labels = [];
                        for (key in f.getProperties()) {
                            if (/string|number/.test(typeof (f.get(key)))) {
                                labels.push(key + ': ' + f.get(key));
                            }
                        }
                        f.set('text', labels.join('<br>'));
                    } catch( e ) {}
                });
                newLayer.setSource( new ol.source.Vector( {
                    features: featuresLayer //(new ol.format.GeoJSON()).readFeatures(geojson, {featureProjection: 'EPSG:3857'})
                }));
            });
        }
        if (newLayer) {
            this.addLayer(newLayer);
        }
        return newLayer;
    }


    /**
     * atualizar o layer switch de acordo com os layers do mapa
     */
    this.updateLayerSwitch = function() {

        // TODO - alterar logica de criação para separar os baselayers dos vectors e depois adicionar no switcher
        var $ul = $('#layer-switch-' + this.el + ' ul')
        if (!$ul) {
            return;
        }
        // radio/check com layers
        var visibleFound = false;
        var i = 1;
        var li = ''
        var label = ''
        for (key in this.layers) {

            var layer = self.layers[key];
            li = document.createElement('li');
            label = document.createElement('label');
            label.style.paddingTop = '0px';
            label.style.display = 'inline';

            var input = document.createElement('input');
            if( layer.get('name') ) {
                var layerName = self.layers[key].get('name');
                var layerId = layer.get('id');
                var inputId = 'input-layerswitcher-' + (i++) + '-' + self.el;
                var options = (layer.get('options') ? layer.get('options') : {});
                var legendId = options.legendId;

                li.className = 'ol-layer-switch-li';
                if (!layerId) {
                    layerId = 'layer-' + self.uid();
                    layer.set('id', layerId);
                }
                // adicionar somente se não existir
                if ($ul.find('input[value=' + layerId + ']').size() == 0) {
                    input.setAttribute('id', inputId);
                    label.setAttribute('for', inputId);
                    label.style.cursor = 'pointer';
                    if( options.hint ) {
                        label.setAttribute('title',options.hint);
                    }
                    if (layer.get('type')) {
                        if (layer.get('type').toLowerCase() == 'baselayer') {
                            input.setAttribute('type', 'radio');
                            input.setAttribute('name', 'radio' + self.el);
                            if (layer.getVisible() && !visibleFound) {
                                visibleFound = true;
                                input.setAttribute('checked', 'true');
                            }
                        } else {
                            input.setAttribute('type', 'checkbox');
                            if (layer.getVisible()) {
                                input.setAttribute('checked', 'true');
                            }
                        }
                        input.setAttribute('value', layerId);
                        input.addEventListener('click', function (e) {
                            var layerId = e.target.value.toLowerCase();
                            if (e.target.getAttribute('type').toLowerCase() == 'radio') {
                                for (key in self.layers) {
                                    if (self.layers[key].get('type').toLowerCase() == 'baselayer') {
                                        if (self.layers[key].get('id').toLowerCase() == layerId) {
                                            self.layers[key].setVisible(true);
                                        } else {
                                            self.layers[key].setVisible(false);
                                        }
                                    }
                                }
                            } else if (e.target.getAttribute('type').toLowerCase() == 'checkbox') {
                                for (key in self.layers) {
                                    if (self.layers[key].get('id').toLowerCase() == layerId) {
                                        self.layers[key].setVisible(e.target.checked);
                                    }
                                }
                            }
                        }, false);
                        label.innerHTML = layerName;

                        if ( options.legendHtml ) {
                            label.innerHTML = options.legendHtml + layerName;
                        }

                        // criar grupo registros no switcher
                        if(layerId == 'regNaoUtilizados') {
                            var divRegistros = document.createElement('div');
                            divRegistros.style.borderBottom = '1px dashed white';
                            divRegistros.style.display = 'block';
                            divRegistros.style.fontWeight = 'bold';
                            divRegistros.style.marginTop = '8px';
                            divRegistros.style.marginBottom = '8px';
                            divRegistros.innerHTML = 'Registros';
                            li.appendChild(divRegistros)
                        }


                        li.appendChild(input);

                        if( !legendId ) {
                            // adicionar a legenda
                            options.options = options.options || {};
                            var icon = options.legend && options.legend.image ? options.legend.image : '';
                            var color = options.legend && options.legend.color ? options.legend.color : '';
                            var text  = options.legend && options.legend.text ? options.legend.text : '';
                            if( !icon && options.options.legend && options.options.legend.image ){
                                icon = options.options.legend.image
                            }
                            if( !color && options.options.legend && options.options.legend.color ){
                                color = options.options.legend.color
                            }
                            if( !text && options.options.legend && options.options.legend.text ){
                                color = options.options.legend.text
                            }
                            if( !color && !icon && layer.get('color')){
                                color = layer.get('color');
                            }
                            if( text ){
                                label.innerHTML = text;
                            }
                            if( icon ){
                                var legend = document.createElement('img');
                                var img = icon
                                legend.setAttribute('src', "/salve-consulta/assets/markers/" + img);
                                legend.setAttribute('width', iconSize+"px;");
                                legend.setAttribute('height', iconSize+"px;");
                                legend.setAttribute('alt', '');
                                legend.style.marginTop  = '-1px';
                                legend.style.marginLeft     = '3px';
                                legend.style.marginRight    = '0px';
                                legend.style.marginLeft     = '5px';
                                legend.style.marginRight    = '3px';
                                li.appendChild(legend);

                            } else if ( color ) {
                                var legend = document.createElement('i');
                                legend.className = 'fa fa-circle';
                                legend.setAttribute('style', 'font-size:' + iconSize + 'px' + ' !important');
                                legend.style.marginLeft = '5px';
                                legend.style.marginRight = '3px';

                                li.classList.add('ol3-legend-item');
                                legend.setAttribute('aria-hidden', "true");
                                if( options.title ){
                                    li.setAttribute('title', options.title );
                                }
                                if (options.color.indexOf('.png') == -1) {
                                    legend.style.color = color;
                                } else {
                                    legend = document.createElement('img');
                                    var img = options.color
                                    //var img = options.color.replace(/[0-9]{1,2}x[0-9]{1,2}/, 'legend');
                                    legend.setAttribute('src', "/salve-consulta/assets/markers/" + img);
                                    legend.setAttribute('width', iconSize+"px;");
                                    legend.setAttribute('height', iconSize+"px;");
                                    legend.setAttribute('alt', layerName);
                                    legend.style.marginTop = '-4px';
                                }

                                li.appendChild(legend);
                            } else {
                                label.innerHTML = '&nbsp;' + label.innerHTML;
                            }
                        } else {
                            label.style.paddingLeft='6px';
                        }
                        li.appendChild(label);
                        // criar tag ui vazia para que a legenda seja criada dinamicamente pela funcao occurrenceStyle()
                        if( legendId ) {
                            var ulLegend = document.createElement('ul');
                            ulLegend.style.listStyleType='none';
                            ulLegend.style.padding='5px';
                            ulLegend.style.paddingLeft='11px';
                            ulLegend.setAttribute('id','ul-legend-'+legendId);
                            li.appendChild( ulLegend );
                        }
                        var inputType = input.getAttribute('type');
                        var $lastLi = null;
                        if (inputType == 'radio') {
                            $lastLi = $ul.find("input[type=radio]:last");
                        } else if (inputType == 'checkbox') {
                            $lastLi = $ul.find("input[type=checkbox]:last");
                            if ($lastLi.size() == 0) {
                                $lastLi = $ul.find("input[type=radio]:last");
                            }
                        }
                        if ($lastLi && $lastLi.size() == 1) {
                            $(li).insertAfter($lastLi.parent());
                        } else {
                            $ul.prepend(li);
                        }
                    }
                } else {
                    if (layer.getVisible()) {
                        $ul.find('input[value=' + layerId + ']').prop('checked', true);
                    }
                }
            }
        }
        // adicionar legenda para subespecie
        var liId = 'li_legend_subespecies_'+self.el

        if ( $ul.find('li#'+liId).size() == 0) {
            li = document.createElement('li');
            li.setAttribute('id',liId);
            li.style.display='none';
            li.setAttribute('title','Registros de subespecies');
            label = document.createElement('label');
            label.style.paddingTop = '0px';
            label.style.display = 'inline';
            label.innerHTML = 'Subespecie';
            var legend = document.createElement('img');
            legend.setAttribute('src', "/salve-consulta/assets/markers/legenda_subespecie_12x12.png");
            legend.setAttribute('width', iconSize + "px;");
            legend.setAttribute('height', iconSize + "px;");
            legend.setAttribute('alt', '');
            legend.style.marginTop = '0px';
            legend.style.marginLeft = '0px';
            legend.style.marginRight = '1px';
            var hr = document.createElement('hr')
            hr.style.margin = '0px';
            hr.style.marginTop = '3px';
            li.appendChild(hr);
            li.appendChild(legend);
            li.appendChild(label);
            $ul.append(li);
        }
    };

    /**
     * adicionar controle no mapa e evitar duplicidade caso o controle já exista
     */
    this.addControl = function(Control) {
        var exists = false;
        var newInstance = new Control({ self: this });
        self.map.getControls().forEach(function(el, i) {
            if (el.element.getAttribute('name') == newInstance.element.getAttribute('name')) {
                exists = true;
            }
        });
        if (!exists) {
            self.map.addControl(newInstance);
        }
    }

    /**
     *   Criar os controles customizados para desenhar, arrastar, adicionar camada wms
     */
    this.addCustomControls = function() {
        /**
         * Criar a barra de ferramentas com os botões no mapa
         */
        function GenerateEditToolbar(objOl3Map) {
            var buttons = [
                { label: 'fa-mouse-pointer', title: 'Desligar modo desenho', 'type': 'Stop' }
                //,{label:'fa-object-group'    ,title:'Desenhar poligono e medir a área'           ,'type':'Polygon'}
                , { label: 'fa-bookmark-o', title: 'Desenhar poligono e calcular a área.', 'type': 'Polygon' }
                //,{label:'fa-circle-o'       ,title:'Desenhar círculo'            ,'type':'Circle'}
                , { label: 'fa-clone', title: 'Desenhar e calcular a área', 'type': 'Area' }
                , { label: 'fa-edit', title: 'Editar desenho selecionado', 'type': 'Edit' }
                , { label: 'fa-eraser', title: 'Excluir desenho', 'type': 'Delete' }
                , { label: 'fa-align-justify', title: 'Adicionar camada WMS', 'type': 'AddWms' }
                , { label: 'fa-file-image-o', title: 'Adicionar SHP', 'type': 'AddShp' }
                //, { label: 'fa-object-ungroup', title: 'Calcular a área', 'type': 'Measure-Area' }
                , { label: 'fa-arrows-h', title: 'Medir a distância', 'type': 'Measure-Distance' }
                ];
            if( objOl3Map.self.showEoo ) {
                buttons.push( {label: 'fa-object-group', title: 'Calcular EOO', 'type': 'Calcular-Eoo'} );
            }
            // aoo
             if ( objOl3Map.self.showAoo ) {
                 buttons.push({
                     label: 'fa-table',
                     title: 'Ativar/Desativar calculo da área de ocupação (AOO)',
                     'type': 'Calcular-Aoo'
                 });
             }

            buttons.push( { label: 'fa-filter', title: 'Selecionar filtro', 'type': 'dialogMapFilters' });
            buttons.push( { label: 'fa-retweet', title: 'Atualizar/Limpar filtro', 'type': 'Reload' } );

            var options = objOl3Map || {};
            var divId = 'map-draw-toolbar-' + self.el;
            var $element = $('<div class="ol-control" style="top:8px;left:3em" name="GenerateEditToolbar" id="'+divId+'"></div>')
            for (key in buttons) {
                var text = ( buttons[key].label.indexOf('fa-')==0 ? '<i data-type="'+buttons[key].type+'" class="fa '+ buttons[key].label+'"></i>' : '<span style="font-size:14px;">'+buttons[key].label) +'<span>';
                $element.append('<button data-type="'+buttons[key].type+'" type="button" title="'+buttons[key].title+'" style="display:inline;cursor:pointer;margin-bottom:3px;">'+text+'</button>');
            }
            // adicionar evento aos botões
            $element.find('button').each(function(i, button )
            {
                //$(button).css('color','black');
                $(button).on( 'click', function(e) {
                    //$(e.target).closest('div').find('button').css('color','black');
                    //$(e.target).closest('button').css('color','white');
                    e.preventDefault();
                    objOl3Map.self.addDrawInteraction(e.target.getAttribute('data-type'))
                });
            });

            // criar a div para exibição da Extensão de Ocorrência EOO
            var buttonsEoo = '';
            var footerEoo = '';
            var minHeight=66;
            buttonsEoo = '<div style="text-align:center;border:1px solid gray;padding:3px;font-size:14px !important;color:blue;font-weight:bold;position: absolute;top:3px;right:5px;">' +
                         '<button id="' + divId + '_eoo_btn_calc" style="display:inline-block" type="button" title="Calcular a partir dos pontos utilizados na avaliação"><i class="fa fa-map-pin"></i></button>';

                if( ! objOl3Map.self.options.readOnly ) {
                        buttonsEoo += '<button id="' + divId + '_eoo_btn_undo" style="display:inline-block;margin-left:5px;margin-right:5px;" type="button" title="Desfazer alterações realizadas antes da última gravação"><i class="fa fa-recycle"></i></button>'+
                        '<button id="' + divId + '_eoo_btn_save" style="color:#fff;display:inline-block;margin-right:5px;" type="button" title="Gravar"><i class="fa fa-save"></i></button>' +
                        '<button id="' + divId + '_eoo_btn_delete" style="display:inline-block;margin-right:0px;color:#facece;" type="button" title="Exlcluir a EOO"><i class="fa fa-trash"></i></button>';
                //if( isCbc ) {
                    buttonsEoo += '<br>' +
                        '<button id="' + divId + '_eoo_btn_shp1" data-active="false" style="display:inline-block;margin-right:5px;color:#fcf6bd;" type="button" title="Ativar/Desativar a camada hydrosheds nível 8"><span>8</span></button>' +
                        '<button id="' + divId + '_eoo_btn_shp2" data-active="false" style="display:inline-block;margin-right:5px;color:#d0f4de;" type="button" title="Ativar/Desativar a camada hydrosheds nível 9"><span>9</span></button>' +
                        '<button id="' + divId + '_eoo_btn_shp3" data-active="false" style="display:inline-block;margin-right:0px;color:#a9def9;" type="button" title="Ativar/Desativar a camada hydrosheds nível 10"><span>10</span></button>'
                //}
            }
            buttonsEoo += '</div>';
            footerEoo = '<p id="'+divId+'_footer_eoo" style="display:none;color:#fff;margin:0px;margin-top:25px;font-size:12px !important;background-color: #a5b7cd !important;" title="Pressione a tecla CTRL ou CMD e clique no mapa para selecionar/deselecionar uma área.">Ctrl+Click ou CMD+Click no mapa para selecionar/deselecionar uma área.</p>';
            $element.append('<div id="'+divId+'_eoo" style="background-color:#fefefead;position:relative;display:none;text-align:left;padding:3px;width:300px;min-height:'+minHeight+'px;height:auto;font-weight:bold;border:1px solid gray;border-radius: 5px">' +
                buttonsEoo +
                '<p style="color:gray;margin:0px;font-size:12px !important;">Extensão de Ocorrência (EOO)</p>' +
                '<span id="'+divId+'_eoo_vl_eoo" style="font-size:12px !important;color:black;font-weight:bold;">0 km<sup>2</sup></span><br>' +
                footerEoo +
                '</div>'
            );

            // botao calcular EOO
            $element.find('button#' + divId + '_eoo_btn_calc').on('click', function (e) {
                e.preventDefault();
                $("#" + divId + '_footer_eoo').hide();
                self.btnToolBoxEooClick('calc',divId);
            });

            if( ! objOl3Map.self.options.readOnly ) {
                // adicionar os eventos nos botões da eoo
                $element.find('button#' + divId + '_eoo_btn_delete').on('click', function (e) {
                    e.preventDefault();
                    self.btnToolBoxEooClick('delete')
                });
                $element.find('button#' + divId + '_eoo_btn_undo').on('click', function (e) {
                    e.preventDefault();
                    self.btnToolBoxEooClick('undo')
                });
                $element.find('button#' + divId + '_eoo_btn_save').on('click', function (e) {
                    e.preventDefault();
                    self.btnToolBoxEooClick('save')
                });



                $element.find('button#' + divId + '_eoo_btn_shp1').on('click', function (e) {
                    e.preventDefault();
                    self.btnToolBoxEooClick('loadShp1',divId);
                });

                $element.find('button#' + divId + '_eoo_btn_shp2').on('click', function (e) {
                    e.preventDefault();
                    self.btnToolBoxEooClick('loadShp2',divId);
                });

                $element.find('button#' + divId + '_eoo_btn_shp3').on('click', function (e) {
                    e.preventDefault();
                    self.btnToolBoxEooClick('loadShp3',divId);
                });
            }
            // criar a div para exibição da Area de Ocupação AOO
            var buttonsAoo = '';
            var footerAoo = '';
            if( ! objOl3Map.self.options.readOnly ) {
                buttonsAoo = '<div style="border:1px solid gray;padding:3px;font-size:14px !important;color:blue;font-weight:bold;position: absolute;top:3px;right:5px;">' +
                  '<button id="' + divId + '_aoo_btn_update" style="display:inline-block;margin-right:5px;" type="button" title="Calcular a partir dos pontos utilizados na avaliação"><i class="fa fa-map-pin"></i></button>' +
                  '<button id="' + divId + '_aoo_btn_undo" style="display:inline-block;margin-right:5px;" type="button" title="Desfazer alterações realizadas antes da última gravação"><i class="fa fa-recycle"></i></button>' +
                  '<button id="' + divId + '_aoo_btn_save" style="color:#fff;display:inline-block;margin-right:5px;" type="button" title="Gravar na ficha"><i class="fa fa-save"></i></button>' +
                  '<button id="' + divId + '_aoo_btn_delete" style="display:inline-block;color:#facece" type="button" title="Excluir a AOO da ficha e do mapa"><i class="fa fa-trash"></i></button>' +
                  '</div>';
                footerAoo = '<p style="color:white;margin:0px;margin-top:10px;font-size:12px !important;background-color: #a5b7cd !important;" title="Pressione a tecla CTRL ou CMD e clique no mapa para adicionar ou remover uma área.">Ctrl+Click para adicionar/remover área.</p>';
            }
            $element.append('<div id="'+divId+'_aoo" style="background-color:#fefefead;position:relative;display:none;text-align:left;padding:3px;width:300px;height:auto;font-weight:bold;border:1px solid gray;border-radius: 5px">' +
               buttonsAoo +
              '<p style="color:gray;margin:0px;font-size:12px !important;">Área de ocupação (AOO)</p>' +
              '<span id="'+divId+'_aoo_vl_aoo" style="font-size:12px !important;color:black;font-weight:bold;">0</span><br>' +
               footerAoo +
               '</div>'
            );

            // adicionar os eventos nos botões da eoo
            $element.find('button#'+divId+'_aoo_btn_undo').on( 'click', function(e) {
                e.preventDefault();
                objOl3Map.self.undoPointsAoo(e)
            });
            $element.find('button#'+divId+'_aoo_btn_update').on( 'click', function(e) {
                e.preventDefault();
                objOl3Map.self.updatePointsAoo(e)
            });

            $element.find('button#'+divId+'_aoo_btn_save').on( 'click', function(e) {
                    e.preventDefault();
                    objOl3Map.self.saveAoo(e)
            });

            $element.find('button#'+divId+'_aoo_btn_delete').on( 'click', function(e) {
                    e.preventDefault();
                    objOl3Map.self.deleteAoo(e)
            });


            ol.control.Control.call(this, {
                element: $element[0],
                target: options.target
            });
        };
        ol.inherits(GenerateEditToolbar, ol.control.Control);

        /**
         * Criar o trocador de camadas
         */
        function GenerateLayerSwitch(opt_options) {

            var options = opt_options || {};
            var self = options.self;
            var divContainer = document.createElement('div');
            divContainer.setAttribute('id', 'layer-switch-' + self.el);
            divContainer.setAttribute('name', 'GenerateLayerSwitch');
            divContainer.className = 'ol-control ol-layer-switch';
            divContainer.style.whiteSpace = 'nowrap';
            divContainer.style.backgroundColor = '#7c99bd';
            divContainer.style.top = '45px';
            divContainer.style.left = 'auto';
            divContainer.style.position = 'absolute';
            divContainer.style.padding = '10px 8px';
            divContainer.style.paddingTop = '2px';
            divContainer.style.fontSize = '14px';
            divContainer.style.color = '#fff';
            divContainer.style.right = '10px';
            //divContainer.style.minWidth = '120px';
            //divContainer.style.minHeight = '50px'
            divContainer.style.overflow='hidden';
            divContainer.style.margin = '0px';
            divContainer.style.opacity = '0.7';

            // titulo
            var divTitle = document.createElement('div');
            divTitle.className = 'ol-layer-switch-title';
            divTitle.style.textAlign = 'center';
            divTitle.style.borderBottom = '1px solid #fff';
            divTitle.style.marginBottom = '5px';
            //divTitle.style.fontSize      = '14px';
            divTitle.style.fontWeight = 'bold';
            divTitle.style.padding = '2px';
            divTitle.style.paddingBottom = '2px';
            if( self.expandLayerSwitcher == false ) {
                divContainer.style.height = '30px';
                // exibir o layer switcher miniminzado
                divTitle.innerHTML = '<span>Camadas</span><i class="fa fa-chevron-up fa-chevron-down pull-right cursor-pointer"></i>';
            } else {
                divTitle.innerHTML = '<span>Camadas</span><i class="fa fa-chevron-up pull-right cursor-pointer"></i>';
            }
            divContainer.appendChild(divTitle);
            $( divTitle ).find('i.fa-chevron-up').on('click',function(evt) {
               self.toggleLayerSwitch(evt);
            });

            var ul = document.createElement('ul');
            ul.className = 'ol-layer-switch-ul';
            ul.style.textAlign = 'left';
            ul.style.listStyle = 'none';
            ul.style.margin = '0px';
            ul.style.padding = '0px';
            ul.style.cursor = 'pointer';
            divContainer.appendChild(ul)
            // adicionar o botão de exportar o mapa
            if( self.showExportButton ) {
                var hr = document.createElement('hr')
                hr.style.padding='1px';
                hr.style.margin='4px';
                divContainer.appendChild(hr);
                var btnExportMap = document.createElement('button');
                btnExportMap.style.cursor = 'pointer';
                btnExportMap.style.width = '100%';
                btnExportMap.style.fontSize = '14px !important';
                btnExportMap.setAttribute('type', 'button');
                btnExportMap.setAttribute('title', 'Salvar imagem do mapa atual no formato png.');
                btnExportMap.innerHTML = 'Exportar Mapa (png)';
                divContainer.appendChild(btnExportMap)
                btnExportMap.addEventListener('click', function (e) {
                    var dateTime = new Date();
                    var nomeEspecie = $("#nmCientificoMapa").val();
                    nomeEspecie = nomeEspecie == '' ? 'Mapa-exportado-salve-' : 'Mapa '+nomeEspecie+' ';
                    var imageName = nomeEspecie + dateTime.getDate() + '-' +
                        (dateTime.getMonth()+1) + '-' +
                        dateTime.getFullYear() + '-' +
                        dateTime.getHours() + '-' +
                        dateTime.getMinutes() + '-' +
                        dateTime.getSeconds() + '.png';
                    self.map.once('postcompose', function (event) {
                        var canvas = event.context.canvas;
                        if (navigator.msSaveBlob) {
                            navigator.msSaveBlob(canvas.msToBlob(), imageName);
                        } else {
                            canvas.toBlob(function (blob) {
                                saveAs(blob, imageName);
                            });
                        }
                    });
                    self.map.renderSync();
                });
            }
            ol.control.Control.call(this, {
                element: divContainer,
                target: options.target
            });

        }
        ol.inherits(GenerateLayerSwitch, ol.control.Control);

        /**
        carregar um servico WMS externo
        ex:
        url   : https://demo.boundlessgeo.com/geoserver/ne/wms
        camada: ne:ne
        server: GeoServer
        */
        function GenerateDialogAddWms(opt_options) {
            var options = opt_options || {};
            var self = options.self;
            var divId = 'dlg-add-wms-' + self.el;
            var divContainer = document.createElement('div');
            var urlWmsSigeo  = 'http://mapas.icmbio.gov.br/cgi-bin/mapserv?map=/geodados/icmbio/mapfile/';
            var urlWmsSigeo2  = 'https://mapas2.icmbio.gov.br/fcgi-bin/mapserv'
            var wmsVersion   = '&version=1.1.1&service=WMS&request=GetLegendGraphic&format=image/png';
            var wmsVersion2   = '&VERSION=1.3.0&SERVICE=WMS&REQUEST=GetMap&FORMAT=image/png';
            // '<option value="Uc Estadual" data-layer="uc_estadual" data-name="Uc Estadual" data-server="mapserver" data-url="'+urlWmsSigeo+'mma_UcEstadual.map'+wmsVersion+'" >UCs Estaduais</option>' +

            divContainer.setAttribute('id', divId);
            divContainer.setAttribute('name', 'GenerateDialogAddWms');
            divContainer.className = 'ol-control ol-dialog';
            divContainer.style.display = 'none';
            divContainer.style.whiteSpace = 'nowrap';
            divContainer.style.backgroundColor = '#7c99bd';
            divContainer.style.top = '45px';
            divContainer.style.left = '50px';
            divContainer.style.position = 'absolute';
            divContainer.style.padding = '10px 8px';
            divContainer.style.paddingTop = '2px';
            divContainer.style.fontSize = '14px';
            divContainer.style.color = '#fff';
            divContainer.style.width = '40%';
            divContainer.style.minWidth = '200px';
            divContainer.style.maxWidth = '400px';
            divContainer.style.minHeight = '285px';
            divContainer.style.margin = '0px';
            divContainer.style.opacity = '0.9';

            // titulo
            var divTitle = document.createElement('div');
            divTitle.className = 'ol-dialog-title';
            divTitle.style.textAlign = 'center';
            divTitle.style.borderBottom = '1px solid #fff';
            divTitle.style.marginBottom = '5px';
            //divTitle.style.fontSize      = '14px';
            divTitle.style.fontWeight = 'bold';
            divTitle.style.padding = '2px';
            divTitle.style.paddingBottom = '2px';
            divTitle.innerHTML = 'Adicionar camada WMS';
            divContainer.appendChild(divTitle);

            ol.control.Control.call(this, {
                element: divContainer,
                target: options.target
            });

            var form = document.createElement('form');
            var formId = 'form-dlg-add-wms-' + self.el;

            form.setAttribute('id', formId );
            form.setAttribute('data-el', self.el);
            form.style.marginBottom = "0px";
            // '<option value="ZEE" data-layer="zee" data-name="ZEE" data-server="mapserver" data-url="'+urlWmsSigeo+'ibge_zee.map'+wmsVersion+'" >ZEE</option>' +
            // '<option value="Uc Federal" data-layer="uc_Federal" data-name="Uc Federal" data-server="mapserver" data-url="'+urlWmsSigeo+'uc_Federal.map'+wmsVersion+'" >UCs Federais</option>' +
            // '<option value="Biomas" data-layer="biomas" data-name="Biomas" data-server="mapserver" data-url="'+urlWmsSigeo+'biomas.map'+wmsVersion+'" >Biomas</option>' +
            // '<option value="Uc Estadual" data-layer="uc_estadual" data-name="Uc Estadual" data-server="mapserver" data-url="'+urlWmsSigeo+'mma_UcEstadual.map'+wmsVersion+'" >UCs Estaduais</option>' +
            // '<option value="Usinas Hidrelétricas" data-layer="aneel_usinas_hidreletricas" data-name="Usinas Hidrelétricas" data-server="mapserver" data-url="'+urlWmsSigeo+'aneel_usinas_hidreletricas.map'+wmsVersion+'" >Usinas Hidrelétricas (ANEEL)</option>' +
            // '<option value="Terras Indígenas (FUNAI)" data-type="geojson" data-shp="true" data-id="terrasIndigenas" data-color="rgba(203,70,19,0.8)" data-epsg="EPSG:3857" data-name="TIs out/2019" data-url="'+baseUrl+'/api/shapefile/ti_sirgas2000">Terras Indígenas (FUNAI)</option>'+
            // '<option value="Estados" data-layer="ibge_UF" data-name="Estados" data-server="mapserver" data-url="'+urlWmsSigeo+'ibge_Uf.map'+wmsVersion+'">Estados</option>' +

            // https://sigel.aneel.gov.br/arcgis/rest/services/PORTAL/Downloads/MapServer

            form.innerHTML = '<div style="border-bottom:1px solid white;padding-bottom:4px;margin-bottom:2px;">' +
                    '<button type="button" name="btnSelectTab1" style="border-radius:5px;cursor:pointer;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:120px;float:right;">Outras</button>' +
                    '<button type="button" name="btnSelectTab2" style="border-radius:5px;cursor:default;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:120px;color:silver;" disabled="true">Pré-cadastradas</button>' +
                '</div>'+
                '<div name="tab2" style="display:block;">' +
                    '<div style="display:inline-block;height:183px;">' +
                        '<label>Visualizar camada</label><br/>'+
                        '<select name="layerSigeo" class="ignoreChange" style="color:#000;height:25px;width:100%;padding:0px;" title="">' +
                            '<option selected value="" style="color:#000;">-- selecione --</option>' +
                            '<option value="Estados" data-layer="2_estados" data-name="Estados" data-server="mapserver" data-url="'+urlWmsSigeo2+'?map=/var/www/sigeo/storage/app/uploads/limites/estados.map'+wmsVersion2+'&id=2">Estados</option>' +
                            '<option value="Uc Municipal" data-layer="uc_municipal" data-name="Uc Municipal" data-server="mapserver" data-url="'+urlWmsSigeo+'mma_UcMunicipal.map'+wmsVersion+'" >UCs Municipais</option>' +
                            '<option value="Rppn" data-layer="rppn" data-name="RPPN" data-server="mapserver" data-url="'+urlWmsSigeo+'icmbio_Rppn.map'+wmsVersion+'">RPPN</option>' +
                            '<option value="Cavernas" data-layer="cavernas" data-name="Cavernas" data-server="mapserver" data-url="'+urlWmsSigeo+'cavernas.map'+wmsVersion+'" >Cavernas</option>' +
                            '<option value="Hidrografia 1:1.000.000 (ANA)" data-layer="hidrografia" data-name="Hidrografia 1:1.000k (ANA)" data-server="mapserver" data-url="'+urlWmsSigeo+'ana_Hidrografia_100000.map'+wmsVersion+'" >Hidrografia 1:1.000.000 (ANA)</option>' +
                            '<option value="Hidrografia 1:2.500.000 (ANA)" data-layer="hidrografia" data-name="Hidrografia 1:2.500k (ANA)" data-server="mapserver" data-url="'+urlWmsSigeo+'ana_Hidrografia_250000.map'+wmsVersion+'" >Hidrografia 1:2.500.000 (ANA)</option>' +
                            '<option value="Usinas Hidrelétricas-UHE (ANNEL)" data-layer="0,10" data-name="<i class=\'fa fa-caret-up red-dark\'></i>&nbsp;UHE (ANNEL)" data-server="mapserver" data-url="https://sigel.aneel.gov.br/arcgis/services/PORTAL/Camadas/MapServer/WmsServer" >Usinas Hidrelétricas-UHE (ANEEL)</option>' +
                            '<option value="Pequenas Centrais Hidrelétricas-PCH (ANNEL)" data-layer="0,7" data-name="<i class=\'fa fa-caret-up blue\'></i>&nbsp;PCH (ANNEL)" data-server="mapserver" data-url="https://sigel.aneel.gov.br/arcgis/services/PORTAL/Camadas/MapServer/WmsServer" >Pequenas Centrais Hidrelétricas-PCH (ANNEL)</option>'+
                            '<option value="Ecorregiões Aquáticas Continentais" data-color="#0e7b27" data-layer="Ecorregioes_aquaticas_x_Brasil" data-type="shapefile" data-name="Ecorregiões Aquáticas Continentais" >Ecorregiões Aquáticas Continentais</option>'+
                '</select>' +
                    '</div>' +
                '</div>'+
                '<div name="tab1" style="display:none;">' +
                    '<div style="display:inline">Titulo&nbsp;<span style="color:red;">*</span></div>' +
                    '<div style="display:block;">' +
                    '<input name="layerName" type="text" class="ignoreChange" style="color:#000;width:100%;"></div>' +
                    '<div style="display:inline">URL<span style="color:red;">*</span></div><div style="display:block;"><input name="layerUrl" type="text" class="ignoreChange" style="color:#000;width:100%;"></div>' +
                    '<div style="display:inline">Camada<span style="color:red;">*</span></div><div style="display:block;"><input name="layerLayer" type="text" class="ignoreChange" style="color:#000;width:100%;"></div>' +
                    '<div style="display:inline-block;height:auto;">' +
                    '<label>Tipo Servidor<span style="color:red;">*</span></label><br/>' +
                    '<select name="layerServer" style="color:#000;height:25px;padding:0px;" class="ignoreChange">' +
                    '<option selected value="mapserver" style="color:#000;">MapServer</option>' +
                    '<option value="geoserver">GeoServer</option>' +
                    '</select>' +
                    '</div>'+
                '</div>'+
                '<hr style="padding:1px;margin:4px;">'+
                '<div>' +
                    '<button name="btnFechar" type="button" style="cursor:pointer;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:100px;float:right;">Fechar</button>' +
                    '<button name="btnAdicionar" type="button" style="cursor:pointer;font-weight:normal;padding:0px;margin:0px;display:none;width:100px;">Adicionar</button>' +

                '</div>';
            divContainer.appendChild( form );

            // adicionar os eventos no formulário
            $(form).find('[name="btnSelectTab1"]').on('click',function( e ) {
                var $btn = $(e.target);
                var $form = $btn.closest('form');
                $form.find('div[name=tab1]').show();
                $form.find('div[name=tab2]').hide();
                var $btn1 = $form.find('button[name=btnSelectTab1]');
                $btn1.css({cursor:'default',color:'silver'});
                $btn1.prop('disabled',true);
                var $btn2 = $form.find('button[name=btnSelectTab2]');
                $btn2.css({cursor:'pointer',color:'#fff'});
                $btn2.prop('disabled',false);
                $form.find('button[name=btnAdicionar]').show();
            });
            $(form).find('[name="btnSelectTab2"]').on('click',function( e ) {
                var $btn = $(e.target);
                var $form = $btn.closest('form');
                $form.find('div[name=tab2]').show();
                $form.find('div[name=tab1]').hide();
                var $btn1 = $form.find('button[name=btnSelectTab1]');
                $btn1.css({cursor:'pointer',color:'#fff'});
                $btn1.prop('disabled',false);
                var $btn2 = $form.find('button[name=btnSelectTab2]');
                $btn2.css({cursor:'default',color:'silver'});
                $btn2.prop('disabled',true);
                $form.find('button[name=btnAdicionar]').hide();
            });

            $(form).find('[name=btnAdicionar]').on('click',function( e ) {
                var form = $(e.target).closest('form');
                var name = $(form).find('input[name=layerName]').val().trim();
                var url = $(form).find('input[name=layerUrl]').val().trim();
                var layer = $(form).find('input[name=layerLayer]').val().trim();
                var server = $(form).find('select[name=layerServer]').val().trim();
                var errors = [];
                if (!name) {
                    errors.push('Titulo é obrigatório.');
                }
                if (!url) {
                    errors.push('Url do serviço é obrigatória.');
                }

                if (!layer) {
                    errors.push('Nome da camada que será exibida é obrigatória.');
                }

                if (!server) {
                    errors.push('Tipo de servidor é obrigatório.');
                }
                if (errors.length > 0) {
                    self.alert(errors.join('\n'),6);
                    return;
                }
                var options = {
                    type: 'wms',
                    name: name,
                    url: url,
                    server: server,
                    layer: layer,
                    visible: true
                }
                // verificar se o layer já existe
                for (key in self.layers) {
                    var layer = self.layers[key];
                    if( layer.get('name') == options.name )
                    {
                        self.alert('Camada '+options.name+' já cadastrada!');
                        return;
                    }
                }
                $("#" + divId).hide();
                self.createLayer(options);
            });

            $(form).find('[name=btnFechar]').on('click',function( e ) {
                $("#" + divId).hide();
            });

            // adicionar evento no select das camadas do SIGEO
            $(divContainer).find('select[name=layerSigeo]').on('change', function(e) {
                //console.log( 'LayerSigeo change');
                var $select = $(e.target);
                var $option = $select.find('option:selected');
                var data = $option.data();
                data.type= data.type || 'wms';
                $option.prop('disabled',true);
                var options = {
                    type: data.type,
                    name: data.name,
                    url: data.url,
                    server: data.server,
                    layer: data.layer,
                    visible: true
                };
                $.extend( options,data);
                self.createLayer( options );
            });
        } // fim dialogo wms
        ol.inherits(GenerateDialogAddWms, ol.control.Control);

        /**
        carregar um arquivp shp zipado
        url   : https://github.com/gipong/shp2geojson.js
        */
        function GenerateDialogAddShp(opt_options) {
            var options = opt_options || {};
            var self = options.self;
            var divId = 'dlg-add-shp-' + self.el;
            var formId = 'form-dlg-add-shp-' + self.el;
            var divContainer = $('<div id="'+divId+'" name="GenerateDialogAddShp" class="ol-control ol-dialog"' +
                'style="display:none;white-space:nowrap;background-color:#7c99bd;top:45px;left:50px;position:absolute;padding:10px 8px;padding-top:2px;' +
                'font-size:14px;color:#fff;width:40%;min-width:200px;max-width:400px;min-height:145px;margin:0px;opacity:0.9">' +
                '<div class="ol-dialog-title" style="text-align:center;border-bottom:1px solid #fff;margin-bottom:5px;font-weight:bold;padding:2px;padding-bottom:2x;">Adicionar Shapefile (zip)</div>' +
                    '<form id="'+formId +'" data-el="'+self.el+'" style="margin-bottom:0px;">' +
                        '<label>Arquivo shapefile (zip)<span style="color:red;">*</span></label><br/>'+
                        '<input type="file" class="ignoreChange" style="max-width: 275px" name="shapefile" data-show-preview="false" data-show-upload="false" data-show-caption="true" data-show-remove="true" data-max-file-count="1"/>' +
                        '<label>Nome<span style="color:red;">*</span></label><br/>' +
                        '<input type="text" class="ignoreChange" name="shapefileName" style="width:100%;color:#000;" placeholder="" value=""/>' +
                        '<div style="margin-top:10px;text-align:center;">' +
                            '<button name="btnFechar" type="button" style="cursor:pointer;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:70px;float:right;">Fechar</button>' +
                            '<button name="btnReset" type="button" style="cursor:pointer;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:70px;">Limpar</button>' +
                            '<i class="fa fa-spin fa-spinner " style="display:none;font-size:2rem;float:left;"></i>'+
                            '<button name="btnAdicionar" type="button" style="cursor:not-allowed;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:70px;float:left;" disalbled="true">Adicionar</button>' +
                        '</div>'+
                    '</form>'+
                '</div>');

            ol.control.Control.call(this, {
                element: divContainer[0],
                target: options.target
            });

            // adicionar os eventos no formulário do shpfile
            $(divContainer).find('[name=btnReset]').on('click',function( e ) {
                $(e.target).closest('form')[0].reset();
            });
                // adicionar os eventos no formulário do shpfile
            $(divContainer).find('[name=btnAdicionar]').on('click',function( e ) {
                var form = $(e.target).closest('form');
                var name = $(form).find('input[name=shapefileName]').val().trim();
                var errors = [];
                if (!name) {
                    errors.push('Informe o nome para a camada.');
                }
                if (errors.length > 0) {
                    self.alert(errors.join('\n'));
                    return
                };
                try {
                    self.loadShpZip( {layerName: name} );

                } catch( e ) {
                    self.alert('Arquivo shp inválido!\n\n'+e.message );
                }
            });

            // tratar o arquivo zip adicionado
            $(divContainer).find('[name=shapefile]').on('change',function( evt ) {
                var $btnAdicionar= $(divContainer).find('[name=btnAdicionar]')
                $btnAdicionar.prop('disabled',true);
                $btnAdicionar.css({'cursor':'not-allowed','color':'#808080'});
                self.file=null;
                if( ! evt.target.files )
                {
                    return;
                }
                var file = evt.target.files[0];

                if(file && file.size > 0) {
                    if( !/\.zip$/.test( file.name) )
                    {
                        self.alert('Selecione um arquivo zipado contendo os arquivos que compem o shapefile!');
                        return;
                    }
                    $(divContainer).find('[name=shapefileName]').focus()
                    $btnAdicionar.prop('disabled',false);
                    $btnAdicionar.css({'cursor':'pointer','color':'#fff'});
                    self.file=file;
                }
            });
           $(divContainer).find('[name=btnFechar]').on('click',function( e ) {
                $("#" + divId).hide();
            });
        } // fim dialogo shp
        ol.inherits(GenerateDialogAddShp, ol.control.Control);

        /**
         * criar o formulário de filtros
         */
        function GenerateDialogFiltersMap(opt_options) {
            var options = opt_options || {};
            var self = options.self;
            var divId = 'dlg-filters-map-' + self.el;
            var formId = 'form-dlg-filters-map-' + self.el;
            var divContainer = $('<div id="'+divId+'" name="GenerateDialogFilterMap" class="ol-control ol-dialog"' +
                'style="display:none;white-space:nowrap;background-color:#7c99bd;top:45px;left:50px;position:absolute;padding:10px 8px;padding-top:2px;' +
                'font-size:14px;color:#fff;width:40%;min-width:200px;max-width:295px;min-height:145px;margin:0px;opacity:0.9">' +
                '<div class="ol-dialog-title" style="text-align:center;border-bottom:1px solid #fff;margin-bottom:5px;font-weight:bold;padding:2px;padding-bottom:2x;">Filtrar Registros</div>' +
                '<form id="'+formId +'" data-el="'+self.el+'" style="margin-bottom:0px;">' +

                '<label style="padding-bottom:2px;border-bottom: 1px solid #5c81af;;font-weight: bold;">Base de dados</label><br>'+
                '<div name="div-checkbox-base-dados-wrapper">'+
                    '<label style="padding:0px;" class="cursor-pointer"><input type="checkbox" value="salve" data-field="sistema"/> SALVE</label><br>'+
                    '<label style="padding:0px;" class="cursor-pointer"><input type="checkbox" value="salve-consulta" data-field="sistema"/> SALVE consulta</label><br>'+
                    '<label style="padding:0px;" class="cursor-pointer"><input type="checkbox" value="ara" data-field="sistema"/> ARA</label><br>'+
                    '<label style="padding:0px;" class="cursor-pointer"><input type="checkbox" value="sisbio" data-field="sistema"/> SISBio</label><br>'+
                    '<label style="padding:0px;" class="cursor-pointer"><input type="checkbox" value="simam" data-field="sistema"/> SIMMAM</label><br>'+
                    '<label style="padding:0px;" class="cursor-pointer"><input type="checkbox" value="sismonitora" data-field="sistema"/> SISMONITORA</label><br>'+
                    '<label style="padding:0px;" class="cursor-pointer"><input type="checkbox" value="sisquelonios" data-field="sistema"/> SISQUELONIOS</label><br>'+
                    '<label style="padding:0px;" class="cursor-pointer"><input type="checkbox" value="sitamar" data-field="sistema"/> SITAMAR</label><br>'+
                '</div>'+

                '<label style="display:none;padding-bottom:2px;border-bottom: 1px solid #5c81af;font-weight: bold;">Situação</label><br>'+
                '<div name="div-checkbox-base-dados-wrapper">'+
                    '<label style="padding:0px;display:none;" class="cursor-pointer"><input type="checkbox" value="S" data-field="emCarencia"/> Em carência</label><br>'+
               '</div>'+
                '<div style="margin-top:10px;text-align:center;">' +
                    /*'<button name="btnAplicar" type="button" style="cursor:pointer;font-weight:normal;padding:0px;margin:0px;margin-right: 10px; display:inline-block;width:70px">Aplicar</button>'
                    '<button name="btnReset" type="button" style="cursor:pointer;font-weight:normal;padding:0px;margin:0px;margin-right: 10px;display:inline-block;width:70px">Limpar</button>' +*/
                    '<button name="btnFechar" type="button" style="cursor:pointer;font-weight:normal;padding:0px;margin:0px;display:inline-block;width:70px">Fechar</button>' +
                '</div>'+
                '</form>'+
                '</div>');

            ol.control.Control.call(this, {
                element: divContainer[0],
                target: options.target
            });

            $(divContainer).find('div[name=div-checkbox-base-dados-wrapper] input:checkbox').on('click',function( e ) {
                self.aplicarFiltrosDialogFiltersMap(e)
            });
            $(divContainer).find('[name=btnReset]').on('click',function( e ) {
                $(e.target).closest('form')[0].reset();
                if( self.this.filtersMetadata.length > 0 ) {
                    self.filtersMetadata = [];
                    self.updateFilter();
                }
            });
            $(divContainer).find('[name=btnFechar]').on('click',function( e ) {
                $("#" + divId).hide();
            });
            // filters-map
        } // fim dialogo filtros mapa
        ol.inherits(GenerateDialogFiltersMap, ol.control.Control);



        // adicionar os controles no mapa
        if (self.options.showToolbar) {
            self.addControl(GenerateEditToolbar);
            self.addControl(GenerateDialogAddWms);
            self.addControl(GenerateDialogAddShp);
            self.addControl(GenerateDialogFiltersMap);
        }
        self.addControl(GenerateLayerSwitch);

    }

    /**
     * Exibir janela popup com inoformações do ponto clicado
     */
    this.showPopup = function(event) {
        var popup = $(this.overlayPopup.getElement());
        var loadAjax = false;

        if( popup.size() == 0 ){
            return;
        }
        this.overlayPopup.currentIndex=0;
        this.overlayPopup.totalItens=0;
        var popupContent = popup.find('div#popup-content');
        if ( this.clickedFeature ) {
            var coord = event.coordinate;
            // transformar para decimal degrees
            var degrees = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');

            // verificar se o ponto clicado é um cluster de pontos
            if( this.clickedFeature && this.clickedFeature.get('features') && ( this.clickedFeature.get('features').length > 0 ) ) {
                this.overlayPopup.totalItens = this.clickedFeature.get('features').length;
                this.overlayPopup.currentIndex = 0;
                this.overlayPopup.itens = [];
                this.clickedFeature.get('features').forEach(function (feature) {
                    text = feature.get('text') || '';
                    var id, bd;

                    if( feature.get('origem') ){
                        id = feature.get('sqOcorrencia');
                        bd = feature.get('origem');
                    } else {
                        id = feature.get('id_registro') || feature.get('id');
                        bd = feature.get('bd') || feature.get('no_base_dados');
                    }
                    var txNaoUtilizado = feature.get('txNaoUtilizadoAvaliacao') || '';
                    var sqMotivoNaoUtilizado = feature.get('sqMotivoNaoUtilizadoAvaliacao') || '';
                    //var inUtilizadoAvaliacao = feature.get('utilizadoAvaliacao') || feature.get('in_utilizado_avaliacao') || '';
                    //var inUtilizadoAvaliacao = (feature.get('noLayer') == 'regUtilizados' ? 's':'n');
                    var inUtilizadoAvaliacao = feature.get('inUtilizadoAvaliacao');
                    //inUtilizadoAvaliacao = ( /^s/i.test( inUtilizadoAvaliacao ) ? 's' : 'n' );
                    self.overlayPopup.itens.push( {
                            text: text,
                            id: id,
                            bd: bd,
                            textoNaoUtilizado : txNaoUtilizado,
                            sqMotivoNaoUtilizado:sqMotivoNaoUtilizado,
                            inUtilizadoAvaliacao:inUtilizadoAvaliacao
                        }
                    );
                });

                if (this.overlayPopup.itens.length > 1) {
                    text ='' ; // o text não será utilizado quando tem vários pontos no mesmo lugar
                    var htmlNavegacao = '<button class="btn btn-secondary btn-xs mr5 ol3-popup-button" disabled="true" data-to="first"><i class="fa fa-step-backward"></i>&nbsp;Início</button>';
                    htmlNavegacao += '<button class="btn btn-secondary btn-xs mr5 ol3-popup-button" disabled="true " data-to="prior"><i class="fa fa-chevron-left"></i>&nbsp;Anterior</button>'
                    htmlNavegacao += '<button class="btn btn-secondary btn-xs mr5 ol3-popup-button" data-to="next">Próximo&nbsp;<i class="fa fa-chevron-right"></i></button>'
                    htmlNavegacao += '<button class="btn btn-secondary btn-xs ol3-popup-button" data-to="last">Fim&nbsp;<i class="fa fa-step-forward"></i></button>'
                    htmlNavegacao += '<div class="btn-group pull-right">' +
                        '<select class="ol3-popup-select ignoreChange" title="Mostrar opções">' +
                        '<option class="ol3-popup-select-option" value="" selected disabled="true" style="color:#0D76E5;font-width: bold;">Opções do registro</option>' +
                        '<option class="ol3-popup-select-option" value="UTILIZAR_AVALIACAO">Utilizar na avaliação</option>'+
                        '<option class="ol3-popup-select-option" value="NAO_UTILIZAR_AVALIACAO"">Não utilizar na avaliação</option>'+
                        '<option class="ol3-popup-select-option" value="MARCAR_DESMARCAR_REGISTRO_HISTORICO">Marcar/Desmacar como registro histórico</option>'+
                        '<option class="ol3-popup-select-option" value="MARCAR_DESMARCAR_REGISTRO_SENSIVEL">Marcar/Desmacar como sensível</option>'+
                        '<option class="ol3-popup-select-option" value="EXCLUIR">Excluir ou marcar como excluído (Portalbio)</option>'+
                        '<option class="ol3-popup-select-option" value="RESTAURAR">Restaurar registro excluído (Portalbio)</option>'+
                        '<option class="ol3-popup-select-option" value="TRANSFERIR_REGISTRO">Transferir para outra ficha</option>'+
                        '<option class="ol3-popup-select-option" value="LOCALIZAR_GRIDE">Localizar no gride</option>'+
                        '</select>  '+
                        '</div>';
                    var htmlRecordPosition = '<div id="popup-' + self.el + '-current-item" class="ol3-popup-current-item">Reg: <span>1</span> de ' + this.overlayPopup.itens.length + '</div>'
                    loadAjax = this.overlayPopup.itens[0].text == '';
                    popupContent.html(
                        //htmlRecordPosition +

                        //'<div id="popup-' + self.el + '-text">Carregando...</div>' +
                        '<div id="popup-' + self.el + '-text" style="min-height: 255px;">' + this.overlayPopup.itens[0].text + '</div>' +
                        '<hr style="margin:4px;padding:1px;"><div class="text-center text-nowrap ol3-popup-buttons">'+
                        htmlNavegacao + htmlRecordPosition + '</div>'
                    );
                    popupContent.find('select.ol3-popup-select').on('change',function( e ) {
                        var action = e.currentTarget.value;
                        if( action ) {
                            var currentItem = self.overlayPopup.itens[ self.overlayPopup.currentIndex ];
                            var ids = [ { id:currentItem.id, bd:currentItem.bd } ];
                            self.contextMenuAction({action: e.currentTarget.value,
                                ids                             : ids,
                                txNaoUtilizadoAvaliacao         : currentItem.txNaoUtilizadoAvaliacao,
                                sqMotivoNaoUtilizadoAvaliacao   : currentItem.sqMotivoNaoUtilizadoAvaliacao,
                                inUtilizadoAvaliacao            : currentItem.inUtilizadoAvaliacao
                            });
                            e.currentTarget.value='';
                        }
                    });
                    // adicionar os eventos ao botões
                    popupContent.find('button.ol3-popup-button').on('click', function (e) {
                        var btnTo = $(e.target).data('to');
                        switch (btnTo) {
                            case 'first':
                                self.overlayPopup.currentIndex = 0;
                                break;
                            case 'prior':
                                self.overlayPopup.currentIndex--;
                                self.overlayPopup.currentIndex = Math.max(0, self.overlayPopup.currentIndex);
                                break;
                            case 'next':
                                self.overlayPopup.currentIndex++;
                                self.overlayPopup.currentIndex = Math.min(self.overlayPopup.totalItens - 1, self.overlayPopup.currentIndex);
                                break;
                            case 'last':
                                self.overlayPopup.currentIndex = self.overlayPopup.totalItens - 1;
                                break;
                        }
                        $("div#popup-" + self.el + "-text").html( self.overlayPopup.itens[ self.overlayPopup.currentIndex ].text);
                        $("div#popup-" + self.el + "-current-item > span").html((self.overlayPopup.currentIndex + 1));
                        // desabilitar botoes de acordo com a posição do array
                        if (self.overlayPopup.currentIndex == 0) {
                            popup.find("button.ol3-popup-button[data-to=first]").prop('disabled', true);
                            popup.find("button.ol3-popup-button[data-to=prior]").prop('disabled', true);
                        } else {
                            popup.find("button.ol3-popup-button[data-to=first]").prop('disabled', false);
                            popup.find("button.ol3-popup-button[data-to=prior]").prop('disabled', false);
                        }
                        if (self.overlayPopup.currentIndex + 1 == self.overlayPopup.totalItens) {
                            popup.find("button.ol3-popup-button[data-to=last]").prop('disabled', true);
                            popup.find("button.ol3-popup-button[data-to=next]").prop('disabled', true);
                        } else {
                            popup.find("button.ol3-popup-button[data-to=last]").prop('disabled', false);
                            popup.find("button.ol3-popup-button[data-to=next]").prop('disabled', false);
                        }
                        if( ! self.overlayPopup.itens[ self.overlayPopup.currentIndex ].text ) {
                            self.loadPopupInfo(self.overlayPopup.currentIndex );
                        }
                    });
                } else {
                    if ( ! text) {
                        if( this.overlayPopup.itens.length ) {
                            //text =  '<div id="popup-' + self.el + '-text"><b>Carregando...</b></div>';
                            text =  '<div id="popup-' + self.el + '-text" style="min-height: 255px"></div>';
                            loadAjax=true;
                        } else {
                            text = 'Não há informações.'
                        }
                    }
                }
            } else {
                text = this.clickedFeature.get('text');
                if ( ! text ) {
                    text = 'Não há informações.'
                }
            }
            // só exibir o popup se tiver texto
            if( text ) {

                //text = '<div style="position:absolute;top:2px;left:376px;color:#f00;cursor:pointer;" onClick="$(this).parent().hide();">X</div>'+text;
                // extract the spatial coordinate of the click event in map projection units

                var degreesPonto = [];
                if (this.clickedFeature.getGeometry().getType() == 'Point') {
                    degreesPonto = this.clickedFeature.getGeometry().getCoordinates();
                    degreesPonto = ol.proj.transform(degreesPonto, 'EPSG:3857', 'EPSG:4326');
                } else {
                    degreesPonto = degrees;
                }
                // format a human readable version
                var hdms = ol.coordinate.toStringHDMS(degreesPonto, 5).replace(/\.\d{1,5}/g, '');

                // update the overlay popup's content
                //var btnClose = '<span id="btnClosePopup-'+this.el+'" title="fechar" style="cursor:pointer;color:red;position:relative;top:0px;right:-2px;float:right;">X</span>'
                popupContent.html(text + '<hr style="margin:4px;padding:1px;">' + hdms);
            }
            // guardar a posicao para mostrar o popup depois que o ajax retornar
            this.overlayPopup.currentMapPosition  = this.map.getView().getCenter();
            this.overlayPopup.coord = coord;
            // ler o conteudo remotamente via ajax
            if( loadAjax ){
                setTimeout(function(){
                    self.loadPopupInfo( 0);
                },500);
            } else {
                popup.show();
                // position the popup (using the coordinate in the map's projection)
                this.overlayPopup.setPosition(coord);
            }

            // and add it to the map
            // this.map.addOverlay( this.overlayPopup );
            // console.log( degrees[0], degrees[1] );
            // var px = degrees[0];
            // var py = degrees[1];
            // var zoom = self.map.getView().getZoom()
            // this.centerMap(px, py);
            // popup.show();
        } else {
            popupContent.html('');
            popup.hide();
         }
    };

    /**
     * Estilo padrão das ocorrências com a criação da legenda automaticamente
     * As features devem possuir as propriedades color e legend
     * A cor padrão será vermelha se não for informada
     * @param feature
     * @param resolution
     * @returns {*|Style}
     */
    // zzz3
    this.occurrenceStyle = function(feature, resolution){
        var color       = feature.get('color') || feature.get('no_color');
        var icon        = feature.get('icon');
        var legend      = feature.get('legend');
        var legendId    = feature.get('legendId');
        var isSubespecie= feature.get('isSubespecie');
        var isCentroide = feature.get('isCentroide');

        // pontos com cluster
        var size=0;

        if( feature.get('features') ) {
            size = feature.get('features').length;
            color = feature.get('features')[0].get('color') || feature.get('features')[0].get('no_color');
            icon = feature.get('features')[0].get('icon') || feature.get('features')[0].get('no_icon');

            // se o cluster possuir mais de uma ocorrencia é preciso exibir a quantidade dentro do ponto
            if( size > 1 ){
                icon=''
            }
            if( !icon && /jpe?g|png|ico/.test( color ) ){
                icon = color;
                feature.set('icon', icon);
            }
            // adicionar ao ponto cluster se ele é composto por pontos utilizado ou não na avaliação
            if( feature.get('features')[0].get('utilizadoAvaliacao') ){
                feature.set('utilizadoAvaliacao', feature.get('features')[0].get('utilizadoAvaliacao'))
            } else if( feature.get('features')[0].get('in_utilizado_avaliacao') ){
                feature.set('utilizadoAvaliacao', feature.get('features')[0].get('in_utilizado_avaliacao').toLowerCase() == 's' ? 'Sim': 'Não' )
            }

            // mostrar o estilo da subespecie somente quando o clulster possuir somente 1 ponto
            if( size == 1 ){
                isSubespecie = feature.get('features')[0].get('in_subespecie') == 'S' || feature.get('features')[0].get('isSubespecie');
                isCentroide = feature.get('features')[0].get('isCentroide');
            }
        }
        // não exibir no mapa os registros que forem centroide quando o Estado rachurado existir
        if( isCentroide ) {
            var uf = feature.get('features')[0].get('sgEstado');
            if( uf ){
                var poligonoEstado = self.estadosCentroideLayer.getSource().getFeatures().filter(function( f ) {
                    return f.get('sigla') == uf;
                })

                if( poligonoEstado.length > 0 ) {
                    return self.hiddenStyle;
                }
            } else {
                return self.hiddenStyle;
            }
        }

        // mostrar/esconder os pontos/clusters
        if( ! self.isFeatureInFilter( feature ) ) {
            return self.hiddenStyle;
        }


        color = color ? color : '#ff0000'; // cor padrão vermelha
        var stroke;
        var fill = null;
        var radius = 5.5;
        var textSize=null;
        var strokeWidth = 0.5; // borda preta
        var image=null;
        var keyStyle = color + ( isSubespecie ? '-sub' :'' )+'-'+size;
        if( icon ) {
            keyStyle = icon
            if( isSubespecie ){
                $("li#li_legend_subespecies_"+self.el).show();
            }

        }
        // colocar o style em cache para não ficar recriando o mesmo para cada ponto
        if( styleCache[ keyStyle ]  ) {
            return styleCache[keyStyle];
        }
        if( ! icon ) {
            // criar o estilo do ponto
            if (isSubespecie) {
                strokeWidth = 3;
                radius = 3.5;
                // borda
                stroke = new ol.style.Stroke({
                    color: color,
                    width: strokeWidth,
                });
                // preenchimento
                var fillColor = (color.toUpperCase() == '#0000FF' ? '#FFFFFF' : '#000000')
                fill = new ol.style.Fill({
                    color: self.hex2rgba(fillColor, "1"),
                });
            } else {

                // borda preta de 0.5 pixels
                stroke = new ol.style.Stroke({
                    color: '#000000',
                    width: strokeWidth,
                });
                // preenchimento
                fill = new ol.style.Fill({
                    color: self.hex2rgba(color, "1"),
                })
                if( size > 1 ) {
                    textSize = new  ol.style.Text({
                        text: size.toString(),
                        font: ( size > 1 ? '9.7' : '0' ) + 'px sans-serif',
                        fill: new ol.style.Fill({
                            color: '#0b0b0b',
                        }),
                    })
                }
            }

            image = new ol.style.Circle( {
                radius: radius,
                stroke: stroke,
                fill  : fill
            } )
        } else {
            // adicionar o caminho padrão dos icones do mapa se o icon não possuir
            if( ! ( /\//.test( icon ) ) ) {
                icon = '/salve-consulta/assets/markers/'+icon
            }
            image = new ol.style.Icon({
                anchor: [0.5, 0.5],
                offset:[0,0],
                size: [iconSize,iconSize],
                scale: 1,
                opacity: 1,
                //anchorXUnits: 'fraction',
                //anchorYUnits: 'pixels',
                src: icon
            })
        }
        styleCache[keyStyle] = new ol.style.Style({ image: image, text  : textSize });

        // TODO - adicionar na legenda a cor ou a imagem aqui utilizando o layerId
        if( legendId ) {
            var $ulLegend = $("#ul-legend-"+legendId);
            if( $ulLegend.size() == 1 ) {
                var img;
                var li = document.createElement('li');
                var legend = document.createElement('i');
                li.className='ol3-legend-background';
                legend.className = 'fa fa-circle';
                legend.setAttribute('aria-hidden', "true");
                if (color.indexOf('.png') == -1) {
                    legend.style.color = color;
                } else {
                    var legend = document.createElement('img');
                    //var img = icon.color.replace(/[0-9]{1,2}x[0-9]{1,2}/, 'legend');
                    var img = icon.color
                    legend.setAttribute('src', "/salve-consulta/assets/markers/" + img);
                    legend.setAttribute('width', self.iconSize+"px;");
                    legend.setAttribute('height', self.iconSize+"px;");
                    legend.setAttribute('alt', '');
                    legend.style.marginTop = '-4px';
                }
                legend.style.marginLeft = '8px';
                legend.style.marginRight = '5px';
                li.appendChild(legend);
                $ulLegend.append(li)
            }
        }
        return styleCache[keyStyle];
    };
    /**
     * verificar se a feature está dentro do filtro selecioanado
     */
    this.isFeatureInFilter = function( feature ) {
        if (this.filters.length == 0 && this.filtersMetadata.length == 0 ) {
            this.filteredIds = [];
            return true; // não possui nenhum filtro ativo então exibir o ponto
        }
        var coords = feature.getGeometry().getCoordinates();
        var result = false;
        // verificar se o ponto está dentro dos filtros
        this.filters.map(function (filtro, i) {
            if( ! result ) {
                var filterGeometry = Object.values(filtro)[0].getGeometry();
                if (filterGeometry.intersectsCoordinate(coords)) {
                    result = true;
                }
            }
        })
        if( !result && this.filters.length == 0  ){
            this.filtersMetadata.map( function( filtro,i ){
                if( !result ) {
                    for( var key in filtro ){
                        // se for clusterizada tem features
                        if ( feature.get('features')) {
                            // pontos com cluster
                            feature.get('features').forEach(function (f) {
                                if( ! result ) {
                                    result = f.get(key) == filtro[key]
                                }
                            })
                        } else {
                            result = feature.get(key) == filtro[key]
                        }
                    }
                }
            });
        }
        return result;
    };


    /**
     * Localiza e adiciona uma animação no ponto informado
     * @param data - {id:1,bd:'salve']
     * @param center - true/false
     * @param pulsateCount - quantidade de piscadas
     */
    this.identifyPoint = function( data, center, pulsateCount) {
        var layerFound = null;
        if ( self.animating ) {
            return;
        }

        // valores padrão
        pulsateCount = pulsateCount || 5;
        if (data && data.id && data.bd) {
            if (data.bd != 'portalbio') {
                data.bd = data.bd = 'salve-consulta' ? 'salve' : data.bd;
            }
            center = (center == true ? true : false);
            self.animatedPoint = null;
            if ( data.id && data.bd ) {
                self.layers.map(function (layer, i) {
                    if( ! self.animatedPoint ) {
                        // procurar somente nos layer de ocorrencias
                        layerFound = layer;
                        var options = layer.get('options');

                        if (options && !self.animatedPoint && (options.type == 'geojson' || layer.get('options').cluster ) ) {
                            if ( layer.get('options').cluster) {
                                self.animatedPoint = layer.getSource().getSource().getFeatureById(data.id + data.bd.toLowerCase());
                                // procurar pelo novo formato utilizado no cluster baseDados-id
                                if( !self.animatedPoint){
                                    self.animatedPoint = layer.getSource().getSource().getFeatureById(data.bd.toLowerCase()+'-'+data.id);
                                }
                            } else {
                                self.animatedPoint = layer.getSource().getFeatureById(data.id + data.bd.toLowerCase());
                                if( !self.animatedPoint ){
                                    self.animatedPoint = layer.getSource().getFeatureById(data.bd.toLowerCase()+'-'+data.id);
                                }
                            }
                        }
                    }
                })
                if (self.animatedPoint && layerFound ) {
                    self.findGridRow(data, false); // pintar a primeira coluna para identificar a linha do gride
                    if (!layerFound.getVisible()) {
                        layerFound.setVisible(true);
                        self.updateLayerSwitch();
                    }
                }
            }
        }
        if( self.animatedPoint ) {
            self.animating = true;
            // verificar se o ponto está na área visível do mapa
            var mapExtent   = self.map.getView().calculateExtent( self.map.getSize() );
            var geomTemp     = self.animatedPoint.getGeometry();
            if ( !ol.extent.containsCoordinate(mapExtent, geomTemp.getCoordinates() ) ) {
                if( ! center ) {
                    self.animating = false;
                    return;
                }
            }
            //console.log('Animar o ponto');
            var style, image, r, currR, maxR, sign;
            var pontoTemp = new ol.Feature({
                geometry: geomTemp,
                name: 'pulse-' + new Date()
            });
            self.drawLayer.getSource().addFeature(pontoTemp);
            pontoTemp.setStyle(new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    fill: new ol.style.Fill({
                        color: 'rgba(255,255,255,0)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'rgba(255,0,0,0.6)',
                        width: 2
                    })
                })
            }));
            if ( center ) {
                //document.location.href="#"+self.el
                //console.log( "#"+self.el );
                $(document).scrollTop(225);
                self.map.getView().setCenter(geomTemp.getCoordinates());
            }
            image = pontoTemp.getStyle().getImage();
            style = pontoTemp.getStyle();
            image = style.getImage();
            r = image.getRadius();
            currR = r;
            maxR = 4 * r;
            sign = 1;
            var pulsate = function(event) {
                var vectorContext = event.vectorContext;
                if (currR > maxR) {
                    sign = -1;
                    pulsateCount--;
                } else if (currR < r) {
                    sign = 1;
                    if (!pulsateCount) {
                        self.map.un('postcompose', pulsate);
                        self.drawLayer.getSource().removeFeature(pontoTemp);
                        self.animating = false;
                        return;
                    }
                }
                currR += sign * 1.5;
                    vectorContext.drawFeature(pontoTemp, new ol.style.Style({
                    image: new ol.style.Circle({
                        radius: currR,
                        fill: image.getFill(),
                        stroke: image.getStroke()
                    })
                }));
                self.map.render();
            };
            self.map.on('postcompose', pulsate);
        }
    }

    /**
     * função para mostrar/esconder as linhas do grid em função do filtro atual
     */
    this.filterGrid = function(){
        var $grid;
        if( ! this.gridId ) {
            return;
        }
        $grid = $("#"+this.gridId.replace(/#/g,'') );
        if( $grid.size() == 0 )
        {
            return;
        }
        //console.log('Atualizar linhas do gride '+this.gridId)
        //console.log( this.filteredIds)
        $grid.find('tr').map( function(i,tr){
            var id = $(tr).data('id');
            var bd = $(tr).data('bd');
            if( bd != 'portalbio' ) {
                bd = ( bd== 'salve-consulta' ? 'salve': bd );
            }
            if( id && bd ) {
                var valid = false;
                if( self.filteredIds.length == 0 )
                {
                    valid=true; // não tem filtro ativo
                }
                else {
                    valid = self.filteredIds.filter(function (item) {
                        return String(item.id) == String(id) && item.bd == bd;
                    }).length > 0;
                }
                if ( valid ) {
                    $(tr).show();
                }
                else {
                    $(tr).hide();
                }
            }
        })
    }

    /**
     * criar item do menu de contexto
     * @param featureType - Point, polygon ou vazio para todos
     * @param faIcon - nome do icone da fonte awesome. ex: fa-plus
     * @param text - texto do menu
     * @param iconColor - cor do icone
     * @param callback - função de callback
     */
    this.addContextMenuItem = function(featureType, faIcon, text, iconColor, callback, name, hint){
        var menuItem={};
        var menuIcon='';
        hint = hint ? hint : '';
        iconColor   = ( iconColor ? iconColor: '#0000ff');
        featureType = (featureType ? featureType : '');
        if( faIcon )
        {
            menuIcon = '<i style="color:'+iconColor+';" class="fa '+faIcon+'"></i>&nbsp;'
        }
        menuItem.text = '<span title="'+hint+'">'+menuIcon+text+'</span>';
        menuItem.callback = callback;
        menuItem.featureType = featureType;
        menuItem.name = name; // identificado do item do menu
        this.contextMenuItems.push( menuItem )
    }
    /**
     * Método para reprocessar os pontos no mapa sem ler novamente do servidor
     */
    this.refresh = function() {
        //self.filters = [];
        // limpar os desenhos
        // self.drawLayer.getSource().clear();
        // this.reloadLayers()
        // self.updateFilter();
        self.map.getLayers().getArray().map( function(layer){
            var options = layer.get('options');
            if( options && options.filter ) {
                //console.log( 'refresh layer ' + layer.get('name'))
                if( ! options.cluster ) {
                    layer.getSource().changed()
                } else {
                    layer.getSource().getSource().changed()
                }
            }
        })
    }

    /**
     * método para localizar um layer pelo seu id
     * @param id
     * @returns {*}
     */
    this.getLayerById = function(id) {
        var layer;
        this.map.getLayers().forEach(function (lyr) {
            if (id == lyr.get('id')) {
                layer = lyr;
            }
        });
        return layer;
    }

    /**
     * recarregar os pontos de ocorrências
     */
    this.reloadLayers = function(params){
        var dataParams = app.params2data(params,{});
        self.map.getLayers().getArray().map( function(layer){
            var options = layer.get('options');
            if( options && options.filter ) {
                //console.log( 'refresh layer ' + layer.get('name'))
                layer.getSource().clear(true);
                if( options.cluster ) {
                    layer.getSource().getSource().clear(true);
                }
            }
        })
        // recarregar os pontos de ocorrencias
        self.refreshLayerOccurrences(dataParams);


    }

    /**
     * excutar uma ação ajax
     * @param options
     */
    this.ajaxAction = function( options, callback ){
        options = options || {};
        if( ! self.urlAction){
            return;
        }

        //console.log( options );
        //return;
        var msgLoading = ( options.msgLoading ? options.msgLoading : 'Gravando...');
        ajaxJson( self.urlAction,options,msgLoading,function( res ) {
            callback && callback( res );
            setTimeout( function() {
                self.callbackActions && self.callbackActions( res, options);
            },2000);
        });
    }

    /**
     * localizar a linha do gride referente ao ponto clicado no mapa
     * @param params - {id:1,bd:'salve'}
     * @param scroll - true/false para posicionar o gride na área visivel da tela ao localizar a linha
     * @param allPages - true/false para disparar a procura em todoas as paginas
     */
    this.findGridRow = function( params, scroll, allPages ){
        if( ! params || ! params.id )
        {
            return;
        }
        //console.log( 'FindGridRow chamada')
        //console.log( params );
        params.id = String( params.id );
        params.id = params.id ? params.id.replace(/[^0-9]/g,'') : ''; // só numeros
        var $grid = $("#"+this.gridId.replace(/#/g,'') );
        if( $grid.size() == 0  || $grid.find('tr').size() == 1 )
        {
            self.alert('Gride ainda não foi visualizado!');
            return;
        }
        var tableId = $grid.find('table').attr('id');
        var fixedHeaderHeight = 0;
        if( tableId ) {
            var gridId = tableId.replace(/^table/,'');
            var $trGridOcorrenciaColumns    = $("#tr"+gridId.ucFirst()+'Columns');
            var $trGridOcorrenciaFilters    = $("#tr"+gridId.ucFirst()+'Filters');
            var $trGridOcorrenciaPagination = $("#tr"+gridId.ucFirst()+'Pagination');
            fixedHeaderHeight += ( $trGridOcorrenciaColumns.height() || 0 );
            fixedHeaderHeight += ( $trGridOcorrenciaFilters.height() || 0 );
            fixedHeaderHeight += ( $trGridOcorrenciaPagination.height() || 0 );
         }
        //console.log('Atualizar linhas do gride '+this.gridId)
        //console.log( this.filteredIds)
        var found=false;
        $grid.find('tr').map( function(i,tr) {
            var $tr = $(tr);
            // remover marcação anterior
            $tr.find('td:first').css('background-color','#fff');
            if( !found ) {
                var trData = $tr.data();
                if( trData && trData.bd ) {
                     if( trData.bd != 'portalbio' ) {
                        trData.bd = trData.bd == 'salve-consulta' ? 'salve' : trData.bd;
                        trData.bd = trData.bd == 'consulta' ? 'salve' : trData.bd;
                    }
                    //console.log( trData.bd+'='+params.bd+'    '+trData.id+'='+params.id );
                }
                var idTr = ( trData.idOcorrencia ? trData.idOcorrencia : trData.id)
                if ( String(idTr) == String(params.id) && trData.bd == params.bd ) {
                    found = true;
                    //var idTr = $tr.prop('id');
                    if ( ! $tr.prop('id') ) {
                        idTr = 'tr-' + self.gridId + '-' + trData.id + '-' + trData.bd;
                        $tr.prop('id', idTr);
                    }
                    else
                    {
                        idTr = $tr.prop('id');
                    }
                    if( scroll ) {
                        document.location.href = "#" + idTr;
                        // retirar o item selecionado de baixo da toolbar
                        $('html').scrollTop( $('html').scrollTop() - 166 );
                        setTimeout(function(){
                            $("#"+idTr)[0].scrollIntoView(true);
                            // posicionar a row na area visível do gride
                            var $container = $("#"+self.gridId);
                            if( $container.size() == 1 ) {
                                // remover a coluna selecionada debaixo do cabecalho fixo

                                if( fixedHeaderHeight > 0 ) {
                                    var scrollTop = $container.scrollTop();
                                    $("#" + idTr).parent().parent().parent().scrollTop(scrollTop - fixedHeaderHeight);
                                }
                            }
                        },500);
                    }
                    $tr.find('td:first').css('background-color','#d6e2ca');
                    var bgColor = $tr.css('background-color');
                    $tr.css('background-color','#a0d3e2');
                    //window.setTimeout(function() {
                    for(var i=0;i<4;i++) {
                        $tr.fadeIn(200).fadeOut(200).fadeIn(200);
                    }
                    $tr.css('background-color',bgColor);
                    //},700);
                }
            }
        });

        // verificar se o gride está paginado e disparar a procura pela página
        if( allPages != false && ! found && $grid.find("#table"+$grid.attr('id')+'Pagination') ) {
            params.action='locateRow'
            params.scroll = scroll
            self.callbackActions && self.callbackActions( '', params);
        }
    };

    /**
     * posicionar o mapa nas coordenadas aplicando efeito de animação
     * @param x
     * @param y
     */
    this.moveToXY = function(x,y){
        this.map.getView().animate({
            center: ol.proj.fromLonLat([x, y]),
            duration: 500,
            /*easing: function(t) {
                // An elastic easing method (from https://github.com/DmitryBaranovskiy/raphael).
                return Math.pow(2, -10 * t) * Math.sin((t - 0.075) * (2 * Math.PI) / 0.3) + 1;
            }*/
        });
    };

    /**
     * função para centralizar o mapa em um determinado ponto
     * @param x
     * @param y
     */
    this.centerMap = function( x, y , zoom){
        //zoom = (typeof(zoom) == 'undefined' ? 13 : zoom );
        var lonLat = ol.proj.fromLonLat([x, y]);
        //this.map.getView().setCenter(lonLat);
        this.moveToXY(x,y);
        if( ! typeof(zoom) == 'undefined' ) {
            this.map.getView().setZoom( parseInt( zoom ) );
        };
    };

    this.createMeasureTooltip = function( feature ) {
        if( !feature )
        {
            return;
        }
        var randomId = 'tooltip_'+this.uid()
        var div         = document.createElement('div');
        var divContent = document.createElement('span');
        var divClose   = document.createElement('span');
        divContent.innerHTML = '&nbsp;'
        divContent.className = 'content-tooltip'
        divContent.style.marginRight ='8px';
        divClose.className = 'close-tooltip';
        divClose.innerHTML = 'x';
        divClose.style.position = 'absolute';
        divClose.style.top = '-3px';
        divClose.style.color = '#fffebb';
        divClose.style.cursor = 'pointer';
        divClose.setAttribute('title','Fechar');
        divClose.addEventListener('click', function (e) {
            self.removeTooltip(randomId, feature );
        } );
        div.append(divContent);
        div.append(divClose);
        feature.getGeometry().measureTooltipElement = div;
        feature.getGeometry().measureTooltipElement.setAttribute('id',randomId);
        feature.getGeometry().measureTooltipElement.className = 'tooltip tooltip-measure';
        feature.getGeometry().measureTooltip        = new ol.Overlay({
            element: feature.getGeometry().measureTooltipElement,
            offset: [0, -15],
            positioning: 'bottom-center',
        });
        //feature.getGeometry().measureTooltip.geometry = feature.getGeometry();
        this.map.addOverlay( feature.getGeometry().measureTooltip );
    };

    /**
     * format length output
     * @param {ol.geom.LineString} line
     * @return {string}
     */
    this.formatLength = function(line) {
        var length;
        var wgs84Sphere = new ol.Sphere(6378137);

        // considerar a curvatura da terra
        if (true /*geodesicCheckbox.checked*/) {
            var coordinates = line.getCoordinates();
            length = 0;
            var sourceProj = this.map.getView().getProjection();
            for (var i = 0, ii = coordinates.length - 1; i < ii; ++i) {
                var c1 = ol.proj.transform(coordinates[i], sourceProj, 'EPSG:4326');
                var c2 = ol.proj.transform(coordinates[i + 1], sourceProj, 'EPSG:4326');
                length += wgs84Sphere.haversineDistance(c1, c2);
            }
        } else {
            length = Math.round(line.getLength() * 100) / 100;
        }
        var output;
        if (length > 100) {
            length = (Math.round(length / 1000 * 100) / 100);
            if( typeof( length.formatMoney ) != 'undefined') {
                output = length.formatMoney() + ' ' + 'km';
            } else {
                output = length +'  '+'km';
            }
        } else {
            length = (Math.round(length * 100) / 100);
            if( typeof( length.formatMoney ) != 'undefined') {
                output = length.formatMoney() + ' ' + 'm';
            } else {
                output = length +'  '+'km';
            }
        }
        return output;
    };


    /**
     * format length output
     * @param {ol.geom.Polygon} polygon
     * @return {string}
     */
    this.formatArea = function(polygon) {
        var area;
        var wgs84Sphere = new ol.Sphere(6378137);
        // considerar a curvatura da terra
        if (true /* geodesicCheckbox.checked*/ ) {
            var sourceProj = this.map.getView().getProjection();
            var geom = /** @type {ol.geom.Polygon} */(polygon.clone().transform(
                sourceProj, 'EPSG:4326'));
          area = 0;
          if (geom instanceof ol.geom.MultiPolygon) {
              geom.getPolygons().forEach( function( poligon ) {
                 area += Math.abs(wgs84Sphere.geodesicArea( poligon.getLinearRing(0).getCoordinates() ) );
              })
          }
          else {
              area = Math.abs(wgs84Sphere.geodesicArea( geom.getLinearRing(0).getCoordinates()  ));
          }
        } else {
            area = polygon.getArea();
        }
        var unid='m';
        var casasDecimais=2;
        if ( area > 10000) {
            area = (Math.round(area / 1000000 * 100 ) / 100);
            unid='km';
            casasDecimais=0;
        } else {
            area =  (Math.round(area * 100) / 100 );
        }
        area = self.formatDecimal(area,casasDecimais,',','.');
        return area + ' ' + unid + '<sup>2</sup>';
    };

    this.calcArea = function( feature, evt ){
      // adicionar tooltip no poligono
      if( ! feature.getGeometry().measureTooltipElement) {
          self.createMeasureTooltip(feature);
          feature.getGeometry().on('change', function (evt) {
              var geom = evt.target;
              var output;
              var tooltipCoord;
              if (geom instanceof ol.geom.Polygon) {
                  output = self.formatArea(geom);
                  /*try {
                      var turfPolygon = turf.polygon(geom.getCoordinates());
                      var bboxPolygon = turf.bbox(turfPolygon.geometry);
                      //tooltipCoord = ol.proj.fromLonLat([5.2, 52.25], "EPSG:3857")
                      tooltipCoord = ol.proj.fromLonLat([ bboxPolygon[0], bboxPolygon[1] ])
                      console.log( tooltipCoord);
                      console.log( bboxPolygon );
                  } catch(e) {
                      //tooltipCoord = geom.getInteriorPoint().getCoordinates();
                      tooltipCoord = geom.getCoordinates()[0][0];
                  }
                  */
                  // encontrar o ponto mais ao SUL
                  tooltipCoord = geom.getCoordinates()[0][0];
                  geom.getCoordinates()[0].forEach(function(coord ){
                      if( coord[1] > tooltipCoord[1] )
                      {
                          tooltipCoord = coord
                      }
                  });
              }
              else if (geom instanceof ol.geom.MultiPolygon) {
                  output = self.formatArea(geom);
                  tooltipCoord = geom.getPolygons()[0].getLastCoordinate();
              } else if (geom instanceof ol.geom.LineString) {
                  output = self.formatLength(geom);
                  tooltipCoord = geom.getLastCoordinate();
              }
              if( feature.name == 'EOO' )
              {
                  var valEoo = output.split(' ')[0].replace('.','').trim();
                  output ='EOO '+output + '<a class="copy" title="copiar para área de transferência." href="javascript:void(0);" onClick="olMapCopy2Clip(\''+valEoo+'\');"><i class="glyphicon glyphicon-duplicate white"></i></a>';
              };
              var divContent = geom.measureTooltipElement.getElementsByClassName('content-tooltip')
              if( divContent.length == 1 ) {
                  divContent[0].innerHTML = output
              } else {
                  geom.measureTooltipElement.innerHTML = output;
              }
              geom.measureTooltip.setPosition(tooltipCoord);
          });
          feature.getGeometry().changed(); // atualizar medida da área
      }
    };

    this.removeTooltip = function( id, feature ) {
        if( feature && feature.getGeometry() ) {
            feature.getGeometry().measureTooltip = null;
            feature.getGeometry().measureTooltipElement = null;
        }
        this.map.getOverlays().getArray().slice(0).forEach( function(overlay) {
            if( overlay.getElement().getAttribute('id') == id ) {
                self.map.removeOverlay( overlay );
            }
        });
    }

    this.toggleLayerSwitch = function( evt ) {
        var $lw = $("#layer-switch-"+this.el);
        if( $lw.size() == 1) {

            //var $i = $(evt.target); //$lw.find('[name=ol-layer-switch-title]').find('i');
            var $i = $lw.find('i.fa');



            if( $i.hasClass('fa-chevron-down') ) // maximizar
            {
                $i.removeClass('fa-chevron-down').addClass('fa-chevron-up');
                $lw.css('height','auto');
            }
            else // minimizar
            {
                $i.removeClass('fa-chevron-up').addClass('fa-chevron-down');
                $lw.css('height','30px');
            }
        }
    };

    this.loadShpZip = function(options) {
       // var epsg = 4326;
       // var encoding ='UTF-8';
        var self=this;
        var reader;
        if( !self.file )
        {
            self.alert('Nenhum arquivo selecionado!');
            return;
        }

        if( self.file && /\.zip$/.test( self.file.name ) ) {
            var $form = $('#form-dlg-add-shp-' + self.el);
            var divId = 'dlg-add-shp-' + self.el;

            $form.find('.fa-spin').show();
            $form.find('[name=btnAdicionar]').hide();
            try {
                reader = new FileReader();
                reader.onload = function()
                {
                    if( reader.readyState == 2 ) {
                        shp( reader.result ).then( function( geojson ) {
                            self.file = null;
                            $form.find('.fa-spin').hide();
                            $form.find('[name=btnAdicionar]').show();
                            $form[0].reset();
                            $('#' + divId).hide();
                            self.createLayer({
                                type: 'geojson'
                                , name: options.layerName + ' (shp)'
                                , shp:true
                                , visible: true
                                , data: geojson
                                , zIndex: 2
                            });
                        },function() {
                            $form.find('.fa-spin').hide();
                            $form.find('[name=btnAdicionar]').show();
                            self.alert('Arquivo shapefile inválido.')

                        });
                    } else if( reader.error ) {
                        $form.find('.fa-spin').hide();
                        $form.find('[name=btnAdicionar]').show();
                        self.alert('Erro na leitura do arquivo shapefile.');
                    }
                };
                reader.readAsArrayBuffer(self.file);
/*
                loadshp({
                    url: self.file,
                    encoding: encoding,
                    EPSG: epsg
                }, function (data) {
                    if( data.error )
                    {
                        $form.find('.fa-spin').hide();
                        $form.find('[name=btnAdicionar]').show();
                        alert( data.error );
                    }
                    else {
                        self.file = null;
                        var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
                        URL.createObjectURL(new Blob([JSON.stringify(data)], {type: "application/json"}));
                        self.createLayer({
                            type: 'geojson'
                            , name: options.layerName + ' (shp)'
                            , shp:true
                            , visible: true
                            , data: data
                            , zIndex: 2
                            //, style: self.occurrenceStyle
                        });
                        // enquadrar o mapa no shapefile
                        var extent = ol.proj.transformExtent( data.bbox, "EPSG:4326", "EPSG:3857");
                        self.map.getView().fit(extent,self.map.getSize() );
                        self.map.getView().setZoom(self.map.getView().getZoom()-1)
                        $form.find('.fa-spin').hide();
                        $form.find('[name=btnAdicionar]').show();
                        $form[0].reset();
                        $('#' + divId).hide();
                    }
                });
                */
            } catch( e )
            {
                $form.find('.fa-spin').hide();
                $form.find('[name=btnAdicionar]').show();
                self.alert( e );
            }
        } else {
            $('.modal').modal('show');
        }
    };

    /**
     * método para validar coordenadas de pontos inválidos
     * ex: Infinity -Infinity, NaN
     */
    this.getGeojsonValidFeatures = function( geojson ) {
        try {
            var features = (new ol.format.GeoJSON()).readFeatures(geojson, {featureProjection: 'EPSG:3857'});
            features = features.filter(function (feature, i) {
                try {
                    var geometry = feature.getGeometry();
                    if (geometry.getType() == 'Point') {
                        var coords = geometry.getCoordinates();
                        if (isNaN(coords[1]) || isNaN(coords[0]) || Math.abs(coords[0]) === Infinity) {
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
                } catch (e) {
                    return false;
                }
            });
            return features;
        } catch (e) {
            return null;
        }
    };
    this.getTurfPoligonBrasil=function() {

        if( typeof brasilGeoJson == 'undefined' ) {
            self.alert('Polígono do Brasil não foi informado. O recorte BRASIL não será feito.');
        } else {
            if (!turfPoligonoBrasil) {
                // formatador para geoJSON
                var format = new ol.format.GeoJSON();
                var brasil = format.readFeatures(brasilGeoJson)[0];
                //var brasil = format.readFeatures(brasilGeoJson2)[0];
                var geometry = brasil.getGeometry();
                if (geometry.getType() == 'MultiPolygon') {
                    turfPoligonoBrasil = turf.polygon(geometry.getPolygons()[0].getCoordinates())
                    //turfPoligonoBrasil = turf.multiPolygon( geometry.getCoordinates() );
                } else if (geometry.getType() == 'Polygon') {
                    turfPoligonoBrasil = turf.polygon(geometry.getCoordinates());
                }
                // utilizar somente o primeiro poligono do mapa do brasil para o recorte
                //turfPoligonoBrasil = turf.polygon(geojson.geometry.coordinates[0]);
            }
        }
        return turfPoligonoBrasil
    }

    this.getTurfPoligonZee = function() {
        if( ! turfPoligonoZee ) {
            var wkt = new ol.format.WKT();
            var feature = wkt.readFeature( zeePoligono, {
                dataProjection: 'EPSG:3426',
                featureProjection: 'EPSG:3426'
            });
            var geometry = feature.getGeometry();
            if( geometry.getType() == 'MultiPolygon') {
                turfPoligonoZee = turf.polygon( geometry.getPolygons()[0].getCoordinates() )
            } else if( geometry.getType() == 'Polygon') {
                turfPoligonoZee = turf.polygon( geometry.getCoordinates() );
            }
        }
        return turfPoligonoZee
    }

    /**
     * Executar as ações da caixa de ferramenta de cálculo da EOO
     * @param action
     */
    this.btnToolBoxEooClick = function( action, divId ) {
        switch ( action ) {
            case 'undo':
                self.loadEoo(true);
                break;
            case 'save':
                this.saveEoo();
                break;
            case 'delete':
                this.deleteEoo();
                break;
            case 'calc':
                if( this.calcularEoo() ){
                    $('button[id^='+divId+'_eoo_btn_shp]').css('border','none');
                    $('button[id^='+divId+'_eoo_btn_shp]').data('active',false)
                }
                break;
            case 'loadShp1':
                this.carregarEooShp('shp1', divId);
                break;
            case 'loadShp2':
                this.carregarEooShp('shp2', divId);
                break;
            case 'loadShp3':
                this.carregarEooShp('shp3', divId);
                break;
        }
    }

    /**
     * Método para calcuar EOO selecionando poligonos em um shapefile
     */
    this.carregarEooShp = function(shapeName, divId) {
        // evitar chamadas consecutivas
        if( running ) {
            return;
        }
        //this.deleteEoo();
        // configurar os botoes da toolbar
        if( divId ) {
            var $button = $('button#'+divId+'_eoo_btn_'+shapeName);
            var active = $button.data('active');
            $('button#'+divId+'_eoo_btn_shp1').css('border','none');
            $('button#'+divId+'_eoo_btn_shp2').css('border','none');
            $('button#'+divId+'_eoo_btn_shp3').css('border','none');
            $('button#'+divId+'_eoo_btn_shp1').data('active', false)
            $('button#'+divId+'_eoo_btn_shp2').data('active', false)
            $('button#'+divId+'_eoo_btn_shp3').data('active', false)
            $("#"+divId+'_footer_eoo').hide();
            if( active ) {
                // limpar a camada
                this.eooLayer.getSource().clear();
                // adicionar a EOO atual
                if( self.eooFeature ){
                    self.eooLayer.getSource().addFeature( self.eooFeature );
                }
                return;
            }
            if( this.getZoom() < 6 ){
                self.alert('Para utilizar este recurso é necessário que o zoom do mapa esteja acima de 5. Atual é ' + this.getZoom() );
                return;
            }
            $button.data('active', true);
            $button.css('border','1px solid red');
            $("#"+divId+'_footer_eoo').show();
        }

        if( hydroshedLoaded.id == shapeName ){
            this.eooLayerRefresh()
            return;
        }

        this.eooLayer.getSource().clear();
        // ler shape file
        var nomeShape = '';
        var urlShp='';
        if( shapeName == 'shp1') {
            urlShp = urlSalve+'api/shapefile/hydrosheds_level8';
            nomeShape='hydroSHEDS level 8';
        } else if( shapeName == 'shp2') {
            urlShp = urlSalve+'api/shapefile/hydrosheds_level9';
            nomeShape='hydroSHEDS level 9';
        } else if( shapeName == 'shp3') {
            urlShp = urlSalve+'api/shapefile/hydrosheds_level10';
            nomeShape='hydroSHEDS level 10';
        }

        if( !urlShp ) {
            return;
        }
        running = true;
        app.blockUI('Carregando '+nomeShape+'...', function () {
            shp(urlShp).then(function (geojson) {
                var features = self.getGeojsonValidFeatures(geojson);
                hydroshedLoaded.id = shapeName;
                hydroshedLoaded.features = [];
                features.map(function (feat, index) {
                    feat.setStyle(self.featureUnSelectedStyle);
                    feat.set('selected', false);
                    feat.setId('hs-'+index)
                    //self.eooLayer.getSource().addFeature(feat);
                    hydroshedLoaded.features.push( feat );
                })
                self.eooLayerRefresh();
                // adicionar a EOO atual
                //if( self.eooFeature ){
                    //self.eooLayer.getSource().addFeature( self.eooFeature );
                //}
                running=false;
                app.unblockUI();
            }).catch(function(error){
                running=false;
                app.unblockUI();
                self.alert('Shapefile não encontrado.');
                console.log( 'Erro ol3Map.carregarEooShp(): '+ error );
            });
        });
    }

    /**
     * Exibir as hydrosheads somente da área bbox do mapa
     */
    this.eooLayerRefresh = function(){
        // limpar a camada
        this.eooLayer.getSource().clear();
        var bbox = self.map.getView().calculateExtent( self.map.getSize() );
        hydroshedLoaded.features.map(function (feature, index) {
            var geometry = feature.getGeometry();
            if ( geometry.intersectsExtent( bbox ) ) {
                if ( ! self.eooLayer.getSource().getFeatureById( feature.getId() ) ) {
                    // se o poligono estiver dentro da eoo atual, adicionar estilo selected
                    self.eooLayer.getSource().addFeature( feature );
                    // se a feature estiver selecionada marcar com o estilo de selected
                    if( hydroshedLoaded.selectedFeatures.filter( function( f ) {
                        return feature.getId() == f.getId();
                    }).length > 0 ) {
                        feature.setStyle( self.featureSelectedStyle);
                    }
                }
            }
        });
        if( self.eooFeature ){
            // a seleção dos poligonos dentro da eoo não está funcionando.

            //var teste = new ol.format.ogc.filter.Within(geometryName, geometry, opt_srsName);
            //var teste = new ol.format.filter.within()
            /*
            // ver esta ferramenta: https://bjornharrtell.github.io/jsts/
            // https://openlayers.org/en/master/examples/jsts.html?q=jsts

            var polygonGeometry = self.eooFeature.getGeometry();
            self.eooLayer.getSource().forEachFeatureInExtent( eooExtent, function( feature ) {
                var coords = feature.getGeometry().getCoordinates();
                console.log( polygonGeometry.intersectsCoordinate(coords) );
            });
            */
            /*
            var eooClone = self.eooFeature.clone();
            eooClone.getGeometry().transform('EPSG:3857', 'EPSG:4326');
            var turfEooPolygon     = turf.polygon( eooClone.getGeometry().getCoordinates() );
            var eooExtent = self.eooFeature.getGeometry().getExtent();
            //var eooExtent = self.map.getView().calculateExtent( self.map.getSize() );
            self.eooLayer.getSource().forEachFeatureInExtent( eooExtent, function(feature ) {
                var coords = feature.getGeometry().getCoordinates();
                var result = true;
                coords.forEach( function( pontos ) {
                    if( result ) {
                        pontos.forEach(function (ponto) {
                            var turfPoint = turf.point(ol.proj.transform([parseFloat(ponto[0]), parseFloat(ponto[1])], 'EPSG:3857', 'EPSG:4326'));
                            // if( result && ! turf.booleanPointInPolygon(turfPoint, turfEooPolygon) ){
                            //     result=false
                            // }
                            // if (result && !turf.within(turfPoint, turfEooPolygon)) {
                            //     result = false;
                            // }
                            // if ( result && !ol.extent.containsCoordinate( eooExtent, ponto ) ) {
                            //     result=false;
                            // }
                            // if( result && ! self.eooFeature.getGeometry().intersectsCoordinate(ponto) ){
                            //     result=false;
                            // }
                        })
                    }
                });
                if( result ) {
                    feature.setStyle(self.featureSelectedStyle);
                    feature.set('selected', true);
                }
            });
            */
            //debugger;
            //console.log( turf.booleanWithin(self.eooFeature, self.eooFeature) )

            //var eooExtent   = self.eooFeature.getGeometry().getExtent();
            //var featureClone = self.eooFeature.clone();
            //featureClone.getGeometry().transform('EPSG:3857', 'EPSG:4326');
            // var turfEoo     = turf.polygon( featureClone.getGeometry().getCoordinates() );
            //var format = new ol.format.GeoJSON()
            //var turfEoo  = format.writeFeatureObject( featureClone );
            //var turfEoo     = turf.polygon( self.eooFeature.getGeometry().getCoordinates() );
            /*
                self.eooLayer.getSource().forEachFeatureInExtent( eooExtent, function(feature ){
                var featureClone = feature.clone();
                featureClone.getGeometry().transform('EPSG:3857', 'EPSG:4326');
                //var turfPolygon  =  turf.polygon( featureClone.getGeometry().getCoordinates() );
                //var turfPolygon  =  turf.polygon( feature.getGeometry().getCoordinates() );
                var turfPolygon  =  format.writeFeatureObject( featureClone );

                // booleanWithin

                //if( turf.booleanOverlap( turfEoo, turfPolygon )
                if( turf.booleanContains( turfEoo, turfPolygon )
                //|| turf.booleanWithin( turfPolygon, turfEoo)
                //|| turf.intersect( turfEoo, turfPolygon)
                //|| turf.intersect( turfPolygon, turfEoo)
                ) {
                //if ( ol.extent.containsExtent( eooExtent, feature.getGeometry().getExtent())) {
                    //debugger;
                    feature.setStyle(self.featureSelectedStyle);
                    feature.set('selected', true);
                }
            });
            */
            self.eooLayer.getSource().addFeature( self.eooFeature );
        }
    }

    /**
     * Método para calcular a EOO pelos poligons selecionados no shapefile
     */
    this.calcularEooShp = function() {
        if( running ){
            return;
        }
        running = true
        this.cancelDrawInteraction();
        /*if( this.eooFeature ) {
            this.eooLayer.getSource().removeFeature(this.eooFeature);
            this.eooFeature = null;
        }*/
        var hull, pts=[];
        //var selectedFeatures = [];
        var tcollection = turf.featureCollection([])
        hydroshedLoaded.selectedFeatures = [];
        this.eooLayer.getSource().getFeatures().forEach(function (feat) {
            //if( feat.getStyle() == this.featureSelectedStyle ){

            if( feat.get('selected') && feat != self.eooFeature ){
                hydroshedLoaded.selectedFeatures.push( feat );
                var featureClone = feat.clone();
                featureClone.getGeometry().transform('EPSG:3857', 'EPSG:4326');
                if( featureClone.getGeometry().getType() == 'MultiPolygon') {
                    featureClone.getGeometry().getPolygons().forEach( function( p ){
                        var tpolygon = turf.polygon( p.getCoordinates() );
                        tcollection.features.push( tpolygon );
                    });
                } else if( featureClone.getGeometry().getType() == 'Polygon') {
                    var tpolygon = turf.polygon( featureClone.getGeometry().getCoordinates() );
                    tcollection.features.push( tpolygon );
                }
            }
        });
        if( tcollection.features.length > 0 ) {
            var hull = turf.convex(tcollection);
            var hc = hull.geometry.coordinates;
            var poligon = new ol.geom.Polygon(hc);
            //poligon.transform('EPSG:4326','EPSG:3857')
            if( !this.eooFeature ) {
                this.eooFeature = new ol.Feature({
                    geometry: poligon,
                    style: this.drawStyle,
                    id: 'calc-aoo',
                    text: '', // não exibir popup ao clicar
                    zIndex: 2
                });
                this.eooFeature.name='EOO'
                this.eooFeature.setId('calc-eoo');
                this.eooFeature.set('id', 'calc-eoo');
                this.eooFeature.set('selected', true);// para ser salva no banco de dados
                this.eooFeature.set('edit', true); // pode ser editada
                this.eooLayer.getSource().addFeature(this.eooFeature);
                this.eooFeature.getGeometry().on('change', function (evt) {
                    self.updateEooValue();
                });
            } else {
                this.eooFeature.setGeometry(poligon)
            }
            this.intersectBrasil( this.eooFeature );
        } else {
            // nenhuma área selecionada
            if( this.eooFeature ) {
                this.eooLayer.getSource().removeFeature(this.eooFeature);
                this.eooFeature = null;
            }
        }
        self.updateEooValue();
        running=false;
    }

    /**
     * Método para calcular a Extensão de Ocorrência (EOO) pelos pontos utilizados
     */
    this.calcularEoo = function() {
        function cross(a, b, o) {
            return (a[0] - o[0]) * (b[1] - o[1]) - (a[1] - o[1]) * (b[0] - o[0])
        }
        self.cancelDrawInteraction(); // retirar modo de desenho ao fechar o poligono

        //ler todos os pontos utilizados na avaliação
        var points = this.getValidPoints();

        // criar o poligono da EOO e selecionar somente os pontos que estão dentro do brasil
        points.sort(function (a, b) {
            return a[0] == b[0] ? a[1] - b[1] : a[0] - b[0];
        });

        var lower = [];
        for (var i = 0; i < points.length; i++) {
            while (lower.length >= 2 && cross(lower[lower.length - 2], lower[lower.length - 1], points[i]) <= 0) {
                lower.pop();
            }
            lower.push(points[i]);
        }
        var upper = [];
        for (var i = points.length - 1; i >= 0; i--) {
            while (upper.length >= 2 && cross(upper[upper.length - 2], upper[upper.length - 1], points[i]) <= 0) {
                upper.pop();
            }
            upper.push(points[i]);
        }
        upper.pop();
        lower.pop();
        var coords = lower.concat(upper);
        // fechar o polygono
        coords.push( coords[0] );

        if( coords.length < 4 )
        {
            self.alert('Quantidade de ocorrências UTILIZADAS NA AVALIAÇÃO é insuficiente para o cálculo da EOO.');
            return false;
        }

        // limpar o layer
        this.eooLayer.getSource().clear();


        // criar o poligono
        var polyCoords = [];
        //var coordsTemp = [];
        coords.forEach(function( coord ) {
            polyCoords.push( coord )
            //coordsTemp.push( ol.proj.transform( [ parseFloat(coord[0] ), parseFloat( coord[1]) ], 'EPSG:3857','EPSG:4326') );
        });

        var feature = new ol.Feature({
            geometry: new ol.geom.Polygon([polyCoords])
        });
        feature.name='EOO'
        feature.setId('calc-eoo-pontos');
        feature.set('id', 'calc-eoo-pontos');
        feature.set('edit', true); // pode ser editada


        // converter o poligono da EOO para EPSG:4326
        feature.getGeometry().transform('EPSG:3857','EPSG:4326')
        self.intersectBrasil(feature);
        self.eooLayer.getSource().addFeature( feature );
        self.eooFeature = feature;

        // centralizar o mapa
        var extent = feature.getGeometry().getExtent();
        var center = ol.proj.transform(ol.extent.getCenter( extent ), 'EPSG:3857', 'EPSG:4326');
        self.map.getView().fit( extent, self.map.getSize() );
        self.flyTo( ol.proj.fromLonLat( center ), function(){})
        //self.map.getView().fit(extent,self.map.getSize() );
        //self.map.getView().setZoom(self.map.getView().getZoom()-1)
        //self.calcArea( feature );
        self.updateEooValue();
        feature.getGeometry().on('change', function (evt) {
            self.updateEooValue();
        });
        return true;
    };

    /**
     * Método para ajustar o polígon para não ultrapassar as fronteiras do Brasil
     * @param polygon
     * @returns feature
     */
    this.intersectBrasil = function( polygon ){
        var format = new ol.format.GeoJSON();

        self.getTurfPoligonBrasil();

        // transformar poligono da EOO para o formato TURF
        var turfPoligonEoo = format.writeFeatureObject( polygon );

        // calcular a intersecção
        var intersect=null;
        if( turfPoligonoBrasil ) {
            intersect = turf.intersect(turfPoligonoBrasil, turfPoligonEoo);
        }
        if( intersect ) {
            // transformar a intersecção para o formato EPSG:3857
            intersect = format.readFeature(intersect);
            polygon.setGeometry( intersect.getGeometry() );
        }
        polygon.getGeometry().transform('EPSG:4326','EPSG:3857');
        return polygon;
    }

    /**
     * mostrar/esconder a caixa de ferramentas para cáldulo da EOO
     */
    this.toggleBoxEoo = function() {
        var $div = $("#map-draw-toolbar-" + self.el + "_eoo");
        if ( $div.is(":visible") ) {
            //this.aooLayer.setVisible(false);
            $div.hide();
            this.editingEoo=false;
        } else {
            //this.aooLayer.setVisible(true);
            $div.show();
            this.editingEoo=true;
            if( ! this.eooLayer.getVisible() ) {
                this.eooLayer.setVisible(true);
            }
            if( self.eooFeature ) {
                self.centerFeatureOnMap( self.eooFeature );
            }
        }
    }

    /**
     * mostrar/esconder a caixa de ferramentas para cáldulo da AOO
     */
    this.toggleBoxAoo = function() {
        var $div = $("#map-draw-toolbar-" + self.el + "_aoo");
        if ( $div.is(":visible") ) {
            //this.aooLayer.setVisible(false);
            $div.hide();
            this.editingAoo=false;
        } else {
            //this.aooLayer.setVisible(true);
            $div.show();
            this.editingAoo=true;
        }
    }

   /**
     * mostrar/esconder o formulado de filtros do mapa
     */
    this.toggleDialogoFilterMaps = function() {

        var $div = $("#dlg-filters-map-" + self.el);
        if ( $div.is(":visible") ) {
            $div.hide();
        } else {
            $div.show();
        }
    }

    /**
     * método para encontrar a área ( square ) referente às coordendas x,y na projeção EPSG:3587
     * a partir das corrdenadas 0,0
     */
    this.findSquare = function( x, y, in4326 ) {
        //var turfPoint = turf.point( ol.proj.transform( [ parseFloat(coords[0] ), parseFloat( coords[1]) ], 'EPSG:3857','EPSG:4326') );
        //var lonLat = new OpenLayers.LonLat(-0.1279688, 51.5077286).transform(epsg4326, projectTo);
        if( ! this.editingAoo || ! this.aooLayer.getVisible() ) {
            return;
        }
        var xyStart = ol.proj.transform([0, 0], 'EPSG:4326', 'EPSG:3857');
        var xy = [x, y];
        // se as coordendas estiverem na projeção 3426, passar para 3587
        if ( in4326 ) {
            xy = ol.proj.transform(xy, 'EPSG:4326', 'EPSG:3857');
        }
        // criar objetos com as coordendas para facilitar leitura das coordenadas X e Y
        var coordPontoZero = {x: xyStart[0], y: xyStart[1]};
        var coordPonto = {x: xy[0], y: xy[1]};

        // calcular o ponto X
        // se a coordendada x do ponto solicitado for
        // MAIOR que a coord x do ponto zero, então aumentar a coord.x do ponto zero até ficar maior
        // que a coord.x do ponto solicitado
        if (coordPonto.x > coordPontoZero.x) {
            while (coordPonto.x > coordPontoZero.x) {
                coordPontoZero.x += this.x2km;
            }
        }

        // se a coordendada X do ponto solicitado for
        // menor que o X do ponto zero, então diminuir o X zero até ficar menor
        // que o ponto X solicitado
        if (coordPonto.x < coordPontoZero.x) {
            while (coordPonto.x < coordPontoZero.x) {
                coordPontoZero.x -= this.x2km;
            }
        }
        // calcular o ponto Y
        // se a coordendada y do ponto solicitado for MAIOR
        // que o y do ponto zero, então aumentar o y zero até ficar MAIOR
        // que o ponto Y solicitado
        if (coordPonto.y > coordPontoZero.y) {
            while (coordPonto.y > coordPontoZero.y) {
                coordPontoZero.y += this.y2km;
            }
        }
        // se a coordendada Y do ponto solicitado for
        // menor que o Y do ponto zero, então diminuir o Y zero até ficar menor
        // que o ponto Y solicitado
        if (coordPonto.y < coordPontoZero.y) {
            while (coordPonto.y < coordPontoZero.y) {
                coordPontoZero.y -= this.y2km;
            }
        }
        // voltar para a projeçao 3426
        xy = ol.proj.transform([coordPontoZero.x, coordPontoZero.y], 'EPSG:3857', 'EPSG:4326');
        // ponto 1 do quadrante que contem o ponto solicitado
        return {x: xy[0], y: xy[1], epsg: '3426'}
    };

    /**
     * método para desenha o retangulo 2x2 referente às coordenada x,y informadas
     */
    this.drawSquare = function(x,y, in4326) {
        if( ! this.editingAoo || ! this.aooLayer.getVisible() ) {
            return;
        }
        var coordsXY = [x, y];
        // se as coordenadas estiverem na projeção 3426, transformar para 3857
        if (in4326) {
            coordsXY = ol.proj.transform(coordsXY, 'EPSG:4326', 'EPSG:3857');
        }
        // criar o square de 2km a partir do ponto 0
        var p1 = coordsXY;
        var p2 = [p1[0], p1[1] + this.y2km];
        var p3 = [p2[0] + this.x2km, p2[1]];
        var p4 = [p1[0] + this.x2km, p1[1]];
        var feature = new ol.Feature({
            geometry: new ol.geom.Polygon([[p1, p2, p3, p4, p1]]),
            id: 'calc-aoo'
        });
        if( feature ) {
            this.aooLayer.getSource().addFeature( feature );
            var divId = 'map-draw-toolbar-' + self.el;
            $("#"+divId + '_aoo_btn_save').css('color','#facece');
        }
        return feature;

        /*var exists = false;
        var turfQuadrado = turf.polygon( feature.getGeometry().getCoordinates() )
        var turfPoint = turf.point( coordsXY );

        this.aooLayer.getSource().getFeatures().some( function( f ) {
            console.log(f.getGeometry().getType());
            console.log(f);
            var turfPoligono = turf.polygon(f.getGeometry().getCoordinates());
            if( turf.booleanPointInPolygon( turfPoint, turfPoligono ) {
                exists = true;
            }
            //var intersect = turf.intersect(turfPoligono, turfQuadrado);
            //if (intersect) {
            //    exists = true;
            //}
        });
        console.log( exists );

         */
        /*var pixel = self.map.getPixelFromCoordinate( coordsXY );
        var existe = self.map.forEachFeatureAtPixel( pixel, function ( f1 ) {
        if( f1.get('id') =='calc-aoo') {
        return f1;
        }
        });
        if( existe ) {
        console.log( existe )
        } else {
        console.log('Não existe');
        }

        */
        /*
        feature.getGeometry().getCoordinates().forEach(function(coordinate){
            if( ! existe ) {
                coordinate.map(function (coord) {
                    if ( ! existe) {
                        console.log( coord[0] + ' e '+coord[1] )
                        existe = self.aooLayer.getSource().getFeaturesAtCoordinate( coord[0]).length >0
                             || self.aooLayer.getSource().getFeaturesAtCoordinate( coord[1]).length > 0
                    }
                });
            }
        });
        console.log( existe );
        //console.log( self.aooLayer.getSource().getFeaturesInExtent( feature.getGeometry().getExtent() ) )
        */
        /*
                // adicionar a feature se ele ainda não estiver dentro de nanhum quadrado
                if( this.aooLayer.getSource().getFeaturesAtCoordinate( p1 ).length == 0
                    && this.aooLayer.getSource().getFeaturesAtCoordinate( p2 ).length == 0
                    && this.aooLayer.getSource().getFeaturesAtCoordinate( p3 ).length == 0
                    && this.aooLayer.getSource().getFeaturesAtCoordinate( p4 ).length == 0 ) {
                        this.aooLayer.getSource().addFeature( feature );
                        return feature;
                }

         */
        /*
        var coordNova = JSON.stringify( feature.getGeometry().getCoordinates() );
        if( ! this.aooLayer.getSource().getFeatures().some( function( f) {
            if( f.getGeometry().getType().toLowerCase() =='multipolygon') {
                f.getGeometry().getCoordinates().forEach( function( coord ){
                    return coordNova == JSON.stringify( coord );
                });
            } else {
                return coordNova == JSON.stringify(f.getGeometry().getCoordinates());
            }
            return false;
        }) ) {
            this.aooLayer.getSource().addFeature( feature );
            //var format 		= new ol.format.GeoJSON();
            //var geojson = format.writeFeaturesObject( this.aooLayer.getSource().getFeatures() );
            //console.log( geojson);
            return feature;
        };
        return null;*/
    };

    /**
     * método para localizar o square referente ao ponto e cria-lo no mapa
     */
    this.createSquare= function( x,y, in4326, newSquare ) {
        //criar o square para cada ponto
        if( ! this.editingAoo || ! this.aooLayer.getVisible() ) {
            return;
        }
        var feature=null;
        var square=null
        // se as coordenadas estiverem na projeção 3426, transformar para 3857
        if (in4326) {
            coordsXY = ol.proj.transform(coordsXY, 'EPSG:4326', 'EPSG:3857');
            x=coordsXY[0];
            y=coordsXY[1];
        }
        if( ! newSquare ) {
            // verificar se já existe algum square no ponto clicado
            var features = this.aooLayer.getSource().getFeaturesAtCoordinate( [x,y] )
            if( features.length == 0 ) {
                square = this.findSquare(x, y, false); // localiza o square do ponto na grade virtual
            }
        } else {
            square = newSquare
        }
        // se retornar o epsg é porque encontrou o square
        if ( square && square.epsg ) {
            feature = this.drawSquare(square.x, square.y, true);
            /*if( feature ) {
                if (this.map.getView().getZoom() < 11) {
                    this.setZoom(11);
                    this.setCenter(x, y, in4326)
                }
            }*/
        }
        return feature;
    };

    /**
     * método para excluir o square sob o ponto clicado
     * @param x
     * @param y
     * @param in4326
     */
    this.deleteSquare = function( x,y,in4326) {
        //criar o square para cada ponto
        if( ! this.editingAoo || ! this.aooLayer.getVisible() ) {
            return;
        }
        var feature = null;
        var square = null
        // se as coordenadas estiverem na projeção 3426, transformar para 3857
        if (in4326) {
            coordsXY = ol.proj.transform(coordsXY, 'EPSG:4326', 'EPSG:3857');
            x=coordsXY[0];
            y=coordsXY[1];
        }
        var features = this.aooLayer.getSource().getFeaturesAtCoordinate([x, y])
        if ( features.length == 1 ) {
            this.aooLayer.getSource().removeFeature( features[0] );
            var divId = 'map-draw-toolbar-' + this.el;
            $("#"+divId + '_aoo_btn_save').css('color','red');
        }
    }

    /**
     * método para criar os quadrados 2x2 que intersectarem o desenho fornecido
     */
    this.createSquareUsingFeature = function( maskFeature ) {
        if( ! this.editingAoo || ! this.aooLayer.getVisible() ) {
            return;
        }
        var self=this;
        var maxSquares=10000; // maximo de areas que podem ser adicionadas ao mapa
        var countSquares=0; // contador de areas adicionadas
        var maxLoops=10000; // evitar travar no loop while
        if( ! maskFeature && this.interactionSelect.getFeatures().getLength() > 0) {
            this.interactionSelect.getFeatures().forEach(function(feature,i){
                maskFeature=feature;
            })
        }
        if( maskFeature ) {
            if( typeof( maskFeature ) === 'string' ) {
                maskFeature = self.drawWKT(maskFeature);
            }
            // calcular a BBOX da maskFeature
            var bboxMaskFeature = self.getFeatureBbox( maskFeature );
            bboxMaskFeature.setId('bboxMaskFeatureTemp'); // definir id para poder excluir depois
            this.drawLayer.getSource().addFeature( bboxMaskFeature );

            //encontrar o ponto do canto superior esquerdo do bboxMaskFeature
            /*posicao dos pontos  no objeto geometry().getCoordinates()
            1 = A		A- - - -B
            2 = B    	|		|
            0 = C    	|		|
            3 = D    	C- - - -D
            */

            var coords = bboxMaskFeature.getGeometry().getCoordinates()[0];
            var pontoA = coords[1];
            var pontoB = coords[2];
            var pontoC = coords[0];
            var pontoD = coords[3];
            var turfPoligono = null;
            var maskFeatureGeometry = maskFeature.getGeometry();
            var maskFeatureCoordinates =  maskFeature.getGeometry().getCoordinates();

            // não precisa mais do bboxMaskFeature
            this.drawLayer.getSource().getFeatures().forEach( function(source_feature) {
                var source_feature_id = source_feature.getId();
                if ( source_feature_id === 'bboxMaskFeatureTemp' || source_feature==bboxMaskFeature)
                {
                    self.drawLayer.getSource().removeFeature(bboxMaskFeature);
                }
            });

            // quando for multiPolygon fazer loop e chamar recursivamente a createSquare
            if( maskFeatureGeometry.getType() == 'MultiPolygon' ) {
                maskFeatureCoordinates.map( function( coords ,i ) {
                    try {
                        var newFeature = new ol.Feature({
                            geometry: new ol.geom.Polygon(coords)
                        })
                        if( newFeature ) {
                            self.createSquareUsingFeature( newFeature );
                        }
                    } catch( e ) {
                        console.log( e );
                    }
                });
                //maskFeatureCoordinates =  maskFeatureCoordinates[1];
                /*self.drawLayer.getSource().addFeature(
                    new ol.Feature({
                        geometry: new ol.geom.Polygon(maskFeatureCoordinates )
                    })
                );*/
                return;
            }
            try {
                turfPoligono = turf.polygon( maskFeatureCoordinates );
                //turfPoligono = turf.polygon(maskFeature.getGeometry().getCoordinates());
            } catch (e ) {
                self.alert( e )
                return;
            }
            self.aooLayer.getSource().un('change',self.aooLayerChange);
            //app.blockUI('Calculando AOO em ' + self.formatArea( maskFeature.getGeometry() ) ,function(){
            //    setTimeout(function() {
                    // iniciar o loop pelo bbox para encontrar todos os squares 2x2
                    while( maxLoops > 0 ) {
                        //console.log( ' ' + pontoC[1]+' > '+pontoA[1] + ': '+ (pontoC[1]>=pontoA[1]) )
                        if ( pontoC[1] - self.y2km > pontoA[1]) {
                            //console.log('BREAK');
                            break;
                        }
                        var xInicial = pontoA[0]
                        while ( maxLoops > 0 ) {
                            maxLoops--;
                            // verificar se o ponto intersecta o desenho do usuario
                            var square = self.findSquare( xInicial, pontoA[1],false); // retorna em EPSG-4326
                            // converter para epsg-3857
                            var coordsXY = ol.proj.transform([square.x,square.y], 'EPSG:4326', 'EPSG:3857');
                            // criar o square de 2km a partir do ponto 0
                            var p1 = coordsXY;
                            var p2 = [  p1[0]			, p1[1] + self.y2km ];
                            var p3 = [  p2[0]+self.x2km, p2[1] ];
                            var p4 = [  p1[0]+self.x2km, p1[1] ];
                            var newSquare = new ol.Feature({
                                geometry: new ol.geom.Polygon([[ p1, p2, p3, p4, p1 ]])
                            });
                            //var p = new ol.geom.Polygon([[ p1, p2, p3, p4, p1 ]] )
                            var turfQuadrado = turf.polygon( newSquare.getGeometry().getCoordinates() )
                            var intersect = turf.intersect(turfPoligono, turfQuadrado);
                            if ( intersect ) {
                                // verificar se o square já existe
                                var centerNewSquare = ol.extent.getCenter( newSquare.getGeometry().getExtent() );
                                if( self.aooLayer.getSource().getFeaturesAtCoordinate( centerNewSquare ) == 0 ) {
                                    self.createSquare(null, null, false, square);
                                    countSquares++;
                                }
                            }
                            if (pontoB[0] <= xInicial) {
                                break;
                            }
                            xInicial += self.x2km;
                        }
                        pontoA[1] -= self.y2km;
                        if( countSquares >= maxSquares ) {
                            maxLoops=0; // sair dos whiles
                            self.alert('Máximo de '+maxSquares+' áreas 2x2 processadas. Diminua a área de processamento.');
                            break;
                        }
                    }
              //      app.unblockUI();
                    self.aooLayer.getSource().on('change',self.aooLayerChange);
                    self.updateAooValue();
                    //console.log( (20000 - maxLoops) + ' loops' );
                    //console.log( countSquares+' adicionados' );
                //},2000);
            //});
        }
    }

    /**
     * Ler o polígono da EOO gravada no banco de dados
     */
    this.loadEoo = function( centerOnMap ) {
        this.cancelDrawInteraction();
        // evitar recursividade
        if( running ) {
            return;
        }
        if( self.sqFicha ) {
            var url = urlSalve + 'api/layer/eooFicha/' + self.sqFicha;
            if( self.eooFeature ) {
                self.eooLayer.getSource().removeFeature( self.eooFeature );
                self.eooFeature=null;
            }
            self.eooLayer.set('saved',false);

            // remover destaque dos poligons selecionados
            try {
                if (hydroshedLoaded.selectedFeatures.length > 0) {
                    hydroshedLoaded.selectedFeatures.map(function (feat) {
                        feat.setStyle(self.featureUnSelectedStyle);
                        feat.set('selected', false);
                    });
                    hydroshedLoaded.selectedFeatures = [];
                }
            } catch(e){}
            running=true;
            ajaxJson(url, {}, '', function (res) {
                running=false;
                var validFeatures = self.getGeojsonValidFeatures( res );
                if( validFeatures.length > 0 ) {
                    self.eooLayer.set('saved', true);
                    validFeatures.map(function (feature) {
                        // eoo poligono calculado pela camada hydrosheld
                        if( feature.getGeometry().getType().toLowerCase() =='multipolygon') {
                            var polygons = feature.getGeometry().getPolygons();
                            polygons.map( function( p ) {
                                var newFeature = new ol.Feature({
                                    geometry: p
                                });
                                newFeature.set('selected', true);
                                newFeature.set('edit', false);
                                if( !self.eooFeature || p.getArea() > self.eooFeature.getGeometry().getArea() ) {
                                    self.eooFeature=newFeature;
                                }
                                self.eooLayer.getSource().addFeature(newFeature);
                                hydroshedLoaded.selectedFeatures.push( newFeature );
                            });
                            // colocar o estilo nos poligonos utilizados para calcular a EOO
                            self.eooLayer.getSource().getFeatures().forEach( function(feat){
                               if( feat != self.eooFeature ){
                                   feat.setStyle( self.featureSelectedStyle );
                               }
                            });
                            self.eooFeature.setId( feature.get('id') );
                            self.eooFeature.set('id',feature.get('id') );
                            self.eooFeature.set('sqFicha',feature.get('sqFicha') );
                            self.eooFeature.set('name',feature.get('name') );
                        } else {
                            // eoo poligono unico calculado pelos pontos utilizados
                            self.eooFeature = feature;
                            feature.set('selected', true); // para ser salva no banco de dados
                            feature.set('edit', true);
                            hydroshedLoaded.selectedFeatures.push( feature );
                            self.eooLayer.getSource().addFeature(feature);
                        }

                        /*
                        self.eooLayer.getSource().addFeature(feature);
                        // pode haver vários poligons salvos na camada
                        //hydroshedLoaded.selectedFeatures.push( feature );
                        if ( ! self.eooFeature && feature.get('id') == 'calc-eoo' ) {
                            //self.eooLayer.getSource().addFeature(feature);
                            self.eooFeature = feature;
                            self.eooFeature.set('edit', true); // pode ser editada
                            self.eooFeature.set('selected', true); // para ser salva no banco de dados
                        }*/

                    });
                }
                if( self.eooFeature ) {
                    self.updateEooValue();
                    self.eooFeature.getGeometry().on('change', function (evt) {
                        self.updateEooValue();
                    });
                    if( centerOnMap ){
                        self.centerFeatureOnMap( self.eooFeature );
                    };
                }
                $("#map-draw-toolbar-" + self.el + "_eoo_btn_save").css('color','#fff');
            });
        }
        running=false;
    }

    /**
     * Método para gravar os poligonos utilizados para calcular o EOO ou somente o poligono da EOO quando
     * não for utilizado um shp
     * @param e
     */
    this.saveEoo = function(confirmed) {
        var self=this;
        // gerar o wkt de todos os squares no formato: MULTIPOLYGON()
        if( ! this.eooFeature ) {
            self.alert('Eoo não está calculado.')
            return;
        }
        if( ! confirmed ) {
            app.confirm('Confirma a gravação da EOO?', function () {
                self.saveEoo(true);
            });
            return;
        }
        self.cancelDrawInteraction();
        var $btnSaveEoo = $("#map-draw-toolbar-" + self.el + "_eoo_btn_save");
        $btnSaveEoo.css('color','#fff');

        /** /
        var wkt = new ol.format.WKT();
        var featureClone = this.eooFeature.clone();
        featureClone.getGeometry().transform('EPSG:3857', 'EPSG:4326');
        var featureWkt = wkt.writeFeature(featureClone);
        console.log( featureWkt );
        /**/

        /**/
        // gravar todos os poligonos que fazer parte do cálculo da EOO
        featureWkt = this.generateWKT(this.eooLayer, function(feat){
            return  feat.getStyle() == self.featureSelectedStyle || feat==self.eooFeature
        });
        /**/

        if( typeof( app ) != "undefined" ) {
            self.ajaxAction({action: 'saveEOO', sqFicha: this.sqFicha, wkt:featureWkt, vlEoo:this.vlEoo }, function (res) {
                if( res.status == 0 ) {
                    if( res.message ) {
                        self.eooLayer.set('saved',true);
                        var divId = 'map-draw-toolbar-' + self.el;
                        $("#"+divId + '_eoo_btn_save').css('color','white');
                        app.growl(res.message, 2, '', 'tc', '', 'success');
                    }
                }
            });
        }
        /*
        var wkt = this.generateWKT(this.eooLayer,true);
        if( typeof(app) != "undefined" ) {
            self.ajaxAction({action: 'saveEOO', sqFicha: this.sqFicha, wkt:wkt, vlEoo:this.vlEoo }, function (res) {
                if( res.status == 0 ) {
                    if( res.message ) {
                        var divId = 'map-draw-toolbar-' + self.el;
                        $("#"+divId + '_eoo_btn_save').css('color','white');
                        app.growl(res.message, 2, '', 'tc', '', 'success');
                    }
                }
            });
        }*/
    }

    /**
     * Excluir o poligono da EOO
     */
    this.deleteEoo = function( confirmed ) {
        // se a EOO ainda não tiver salve no banco de dados não precisa confirmar a exclusão
        confirmed = confirmed ? true : ! this.eooLayer.get('saved');
        if ( this.eooFeature ) {
            var divId = 'map-draw-toolbar-' + self.el;
            if (typeof (app) != "undefined" && self.sqFicha) {
                if ( ! confirmed ) {
                    app.confirm('Confirma a exclusão da EOO?', function () {
                        self.deleteEoo(true);
                    });
                } else {
                    self.ajaxAction({action: 'deleteEOO', sqFicha: self.sqFicha}, function () {
                        self.eooLayer.set('saved',false);
                        $("#" + divId + '_eoo_btn_save').css('color', 'white');
                        self.eooLayer.getSource().removeFeature(self.eooFeature);
                        self.eooFeature = null;
                        self.updateEooValue();
                        self.cancelDrawInteraction();
                        // remover destaque dos poligons selecionados
                        if ( hydroshedLoaded.selectedFeatures.length > 0) {
                            hydroshedLoaded.selectedFeatures.map(function (feat) {
                                feat.setStyle( self.featureUnSelectedStyle );
                                feat.set('selected',false);
                            });
                            hydroshedLoaded = [];
                        }
                    });
                }
            }
        }
    }

    /**
     * atualizar o valor da AOO em Km² exibida no mapa
     */
    this.updateEooValue = function( feature ){
        if( feature ) {
            this.eooFeature =  feature;
            this.eooFeature.set('selected',true); // para ser salva no banco de dados
        }
        var oldEoo = this.vlEoo;
        var $span = $('#map-draw-toolbar-'+self.el+'_eoo_vl_eoo');
        var $btnSaveEoo = $("#map-draw-toolbar-" + self.el + "_eoo_btn_save");
        var area='';
        if( this.eooFeature ) {
            area = self.formatArea(this.eooFeature.getGeometry());
            this.vlEoo = parseFloat(area.split(' ')[0].replace(/\./g,'').trim());
        } else {
            this.vlEoo=0;
            area ='0 km<sup>2</sup>';
        }
        if( oldEoo != this.vlEoo ) {
            $btnSaveEoo.css('color','#facece');
        }
        $span.html( area );
    }
    /**
     * atualizar o valor da AOO em Km² exibida no mapa
     */
    this.updateAooValue = function(){
        var $span = $("#map-draw-toolbar-" + self.el + "_aoo").find('span');
        var qtdSquare = 0;
        this.aooLayer.getSource().getFeatures().forEach(function(feature) {
            // se for o multipolygon, contar o itens
            var featureType = feature.getGeometry().getType().toLowerCase();
            if( featureType == 'multipolygon' ) {
                qtdSquare += feature.getGeometry().getCoordinates().length;
            } else if( featureType == 'polygon' ) {
                //if( feature.get('id') =='calc-aoo')
                qtdSquare ++;
            }
        })
        // cada square é 4m²
        this.vlAoo = ( qtdSquare * 4);
        var vlAooFormatado = String( this.vlAoo );
        if( typeof( this.vlAoo.formatMoney ) != 'undefined') {
            vlAooFormatado = this.vlAoo.formatMoney(0)
        }
        $span.html( vlAooFormatado + ' Km<sup>2</sup>');
    };

    /**
     * método para identificar e retornar o array com os pontos utilizados na avaliação
     * no formato [x,y] EPSG:3587
     * @param inBrasil - somente pontos dentro do brasil
     * @param anyPoint - pontos utilizados e não utilizados  na avaliação
     * @returns {[]}
     */
    this.getValidPoints = function( inBrasil, anyPoint ) {
        var points = [];
        if( typeof( inBrasil) == 'undefined' ) {
            inBrasil=false;
        }
        if( typeof( anyPoint ) == 'undefined' ) {
            anyPoint = false;
        }
        if( inBrasil ) {
            self.getTurfPoligonBrasil();
        }
        for( keyLayer in self.layers ) {
            var layer = self.layers[keyLayer]
            var options = layer.get('options');
            // ler somente os layers com filtro
            //if ( options && options.filter && layer.getVisible() && /^(salveUtilizadas|portalbioUtilizadas|salveConsulta)$/i.test( layer.get('id') ) ) {
            if ( options && options.filter && layer.getVisible() && /^(regUtilizados)$/i.test( layer.get('id') ) ) {
                // ler todos os pontos do layer
                if( ! options.layerId || options.layerId  != 'salveHistorico' ) {
                    layer.getSource().getFeatures().forEach(function (featureFound) {
                        // cluster
                        if( featureFound.get('features') ) {
                            featureFound.get('features').forEach( function( feat ){

                                var coords = feat.getGeometry().getCoordinates();
                                //var inUtilizadoAvaliacao = feat.get('in_utilizado_avaliacao') || feat.get('utilizadoAvaliacao') || '';
                                //if (anyPoint || !self.eooUsedPoints || inUtilizadoAvaliacao.toLowerCase() == 'sim' || inUtilizadoAvaliacao.toLowerCase() == 's') {
                                    if (inBrasil) {
                                        // converter coordenadas para EPSG: 4326 e verificar se está dentro do brasil
                                        var turfPoint = turf.point(ol.proj.transform([parseFloat(coords[0]), parseFloat(coords[1])], 'EPSG:3857', 'EPSG:4326'));
                                        if (turf.booleanPointInPolygon(turfPoint, turfPoligonoBrasil)) {
                                            points.push(coords);
                                        }
                                    } else {
                                        points.push(coords);
                                    }
                                //}
                            });
                        } else {
                            // não cluster
                            var coords = featureFound.getGeometry().getCoordinates();
                            //var inUtilizadoAvaliacao = featureFound.get('in_utilizado_avaliacao') || feat.get('utilizadoAvaliacao') || '';
                            //if (anyPoint || !self.eooUsedPoints || inUtilizadoAvaliacao.toLowerCase() == 'sim' || inUtilizadoAvaliacao.toLowerCase() == 's') {
                            if (anyPoint || !self.eooUsedPoints ) {
                                if (inBrasil) {
                                    // converter coordenadas para EPSG: 4326 e verificar se está dentro do brasil
                                    var turfPoint = turf.point(ol.proj.transform([parseFloat(coords[0]), parseFloat(coords[1])], 'EPSG:3857', 'EPSG:4326'));
                                    if (turf.booleanPointInPolygon(turfPoint, turfPoligonoBrasil)) {
                                        points.push(coords);
                                    }
                                } else {
                                    points.push(coords);
                                }
                            }
                        }
                    });
                }
            }
        }
        return points;
    };

    /**
     * cria um patter de preenchimento com linhas diagonais na cor especificada
     */
    this.makePattern = function(color) {
        var cnv = document.createElement('canvas');
        var ctx = cnv.getContext('2d');
        cnv.width = 6;
        cnv.height = 6;
        ctx.fillStyle = color || 'rgb(255, 0, 0)';
        for(var i = 0; i < 6; ++i) {
            ctx.fillRect(i, i, 1, 1);
        }
        return ctx.createPattern(cnv, 'repeat');
    };

    this.sanitizeHtml = function( html ) {
        return String(html).replace(/<\/?[^>]+(>|$)/g, "");
    };

    /**
     * mostrar um wkt no mapa utilizando pelo layer informado.
     * Se não for informado o layer, será utilizado o drawLayer
     */
    this.drawWKT = function( userPolygonWkt, layer ) {
        var feature=null;
        try {
            wkt = new ol.format.WKT();
            feature = wkt.readFeature(userPolygonWkt, {
                dataProjection: 'EPSG:3857',
                featureProjection: 'EPSG:3857'
            });

            if ( feature  ) {
                //feature.getGeometry().transform('EPSG:4326','EPSG:3857')
                if( layer ) {
                    layer.getSource().addFeature(feature);
                } else {
                    this.drawLayer.getSource().addFeature(feature)
                }
            }
        } catch( e ) {
            console.log('Erro metodo ol3Map.drawWkt()');
            console.error( e );
        }
        return feature;
    }

    /**
     * criar wkt dos poligonos desenhados em uma camada
     */
    this.generateWKT = function( layer, callbackFilter ) {
        var featureWkt, modifiedWkt;
        var unionFeatures = [];
        var wkt = new ol.format.WKT();
        var featuresFound=0;
        var isMultipolygon=false;
        //onlySelctedFeatures = typeof(onlySelctedFeatures) =='undefined' ? false : onlySelctedFeatures;
        if( layer.getSource().getFeatures().length == 0 ) {
            return ''
        }
        layer.getSource().forEachFeature(function (f) {
            // adicionar somentes as features selecionadas ou que não possuem a propriedade selected
            //if( !onlySelctedFeatures || f.get('selected') == true ) {
            if( typeof(callbackFilter) == 'undefined' || callbackFilter( f ) ) {
                var featureClone = f.clone();
                featureClone.getGeometry().transform('EPSG:3857', 'EPSG:4326');
                featureWkt = wkt.writeFeature(featureClone);
                /*
                if(featureWkt.match(/MULTILINESTRING/g)) {
                  modifiedWkt = (featureWkt.replace(/MULTILINESTRING/g, '')).slice(1, -1);
                } else {
                  modifiedWkt = (featureWkt.replace(/,/g, ', ')).replace(/LINESTRING/g, '');
                }
                unionFeatures.push(modifiedWkt);
              });
              drawLayer.getSource().getFeatures().length ? $('#wkt').text('MULTILINESTRING(' + unionFeatures + ')') : $('#wkt').text('');
              */
                if (featureWkt.match(/MULTIPOLYGON/g)) {
                    isMultipolygon=true;
                    modifiedWkt = (featureWkt.replace(/MULTIPOLYGON/g, '')).slice(1, -1);
                } else {
                    modifiedWkt = (featureWkt.replace(/,/g, ', ')).replace(/POLYGON/g, '');
                }
                unionFeatures.push(modifiedWkt);
                featuresFound++;
            }
        });
        if( featuresFound==1 && !isMultipolygon ) {
            return 'POLYGON' + unionFeatures[0]
        } else {
            return 'MULTIPOLYGON(' + unionFeatures.join(',') + ')'
        }

    }

    /**
     * recebe uma feature (polygon,line...) e retorna a feature (polygon)
     * do bbox desta feature
     */
    this.getFeatureBbox = function( feature ) {
        var bboxFeature=null;
        try {
            var bbox = feature.getGeometry().getExtent(); // ler a área do desenho especifico
            var start = [bbox[0], bbox[1]];
            var end = [bbox[2], bbox[3]];
            var geometry = new ol.geom.Polygon(null);
            geometry.setCoordinates([
                [start, [start[0], end[1]], end, [end[0], start[1]], start]
            ]);
            bboxFeature = new ol.Feature({geometry: geometry})
        } catch (e) {
            console.log('Erro ol3map.getFeatureBbox()');
            console.error(e);
        }
        return bboxFeature
    }

    /**
     * varrer os pontos validos das camadas Salve,portalbio e criar squares nos pontos
     * utilizados na avaliação
     * @param e
     */
    this.updatePointsAoo = function(e){
        var points = this.getValidPoints(true);
        this.aooLayer.getSource().un('change',self.aooLayerChange);
        points.forEach(function (p) {
            var feature = self.createSquare(p[0], p[1], false);
            if (feature) {
                // informar que neste square tem pelo menos um ponto para diferenciar dos criados manualmente ao clicar no mapa com a tecla ctrl
                feature.set('hasOccurrence', true);
            }
        });
        this.aooLayer.getSource().on('change',self.aooLayerChange);
        var center = ol.proj.transform(ol.extent.getCenter( this.aooLayer.getSource().getExtent()), 'EPSG:3857', 'EPSG:4326');
        var aooExtent = this.aooLayer.getSource().getExtent();
        if( aooExtent ) {
            this.map.getView().fit(this.aooLayer.getSource().getExtent(), this.map.getSize());
            this.flyTo(ol.proj.fromLonLat(center), function () {
            });
        }
        this.updateAooValue();
    }

    /**
     * varrer os pontos validos das camadas Salve,portalbio e criar squares nos pontos
     * utilizados na avaliação
     * @param e
     */
    this.updatePointsAooOld = function(e){
        var points = this.getValidPoints(true);
        this.aooLayer.getSource().un('change',this.aooLayerChange);
        this.aooLayer.getSource().clear();
        points.forEach(function (p) {
            var feature = self.createSquare(p[0], p[1], false);
            if( feature ) {
                // informar que neste square tem pelo menos um ponto para diferenciar dos criados manualmente ao clicar no mapa com a tecla ctrl
                feature.set('hasOccurrence', true);
            }
        });
        this.aooLayer.getSource().un('change',this.aooLayerChange());
        var center = ol.proj.transform(ol.extent.getCenter( this.aooLayer.getSource().getExtent()), 'EPSG:3857', 'EPSG:4326');
        var aooExtent = this.aooLayer.getSource().getExtent();
        if( aooExtent ) {
            this.map.getView().fit(this.aooLayer.getSource().getExtent(), this.map.getSize());
            this.flyTo(ol.proj.fromLonLat(center), function () {
            });
        }
        this.updateAooValue();
    }

    /**
     * remover áreas 2x2 adicionadas depois da última gravação
     * no banco de dados
     * @param e
     */
    this.undoPointsAoo = function(e){
        this.aooLayer.getSource().clear();
        this.updateAooValue();
    }
    /**
     * Método para calcular a área de ocupação das ocorrências (AOO)
     * utilizando os pontos utilizados na avaliação
     */
    /*this.calcularAoo = function(feature,evt) {

        // cancelar modo desenho
        self.cancelDrawInteraction();

        // teste - se for passada a feature, calcular AOO utilizando o poligono da feature
        if ( feature )  {
            console.log(feature)
            return;
        }

        var qtdSquare = this.aooLayer.getSource().getFeatures().length;
        //this.aooLayer.getSource().clear();
        if ( qtdSquare == 0) {
            var points = this.getValidPoints();
            points.forEach(function (p) {
                var feature = self.createSquare(p[0], p[1], false);
                if( feature ) {
                    // informar que neste square tem pelo menos um ponto para diferenciar dos criados manualmente ao clicar no mapa com a tecla ctrl
                    feature.set('hasOccurrence', true);
                }
            });
        }
        //this.aooLayer.setVisible(true);
    };*/

    this.saveAoo = function(e) {
        var self=this;
        // gerar o wkt de todos os squares no formato: MULTIPOLYGON()
        var wkt = this.generateWKT(this.aooLayer);
        if( typeof(app) != "undefined" ) {
            self.ajaxAction({action: 'saveAOO', sqFicha: this.sqFicha, wkt:wkt, vlAoo:this.vlAoo }, function (res) {
                if( res.status == 0 ) {
                    if( res.message ) {
                        var divId = 'map-draw-toolbar-' + self.el;
                        $("#"+divId + '_aoo_btn_save').css('color','white');
                        app.growl(res.message, 2, '', 'tc', '', 'success');
                    }
                }
            });
        }
    }

    this.deleteAoo = function(e) {
        var self=this;
        var divId = 'map-draw-toolbar-' + self.el;
        if( typeof(app) != "undefined" ) {
            app.confirm('Confirma a exclusão de toda a AOO?<br><br><small>Excluir TODA a AOO da ficha.</small>', function () {
                self.ajaxAction({action: 'deleteAOO', sqFicha: self.sqFicha}, function () {
                    $("#"+divId + '_aoo_btn_save').css('color','white');
                    window.setTimeout(function(){
                        self.aooLayer.getSource().clear();
                    },1000);
                });
            });
        } else {
            if( confirm('Confirma a exclusão de toda a AOO da ficha?')) {
                $("#"+divId + '_aoo_btn_save').css('color','white');
                window.setTimeout(function(){
                    self.aooLayer.getSource().clear();
                },1000);
            }
        }
    }

    // fazer uma animação
    this.flyTo = function( location, done ) {
        var duration = 1000;
        var view = this.map.getView();
        var zoom = view.getZoom();
        var parts = 2;
        var called = false;
        function callback(complete) {
            --parts;
            if (called) {
                return;
            }
            if (parts === 0 || !complete) {
                called = true;
                done( complete );
            }
        }

        view.animate({
            center: location,
            duration: duration
        }, callback);
        view.animate({
            zoom: zoom - 1,
            duration: duration / 2
        }, {
            zoom: zoom,
            duration: duration / 2
        }, callback);
    }

    /**
     * converter cores em hexadecimal to RGBA
     * @param x
     * @param a
     * @returns {string}
     */
    this.hex2rgba = function hex2rgba(x, a) {
        a = (a == undefined) ? 1 : a;
        var r = x.replace('#', '').match(/../g),
            g = [],
            i;
        for (i in r) {
            g.push(parseInt(r[i], 16));
        }
        g.push(a);
        return 'rgba(' + g.join() + ')';
    }

    /**
     * formatar números
     * @param digits
     * @param decimalSeparator
     * @param thousandsSeparator
     * @returns {string}
     */
    this.formatDecimal = function(value, digits, decimalSeparator, thousandsSeparator) {
            var c = isNaN(digits = Math.abs(digits)) ? 2 : digits,
            d = decimalSeparator == undefined ? "," : decimalSeparator,
            t = thousandsSeparator == undefined ? "." : thousandsSeparator, s = value < 0 ? "-" : "",
            i = parseInt(value = Math.abs(+value || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t)
            + (c ? d + Math.abs(value - i).toFixed(c).slice(2) : "");
    }

    this.setTextFromProperties = function( feature ){
        try {
            var labels = [];
            for ( key in feature.getProperties()) {
                if (/string|number/.test(typeof (feature.get(key)))) {
                    labels.push(key + ': ' + feature.get(key));
                }
            }
            feature.set('text', labels.join('<br>'));
        } catch( e ) {}
    }

    /**
     * retornar o nivel de zoom atual do mapa
     * @returns {*}
     */
    this.getZoom = function() {
        return this.map.getView().getZoom()
    }

    this.centerFeatureOnMap = function( feature ) {
        if( feature ) {
            var extent = feature.getGeometry().getExtent();
            var center = ol.extent.getCenter( extent );
            if ( extent && center ) {
                this.map.getView().fit(extent, this.map.getSize());
                //this.flyTo(ol.proj.fromLonLat(center), function () {});
            }
        }
    }

    /**
     * executar ação do menu de contexto ao clicar no mapa com o botão direito do mouse ou no campo select do overlay popup
     * @param data
     */
    this.contextMenuAction = function( data ){
        var ajaxData = { action:data.action,
            sn              : data.sn||'',
            ids             : data.ids,
            txJustificativa : data.txJustificativa || '',
            sqMotivo        : data.sqMotivo || ''
        };
        switch ( data.action ) {
            case "UTILIZAR_AVALIACAO":
                /*
                // Permitir executar a ação mesmo que o registro já esteja utilizado para forçar o recalculo do Estabio, bioma, bacia etc
                if( data.inUtilizadoAvaliacao && data.inUtilizadoAvaliacao.toLowerCase() == 's' ) {
                    return;
                }*/
                ajaxData.action = 'utilizarNaAvaliacao';
                ajaxData.sn     = 'S';
                break;
            //------------------------------------------------
            case "NAO_UTILIZAR_AVALIACAO":
                /*if( data.inUtilizadoAvaliacao && data.inUtilizadoAvaliacao.toLowerCase() == 'n' ) {
                    return;
                }*/
                var textoNaoUtilizado    = data.textoNaoUtilizado || '';
                var sqMotivoNaoUtilizado = data.sqMotivoNaoUtilizado || ''
                textoNaoUtilizado = textoNaoUtilizado.replace('Registro utilizado', 'Registro não utilizado');
                getJustificativa({txJustificativa: textoNaoUtilizado, sqMotivoNaoUtilizado: sqMotivoNaoUtilizado}, function (params) {
                    dlgJustificativa.close();
                    ultimaJustificativa = params.txJustificativa; // variavel glogal declarada no arquivo fichaCompleta.js
                    ajaxData.action = 'naoUtilizarNaAvaliacao';
                    ajaxData.sn = 'N';
                    ajaxData.txJustificativa = params.txJustificativa;
                    ajaxData.sqMotivo = params.sqMotivoNaoUtilizado;
                    self.contextMenuAction( ajaxData );
                });
                return;
                break;
            //------------------------------------------------
            case "MARCAR_DESMARCAR_REGISTRO_HISTORICO":
                ajaxData.action = 'marcarDesmarcarRegistroHistorico';
                /*
                if( ajaxData.ids && ajaxData.ids[0].bd!='salve'){
                    app.alert('Esta ação só pode ser executada nos registros do SALVE.');
                    return;
                }*/
                break;
            //------------------------------------------------
            case "MARCAR_DESMARCAR_REGISTRO_SENSIVEL":
                ajaxData.action = 'marcarDesmarcarRegistroSensivel';
                if( ajaxData.ids && ajaxData.ids[0].bd!='salve'){
                    app.alert('Esta ação só pode ser executada nos registros do SALVE.');
                    return;
                }
                break;
           //------------------------------------------------
            case "EXCLUIR":
                app.confirm('Confirma a exclusão do(s) registro(s)?<br><br><small>obs:registros do SALVE serão excluídos da base de dados e registros do PortalBio serão apenas marcados como excluídos.</small>', function(){
                    ajaxData.action = 'excluir';
                    ajaxData.sn = 'E'
                    self.contextMenuAction( ajaxData );

                });
                return;
                break;

            //------------------------------------------------
            case "RESTAURAR":
                app.confirm('Confirma a recuperação do(s) registro(s)?.</small><br><small>Obs:válido apenas para os registros do portalbio que foram excluídos.</small>', function(){
                    ajaxData.action = 'restaurar';
                    ajaxData.sn = 'R'
                    self.contextMenuAction( ajaxData );
                });
                return;
                break;

            //-------------------------------------------------
            case "TRANSFERIR_REGISTRO":
                if( data.inUtilizadoAvaliacao && data.inUtilizadoAvaliacao.toLowerCase() == 's' ) {
                    app.alert('Registro utilizado na avaliação não pode ser transferido');
                    return;
                }
                self.callbackActions( {ids:ajaxData.ids} , { sqFicha:self.sqFicha, action: 'transferir_registro'} );
                self.animatedPoint=null;
                return;
                break;

            //-------------------------------------------------
            case "LOCALIZAR_GRIDE":
                self.findGridRow(ajaxData.ids[0],true);
                return;
                break;
        }

        if( ! ajaxData.ids ) {
            self.alert('Nenhum ponto selecionado.');
            return;
        }
        self.ajaxAction( ajaxData, function() {
            //self.refresh(); // atualizar mapa
            self.reloadLayers(); // recarregar os pontos
        });
    }

    /**
     * metodo para ler as informações do ponto clicado remotamente via ajax
     */

    this.loadPopupInfo = function(itemIndex){
        function callAjax( item, divContent ) {
            var idRegistro = item.id;
            var noBaseDados = item.bd;
            if( divContent.height() > 0 ) {
                var currentHeight =divContent.height();
                $("#popup-mapaDistribuicaoMain-text").css('min-height', currentHeight + 'px');
                divContent.html('<b>Lendo informações do registro...</b>');
            }
            $.ajax( {
                url : urlSalve+'api/getInfoOcorrencia',
                dataType:'json',
                method:'POST',
                timeout: 5000,  // sets timeout to 5 seconds
                data: {
                    idRegistro:idRegistro,
                    noBaseDados:noBaseDados,
                    contexto: self.contexto
                },
                success:function( res ){
                    if( res.msg ){
                        self.alert( res.msg );
                    }
                    if( res.text ) {
                        divContent.html(res.text);
                        self.overlayPopup.itens[itemIndex].text = res.text;
                        divContent.css('min-height', divContent.height() + 'px');
                        $( self.overlayPopup.getElement() ).show();
                        // position the popup (using the coordinate in the map's projection)
                        self.overlayPopup.setPosition( self.overlayPopup.coord );
                    } else {
                        divContent.html('Nenhuma informação encontrada.');
                    }
                },
                error:function( jqXHR, textStatus, errorThrown){
                    app.alertError('Não foi possível recuperar as informações do ponto<br>Verifique sua conexão com a internet ou se sua sessão não está expirada.')
                }
            }).always = function(){
                app.unblockUI();
            };
        }

        if( itemIndex < 0 ) {
            return;
        }
        var divContent = $("div#popup-" + self.el + '-text');
        if( !divContent.length ){
            return;
        }
        var item = this.overlayPopup.itens[itemIndex]

        if( item.text ) {
            divContent.html(item.text);
            $( self.overlayPopup.getElement() ).show();
            // position the popup (using the coordinate in the map's projection)
            self.overlayPopup.setPosition( self.overlayPopup.coord );
            return;
        }
        // quando o overlay já estiver visível não precisa bloquear a tela
        if( $(self.overlayPopup.getElement()).is(':visible') ) {
            callAjax(item,divContent);
        }  else {
            app.blockUI('Lendo informações do registro. Aguarde...', function () {
                callAjax(item, divContent);
            })
        }
    }



    // ALTERAÇÕES PARA CRIAÇÃO DOS PONTOS COM A NOVA COLORACAO: VERDE AMARELA E VERMELHA
    // zzz2
    this.createLayersOccurrences = function( params ) {
        var data = app.params2data(params, {});
        if( loadingOccurrences || !this.sqFicha ){
            return;
        }
        loadingOccurrences = true;
        occurrenceLayers.map( function( option , index ){
            var layerIndexName = option.id+'Layer';
            var createLayer = true
            if( self.contexto == 'validacao'){
                // layers que não devem aparecer no mapa do módulo validação
                createLayer = ! ( /regUtilizados|regUtilizadoHistorico/i.test( option.id ) );
            }

            if( createLayer ) {
                self[layerIndexName] = new ol.layer.Vector({
                    source: new ol.source.Cluster({
                        distance: parseInt(clusterDistance, 10),
                        source: new ol.source.Vector(),
                    }),
                    type: 'vector',
                    edit: false,
                    visible: true,
                    filter: true,
                    style: self.occurrenceStyle,
                    id: option.id,
                    name: option.name,
                    color: option.color,
                    options: option.options,
                    zIndex: option.zIndex,
                    hint: option.hint,
                });
                self[layerIndexName].set('options', option);
                self.addLayer(self[layerIndexName]);
            }
        })

        loadingOccurrences = false;
        this.refreshLayerOccurrences(data);
    }

    // atualizar as camadas de registros de ocorrências
    this.refreshLayerOccurrences = function( params ){
        var dataParams = app.params2data(params,{} );
        dataParams.msg = dataParams.msg || '';
        if( loadingOccurrences ){
            return;
        }
        // limpar os layer
        occurrenceLayers.map(function( layerData){
            var layer = self.getLayerById(layerData.id)
            if( layer ){
                layer.getSource().getSource().clear();
            }
        })
        var data = {sqFicha:this.sqFicha, contexto:self.contexto};
        loadingOccurrences = true;
        ajaxPost( urlSalve+'api/camadasOcorrencias/', data ,dataParams.msg,function( res ) {
            try{app.unblockUI()} catch(e){};
            if( res.errors.length > 0 ){
                self.alert( res.errors.join('<br>'),20,'error');
                return;
            }
            var validFeatures = self.getGeojsonValidFeatures( res );
            validFeatures.map(function( feature ) {
                var noLayer = feature.get('noLayer');
                if( noLayer && self[noLayer+'Layer']){
                    var layer = self[noLayer+'Layer']
                    var color = layer.get('options').color;
                    var icon = layer.get('options').options.legend.image
                    if( color ) {
                        if( !feature.get('color') ) {
                            feature.set('color', color)
                        }
                    } else if( icon ) {
                        if( !feature.get('icon') ) {
                            feature.set('icon', icon)
                        }
                    }
                    layer.getSource().getSource().addFeature( feature )
                }
            });
            // se ainda não tiver lido os Estados rechurados com ponto no centroide, criar aqui.

            // criar/atualizar a camada dos Estados rachurados ocm o centroide
            self.rachurarEstados()

        });
        // fim carregamento das camadas de ocorrencias
        loadingOccurrences = false;
    }

    this.aplicarFiltrosDialogFiltersMap = function( e ){
        var $divDlg = $('#dlg-filters-map-' + this.el );
        var $form = $divDlg.find('form')
        /**
         * sistemas: salve,salve-consulta, portalbio, sisbio
         */

        this.filtersMetadata = []
        $form.find('input:checkbox:checked').map( function( i, check ){
            var fieldName = $(check).data('field')
            if( fieldName ) {
                var data ={}
                data[fieldName]=check.value;
                self.filtersMetadata.push(data)
                //self.filtersMetadata.push({'sistema': check.value})
                //self.filtersMetadata.push({'origem': check.value})

            }
        });

        self.updateFilter();

        // alterar a cor da imagem do botão Filtro
        var $div=$("#map-draw-toolbar-"+self.el);
        var $icon = $div.find('i[data-type=dialogMapFilters]');
        if( self.filtersMetadata.length == 0 ) {
            self.refresh()
            $icon.removeClass('changed');
            //$icon.css('color','#fec4c4');
        } else {
            $icon.addClass('changed');
        }

            /*
            if( filtroBaseDados.length > 0 ){
                occurrenceLayers.map(function(layer){
                    try {
                        self.getLayerById(layer.id).getSource().getFeatures().each( function (feature, i) {
                            console.log(feature);
                        })
                    } catch( e ) {
                        console.log( e.message )
                    }
                })
            }*/
    }
    /**
     * método para localizar os Estados que possuirem unico ponto como centroide
     * e fazer um rachurado sobre o Estado
     */
    this.rachurarEstados = function(){
        // percorer as camadas dos registros e localizar os estados que possuem 1 ponto como centroide
       var estados = {}
        occurrenceLayers.map(function(item,i){
            var layerId = item.id;
            var layer = self.getLayerById( layerId );
            var features = layer.getSource().getSource().getFeatures();
            var ufData = {}
            features.forEach(function(f,i){
                if( f.get('isCentroide') ) {
                    if( typeof( estados[ f.get('sgEstado')]) == 'undefined' ) {
                       estados[f.get('sgEstado')] = {
                             show        : true
                           , sqFicha     : self.sqFicha
                           , sigla       : f.get('sgEstado')
                           , sistema     : f.get('sistema')
                           , origem      : f.get('origem')
                           , sqOcorrencia: f.get('sqOcorrencia') }
                    }
                } else {
                    estados[f.get('sgEstado')] = {show:false}
                }
            })
        });

        // criar a lista com os Estados válidos
        var estadosValidos = []
        for( key in estados ){
            if( estados[key].show ){
                estadosValidos.push( key )
            }
        }
        // dados para fazer o rquest e buscar os poligons
        //var siglasEstados = estadosValidos.join(',')


        if( !this.estadosCentroideLayer ) {
            this.estadosCentroideLayer = self.createLayer({
                type: 'geojson'
                , id:'estadosCentroide'
                , name: 'Estados com centroide'
                , visible: true
                , style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: self.makePattern('rgba(85,85,89,0.5)')
                    }),
                    stroke: new ol.style.Stroke({
                        color: '#4f4f4f',
                        width: 1
                    })
                })
                , legendHtml : '<div class="map-legenda-estados-centroide"></div>'
                , filter: false
                , zIndex: 6
                , hint: 'Estado com registro no centroide'
            });
        }

        // esconder switcher dos mapas rachurados
        $("input[id$="+self.el+"][value=estadosCentroide]").parent().hide();

        // limpar o layer
        self.estadosCentroideLayer.getSource().clear()

        // teste com todos os estados
        //estadosValidos = ['AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PR','PB','PA','PE','PI','RJ','RN','RS','RO','RR','SC','SE','SP','TO','AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PR','PB','PA','PE','PI','RJ','RN','RS','RO','RR','SC','SE','SP','TO']

        // recuperar os polígonos de cada Estado
        estadosValidos.forEach( function( sigla ) {
            var pars = estados[sigla]
            $.ajax( {
                url :   urlSalve + 'api/estadosCentroide',
                dataType:'json',
                method:'POST',
                cache :true,
                data: pars,
                success:function( res ){
                    if( res.msg ){
                        self.alert( res.msg );
                    }
                    var features = self.getGeojsonValidFeatures( res );
                    if( features.length > 0 ) {
                        self.estadosCentroideLayer.getSource().addFeature( features[0] )
                        // mostrar o switcher dos mapas rachurados
                        $("input[id$="+self.el+"][value=estadosCentroide]").parent().show();
                    }
                },
                always:function(){

                },
                error:function(){
                    self.alert( 'Erro ao recuperar o poligono do estado ' + sigla)
                }
            });
         });

        // criar o layer se não existir
        /*
        if( ! this.estadosCentroideLayer ){
            var url = urlSalve + 'api/estadosCentroide?siglas='+siglasEstados
            this.estadosCentroideLayer = self.createLayer({
                  type: 'geojson'
                , name: 'Estados com centroide'
                , visible: true
                , url: url
                , style: new ol.style.Style({
                        fill: new ol.style.Fill({
                            color: self.makePattern('rgba(85,85,89,0.5)')
                        }),
                        stroke: new ol.style.Stroke({
                            color: '#4f4f4f',
                            width: 1
                        })
                    })
                , filter: false
                , zIndex: 6
                , hint: 'Estado com registro no centroide'
            });
        } else {
            // apenas fazerum refresh
            this.estadosCentroideLayer.getSource().clear()
        }*/
    }


} // fim ol3Map
/**
 * função genérica para copiar um texto para a área de transferência
 * @param e
 * @param texto
 */
var olMapCopy2Clip = function(texto ){
    //var input = $(e).next('input');
    //console.log( input );
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(texto).select();
    document.execCommand("copy");
    $temp.remove();
    var msg = 'Copiado para área de transferência.';
    if( typeof app != 'undefined' && typeof app.growl== 'function') {
        app.growl(msg, 1, '', 'tc','','success');
    }
    else
    {
        if( typeof notify == 'function')
        {
            notify( msg,null,'success',1000);
        }
        else
        {
            alert( msg );
        }
    }
};
var zeePoligono =""
/*
function getTile(tile, url){
    var xhr = new XMLHttpRequest();
    xhr.responseType = "arraybuffer";
    xhr.open('GET', url,true);
    //xhr.setRequestHeader("chave", 'valor');
    xhr.onreadystatechange = function () {
        if ( xhr.readyState === 4 && xhr.status === 0){
                tile.getImage().src = "";
        }
    };
    xhr.onload = function () {
        var arrayBufferView = new Uint8Array(this.response);
        var blob = new Blob([arrayBufferView]);
        var urlCreator = window.URL || window.webkitURL;
        var imageUrl = urlCreator.createObjectURL(blob);
        tile.getImage().src = imageUrl;
    }
    xhr.send();
}
*/
/*
function getTile(tile, src) {
    debugger;
    var client = new XMLHttpRequest();
    client.open('GET', src);
    //client.setRequestHeader('Strict-Transport-Security', 'max-age=0');
    //client.setRequestHeader('Access-Control-Allow-Origin', '*');
    //client.setRequestHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
    //client.setRequestHeader('Access-Control-Allow-Headers', '*');
    //client.setRequestHeader('Referer', 'http://mapas.icmbio.gov.br');
    client.onload = function(evevt ){
         var data = 'data:image/png;base64,' + btoa( unescape( encodeURIComponent( this.responseText ) ) );
         tile.getImage().src = data;
        //var arrayBufferView = new Uint8Array(this.response);
        //var blob = new Blob([arrayBufferView]);
        //var urlCreator = window.URL || window.webkitURL;
        //var imageUrl = urlCreator.createObjectURL(blob);
        //tile.getImage().src = imageUrl;


    };
    client.send();
}
*/
